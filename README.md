# Scepter

OSSO Command and Control API

Contains:

- `scepter/`: Root directory of scepter
- `scepter/api`: A Flask application containing the scepter services
- `scepter/migrations`: Alembic Migrations for Data Models
- `scepter/models`: Model Definitions
- `scepter/migrator`: Python application for running alembic migrations
- `scepter/plugins`: Scepter plugins

Requires:

- Python 3.9+
- PostgeSQL 11+

Optional Requirement:
- Docker

## Development

### Initial config

Install pre-commit to add automated linting via `flake8` and `black`:

```bash
pre-commit install
```

Setup & configure python:

```bash
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements-dev.txt
```

Clone `example.env` to `.env` file in the /scepter dir and update as required incl. DB connection params.

### Running locally

To run the server, from the `scepter` dir:

```bash
python -m gunicorn api:app -c api/gunicorn.conf.py 
```

To test the API and ensure it is set up correctly, use a test HTTP command such as (Using httpie):

```bash
http get http://localhost:5000/satellite x-osso-user:"00000000-0000-0000-0000-000000000000" x-osso-token:"dev"
```

To run tests, from the root dir:

```bash
pytest scepter
```

#### Database migration

To run updates to your local database run:

```bash
cd scepter
python -m migrator.main
```

### Running via Docker

Follow the docker install instructions on the [docker website](https://docs.docker.com/engine/install/ubuntu/). These instructuions assume a 64-bit Ubuntu OS for versions 18.04 - 21.10 however docker also provides support for Windows and MacOS.

If you don't have Postgres locally, you may wish to run it via Docker:

```bash
docker pull postgres:alpine
docker run --name scepter_db -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 postgres:alpine
```

**Note**: Data will be lost when this container is stopped. To avoid, see [volume](https://www.digitalocean.com/community/questions/how-to-create-a-persistent-data-volume-for-postgres-database-container-within-a-docker-project)

To run the server, from the root dir:

```bash
bash scripts/local_docker.sh
```

Pass the `-t` flag to execute tests:

```bash
bash scripts/local_docker.sh -t
```

NOTE: For Mac users, Airplay sharing listens on port 5000. Turn off Airplay receiving under "Sharing" in system
preferences.
