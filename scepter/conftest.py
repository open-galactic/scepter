import os
from pathlib import Path
from uuid import UUID

import pytest
from alembic import command
from alembic.config import Config
from pydantic import BaseSettings
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from api.dependencies import auth_handler

scepter_dir = Path(__file__).parent

from api.config import db_settings  # noqa

migrations_dir = scepter_dir / "migrations"

# NOTE: TestDatabaseSettings MUST match the apiuser settings in scepter.config
# to align with the applications connection string.
# Once tests inject a fixture with a session, these can be updated


class TestDatabaseSettings(BaseSettings):
    host = os.getenv("DB_HOST") or "localhost"
    superuser = os.getenv("DB_SUPERUSER") or "scepter_superuser"
    superuser_password = os.getenv("DB_SUPERUSER_PASSWORD") or "password"
    name = os.getenv("DB_NAME") or "scepter_db"
    apiuser = os.getenv("DB_APIUSER") or "scepter_apiuser"
    apiuser_password = os.getenv("DB_APIUSER_PASSWORD") or "password"
    port = os.getenv("DB_PORT") or 5432

    class Config:
        env_file = scepter_dir / ".env"
        env_prefix = "db_"


db = TestDatabaseSettings()

pytest_plugins = ["api.tests.fixtures.c2"]


def pytest_sessionstart(session):

    if getattr(session.config, "workerinput", None) is not None:
        # Running in spawned process under pytest-xdist, yield common setup to the master process.
        # See https://github.com/pytest-dev/pytest-xdist/issues/271#issuecomment-826396320
        return

    # Connect to pg as the superuser to maintenance, clean up and create the test db
    pg_url = f"postgresql://{db.superuser}:{db.superuser_password}@{db.host}:{str(db.port)}"
    superuser_url = pg_url + f"/{db.superuser}"
    autocommit_session = sessionmaker(create_engine(superuser_url, future=True, isolation_level="AUTOCOMMIT"))
    with autocommit_session() as session:

        session.execute(f"drop database if exists {db.name};")
        session.execute(f"drop user if exists {db.apiuser};")
        session.execute(f"create database {db.name};")
        session.execute(f"create user {db.apiuser} with password '{db.apiuser_password}';")
        session.execute("CREATE SCHEMA IF NOT EXISTS ops")

        session.execute(f"grant all privileges on all sequences in schema ops to {db.apiuser};")

        session.execute(f"GRANT CREATE ON DATABASE {db.name} TO {db.apiuser};")

    # Connect to the test db as the superuser and run migrations
    db_url = pg_url + f"/{db.name}"
    with create_engine(db_url).connect() as db_conn:
        alembic_cfg = Config((migrations_dir / "alembic.ini").resolve())
        alembic_cfg.attributes["connection"] = db_conn
        alembic_cfg.attributes["db_user"] = db.apiuser
        alembic_cfg.attributes["db_user_password"] = db.apiuser_password
        alembic_cfg.attributes["db_name"] = db.name
        alembic_cfg.attributes["db_apiuser"] = db.apiuser
        alembic_cfg.attributes["db_apiuser_password"] = db.apiuser_password
        command.upgrade(alembic_cfg, "head")


engine = create_engine(
    f"postgresql://{db.apiuser}:{db.apiuser_password}@{db.host}:{str(db.port)}/{db.name}",
    future=True,
)

admin_user_id = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")


# allow auth access with admin_user_id
def allow_access(*args, **kwargs):
    return admin_user_id, True, None, None


@pytest.fixture()
def patch_auth(monkeypatch):
    """overwrite auth plugin for test"""
    monkeypatch.setattr(auth_handler, "allow_access", allow_access)


@pytest.fixture()
def session():
    session = Session(engine)
    yield session
