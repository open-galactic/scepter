import logging
import os

logger = logging.getLogger(__name__)


class ScepterSettings:
    @property
    def id_hash_pepper(self) -> str:
        return os.getenv("ID_HASH_PEPPER") or "dev"

    @property
    def secret_key(self) -> str:
        return os.getenv("SECRET_KEY") or "dev"

    @property
    def payload_token_secret_key(self) -> str:
        return os.getenv("PAYLOAD_TOKEN_SECRET_KEY") or "dev"

    @property
    def secret_token(self) -> str:
        return os.getenv("SECRET_TOKEN") or "dev"


config = ScepterSettings()


class DatabaseSettings:
    @property
    def host(self) -> str:
        return os.getenv("DB_HOST") or "localhost"

    @property
    def reader_host(self) -> str:
        return os.getenv("DB_READER_HOST") or "localhost"

    @property
    def port(self):
        return os.getenv("DB_PORT") or 5432

    @property
    def name(self) -> str:
        return os.getenv("DB_NAME") or "scepter_db"

    @property
    def apiuser(self) -> str:
        return os.getenv("DB_APIUSER") or "scepter_apiuser"

    @property
    def apiuser_password(self) -> str:
        return os.getenv("PGPASSWORD") or os.getenv("DB_APIUSER_PASSWORD") or "password"

    @property
    def sqlalchemy_url(self) -> str:
        return f"postgresql+psycopg2://{self.apiuser}:{self.apiuser_password}@{self.host}:{str(self.port)}/{self.name}"

    @property
    def sqlalchemy_reader_url(self) -> str:
        return f"postgresql+psycopg2://{self.apiuser}:{self.apiuser_password}@{self.reader_host}:{str(self.port)}/{self.name}"


db_settings = DatabaseSettings()


class LogSettings:
    level: str = os.getenv("LOG_LEVEL") or "INFO"


log_settings = LogSettings()
