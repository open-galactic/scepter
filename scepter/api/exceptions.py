from enum import Enum
from typing import Literal, Optional, Union


class ReasonCode(Enum):
    SERVER_ERROR = "Internal server error"
    INVALID_CURSOR = "Cursor is invalid or unprocessable"

    ASSET_NOT_FOUND = "Asset does not exist"
    END_TIME_BEFORE_START_TIME = "End time must occur before start time"

    SATELLITE_NOT_FOUND = "Satellite not found"
    SATELLITE_NORAD_ID_EXISTS = "Satellite with that norad id exists"
    SATELLITE_NSSDC_ID_EXISTS = "Satellite with that nssdc id exists"
    SATELLITE_ENCODER_NAME_TOO_LONG = (
        "Invalid encoder: must be less than 64 characters long"
    )
    SATELLITE_DECODER_NAME_TOO_LONG = (
        "Invalid decoder: must be less than 64 characters long"
    )
    SATELLITE_DESCRIPTION_TOO_LONG = (
        "Invalid description: must be less than 1024 characters long"
    )
    SATELLITE_ID_TOO_LONG = "sat_id too long, limit to 24chars"
    SATELLITE_ID_PREFIX_INCORRECT = "sat_id must begin with 's'"
    SATELLITE_ID_ENCODING_INVALID = "sat_id must use only the ascii character set"
    SATELLITE_NAME_TOO_LONG = "sat_name length is limited to 256 characters"
    SATELLITE_TLE_MISSING_FIELD = "tle missing fields required: [norad_cat_id, tle_line_0, tle_line_1, tle_line_2, last_updated, object_type]"
    SATELLITE_TLE_FIELD_TOO_LONG = "tle fields limit to 120 chars"
    SATELLITE_TOO_MANY_IDS = "Too many satellite IDs. Max is 20"
    SYSTEM_NOT_FOUND = "System does not exist for satellite"
    COMPONENT_NOT_FOUND = "Component does not exist in system for satellite"
    COMPONENT_ALREADY_EXISTS = "A component for that system already uses that name"
    SYSTEM_ALREADY_EXISTS = "A system for that satellite already uses that name"
    SATELLITE_TELECOM_NOT_FOUND = "Telecom name not found for satellite."
    SATELLITE_TELECOM_ALREADY_EXISTS = "Telecom name already exists for satellite"

    GROUND_STATION_NOT_FOUND = "Ground Station not found"
    GROUND_STATION_TELECOM_EXISTS = "Telecom name for groundstation already exists"
    GROUND_STATION_TELECOM_NOT_FOUND = "Telecom name not found for groundstation."

    OVERPASS_TOO_MANY_COMPARISONS = "Overpass comparisons exceeded limit"
    OVERPASS_NOT_FOUND = "Overpass not found"
    OVERPASS_WINDOW_EXCEEDS_A_FORTNIGHT = "Window for lookup may not exceed 14 days"
    OVERPASS_WINDOW_TOO_FAR_AHEAD = "Unable to request more than 4 weeks in advance"
    OVERPASS_MISSING_GS_OR_OVERPASS_ID = "Missing gs_id or overpass_id"
    OVERPASS_CANNOT_CREATE_AND_REFERENCE = (
        "Cannot specify an overpass_id with aos, los or gs_id"
    )
    OVERPASS_LOS_BEFORE_AOS = "Overpass LOS occurs before the AOS"
    OVERPASS_AOS_IN_PAST = "Overpass AOS must occur in the future"
    OVERPASS_LOS_WITHOUT_AOS = "Overpass LOS cannot be set without an AOS"
    OVERPASS_LOS_AFTER_AOS = "Overpass LOS cannot occur before the AOS"

    CONTACT_NOT_FOUND = "Contact not found"
    CONTACT_INSTRUCTION_NOT_FOUND = "Instruction not found"
    CONTACT_ALREADY_CANCELLED = "Contact has already been cancelled"
    CONTACT_CANCELLED_NOT_ACTIVE = "Contact has already been cancelled"
    CONTACT_SUPERSEDED_NOT_FOUND = "Could not find contact specified by supersedes"
    CONTACT_SUPERSEDED_NOT_ACTIVE = "Contact has already been superseded"
    CONTACT_SUPERSEDED_WAS_CANCELLED = "Contact to supersede has been cancelled"
    CONTACT_DISPATCH_CANCELLED = "Cannot dispatch cancelled command"
    CONTACT_START_CANCELLED = "Cannot start cancelled command"
    CONTACT_END_CANCELLED = "Cannot end cancelled command"
    CONTACT_INVALID_ARGS = "Invalid args for instruction"

    INSTRUCTION_NOT_FOUND = "Instruction not found"
    SCRIPT_NOT_FOUND = "Script not found"
    COMMAND_NOT_FOUND = "Command not found"
    INVALID_ARG_JSCHEMA = "JSONSchema submitted is invalid under Draft 2019-09"
    PRESET_NOT_FOUND = "Preset not found"
    PRESET_INVALID_ARGS = "Invalid arguments for command"

    UPLINK_REQUEST_NOT_FOUND = "Uplink Request not found"
    UPLINK_REQUEST_UNAUTHORIZED_CREATE = "Unauthorized to create uplink request"
    UPLINK_REQUEST_UNAUTHORIZED_APPROVE = "Unauthorized to approve uplink request"
    UPLINK_REQUEST_UNAUTHORIZED_DENY = "Unauthorized to deny uplink request"

    TELEMETRY_METADATA_NOT_FOUND = "Telemetry metadata not found"

    INVALID_FILE_TYPE = "File type is invalid"
    FILE_NOT_FOUND = "File does not exist"

    TELECOM_MODULATION_INVALID = "Invalid modulation type."
    TELECOM_NAME_TOO_LONG = "Telecom name too long, limit to 255 chars"
    TELECOM_ENCODING_INVALID = "Telecom name must use only the ascii character set"

    REQUESTED_EXECUTION_TIME_INVALID = (
        "Requested execution time out of acceptable range"
    )
    COMMAND_TIME_CONFLICT = (
        "Conflicting command scheduled to the gs at that requested time"
    )
    TIME_SCHEDULE_ERROR = (
        "transmission/execution time scheduled for before current UTC time"
    )

    GROUND_STATION_ID_TOO_LONG = "gs_id too long, limit to 24chars"
    GROUND_STATION_ID_PREFIX_INCORRECT = "gs_id must begin with 'g'"
    GROUND_STATION_ID_ENCODING_INVALID = "gs_id must use only the ascii character set"
    GROUND_STATION_INVALID_LATITUDE = "invalid latitude coords: -90 > lat > 90"
    GROUND_STATION_INVALID_LONGITUDE = "invalid longitude coords: -180 > long > 180"

    PERMISSION_NOT_FOUND = "Permission not found for user and asset"
    PERMISSION_ALREADY_EXIST = "Permission for user and asset already exists"


class Error(Exception):
    def __init__(
        self,
        code_or_msg: Union[str, ReasonCode] = ReasonCode.SERVER_ERROR,
        msg: Optional[str] = None,
    ):
        if isinstance(code_or_msg, ReasonCode):
            self.code = code_or_msg
            self.msg = msg or code_or_msg.value
        else:
            self.code = ReasonCode.SERVER_ERROR
            self.msg = code_or_msg


class NotFoundError(Error):
    def __init__(
        self,
        code: Literal[
            ReasonCode.SATELLITE_NOT_FOUND,
            ReasonCode.GROUND_STATION_NOT_FOUND,
            ReasonCode.FILE_NOT_FOUND,
            ReasonCode.SYSTEM_NOT_FOUND,
            ReasonCode.COMPONENT_NOT_FOUND,
            ReasonCode.OVERPASS_NOT_FOUND,
            ReasonCode.PRESET_NOT_FOUND,
            ReasonCode.CONTACT_NOT_FOUND,
            ReasonCode.CONTACT_INSTRUCTION_NOT_FOUND,
            ReasonCode.CONTACT_SUPERSEDED_NOT_FOUND,
            ReasonCode.SATELLITE_TELECOM_NOT_FOUND,
            ReasonCode.ASSET_NOT_FOUND,
            ReasonCode.UPLINK_REQUEST_NOT_FOUND,
            ReasonCode.INSTRUCTION_NOT_FOUND,
            ReasonCode.TELEMETRY_METADATA_NOT_FOUND,
            ReasonCode.GROUND_STATION_TELECOM_NOT_FOUND,
            ReasonCode.PERMISSION_NOT_FOUND,
        ],
        msg: Optional[str] = None,
    ):
        super(NotFoundError, self).__init__(code, msg)


class ValidationError(Error):
    def __init__(
        self,
        code: Literal[
            ReasonCode.OVERPASS_WINDOW_EXCEEDS_A_FORTNIGHT,
            ReasonCode.OVERPASS_WINDOW_TOO_FAR_AHEAD,
            ReasonCode.INVALID_FILE_TYPE,
            ReasonCode.OVERPASS_TOO_MANY_COMPARISONS,
            ReasonCode.OVERPASS_LOS_BEFORE_AOS,
            ReasonCode.OVERPASS_AOS_IN_PAST,
            ReasonCode.SCRIPT_NOT_FOUND,
            ReasonCode.COMMAND_NOT_FOUND,
            ReasonCode.TELECOM_MODULATION_INVALID,
            ReasonCode.TELECOM_NAME_TOO_LONG,
            ReasonCode.TELECOM_ENCODING_INVALID,
            ReasonCode.PRESET_INVALID_ARGS,
            ReasonCode.INVALID_ARG_JSCHEMA,
            ReasonCode.CONTACT_INVALID_ARGS,
            ReasonCode.REQUESTED_EXECUTION_TIME_INVALID,
            ReasonCode.TIME_SCHEDULE_ERROR,
            ReasonCode.GROUND_STATION_INVALID_LATITUDE,
            ReasonCode.GROUND_STATION_INVALID_LONGITUDE,
            ReasonCode.GROUND_STATION_ID_TOO_LONG,
            ReasonCode.GROUND_STATION_ID_ENCODING_INVALID,
            ReasonCode.GROUND_STATION_ID_PREFIX_INCORRECT,
            ReasonCode.SATELLITE_ENCODER_NAME_TOO_LONG,
            ReasonCode.SATELLITE_DECODER_NAME_TOO_LONG,
            ReasonCode.SATELLITE_DECODER_NAME_TOO_LONG,
            ReasonCode.SATELLITE_ID_TOO_LONG,
            ReasonCode.SATELLITE_ID_PREFIX_INCORRECT,
            ReasonCode.SATELLITE_ID_ENCODING_INVALID,
            ReasonCode.SATELLITE_NAME_TOO_LONG,
            ReasonCode.SATELLITE_TLE_MISSING_FIELD,
            ReasonCode.SATELLITE_TLE_FIELD_TOO_LONG,
            ReasonCode.SATELLITE_TOO_MANY_IDS,
        ],
        msg: Optional[str] = None,
    ):
        super(ValidationError, self).__init__(code, msg)


class ConflictError(Error):
    def __init__(
        self,
        code: Literal[
            ReasonCode.CONTACT_SUPERSEDED_NOT_ACTIVE,
            ReasonCode.CONTACT_SUPERSEDED_WAS_CANCELLED,
            ReasonCode.SATELLITE_TELECOM_ALREADY_EXISTS,
            ReasonCode.CONTACT_ALREADY_CANCELLED,
            ReasonCode.CONTACT_CANCELLED_NOT_ACTIVE,
            ReasonCode.CONTACT_DISPATCH_CANCELLED,
            ReasonCode.CONTACT_START_CANCELLED,
            ReasonCode.CONTACT_END_CANCELLED,
            ReasonCode.COMPONENT_ALREADY_EXISTS,
            ReasonCode.SYSTEM_ALREADY_EXISTS,
            ReasonCode.GROUND_STATION_TELECOM_EXISTS,
            ReasonCode.PERMISSION_ALREADY_EXIST,
            ReasonCode.SATELLITE_NORAD_ID_EXISTS,
            ReasonCode.SATELLITE_NSSDC_ID_EXISTS,
        ],
        msg: Optional[str] = None,
    ):
        super(ConflictError, self).__init__(code, msg)


class ForbiddenError(Error):
    def __init__(
        self,
        code: Literal[
            ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_CREATE,
            ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_APPROVE,
            ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_DENY,
        ],
        msg: Optional[str] = None,
    ):
        super(ForbiddenError, self).__init__(code, msg)
