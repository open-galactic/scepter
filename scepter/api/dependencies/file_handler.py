from typing import Dict, Union

from api.utils.file import FileHandler
from flask import Response
from importlib_metadata import (  # importlib.metadata python > 3.10
    EntryPoint,
    entry_points,
)

# inject the file handler into here
_FILE_HANDLERS: list[FileHandler] = []

entry_point: EntryPoint
for entry_point in entry_points(group="osso_file.plugin"):
    _FILE_HANDLERS.append(entry_point.load())

if (num_handlers := len(_FILE_HANDLERS)) != 1:
    raise Exception(
        f"Invalid number of plugins, only 1 is allowed. No. plugins found: {num_handlers}"
    )


def get(file_key) -> Dict[str, Union[Response, str]]:
    return _FILE_HANDLERS[0].get(file_key)


def create_from_stream(stream, file_key, **kwargs) -> Dict[str, Union[int, str]]:
    return _FILE_HANDLERS[0].create_from_stream(stream, file_key, **kwargs)


def delete(file_key) -> Dict[str, str]:
    return _FILE_HANDLERS[0].delete(file_key)


def generate_presigned_path(file_key) -> Union[str, None]:
    return _FILE_HANDLERS[0].generate_presigned_path(file_key)


def create_from_file(file_key: str, file, tags) -> Dict[str, str]:
    return _FILE_HANDLERS[0].create_from_file(file_key, file, tags)
