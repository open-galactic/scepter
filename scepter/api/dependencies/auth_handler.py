import inspect
import logging
from functools import wraps
from http import HTTPStatus
from typing import Callable, List, Tuple, Union
from uuid import UUID

from importlib_metadata import (  # importlib.metadata python > 3.10
    EntryPoint,
    entry_points,
)

from api.utils.auth import Authable
from models import Asset

logger = logging.getLogger(__name__)


_AUTH_HANDLERS: list[Authable] = []

entry_point: EntryPoint
for entry_point in entry_points(group="osso_auth.plugin"):
    _AUTH_HANDLERS.append(entry_point.load())

logger.info("discovered auth plugins", extra={"handlers": _AUTH_HANDLERS})


def check_users_access_to_asset(
    user_id: Union[UUID, int], asset_id: int, access: str
) -> bool:

    return _AUTH_HANDLERS[0].check_users_access_to_asset(
        user_id=user_id,
        asset_id=asset_id,
        access=access,
    )


def get_users_assets(user_id: Union[UUID, int], access: str) -> List[Asset]:
    return _AUTH_HANDLERS[0].get_users_assets(
        user_id=user_id,
        access=access,
    )


def allow_access(
    access: str = "",
) -> Tuple[Union[UUID, int, None], bool, Union[str, None], Union[HTTPStatus, None]]:
    return _AUTH_HANDLERS[0].allow_access(
        access=access,
    )


def auth_endpoint(access: str = "all", **kwargs):
    def wrapper(wrapped_func: Callable) -> Callable:
        @wraps(wrapped_func)
        def auth_and_except(*args, **req_kwargs):
            combined_kwargs = {**kwargs, **req_kwargs}

            if (num_handlers := len(_AUTH_HANDLERS)) != 1:
                error_msg = f"Invalid number of plugins, only 1 is allowed. No. plugins found: {num_handlers}"
                logger.error(error_msg)
                raise Exception(error_msg)

            authed_user_id, is_allowed, err_msg, err_code, = allow_access(
                access=access,
            )
            logger.debug(
                "auth handler returned",
                extra={"handler": _AUTH_HANDLERS[0], "response": is_allowed},
            )

            if is_allowed:
                if authed_user_id:
                    combined_kwargs = {
                        **{"authed_user_id": authed_user_id},
                        **combined_kwargs,
                    }

                # inspect the wrapped function and remove un-used parameters.
                parameters = inspect.signature(wrapped_func).parameters
                if ("**kwargs" not in parameters.values()) and (
                    "kwargs" not in parameters.keys()
                ):
                    combined_kwargs = _filter_params(combined_kwargs, wrapped_func)
                return wrapped_func(*args, **combined_kwargs)

            return {"msg": err_msg}, err_code

        return auth_and_except

    return wrapper


def is_valid_access(access):
    return access in ["search", "write", "read", "execute", "administrate", "delete"]


def _filter_params(combined_kwargs, wrapped_func):
    params = (
        p.name
        for p in inspect.signature(wrapped_func).parameters.values()
        if p.kind == p.POSITIONAL_OR_KEYWORD
    )

    return {p: combined_kwargs[p] for p in params if p in combined_kwargs}
