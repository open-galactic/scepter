from flask import Response, _app_ctx_stack
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from api import app
from api.config import db_settings

engine = create_engine(db_settings.sqlalchemy_url, connect_args={})
reader_engine = create_engine(db_settings.sqlalchemy_reader_url, connect_args={})

session = scoped_session(
    sessionmaker(
        autoflush=False,
        bind=engine,
    ),
    scopefunc=_app_ctx_stack.__ident_func__,
)
reader_session = scoped_session(
    sessionmaker(
        autoflush=False,
        bind=reader_engine,
    ),
    scopefunc=_app_ctx_stack.__ident_func__,
)


@app.teardown_appcontext
def shutdown_session(exc=None) -> None:
    session.remove()
    reader_session.remove()


@app.after_request
def commit_session(response: Response) -> Response:
    if 200 <= response.status_code < 300:
        session.commit()
    return response
