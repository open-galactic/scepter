from http import HTTPStatus

from api import app


def test_healthcheck():
    with app.test_client() as client:

        response = client.get(
            "/healthcheck",
        )

        assert response.status_code == HTTPStatus.NO_CONTENT
