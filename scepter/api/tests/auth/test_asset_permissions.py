import json
import unittest
from datetime import datetime
from uuid import UUID, uuid4

import pytest

from api import app
from api.database import session
from models import Asset, GroundStation, GsStatus, Permission


@pytest.mark.usefixtures("patch_auth")
class TestPermissions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user_id1 = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")
        cls.user_id2 = uuid4()

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.gs_id = json.loads(response.data)["gs_id"]

        self.per_test_data = {
            "read_access": False,
            "command_access": True,
            "manage_asset": True,
            "manage_users": True,
            "owner": True,
        }

    def tearDown(self):
        session.query(Permission).delete()
        session.query(GsStatus).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_permission_post_valid(self):
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        asset = Asset.query_by_hash(session, self.gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user_id2, asset._id)
        for key in self.per_test_data:
            if key not in ["user_uuid", "asset_id"]:
                self.assertEqual(getattr(per, key), self.per_test_data[key])
            elif key == "user_uuid":
                self.assertEqual(str(self.user_id2), self.per_test_data[key])
            elif key == "asset_id":
                asset = Asset.query_by_hash(session, self.gs_id)
                self.assertEqual(asset._asset_hash, self.per_test_data[key])

    def test_permission_post_no_user_id_or_asset_id(self):
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(self.user_id2, ""),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)
        self.per_test_data["asset_id"] = self.gs_id
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format("", self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_permission_post_no_user_exists(self):
        response = self.app.post(
            "/permissions/user/not_a_user/asset/{}".format(self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "path_params": [
                    {
                        "loc": ["user_id"],
                        "msg": "value is not a valid uuid",
                        "type": "type_error.uuid",
                    }
                ]
            }
        }

    def test_permission_post_bad_boolean(self):
        self.per_test_data["read_access"] = "not_a_bool"
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["read_access"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_permission_post_existing_per_id_fails(self):
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 409)

    def test_permission_get_single_created_asset(self):
        """
        defaults permissions for created assets should all be true
        """
        response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
        )
        self.assertEqual(response.status_code, 200)
        per_data = json.loads(response.data)
        for key in self.per_test_data:
            if key == "id":
                self.assertEqual(per_data[key], None)
            elif key == "user_uuid":
                self.assertEqual(per_data[key], str(self.user_id1))
            elif key == "asset_id":
                self.assertEqual(per_data[key], self.gs_id)
            else:
                self.assertEqual(per_data[key], True)

    def test_permission_get_single_no_per(self):
        response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
        )
        assert response.status_code == 404
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": "Permission not found for user and asset",
            "code": "PERMISSION_NOT_FOUND",
        }

    def test_permission_get_single_no_user(self):
        response = self.app.get(
            "/permissions/user/not_a_user/asset/{}".format(self.gs_id),
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "path_params": [
                    {
                        "loc": ["user_id"],
                        "msg": "value is not a valid uuid",
                        "type": "type_error.uuid",
                    }
                ]
            }
        }

    def test_permission_get_single_no_hidden_vals(self):
        response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
        )
        per_data = json.loads(response.data)
        for key in per_data:
            self.assertFalse(key[0] == "_")

    def test_permission_get_multiple_filter_by_hash(self):
        get_response = self.app.get(f"/permissions/{self.gs_id}")
        self.assertEqual(get_response.status_code, 200)
        per_list = json.loads(get_response.data)
        self.assertTrue(len(per_list) == 1)
        self.assertTrue(per_list[0]["asset_id"] == self.gs_id)

    def test_permission_get_multiple_no_search_results(self):
        per_user_id_filter = f"?user_id={uuid4()}"
        get_response = self.app.get(
            f"/permissions/{self.gs_id}" + per_user_id_filter,
        )
        self.assertEqual(get_response.status_code, 200)
        per_list = json.loads(get_response.data)
        self.assertTrue(len(per_list) == 0)

    def test_permission_get_multiple_no_hidden_vals(self):
        get_response = self.app.get(
            f"/permissions/{self.gs_id}",
        )
        per_list = json.loads(get_response.data)
        per_data = per_list[0]
        for key in per_data:
            self.assertFalse(key[0] == "_")

    def test_permission_get_bad_extra_arg(self):
        bad_arg = "?not_an_arg=1234"
        response = self.app.get(
            f"/permissions/{self.gs_id}" + bad_arg,
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["not_an_arg"],
                        "msg": "extra fields not permitted",
                        "type": "value_error.extra",
                    }
                ]
            }
        }

    def test_permission_get_multiple_valid(self):
        self.per_test_data["read_access"] = True
        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 201

        response = self.app.post(
            "/permissions/user/{}/asset/{}".format(uuid4(), self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 201

        get_response = self.app.get(
            f"/permissions/{self.gs_id}",
        )
        self.assertEqual(get_response.status_code, 200)
        per_list = json.loads(get_response.data)
        self.assertTrue(isinstance(per_list, list))
        self.assertTrue(len(per_list) > 1)
        self.assertTrue(per_list[0]["id"] != per_list[1]["id"])
        self.assertTrue(per_list[0]["asset_id"] == per_list[1]["asset_id"])
        for key in per_list[0]:
            if key not in ["id", "user_uuid", "asset_id", "read_access"]:
                self.assertEqual(per_list[0][key], True)
        for key in self.per_test_data:
            if key not in ["id", "asset_id"]:
                self.assertEqual(per_list[1][key], self.per_test_data[key])

    def test_permission_put_replace_existing(self):
        per_test_data2 = {
            "read_access": True,
            "command_access": False,
            "manage_asset": False,
            "manage_users": True,
            "owner": False,
        }
        response = self.app.put(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
            data=json.dumps(per_test_data2),
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"permission for {self.user_id1} to asset { self.gs_id} updated",
        }

        get_response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
        )
        per_data = json.loads(get_response.data)
        for key in per_test_data2:
            self.assertEqual(per_data[key], per_test_data2[key])

    def test_permission_put_null_blank_fields(self):
        per_data_key_list = ["read_access", "command_access", "manage_asset", "owner"]
        response = self.app.put(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
            data=json.dumps({"manage_users": True}),
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"permission for {self.user_id1} to asset { self.gs_id} updated",
        }
        get_response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id1, self.gs_id),
        )
        self.assertEqual(get_response.status_code, 200)
        per_data = json.loads(get_response.data)
        for key in per_data_key_list:
            self.assertEqual(per_data[key], False)

    def test_permission_put_create_new(self):
        response = self.app.put(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
        )
        per_data = json.loads(get_response.data)
        for key in self.per_test_data:
            self.assertEqual(per_data[key], self.per_test_data[key])

    def test_permission_put_no_user(self):
        response = self.app.put(
            "/permissions/user/not_a_user/asset/{}".format(self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "path_params": [
                    {
                        "loc": ["user_id"],
                        "msg": "value is not a valid uuid",
                        "type": "type_error.uuid",
                    }
                ]
            }
        }

    def test_permission_put_bad_boolean(self):
        self.per_test_data["read_access"] = "not_a_bool"
        response = self.app.put(
            "/permissions/user/{}/asset/{}".format(self.user_id2, self.gs_id),
            data=json.dumps(self.per_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["read_access"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }
