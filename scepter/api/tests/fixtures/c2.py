from datetime import datetime, timedelta
from uuid import UUID, uuid4

import pytest

from models import (
    Alert,
    Command,
    Contact,
    GroundStation,
    Instruction,
    Overpass,
    Permission,
    Satellite,
    Script,
    Subsystem,
    TelemetryMetadata,
)

sat_id = "s1234"
gs_id = "g1234"
user_id = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")
headers = {
    "x-osso-user": user_id,
    "x-osso-token": "dev",
}

no_auth_headers = {
    "x-osso-user": UUID("00000000-0000-0000-0000-000000000000"),
    "x-osso-token": "dev",
}


@pytest.fixture
def satellite(session):
    satellite = Satellite(
        asset_hash=sat_id,
        sat_name="test satellite",
        description="a test satellite",
    )
    session.add(satellite)
    session.flush()
    permission = Permission(
        asset_id=satellite._id,
        user_uuid=user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    session.commit()
    yield satellite
    session.delete(permission)
    session.delete(satellite)
    session.commit()


@pytest.fixture
def system(session, satellite: Satellite):
    system = Subsystem(satellite, "test_system", "system", "1")
    session.add(system)
    session.flush()
    yield system
    session.delete(system)
    session.commit()


@pytest.fixture
def component(session, satellite: Satellite, system: Subsystem):
    component = Subsystem(satellite, "test_component", "component", "1", system)
    session.add(component)
    session.flush()
    session.commit()
    yield component
    session.delete(component)
    session.commit()


@pytest.fixture
def telemetry_metadata(session, satellite: Satellite, system: Subsystem):
    telemetry_metadata = TelemetryMetadata(
        _timestamp=datetime.now(),
        _sat_id=satellite._id,
        tlm_name="test_tlm_name",
        cad_id="test_cad_id",
        uuid=str(uuid4()),
        _subsystem_id=system.id,
        description="test_description",
        units="test_units",
        uncertainty=1.1,
        nominal_min=20,
        nominal_max=40,
        alert_min=10,
        alert_max=50,
    )
    session.add(telemetry_metadata)
    session.commit()
    yield telemetry_metadata
    session.delete(telemetry_metadata)
    session.commit()


@pytest.fixture
def alert(session, satellite: Satellite, telemetry_metadata: TelemetryMetadata):
    alert = Alert(
        metadata_id=telemetry_metadata.uuid,
        satellite=satellite,
        faulted_at=datetime.now() - timedelta(hours=1),
        created_at=datetime.now(),
        value=9,
        error_code="TELEMETRY_BELOW_ALERT_MIN",
    )
    session.add(alert)
    session.commit()
    yield alert
    session.delete(alert)
    session.commit()


@pytest.fixture
def groundstation(session):
    groundstation = GroundStation(
        asset_hash=gs_id,
        sat_name="test_gs_name",
        commission_date=datetime.utcnow().isoformat(),
        latitude=10,
        longitude=20,
        altitude=30,
    )
    session.add(groundstation)
    session.flush()
    permission = Permission(
        asset_id=groundstation._id,
        user_uuid=user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    session.commit()
    yield groundstation
    session.delete(permission)
    session.delete(groundstation)
    session.commit()


@pytest.fixture
def overpass(session, satellite, groundstation):
    offset = timedelta(hours=1)
    overpass = Overpass(
        groundstation=groundstation,
        satellite=satellite,
        source="calculator",
        trajectory={"key": "value"},
        tle_updated_at=datetime.utcnow(),
        maximum_el=50,
        aos=(datetime.utcnow() + offset) - timedelta(minutes=10),
        tca=datetime.utcnow() + offset,
        los=(datetime.utcnow() + offset) + timedelta(minutes=10),
        source_reference=uuid4(),
    )
    session.add(overpass)
    session.commit()
    yield overpass
    session.delete(overpass)


@pytest.fixture
def command_template(session, satellite):
    command = Command(
        id="example_command",
        satellite=satellite,
        name="example_command",
        entrypoint="/customcommands/custom.py",
        code="EX_COMMAND",
        requires_approval=False,
        encoder_data={"property": "value"},
        args={
            "type": "object",
            "properties": {
                "argument_1": {"type": "string"},
                "argument_2": {"type": "integer"},
                "argument_3": {"type": "number"},
            },
            "additionalProperties": False,
        },
    )
    session.add(command)
    session.commit()
    yield command
    session.delete(command)
    session.commit()


@pytest.fixture
def script_template(session, satellite):
    script = Script(
        id="example_script",
        satellite=satellite,
        name="example_script",
        entrypoint="/scripts/example_script.py",
        args={
            "type": "object",
            "properties": {
                "argument": {"type": "string"},
            },
            "additionalProperties": False,
        },
    )
    session.add(script)
    session.commit()
    yield script
    session.delete(script)
    session.commit()


@pytest.fixture
def historic_contact(session, satellite, overpass, script_template):
    contact = Contact(
        group_id=2,
        satellite=satellite,
        overpass=overpass,
        issued_by="user1",
        issued_at=datetime.utcnow(),
    )
    session.add(contact)
    session.flush()

    instruction = Instruction(
        id=0,
        script=script_template,
        contact=contact,
        sequence_number=1,
        args={
            "argument": "script string",
        },
    )
    session.add(instruction)
    session.commit()
    yield contact
    session.delete(contact)
    session.commit()


@pytest.fixture
def upcoming_contact(session, satellite, overpass, command_template, script_template):
    contact = Contact(
        group_id=1,
        overpass=overpass,
        satellite=satellite,
        issued_by="user1",
        issued_at=datetime.utcnow(),
    )
    session.add(contact)
    session.flush()

    command = Instruction(
        id=0,
        command=command_template,
        contact=contact,
        sequence_number=0,
        args={"argument_1": "a string", "argument_2": 1, "argument_3": 3.2},
    )
    session.add(command)

    script = Instruction(
        id=1,
        script=script_template,
        contact=contact,
        sequence_number=1,
        args={
            "argument": "script string",
        },
    )
    session.add(script)
    session.commit()
    yield contact
    session.delete(contact)
    session.commit()


@pytest.fixture
def superseded_contact(session, satellite, overpass):
    contact_1 = Contact(
        group_id=3,
        overpass=overpass,
        satellite=satellite,
        issued_by="user1",
        issued_at=datetime.utcnow(),
    )
    session.add(contact_1)
    # need to actually commit this separately to ensure the created_at
    # timestamp is before contact two's
    session.commit()

    contact_2 = Contact(
        group_id=contact_1.group_id,
        overpass=overpass,
        satellite=satellite,
        issued_by="user2",
        issued_at=datetime.utcnow(),
    )
    session.add(contact_2)
    session.commit()
    yield contact_1, contact_2
    session.delete(contact_1)
    session.delete(contact_2)
    session.commit()
