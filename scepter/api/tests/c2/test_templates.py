from api import app
from api.tests.fixtures.c2 import headers, sat_id


def test_get_template_list(session, command_template, script_template):
    with app.test_client() as client:
        response = client.get(f"/satellite/{sat_id}/templates", headers=headers)
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 1
    assert type(data["cursor"]) == str

    templates = data["templates"]
    assert type(templates) == list
    assert len(templates) == 2

    template = templates[0]
    assert template["id"] == command_template.id
    assert template["args"] == command_template.args
    assert template["entrypoint"] == command_template.entrypoint
    assert template["type"] == "command"

    template = templates[1]
    assert template["id"] == script_template.id
    assert template["args"] == script_template.args
    assert template["entrypoint"] == script_template.entrypoint
    assert template["type"] == "script"


def test_get_template_pagination(session, command_template, script_template):
    # Check first page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?page_size=1", headers=headers
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 2
    templates = data["templates"]
    assert type(templates) == list
    # Check command only result
    assert len(templates) == 1
    assert templates[0]["id"] == command_template.id

    # Check second page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?page_size=1&cursor={data['cursor']}",
            headers=headers,
        )
    data: dict = response.json
    assert response.status_code == 200
    assert type(data) == dict
    assert data["total_pages"] == 2
    templates = data["templates"]
    assert type(templates) == list
    # Check script is only result
    assert len(templates) == 1
    assert templates[0]["id"] == script_template.id

    # Check overrun page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?page_size=1&cursor={data['cursor']}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert data["total_pages"] == 2
    templates = data["templates"]
    assert type(templates) == list
    # Check no results
    assert len(templates) == 0
    assert data["cursor"] is None


def test_get_template_with_unknown_param(session, command_template):
    # Check first page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?unknown=1", headers=headers
        )
    assert response.status_code == 400


def test_get_template_filter_commands(session, command_template, script_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?type=command", headers=headers
        )
    assert response.status_code == 200
    data: dict = response.json
    templates = data["templates"]
    assert len(templates) == 1
    assert templates[0]["id"] == command_template.id


def test_get_template_filter_scripts(session, command_template, script_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/templates?type=script", headers=headers
        )
    assert response.status_code == 200
    data: dict = response.json
    templates = data["templates"]
    assert len(templates) == 1
    assert templates[0]["id"] == script_template.id
