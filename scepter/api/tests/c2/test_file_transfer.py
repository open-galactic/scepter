import json
import random
import unittest
from datetime import datetime, timedelta
from unittest.mock import patch

from api import app
from api.database import session
from api.dependencies import file_handler
from api.tests.fixtures.c2 import headers
from api.utils.utils import b_to_kb
from models import Asset, FileTransferMetadata, Permission, Satellite


class TestFileTransfer(unittest.TestCase):
    @classmethod
    @patch.object(file_handler, "create_from_stream")
    def setUpClass(cls, mock_utils):
        mock_utils.return_value = {"read_length": 300}
        b_to_kb.return_value = 30

        num = f"{random.randint(0, 100000)}"
        cls.norad_id = random.randint(0, 10000)

        cls.app = app.test_client()
        cls.admin_headers = headers

        cls.sat_test_data = {
            "sat_name": "test_sat_name",
            "norad_id": cls.norad_id,
            "nssdc_id": "test_id" + num,
            "commission_date": datetime.utcnow().isoformat(),
            "launch_date": datetime.utcnow().isoformat(),
            "description": "test_description",
        }

        sat_response = cls.app.post(
            "/satellite",
            data=json.dumps(cls.sat_test_data),
            headers=cls.admin_headers,
            content_type="application/json",
        )
        assert sat_response.status_code == 201
        cls.sat_id = json.loads(sat_response.data)["sat_id"]
        cls.read_file_path = "test_read.csv"

        session.commit()

        data = {
            "asset_hash": cls.sat_id,
            "_file_key": f"{cls.sat_id}/{cls.read_file_path}",
            "_timestamp": (datetime.now() - timedelta(hours=1)).isoformat(),
            "_file_size": random.uniform(0, 50),
            "_deleted": 1 >= 20,
            "file_type": "log",
        }
        file_meta = FileTransferMetadata(**data)
        session.add(file_meta)
        session.commit()

    @classmethod
    def tearDownClass(cls):
        session.query(FileTransferMetadata).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    @patch.object(file_handler, "get")
    def test_get_valid(self, mock_get):
        mock_get.return_value = {"stream": "mock"}

        response = self.app.get(
            "/asset/{}/file/{}".format(self.sat_id, self.read_file_path),
            headers=self.admin_headers,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b"mock")

    @patch.object(file_handler, "get")
    def test_get_not_found(self, mock_get):
        mock_get.return_value = {"error": "File does not exist"}

        response = self.app.get(
            "/asset/{}/file/no_such_file".format(self.sat_id),
            headers=self.admin_headers,
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {"msg": "File does not exist"})

    @patch.object(file_handler, "delete")
    def test_delete_valid(self, mock_delete):
        mock_delete.return_value = {"msg": "Success"}

        file_name = self.read_file_path

        response = self.app.delete(
            "/asset/{}/file/{}".format(self.sat_id, file_name),
            headers=self.admin_headers,
        )
        self.assertEqual(response.status_code, 200)

        file_key = f"{self.sat_id}/{file_name}"
        self.assertEqual(
            session.query(FileTransferMetadata).filter(FileTransferMetadata._file_key == file_key).first()._deleted,
            True,
        )

    @patch.object(file_handler, "get")
    def test_delete_not_found(self, mock_get):
        stream_val = "mock"
        mock_get.return_value = {"stream": stream_val}

        response = self.app.delete(
            "/asset/{}/file/no_such_file".format(self.sat_id),
            headers=self.admin_headers,
        )
        self.assertEqual(response.status_code, 404)
