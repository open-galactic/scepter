import json
import unittest
from datetime import datetime
from uuid import UUID

import pytest

from api import app
from api.database import session
from api.exceptions import ReasonCode
from api.utils.messages import SatSystemMessages
from models import Asset, Permission, Satellite, Subsystem


@pytest.mark.usefixtures("patch_auth")
class TestSatelliteSubSystems(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user_id = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")  # TODO: inject this

    def setUp(self):
        sat = Satellite(
            sat_name="test_sat_name",
            description="test_description",
            timestamp=datetime.now().isoformat(),
            asset_hash="s445342",
        )
        session.add(sat)
        session.flush()
        self.sat_hash = sat._asset_hash
        self.sat_id = sat._id

        permission = Permission(
            asset_id=self.sat_id,
            user_uuid=self.user_id,
            read_access=True,
            command_access=True,
        )
        session.add(permission)
        session.flush()

        self.sat_systems = (
            "system-1",
            "system-2",
            "system-3",
        )
        for i, system in enumerate(self.sat_systems):
            sat_system = Subsystem(
                satellite=sat,
                name=system,
                type="system",
                address=str(i),
                parent=None,
            )
            session.add(sat_system)
            session.flush()
            sat_component = Subsystem(
                satellite=sat,
                name=f"{system}-component",
                type="component",
                address=str(i + 3),
                parent=sat_system,
            )
            session.add(sat_component)
        session.commit()

    def tearDown(self):
        session.query(Subsystem).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_sat_system_valid(self):
        get_response = self.app.get(
            "/satellite/{}/systems".format(self.sat_hash),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        self.assertEqual(len(sat_data["systems"]), len(self.sat_systems))

        for i, system in enumerate(self.sat_systems):
            retrieved_system = next(
                (x for x in sat_data["systems"] if x["name"] == system)
            )
            self.assertTrue(retrieved_system)
            self.assertEqual(retrieved_system["type"], "system")
            self.assertEqual(retrieved_system["address"], str(i))

            self.assertEqual(type(retrieved_system["components"]), list)
            component = retrieved_system["components"][0]
            self.assertEqual(component["name"], system + "-component")
            self.assertEqual(component["type"], "component")
            self.assertEqual(component["address"], str(i + 3))

    def test_post_sat_component_valid(self):
        post_data = {
            "name": "some_subssytem",
            "address": "some_address",
            "system_name": self.sat_systems[0],
        }

        post_response = self.app.post(
            "/satellite/{}/systems".format(self.sat_hash),
            data=json.dumps(post_data),
            content_type="application/json",
        )
        assert post_response.status_code == 201
        sat_data = json.loads(post_response.data)
        assert sat_data["code"] == SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.name
        assert sat_data["msg"] == SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.value
        assert sat_data["sat_id"] == self.sat_hash
        assert sat_data["subsystem"]["name"] == post_data["name"]

    def test_post_sat_system_valid(self):
        post_data = {
            "name": "some_subssytem",
            "address": "some_address",
        }

        post_response = self.app.post(
            "/satellite/{}/systems".format(self.sat_hash),
            data=json.dumps(post_data),
            content_type="application/json",
        )
        assert post_response.status_code == 201
        sat_data = json.loads(post_response.data)
        assert sat_data["code"] == SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.name
        assert sat_data["msg"] == SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.value
        assert sat_data["sat_id"] == self.sat_hash
        assert sat_data["subsystem"]["name"] == post_data["name"]

    def test_post_sat_system_duplicate(self):

        post_data = {
            "name": self.sat_systems[0],
            "address": "some_address",
        }

        post_response = self.app.post(
            "/satellite/{}/systems".format(self.sat_hash),
            data=json.dumps(post_data),
            content_type="application/json",
        )
        assert post_response.status_code == 409
        sat_data = json.loads(post_response.data)
        assert sat_data["code"] == ReasonCode.SYSTEM_ALREADY_EXISTS.name
        assert sat_data["msg"] == ReasonCode.SYSTEM_ALREADY_EXISTS.value

    def test_post_sat_component_duplicate(self):

        post_data = {
            "name": f"{self.sat_systems[0]}-component",
            "address": "some_address",
            "system_name": self.sat_systems[0],
        }

        post_response = self.app.post(
            "/satellite/{}/systems".format(self.sat_hash),
            data=json.dumps(post_data),
            content_type="application/json",
        )
        assert post_response.status_code == 409
        sat_data = json.loads(post_response.data)
        assert sat_data["code"] == ReasonCode.COMPONENT_ALREADY_EXISTS.name
        assert sat_data["msg"] == ReasonCode.COMPONENT_ALREADY_EXISTS.value
