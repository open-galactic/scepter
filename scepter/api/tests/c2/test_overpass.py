import json
from datetime import datetime, timedelta
from http import HTTPStatus
from uuid import uuid4

import pytest

from api import app
from api.exceptions import ReasonCode
from api.tests.fixtures.c2 import user_id
from models import GroundStation, Overpass, Permission, Satellite


@pytest.fixture
def satellite_2(session):
    satellite = Satellite(
        asset_hash="s5678",
        sat_name="test satellite 2",
        description="a test satellite 2",
    )
    session.add(satellite)
    session.flush()
    permission = Permission(
        asset_id=satellite._id,
        user_uuid=user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    session.commit()
    yield satellite
    session.delete(permission)
    session.delete(satellite)
    session.commit()


@pytest.fixture
def satellite_no_perms(session):
    satellite = Satellite(
        asset_hash="s91011",
        sat_name="test satellite no perms",
        description="a test satellite no perms",
    )
    session.add(satellite)
    session.flush()
    session.commit()
    yield satellite
    session.delete(satellite)
    session.commit()


@pytest.fixture
def groundstation_2(session):
    groundstation = GroundStation(
        asset_hash="g5678",
        sat_name="test_gs_name2",
        commission_date=datetime.utcnow().isoformat(),
        latitude=10,
        longitude=20,
        altitude=30,
    )
    session.add(groundstation)
    session.flush()
    permission = Permission(
        asset_id=groundstation._id,
        user_uuid=user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    session.commit()
    yield groundstation
    session.delete(permission)
    session.delete(groundstation)
    session.commit()


@pytest.fixture
def groundstation_no_perms(session):
    groundstation = GroundStation(
        asset_hash="g91011",
        sat_name="test_gs_name_no_perms",
        commission_date=datetime.utcnow().isoformat(),
        latitude=10,
        longitude=20,
        altitude=30,
    )
    session.add(groundstation)
    session.flush()
    session.commit()
    yield groundstation
    session.delete(groundstation)
    session.commit()


@pytest.fixture
def create_overpass(
    session,
    satellite: Satellite,
    satellite_2: Satellite,
    satellite_no_perms: Satellite,
    groundstation: GroundStation,
    groundstation_2: GroundStation,
    groundstation_no_perms: GroundStation,
) -> list[Overpass]:

    overpasses: list[Overpass] = []

    def _create_overpass(
        overpass_offset: timedelta,
        groundstation: GroundStation = groundstation,
        satellite: Satellite = satellite,
    ):
        overpass = Overpass(
            groundstation=groundstation,
            satellite=satellite,
            source="calculator",
            trajectory={"key": "value"},
            tle_updated_at=datetime.utcnow(),
            maximum_el=50,
            aos=(datetime.utcnow() + overpass_offset) - timedelta(minutes=10),
            tca=datetime.utcnow() + overpass_offset,
            los=(datetime.utcnow() + overpass_offset) + timedelta(minutes=10),
            source_reference=uuid4(),
        )
        session.add(overpass)
        overpasses.append(overpass)
        session.commit()
        return overpass

    yield _create_overpass

    for overpass in overpasses:
        session.delete(overpass)
    session.commit()


def test_get_next_overpasses(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(hours=1))

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}",
        )
        print(response.data)
        assert response.status_code == HTTPStatus.OK
        overpass = json.loads(response.data)[0]
        assert overpass["sat_id"] == satellite._asset_hash
        assert overpass["gs_id"] == groundstation._asset_hash
        assert overpass["leapfrog_path"] == {"key": "value"}
        assert overpass["maxima_el"] == 50.0


def test_get_multiple_overpasses(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    for i in range(1, 52):
        create_overpass(timedelta(hours=i))

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}",
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 51
        for overpass in overpasses:
            assert type(overpass) == dict


def test_get_outside_default_range(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(days=8))
    create_overpass(timedelta(days=-1))

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}",
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 0


def test_get_using_start_window(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(days=-1))
    create_overpass(timedelta(days=5))

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
            f"&start_time={(datetime.utcnow() - timedelta(days=2)).isoformat()}"
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 2


def test_get_overpasses_outside_range(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
            f"&start_time={(datetime.utcnow() + timedelta(days=4 * 7)).isoformat()}"
        )

        assert response.status_code == HTTPStatus.BAD_REQUEST
        data: dict = response.json
        assert type(data) == dict
        assert data["msg"] == ReasonCode.OVERPASS_WINDOW_TOO_FAR_AHEAD.value
        assert data["code"] == ReasonCode.OVERPASS_WINDOW_TOO_FAR_AHEAD.name


def test_get_using_start_and_end_window(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    for i in range(-2, 2):
        create_overpass(timedelta(days=i))

    with app.test_client() as client:

        start_time: datetime = (datetime.utcnow() - timedelta(days=1.5)).isoformat()
        end_time: datetime = (datetime.utcnow() + timedelta(days=1.5)).isoformat()

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
            f"&start_time={start_time}"
            f"&end_time={end_time}"
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 3
        for overpass in overpasses:
            assert datetime.strptime(
                start_time, "%Y-%m-%dT%H:%M:%S.%f"
            ) < datetime.strptime(overpass["rise_time"], "%Y-%m-%dT%H:%M:%S.%f")

            assert datetime.strptime(
                end_time, "%Y-%m-%dT%H:%M:%S.%f"
            ) > datetime.strptime(overpass["set_time"], "%Y-%m-%dT%H:%M:%S.%f")


def test_get_no_gs_id(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(days=1))

    with app.test_client() as client:

        response = client.get(f"/overpass?sat_id={satellite._asset_hash}")

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 1


def test_get_no_gs_id_default_many_gs(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
    groundstation_2: GroundStation,
):

    create_overpass(timedelta(days=1))
    create_overpass(timedelta(days=2), groundstation=groundstation_2)

    with app.test_client() as client:

        response = client.get(f"/overpass" f"?sat_id={satellite._asset_hash}")

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 2
        assert overpasses[0]["gs_id"] == groundstation._asset_hash
        assert overpasses[1]["gs_id"] == groundstation_2._asset_hash


def test_get_many_gs_ids(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
    groundstation_2: GroundStation,
):

    create_overpass(timedelta(days=1))
    create_overpass(timedelta(days=2), groundstation=groundstation_2)

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
            f"&gs_id={groundstation_2._asset_hash}"
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 2
        assert overpasses[0]["gs_id"] == groundstation._asset_hash
        assert overpasses[1]["gs_id"] == groundstation_2._asset_hash


def test_get_many_sat_ids(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    satellite_2: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(days=1))
    create_overpass(timedelta(days=2), satellite=satellite_2)

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&sat_id={satellite_2._asset_hash}"
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 2
        assert overpasses[0]["sat_id"] == satellite._asset_hash
        assert overpasses[1]["sat_id"] == satellite_2._asset_hash


def test_get_many_sats_and_gs(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    satellite_2: Satellite,
    groundstation: GroundStation,
    groundstation_2: GroundStation,
):

    for i, sat in enumerate([satellite, satellite_2]):
        for j, gs in enumerate([groundstation, groundstation_2]):
            create_overpass(
                timedelta(days=2 * i + j + 1), satellite=sat, groundstation=gs
            )

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&sat_id={satellite_2._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
            f"&gs_id={groundstation_2._asset_hash}"
        )

        assert response.status_code == HTTPStatus.OK
        overpasses = json.loads(response.data)
        assert len(overpasses) == 4
        for i, sat_id in enumerate([satellite._asset_hash, satellite_2._asset_hash]):
            for j, gs_id in enumerate(
                [groundstation._asset_hash, groundstation_2._asset_hash]
            ):

                assert overpasses[2 * i + j]["sat_id"] == sat_id
                assert overpasses[2 * i + j]["gs_id"] == gs_id


def test_get_no_sat_id(
    patch_auth,
    create_overpass: list[Overpass],
):

    with app.test_client() as client:

        response = client.get("/overpass")

        assert response.status_code == HTTPStatus.BAD_REQUEST
        data: dict = response.json
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["sat_id"],
                        "msg": "field required",
                        "type": "value_error.missing",
                    }
                ]
            }
        }


def test_get_window_too_long(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&start_time={datetime.utcnow().isoformat()}"
            f"&end_time={(datetime.utcnow() + timedelta(days=15)).isoformat()}"
        )

        assert response.status_code == HTTPStatus.BAD_REQUEST
        data: dict = response.json
        assert type(data) == dict
        assert data["msg"] == ReasonCode.OVERPASS_WINDOW_EXCEEDS_A_FORTNIGHT.value
        assert data["code"] == ReasonCode.OVERPASS_WINDOW_EXCEEDS_A_FORTNIGHT.name


def test_get_bad_gs_permissions(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation_no_perms: GroundStation,
):

    create_overpass(timedelta(days=1), groundstation=groundstation_no_perms)

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite._asset_hash}"
            f"&gs_id={groundstation_no_perms._asset_hash}"
        )

        assert response.status_code == HTTPStatus.NOT_FOUND
        data: dict = response.json
        assert type(data) == dict
        assert data["code"] == ReasonCode.GROUND_STATION_NOT_FOUND.name
        assert (
            data["msg"]
            == f"Ground Station with ID {groundstation_no_perms._asset_hash} not found"
        )


def test_get_bad_sat_permissions(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    satellite_no_perms: Satellite,
    groundstation: GroundStation,
):

    create_overpass(timedelta(days=1), satellite=satellite_no_perms)

    with app.test_client() as client:

        response = client.get(
            f"/overpass"
            f"?sat_id={satellite_no_perms._asset_hash}"
            f"&gs_id={groundstation._asset_hash}"
        )

        assert response.status_code == HTTPStatus.NOT_FOUND
        data: dict = response.json
        assert type(data) == dict
        assert data["code"] == ReasonCode.SATELLITE_NOT_FOUND.name
        assert (
            data["msg"]
            == f"Satellite with ID {satellite_no_perms._asset_hash} not found"
        )


def test_get_single_valid(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    overpass: Overpass = create_overpass(timedelta(days=1))

    with app.test_client() as client:

        response = client.get(f"/overpass/{overpass.id}")

        assert response.status_code == HTTPStatus.OK
        overpass = json.loads(response.data)
        assert overpass["sat_id"] == satellite._asset_hash
        assert overpass["gs_id"] == groundstation._asset_hash
        assert overpass["leapfrog_path"] == {"key": "value"}
        assert overpass["maxima_el"] == 50.0


def test_get_single_no_overpass(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation: GroundStation,
):

    with app.test_client() as client:

        response = client.get(f"/overpass/{uuid4()}")

        assert response.status_code == HTTPStatus.NOT_FOUND
        data: dict = response.json
        assert type(data) == dict
        assert data["msg"] == ReasonCode.OVERPASS_NOT_FOUND.value
        assert data["code"] == ReasonCode.OVERPASS_NOT_FOUND.name


def test_get_single_invalid_gs_auth(
    patch_auth,
    create_overpass: list[Overpass],
    satellite: Satellite,
    groundstation_no_perms: GroundStation,
):

    overpass: Overpass = create_overpass(
        timedelta(days=1), groundstation=groundstation_no_perms
    )

    with app.test_client() as client:

        response = client.get(f"/overpass/{overpass.id}")

        assert response.status_code == HTTPStatus.NOT_FOUND
        data: dict = response.json
        assert type(data) == dict
        assert data["msg"] == ReasonCode.GROUND_STATION_NOT_FOUND.value
        assert data["code"] == ReasonCode.GROUND_STATION_NOT_FOUND.name


def test_get_single_invalid_sat_auth(
    patch_auth,
    create_overpass: list[Overpass],
    satellite_no_perms: Satellite,
):

    overpass: Overpass = create_overpass(
        timedelta(days=1), satellite=satellite_no_perms
    )

    with app.test_client() as client:

        response = client.get(f"/overpass/{overpass.id}")

        assert response.status_code == HTTPStatus.NOT_FOUND
        data: dict = response.json
        assert type(data) == dict
        assert data["msg"] == ReasonCode.SATELLITE_NOT_FOUND.value
        assert data["code"] == ReasonCode.SATELLITE_NOT_FOUND.name
