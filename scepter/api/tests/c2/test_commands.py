from api import app
from api.tests.fixtures.c2 import headers, sat_id
from models import Command


def test_get(session, command_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/{command_template.id}", headers=headers
        )
    assert response.status_code == 200

    data: dict = response.json
    assert data["name"] == command_template.name
    assert data["args"] == command_template.args
    assert data["entrypoint"] == command_template.entrypoint
    assert data["code"] == command_template.code
    assert data["requires_approval"] == command_template.requires_approval
    assert data["encoder_data"] == command_template.encoder_data


def test_get_using_unknown_id(session, command_template):
    with app.test_client() as client:
        response = client.get(f"/satellite/{sat_id}/commands/unknown", headers=headers)
    assert response.status_code == 404


def test_create_with_put(session, satellite):
    id = "new_put_command"
    data = {
        "name": "new_command_name",
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/new/entrypoint",
        "code": "some new code",
        "requires_approval": True,
        "encoder_data": {"key": "some new value"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{id}",
            headers=headers,
            json=data,
        )
    assert response.status_code == 201

    command = response.json["command"]
    db_command = Command.get_by_id(id, satellite)
    # Check repsonse matches db data
    assert command["id"] == db_command.id
    assert command["name"] == db_command.name
    assert command["args"] == db_command.args
    assert command["entrypoint"] == db_command.entrypoint
    assert command["code"] == db_command.code
    assert command["requires_approval"] == db_command.requires_approval
    assert command["encoder_data"] == db_command.encoder_data

    # Check db matches input values
    assert id == db_command.id
    assert data["args"] == db_command.args
    assert data["name"] == db_command.name
    assert data["code"] == db_command.code
    assert data["requires_approval"] == db_command.requires_approval
    assert data["encoder_data"] == db_command.encoder_data
    assert data["entrypoint"] == db_command.entrypoint

    session.query(Command).delete()
    session.commit()


def test_update_with_put(session, satellite, command_template):
    id = command_template.id
    data = {
        "name": "new name",
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
                "new_put_argument2": {"type": "string"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/new_entrypoint",
        "code": "some code",
        "requires_approval": False,
        "encoder_data": {"key": "some value"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{id}",
            headers=headers,
            json=data,
        )
    assert response.status_code == 201

    command = response.json["command"]
    # gross hack to close the fixture session, and reload the new data from the
    # request. TODO fix this once injection is working
    session.commit()

    # Check repsonse matches db data
    assert command["name"] == command_template.name
    assert command["args"] == command_template.args
    assert command["entrypoint"] == command_template.entrypoint
    assert command["code"] == command_template.code
    assert command["requires_approval"] == command_template.requires_approval
    assert command["encoder_data"] == command_template.encoder_data

    # Check db matches input values
    assert command_template.id == id
    assert data["name"] == command_template.name
    assert data["args"] == command_template.args
    assert data["entrypoint"] == command_template.entrypoint
    assert data["code"] == command_template.code
    assert data["requires_approval"] == command_template.requires_approval
    assert data["encoder_data"] == command_template.encoder_data


def test_create_with_unknown_param(session, satellite):
    data = {
        "name": "example name",
        "unknown_param": 1,
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/new_id",
            headers=headers,
            json=data,
        )
    assert response.status_code == 400


def test_create_with_invalid_jschema(session, satellite):
    data = {
        "name": "example name",
        "args": {
            "type": "not an object",
            "properties": {
                "new_put_argument": {"type": "unknown datatype"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/new_id",
            headers=headers,
            json=data,
        )
    assert response.status_code == 400
