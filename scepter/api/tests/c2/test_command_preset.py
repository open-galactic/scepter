from uuid import uuid4

import pytest

from api import app
from api.tests.fixtures.c2 import headers, sat_id
from models import Command


@pytest.fixture
def command_presets(session, command_template):
    preset_a = command_template.create_preset("preset A", {"argument": "A"})
    preset_b = command_template.create_preset("preset B", {"argument": "B"})
    session.add(preset_a)
    session.add(preset_b)
    session.commit()
    yield preset_a, preset_b
    session.delete(preset_a)
    session.delete(preset_b)
    session.commit()


@pytest.fixture
def second_command_preset(session, satellite):
    command = Command(
        id="second_command",
        satellite=satellite,
        name="second_command",
        code=10,
        requires_approval=False,
        encoder_data={},
        entrypoint="/customcommands/custom.py",
        args={"type": "object", "properties": {"argument": {"type": "string"}}},
    )
    session.add(command)
    session.flush()

    preset = command.create_preset("preset A", {"argument": "AAAAA"})
    session.add(preset)
    session.commit()
    yield preset, command
    session.delete(preset)
    session.delete(command)
    session.commit()


def test_list_preset(session, command_template, command_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    presets = data["presets"]
    assert type(presets) == list
    assert len(presets) == 2

    for i in range(2):
        assert presets[i]["id"] == str(command_presets[i].id)
        assert presets[i]["name"] == command_presets[i].name
        assert presets[i]["args"] == command_presets[i].args


def test_list_preset_with_unknown_template(session, command_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/unknown/presets", headers=headers
        )
    assert response.status_code == 404


def test_list_preset_with_unknown_sat(session, command_presets, command_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/unknown/commands/{command_template.name}/presets",
            headers=headers,
        )
    assert response.status_code == 404


def test_list_preset_is_scoped_by_command(
    session, command_presets, second_command_preset
):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/{second_command_preset[1].id}/presets",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    presets = data["presets"]
    assert type(presets) == list
    assert len(presets) == 1
    assert presets[0]["id"] == str(second_command_preset[0].id)


def test_get_preset(session, command_template, command_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/{command_template.name}"
            f"/presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict

    assert data["id"] == str(command_presets[0].id)
    assert data["name"] == command_presets[0].name
    assert data["args"] == command_presets[0].args


def test_get_preset_for_unknown_template(session, command_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/unknown/presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_get_preset_for_unknown_sat(session, command_presets, command_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/unknown/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_get_preset_for_missmatched_template(
    session, command_presets, second_command_preset
):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/commands/{second_command_preset[1].id}/"
            f"presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_create_new(session, command_template):
    data = {"name": "new preset", "args": {"argument_1": "new"}}
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json
    assert "msg" in data
    preset = data["preset"]
    assert preset["id"]

    db_preset = command_template.get_preset_by_id(preset["id"])
    assert db_preset.name == preset["name"]
    assert db_preset.args == preset["args"]
    assert db_preset.created_at
    session.delete(db_preset)
    session.commit()


def test_create_new_with_bad_args(session, command_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_create_new_missing_name(session, command_template):
    data = {"args": {"argument_1": "new", "argument_2": 20}}
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_create_for_unknown_sat(session, command_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/unknown/commands/{command_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_create_for_unknown_template(session, command_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/commands/unkwnown/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update(session, command_template, command_presets):
    data = {"name": "new name", "args": {"argument_1": "new"}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json
    assert "msg" in data
    preset = data["preset"]
    assert preset["id"] == str(command_presets[0].id)

    # TODO replace this once session injection works
    session.commit()

    assert command_presets[0].name == preset["name"]
    assert command_presets[0].args == preset["args"]
    assert command_presets[0].created_at


def test_update_missing_name(session, command_template, command_presets):
    data = {"args": {"argument_1": "new", "argument_2": 20}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_update_bad_args(session, command_template, command_presets):
    data = {"name": "new name", "args": {"argument_1": 30, "argument_2": "not an int"}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_update_for_unknown_sat(session, command_template, command_presets):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/unknown/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update_for_unknown_template(session, command_template, command_presets):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/unknown/presets/{command_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update_for_unknown_preset(session, command_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets/{uuid4()}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_delete(session, command_template):
    preset = command_template.create_preset("test name", {"argument_1": 10})
    session.add(preset)
    session.commit()

    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/commands/{command_template.name}/"
            f"presets/{preset.id}",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    assert "msg" in data

    # TODO replace this once session injection works
    session.commit()
    assert len(command_template.presets) == 0


def test_delete_for_unknown_sat(session, command_template, command_presets):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/unknown/commands/{command_template.name}/"
            f"presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_unknown_template(session, command_template, command_presets):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/commands/unknown/presets/{command_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_unknown_preset(session, command_template):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/commands/{command_template.name}/presets/{uuid4()}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_missmatched_presets(
    session, command_template, second_command_preset
):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/commands/{command_template.name}/"
            f"presets/{second_command_preset[0].id}",
            headers=headers,
        )
    assert response.status_code == 404
