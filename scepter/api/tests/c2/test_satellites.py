import json
import unittest
from datetime import datetime

from api import app
from api.database import session
from api.tests.fixtures.c2 import headers
from models import Asset, Permission, Satellite


class TestSatellite(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.headers = headers

    def setUp(self):
        self.sat_test_data = {
            "sat_name": "test_sat_name",
            "norad_id": 1,
            "nssdc_id": "test_id",
            "commission_date": datetime.utcnow().isoformat(),
            "launch_date": datetime.utcnow().isoformat(),
            "description": "test_description",
            "tle": {
                "norad_cat_id": 1,
                "tle_line_0": "tle line 0 values",
                "tle_line_1": "tle line 1 values",
                "tle_line_2": "tle line 2 values",
                "last_updated": datetime.utcnow().isoformat(),
                "object_type": "test type",
            },
        }
        sat_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert sat_response.status_code == 201

        self.sat_id = json.loads(sat_response.data)["sat_id"]

    def tearDown(self):
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_satellite_post_valid(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "test_id2"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        get_response = self.app.get(
            "/satellite/{}".format(sat_id),
            headers=self.headers,
        )
        sat_data = json.loads(get_response.data)
        for key in self.sat_test_data:
            if key in ["commission_date", "launch_date"]:
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_data[key], self.sat_test_data[key])

    def test_satellite_post_invalid_iso_datetime(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        self.sat_test_data["commission_date"] = "not iso8601"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["commission_date"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

        self.sat_test_data["commission_date"] = datetime.utcnow().isoformat()
        self.sat_test_data["launch_date"] = "not iso8601"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["launch_date"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

    def test_satellite_post_no_sat_name(self):
        del self.sat_test_data["sat_name"]
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["sat_name"],
                        "msg": "field required",
                        "type": "value_error.missing",
                    }
                ]
            }
        }

    def test_satellite_post_invalid_sat_name(self):
        self.sat_test_data["norad_id"] = 54321
        self.sat_test_data["nssdc_id"] = 54321
        self.sat_test_data["sat_name"] = "a" * 257
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_NAME_TOO_LONG",
            "msg": "sat_name length is limited to 256 characters",
        }

    def test_satellite_post_bad_json(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        self.sat_test_data["tle"] = "not_a_json"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["tle"],
                        "msg": "value is not a valid dict",
                        "type": "type_error.dict",
                    }
                ]
            }
        }

    def test_satellite_post_tle_missing_keywords(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        tle_dict = {
            "tle_line_0": "tle line 0 values",
            "tle_line_1": "tle line 1 values",
            "last_updated": datetime.utcnow().isoformat(),
            "object_type": "test type",
        }

        self.sat_test_data["tle"] = tle_dict
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_TLE_MISSING_FIELD",
            "msg": "tle missing fields required: [norad_cat_id, tle_line_0, tle_line_1, tle_line_2, last_updated, object_type]",
        }

    def test_satellite_post_tle_too_many_columns(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        tle_dict = {
            "norad_cat_id": 25544,
            "tle_line_0": "tle line 0 values",
            "tle_line_1": "tle line 1 values",
            "tle_line_2": "tle line 2 values",
            "last_updated": datetime.utcnow().isoformat(),
            "object_type": "test type",
        }
        for invalid_key in tle_dict.keys():
            invalid_tle_data = {key: ("a" * 121 if key == invalid_key else value) for key, value in tle_dict.items()}
            self.sat_test_data["tle"] = invalid_tle_data
            response = self.app.post(
                "/satellite",
                data=json.dumps(self.sat_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "SATELLITE_TLE_FIELD_TOO_LONG",
                "msg": "tle fields limit to 120 chars",
            }

    def test_satellite_post_invalid_auth(self):
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 401)

    def test_satellite_post_duplicate_norad_id(self):
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 409)

    def test_satellite_post_duplicate_nssdc_id(self):
        self.sat_test_data["norad_id"] = 20580
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 409)

    def test_satellite_post_same_data_new_sat_id(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertFalse(self.sat_id == json.loads(response.data)["sat_id"])

    def test_satellite_post_bad_boolean(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        self.sat_test_data["publicly_visible"] = "not_a_bool"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["publicly_visible"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_satellite_post_existing_sat_id_fails(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.post(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 405)

    def test_satellite_get_single_valid_sat_id(self):
        get_response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        for key in self.sat_test_data:
            if key in ["commission_date", "launch_date"]:
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_data[key], self.sat_test_data[key])

    def test_satellite_get_single_valid_norad_id(self):
        get_response = self.app.get(
            "/satellite/{}".format(self.sat_test_data["norad_id"]),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        for key in self.sat_test_data:
            if key in ["commission_date", "launch_date"]:
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_data[key], self.sat_test_data[key])

    def test_satellite_get_single_manual_tle_returned(self):
        del self.sat_test_data["norad_id"]
        get_response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        self.assertEqual(sat_data["tle"]["tle_line_0"], self.sat_test_data["tle"]["tle_line_0"])

    def test_satellite_get_single_norad_tle_returned(self):
        self.sat_test_data["norad_id"] = 20580
        del self.sat_test_data["nssdc_id"]
        del self.sat_test_data["tle"]
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}".format(self.sat_test_data["norad_id"]),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        self.assertTrue(sat_data.get("tle") is None)

    def test_satellite_get_single_no_tle_returned(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        del self.sat_test_data["tle"]
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        get_response = self.app.get(
            "/satellite/{}".format(sat_id),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        self.assertFalse(sat_data.get("tle"))

    def test_satellite_get_single_no_sat_id(self):
        get_response = self.app.get(
            "/satellite/{}".format("not_a_sat_id"),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 404)

    def test_satellite_get_single_no_hidden_vals(self):
        get_response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            headers=self.headers,
        )
        sat_data = json.loads(get_response.data)
        for key in sat_data:
            self.assertFalse(key[0] == "_")

    def test_satellite_get_single_invalid_auth(self):
        response = self.app.get("/satellite/{}".format(self.sat_id))
        self.assertEqual(response.status_code, 401)

    def test_satellite_get_multiple_valid(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get("/satellite", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 1)
        sat_list = sat_list["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) > 1)
        self.assertTrue(sat_list[0]["sat_id"] != sat_list[1]["sat_id"])
        self.assertTrue(sat_list[0]["norad_id"] != sat_list[1]["nssdc_id"])

    def test_satellite_get_multiple_paginated(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        for i in range(105):
            response = self.app.post(
                "/satellite",
                data=json.dumps(self.sat_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)

        get_response = self.app.get("/satellite", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 1)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 3)
        sat_list = sat_list["sat_results"]
        self.assertEqual(len(sat_list), 50)

        # test whether we can get a page of results
        get_response = self.app.get("/satellite?page=3", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 3)
        sat_list = sat_list["sat_results"]
        self.assertEqual(len(sat_list), 6)

        # test we can never get more than 100 results per page
        get_response = self.app.get("/satellite?page_size=200", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 2)
        sat_list = sat_list["sat_results"]
        self.assertEqual(len(sat_list), 100)

    def test_satellite_get_multiple_can_set_page_size(self):
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        for i in range(14):
            response = self.app.post(
                "/satellite",
                data=json.dumps(self.sat_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)

        get_response = self.app.get("/satellite?page_size=5", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 1)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 3)
        sat_list = sat_list["sat_results"]
        self.assertEqual(len(sat_list), 5)

    def test_satellite_get_multiple_manual_tle_returned(self):
        get_response = self.app.get("/satellite", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)["sat_results"]
        for sat in sat_list:
            self.assertTrue(sat.get("tle"))

    def test_satellite_get_multiple_booleans_succeed(self):
        get_response = self.app.get("/satellite?publicly_visible=True", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)["sat_results"]
        self.assertTrue(len(sat_list) == 0)

    def test_satellite_get_multiple_no_hidden_vals(self):
        get_response = self.app.get("/satellite", headers=self.headers)
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)["sat_results"][0]
        for key in sat_data:
            self.assertFalse(key[0] == "_")

    def test_satellite_get_multiple_bad_boolean(self):
        sat_filter = "?publicly_visible=not_a_bool"
        response = self.app.get("/satellite{}".format(sat_filter), headers=self.headers)
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["publicly_visible"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_satellite_get_multiple_invalid_auth(self):
        response = self.app.get("/satellite")
        self.assertEqual(response.status_code, 401)

    def test_satellite_put_replace_existing_id(self):
        sat_test_data2 = {
            "sat_name": "test_sat_name2",
            "norad_id": 20580,
            "nssdc_id": "test_id2",
            "commission_date": datetime.utcnow().isoformat(),
            "launch_date": datetime.utcnow().isoformat(),
            "description": "test_description2",
            "tle": {
                "norad_cat_id": 20580,
                "last_updated": datetime.utcnow().isoformat(),
                "tle_line_0": "test_tle data line 0",
                "tle_line_1": "test_tle data line 1",
                "tle_line_2": "test_tle data line 2",
                "object_type": "payload",
            },
        }
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(sat_test_data2),
            headers=self.headers,
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"satellite {self.sat_id} updated",
            "sat_id": f"{self.sat_id}",
        }

        get_response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            headers=self.headers,
        )
        sat_data = json.loads(get_response.data)
        for key in self.sat_test_data:
            if key in ["commission_date", "launch_date"]:
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(sat_test_data2[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_data[key], sat_test_data2[key])

    def test_satellite_put_use_existing_norad_id(self):
        sat_test_data2 = {
            "sat_name": "test_sat_name2",
            "norad_id": 1,
            "commission_date": datetime.utcnow().isoformat(),
            "description": "test_description2",
        }
        response = self.app.put(
            "/satellite/s_new_sat",
            data=json.dumps(sat_test_data2),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 409
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_NORAD_ID_EXISTS",
            "msg": "Satellite with that norad id exists",
        }

    def test_satellite_put_use_existing_nssdc_id_with_id(self):
        sat_test_data2 = {
            "sat_name": "test_sat_name2",
            "norad_id": 2,
            "nssdc_id": "test_id",
            "commission_date": datetime.utcnow().isoformat(),
            "description": "test_description2",
        }
        response = self.app.put(
            "/satellite/s_new_sat",
            data=json.dumps(sat_test_data2),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 409
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_NSSDC_ID_EXISTS",
            "msg": "Satellite with that nssdc id exists",
        }

    def test_satellite_put_null_blank_fields(self):
        sat_data_key_list = [
            "norad_id",
            "nssdc_id",
            "commission_date",
            "launch_date",
            "tle",
        ]
        sat_test_data = {"sat_name": "new_sat_name", "description": "test_description"}

        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"satellite {self.sat_id} updated",
            "sat_id": f"{self.sat_id}",
        }

        get_response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            headers=self.headers,
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        for key in sat_data_key_list:
            self.assertEqual(sat_data[key], None)

    def test_satellite_put_create_new_id(self):
        defined_sat_id = "s_new_sat_id"
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        self.sat_test_data["norad_id"] = 1234
        response = self.app.put(
            "/satellite/" + defined_sat_id,
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/" + defined_sat_id,
            headers=self.headers,
        )
        sat_data = json.loads(get_response.data)
        for key in self.sat_test_data:
            if key in ["commission_date", "launch_date"]:
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_data[key], self.sat_test_data[key])

    def test_satellite_put_bad_boolean(self):
        defined_sat_id = "s_new_sat_id"
        self.sat_test_data["publicly_visible"] = "not_a_bool"
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.put(
            "/satellite/" + defined_sat_id,
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["publicly_visible"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_satellite_put_bad_json(self):
        defined_sat_id = "s_new_sat_id"
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        self.sat_test_data["tle"] = "not_a_json"
        response = self.app.put(
            "/satellite/" + defined_sat_id,
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["tle"],
                        "msg": "value is not a valid dict",
                        "type": "type_error.dict",
                    }
                ]
            }
        }

    def test_satellite_put_tle_missing_keywords(self):
        defined_sat_id = "s_new_sat_id"
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        tle_dict = {
            "norad_cat_id": 25544,
            "tle_line_0": "tle line 0 values",
            "tle_line_1": "tle line 1 values",
            "tle_line_2": "tle line 2 values",
            "last_updated": datetime.utcnow().isoformat(),
            "object_type": "test type",
        }
        for invalid_key in tle_dict.keys():
            invalid_tle_data = {key: value for key, value in tle_dict.items() if key != invalid_key}
            self.sat_test_data["tle"] = invalid_tle_data
            response = self.app.put(
                "/satellite/" + defined_sat_id,
                data=json.dumps(self.sat_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "SATELLITE_TLE_MISSING_FIELD",
                "msg": "tle missing fields required: [norad_cat_id, tle_line_0, tle_line_1, tle_line_2, last_updated, object_type]",
            }

    def test_satellite_put_tle_too_many_columns(self):
        defined_sat_id = "s_new_sat_id"
        del self.sat_test_data["norad_id"]
        del self.sat_test_data["nssdc_id"]
        tle_dict = {
            "norad_cat_id": 25544,
            "tle_line_0": "tle line 0 values",
            "tle_line_1": "tle line 1 values",
            "tle_line_2": "tle line 2 values",
            "last_updated": datetime.utcnow().isoformat(),
            "object_type": "test type",
        }
        for invalid_key in tle_dict.keys():
            invalid_tle_data = {key: ("a" * 121 if key == invalid_key else value) for key, value in tle_dict.items()}
            self.sat_test_data["tle"] = invalid_tle_data
            response = self.app.put(
                "/satellite/" + defined_sat_id,
                data=json.dumps(self.sat_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "SATELLITE_TLE_FIELD_TOO_LONG",
                "msg": "tle fields limit to 120 chars",
            }

    def test_satellite_put_bad_sat_id(self):
        self.sat_test_data["norad_id"] = 20580
        self.sat_test_data["nssdc_id"] = "nssdc_id2"
        response = self.app.put(
            "/satellite/" + "".join(["a" for i in range(25)]),
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertTrue("too long," in json.loads(response.data)["msg"])
        response = self.app.put(
            "/satellite/" + "π",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertTrue("must use only" in json.loads(response.data)["msg"])
        response = self.app.put(
            "/satellite/" + "12345bad_id",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_ID_PREFIX_INCORRECT",
            "msg": "sat_id must begin with 's'",
        }

    def test_satellite_put_invalid_sat_name(self):
        self.sat_test_data["norad_id"] = 54321
        self.sat_test_data["nssdc_id"] = 54321
        self.sat_test_data["sat_name"] = "a" * 257
        response = self.app.put(
            "/satellite/12345",
            data=json.dumps(self.sat_test_data),
            headers=self.headers,
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_NAME_TOO_LONG",
            "msg": "sat_name length is limited to 256 characters",
        }

    def test_satellite_put_bad_datetime(self):
        count = 0
        for date_key in ["launch_date", "commission_date"]:
            new_test_data = {
                "sat_name": str(count),
                "description": "test_description",
                date_key: "not_a_date",
            }
            count += 1
            response = self.app.put(
                "/satellite/{}".format(self.sat_id + str(count)),
                data=json.dumps(new_test_data),
                headers=self.headers,
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "validation_error": {
                    "body_params": [
                        {
                            "loc": [date_key],
                            "msg": "invalid datetime format",
                            "type": "value_error.datetime",
                        }
                    ]
                }
            }

    def test_satellite_put_invalid_auth(self):
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
        )
        self.assertEqual(response.status_code, 401)
