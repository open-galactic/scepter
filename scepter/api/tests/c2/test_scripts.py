from api import app
from api.tests.fixtures.c2 import headers, sat_id
from models import Script


def test_get(session, script_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/{script_template.name}", headers=headers
        )
    assert response.status_code == 200

    data: dict = response.json
    assert data["name"] == script_template.name
    assert data["args"] == script_template.args
    assert data["entrypoint"] == script_template.entrypoint


def test_get_using_unknown_id(session, script_template):
    with app.test_client() as client:
        response = client.get(f"/satellite/{sat_id}/scripts/unknown", headers=headers)
    assert response.status_code == 404


def test_create_with_put(session, satellite):
    id = "new_put_script"
    data = {
        "name": "new_script_name",
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{id}",
            headers=headers,
            json=data,
        )
    assert response.status_code == 201

    script = response.json["script"]
    db_script = Script.get_by_id(id, satellite)
    # Check repsonse matches db data
    assert script["id"] == db_script.id
    assert script["name"] == db_script.name
    assert script["args"] == db_script.args
    assert script["entrypoint"] == db_script.entrypoint

    # Check db matches input values
    assert id == db_script.id
    assert data["args"] == db_script.args
    assert data["entrypoint"] == db_script.entrypoint

    session.query(Script).delete()
    session.commit()


def test_update_with_put(session, satellite, script_template):
    id = script_template.id
    data = {
        "name": "new name",
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
                "new_put_argument2": {"type": "string"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/new_entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{id}",
            headers=headers,
            json=data,
        )
    assert response.status_code == 201

    script = response.json["script"]
    # gross hack to close the fixture session, and reload the new data from the
    # request. TODO fix this once injection is working
    session.commit()

    # Check repsonse matches db data
    assert script["name"] == script_template.name
    assert script["args"] == script_template.args
    assert script["entrypoint"] == script_template.entrypoint

    # Check db matches input values
    assert script_template.id == id
    assert data["name"] == script_template.name
    assert data["args"] == script_template.args
    assert data["entrypoint"] == script_template.entrypoint


def test_create_with_unknown_param(session, satellite):
    data = {
        "name": "example name",
        "unknown_param": 1,
        "args": {
            "type": "object",
            "properties": {
                "new_put_argument": {"type": "integer"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/new_id",
            headers=headers,
            json=data,
        )
    assert response.status_code == 400


def test_create_with_invalid_jschema(session, satellite):
    data = {
        "name": "example name",
        "args": {
            "type": "not an object",
            "properties": {
                "new_put_argument": {"type": "unknown datatype"},
            },
            "additionalProperties": False,
        },
        "entrypoint": "/example/entrypoint",
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/new_id",
            headers=headers,
            json=data,
        )
    assert response.status_code == 400
