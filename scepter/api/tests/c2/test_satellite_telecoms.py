import json
import unittest

import pytest

from api import app
from api.database import session
from models import Asset, Permission, Satellite, SatTelecom


@pytest.mark.usefixtures("patch_auth")
class TestSatelliteTelecoms(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()

    def setUp(self):
        self.sat_test_data = {
            "sat_name": "test_sat_name",
            "description": "test_description",
        }
        sat_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            content_type="application/json",
        )
        self.sat_id = json.loads(sat_response.data)["sat_id"]

        self.sat_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": [9600]},
            "transmits": True,
            "receives": True,
            "encrypted": False,
        }
        sat_telecom_response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        assert sat_telecom_response.status_code == 201
        self.telecom_name = json.loads(sat_telecom_response.data)["telecom_name"]

    def tearDown(self):
        session.query(SatTelecom).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_sat_telecom_post_valid(self):
        self.sat_telecom_test_data["telecom_name"] = "test_telecom_name2"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        telecom_name = json.loads(response.data)["telecom_name"]
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, telecom_name),
        )

        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == self.sat_telecom_test_data["telecom_name"]
        assert sat_data["telecom_type"] == self.sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == self.sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == self.sat_telecom_test_data["modulations"]
        assert sat_data["receives"] == self.sat_telecom_test_data["receives"]
        assert sat_data["encrypted"] == self.sat_telecom_test_data["encrypted"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_post_multiple_modulations(self):

        sat_telecom_test_data = {
            "telecom_name": "test_telecom_name2",
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": [9600], "1024-QAM": [9600, 115000]},
            "transmits": True,
            "receives": True,
            "encrypted": True,
        }

        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        telecom_name = json.loads(response.data)["telecom_name"]
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, telecom_name),
        )

        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == sat_telecom_test_data["telecom_name"]
        assert sat_data["telecom_type"] == sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == sat_telecom_test_data["modulations"]
        assert sat_data["receives"] == sat_telecom_test_data["receives"]
        assert sat_data["encrypted"] == sat_telecom_test_data["encrypted"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_post_bad_modulation_type(self):
        self.sat_telecom_test_data["modulations"] = json.dumps(
            {"BPSK": ["9600", "4800"], "not_a_modulation": ["200"]}
        )
        self.sat_telecom_test_data["telecom_name"] = "test_telecom_2"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["modulations"],
                        "msg": "value is not a valid dict",
                        "type": "type_error.dict",
                    }
                ]
            }
        }

    def test_sat_telecom_post_existing_telecom_name_fails(self):
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 409
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SATELLITE_TELECOM_ALREADY_EXISTS",
            "msg": "Telecom name already exists for satellite",
        }

    def test_sat_telecom_post_to_existing_telecom(self):
        response = self.app.post(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 405)

    def test_sat_telecom_get_single_valid(self):
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
        )
        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == self.sat_telecom_test_data["telecom_name"]
        assert sat_data["telecom_type"] == self.sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == self.sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == self.sat_telecom_test_data["modulations"]
        assert sat_data["receives"] == self.sat_telecom_test_data["receives"]
        assert sat_data["encrypted"] == self.sat_telecom_test_data["encrypted"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_get_single_no_telecom_name(self):
        get_response = self.app.get(
            "/satellite/{}/telecom/not_a_telecom".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 404)

    def test_sat_telecom_get_single_no_hidden_vals(self):
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
        )
        sat_data = json.loads(get_response.data)
        for key in sat_data:
            self.assertFalse(key[0] == "_")

    def test_sat_telecom_get_multiple_valid(self):
        self.sat_telecom_test_data["telecom_type"] = "test_telecom_type2"
        self.sat_telecom_test_data["telecom_name"] = "test_telecom_name2"
        self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        get_response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)
        self.assertIn("page", telecom_list)
        self.assertEqual(telecom_list["page"], 1)
        telecom_list = telecom_list["telecom_results"]
        self.assertTrue(isinstance(telecom_list, list))
        self.assertTrue(len(telecom_list) > 1)
        self.assertTrue(
            telecom_list[0]["telecom_type"] != telecom_list[1]["telecom_type"]
        )
        self.assertTrue(
            telecom_list[0]["telecom_name"] != telecom_list[1]["telecom_name"]
        )
        self.assertEqual(telecom_list[0]["sat_id"], self.sat_id)

    def test_sat_telecom_get_multiple_paginated(self):
        telecom_name = self.sat_telecom_test_data["telecom_name"]
        for i in range(105):
            self.sat_telecom_test_data["telecom_name"] = telecom_name + str(i)
            response = self.app.post(
                "/satellite/{}/telecom".format(self.sat_id),
                data=json.dumps(self.sat_telecom_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 1)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 3)
        sat_list = sat_list["telecom_results"]
        self.assertEqual(len(sat_list), 50)

        # test we can get a new page
        get_response = self.app.get(
            "/satellite/{}/telecom?page=3".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 3)
        sat_list = sat_list["telecom_results"]
        self.assertEqual(len(sat_list), 6)

        # test we can't get more than 100 results per page
        get_response = self.app.get(
            "/satellite/{}/telecom?page_size=200".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 2)
        sat_list = sat_list["telecom_results"]
        self.assertEqual(len(sat_list), 100)

    def test_sat_telecom_get_multiple_can_set_page_size(self):
        telecom_name = self.sat_telecom_test_data["telecom_name"]
        for i in range(14):
            self.sat_telecom_test_data["telecom_name"] = telecom_name + str(i)
            response = self.app.post(
                "/satellite/{}/telecom".format(self.sat_id),
                data=json.dumps(self.sat_telecom_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom?page_size=5".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_list = json.loads(get_response.data)
        self.assertIn("page", sat_list)
        self.assertEqual(sat_list["page"], 1)
        self.assertIn("total_pages", sat_list)
        self.assertEqual(sat_list["total_pages"], 3)
        sat_list = sat_list["telecom_results"]
        self.assertEqual(len(sat_list), 5)

    def test_sat_telecom_get_multiple_filter_by_type(self):
        self.sat_telecom_test_data["telecom_name"] = "test_telecom_name2"
        self.sat_telecom_test_data["telecom_type"] = "test_telecom_type2"
        self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            content_type="application/json",
        )
        sat_name_filter = "?telecom_type=type2"
        get_response = self.app.get(
            "/satellite/{}/telecom{}".format(self.sat_id, sat_name_filter),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)["telecom_results"]
        self.assertTrue(len(telecom_list) == 1)
        self.assertTrue(
            telecom_list[0]["telecom_type"]
            == self.sat_telecom_test_data["telecom_type"]
        )

    def test_sat_telecom_get_multiple_no_search_results(self):
        telecom_filter = "?telecom_type=not_a_type"
        get_response = self.app.get(
            "/satellite/{}/telecom{}".format(self.sat_id, telecom_filter),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)["telecom_results"]
        self.assertTrue(len(telecom_list) == 0)

    def test_sat_telecom_get_multiple_no_hidden_vals(self):
        get_response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
        )
        sat_list = json.loads(get_response.data)["telecom_results"]
        sat_data = sat_list[0]
        for key in sat_data:
            self.assertFalse(key[0] == "_")

    def test_sat_telecom_put_replace_existing(self):
        telecom_test_data2 = {
            "telecom_type": "test_type2",
            "frequency": 20000,
            "transmits": True,
            "receives": False,
            "encrypted": True,
        }
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(telecom_test_data2),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
        )
        sat_data = json.loads(get_response.data)
        for key in telecom_test_data2:
            self.assertEqual(sat_data[key], telecom_test_data2[key])
        self.assertEqual(sat_data["sat_id"], self.sat_id)

    def test_sat_telecom_put_create_new(self):
        defined_telecom_name = "666666"

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": [9600]},
            "transmits": True,
            "receives": True,
            "encrypted": True,
        }
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
        )

        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == defined_telecom_name
        assert sat_data["telecom_type"] == sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == sat_telecom_test_data["modulations"]
        assert sat_data["receives"] == sat_telecom_test_data["receives"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_put_create_new_string_baud(self):
        defined_telecom_name = "666666"

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": ["9600"]},
            "transmits": True,
            "receives": True,
        }
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
        )

        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == defined_telecom_name
        assert sat_data["telecom_type"] == sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == {"FM": [9600]}
        assert sat_data["receives"] == sat_telecom_test_data["receives"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_put_create_new_multiple_bauds_modulations(self):
        defined_telecom_name = "666666"

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": [9600], "1024-QAM": [9600, 115000]},
            "transmits": True,
            "receives": True,
        }
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, defined_telecom_name),
        )

        assert get_response.status_code == 200

        sat_data = get_response.json

        assert sat_data["telecom_name"] == defined_telecom_name
        assert sat_data["telecom_type"] == sat_telecom_test_data["telecom_type"]
        assert sat_data["frequency"] == sat_telecom_test_data["frequency"]
        assert sat_data["modulations"] == sat_telecom_test_data["modulations"]
        assert sat_data["receives"] == sat_telecom_test_data["receives"]
        assert sat_data["sat_id"] == self.sat_id

    def test_sat_telecom_put_bad_sat_id(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"FM": [9600]},
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/satellite/{}/telecom/{}".format(
                self.sat_id, "".join(["a" for i in range(256)])
            ),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "TELECOM_NAME_TOO_LONG",
            "msg": "Telecom name too long, limit to 255 chars",
        }

        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, "π"),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "TELECOM_ENCODING_INVALID",
            "msg": "Telecom name must use only the ascii character set",
        }

    def test_sat_telecom_put_bad_modulation_type(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 21000000,
            "modulations": {"BPSK": [9600, 4800], "not_a_modulation": [200]},
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(sat_telecom_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "TELECOM_MODULATION_INVALID",
            "msg": "Invalid modulation type.",
        }
