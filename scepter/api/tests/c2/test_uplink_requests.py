from typing import Dict
from uuid import uuid4

import pytest
from mock import patch  # type: ignore

from api import app
from api.exceptions import ReasonCode
from api.tests.fixtures.c2 import headers, no_auth_headers, sat_id
from models import UplinkRequest


@pytest.fixture
def pending_uplink_request(session, upcoming_contact: Dict[str, Dict]) -> str:
    uplink_request = UplinkRequest(upcoming_contact.instructions[0], {"command_name": "pending command"})
    session.add(uplink_request)
    session.commit()
    yield uplink_request.id
    session.delete(uplink_request)
    session.commit()


@pytest.fixture
def approved_uplink_request(session, upcoming_contact: Dict[str, Dict]) -> str:
    uplink_request = UplinkRequest(upcoming_contact.instructions[0], {"command_name": "approved_command"})
    session.add(uplink_request)
    uplink_request.approve("user")
    session.commit()
    yield uplink_request.id
    session.delete(uplink_request)
    session.commit()


@pytest.fixture
def denied_uplink_request(session, upcoming_contact: Dict[str, Dict]) -> str:
    uplink_request = UplinkRequest(upcoming_contact.instructions[0], {"command_name": "approved_command"})
    session.add(uplink_request)
    uplink_request.deny("user")
    session.commit()
    yield uplink_request.id
    session.delete(uplink_request)
    session.commit()


def test_creating_uplink_requests(session, upcoming_contact):
    uplink_request = {
        "contact_id": upcoming_contact.id,
        "command_id": upcoming_contact.instructions[0].id,
        "context": {
            "command_name": "a command",
            "script_line_number": 10,
        },
    }
    with app.test_client() as client:
        response = client.post("/uplinkrequests", headers=headers, json=uplink_request)
    assert response.status_code == 201
    data: dict = response.json
    uplink_request = data["uplink_request"]

    db_entry = UplinkRequest.get_by_id(session, uplink_request["id"])
    assert uplink_request["id"] == str(db_entry.id)
    assert uplink_request["instruction_id"] == db_entry.instruction_id_repr
    assert uplink_request["context"] == db_entry.context
    assert uplink_request["created_at"] == db_entry.created_at.isoformat()

    assert db_entry.approved is None
    assert db_entry.reviewed_at is None
    assert db_entry.reviewed_by is None
    session.delete(db_entry)
    session.commit()


def test_create_with_unknown_contact(upcoming_contact):
    uplink_request = {
        "contact_id": uuid4(),
        "command_id": upcoming_contact.instructions[0].id,
        "context": {
            "command_name": "a command",
            "script_line_number": 10,
        },
    }
    with app.test_client() as client:
        response = client.post("/uplinkrequests", headers=headers, json=uplink_request)
    assert response.status_code == 404


def test_create_with_unknown_command(upcoming_contact):
    uplink_request = {
        "contact_id": upcoming_contact.id,
        "command_id": 10,
        "context": {
            "command_name": "a command",
            "script_line_number": 10,
        },
    }
    with app.test_client() as client:
        response = client.post("/uplinkrequests", headers=headers, json=uplink_request)
    assert response.status_code == 404


def test_create_without_context(upcoming_contact):
    uplink_request = {
        "contact_id": upcoming_contact.id,
        "command_id": upcoming_contact.instructions[0].id,
    }
    with app.test_client() as client:
        response = client.post("/uplinkrequests", headers=headers, json=uplink_request)
    assert response.status_code == 400


def test_list_uplink_requests(pending_uplink_request, approved_uplink_request, denied_uplink_request):
    with app.test_client() as client:
        response = client.get("/uplinkrequests", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_requests = data["uplink_requests"]
    assert len(uplink_requests) == 3

    denied = uplink_requests[0]
    assert denied["id"] == str(denied_uplink_request)
    assert denied["approved"] is False
    assert denied["reviewed_at"]
    assert denied["reviewed_by"]
    assert denied["context"]
    assert denied["contact_id"]
    assert denied["instruction_id"] is not None
    assert denied["sat_id"] == sat_id

    approved = uplink_requests[1]
    assert approved["id"] == str(approved_uplink_request)
    assert approved["approved"] is True
    assert approved["reviewed_at"]
    assert approved["reviewed_by"]
    assert approved["context"]
    assert approved["contact_id"]
    assert approved["instruction_id"] is not None
    assert approved["sat_id"] == sat_id

    pending = uplink_requests[2]
    assert pending["id"] == str(pending_uplink_request)
    assert pending["approved"] is None
    assert not pending["reviewed_at"]
    assert not pending["reviewed_by"]
    assert pending["context"]
    assert pending["contact_id"]
    assert pending["instruction_id"] is not None
    assert pending["sat_id"] == sat_id


def test_list_pending_uplink_requests(pending_uplink_request, approved_uplink_request):
    with app.test_client() as client:
        response = client.get("/uplinkrequests?reviewed=false", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_requests = data["uplink_requests"]
    assert len(uplink_requests) == 1
    assert not uplink_requests[0]["reviewed_at"]


def test_list_approved_uplink_requests(pending_uplink_request, approved_uplink_request):
    with app.test_client() as client:
        response = client.get("/uplinkrequests?approved=true", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_requests = data["uplink_requests"]
    assert len(uplink_requests) == 1
    assert uplink_requests[0]["reviewed_at"]


def test_list_no_uplink_requests(pending_uplink_request, approved_uplink_request):
    with app.test_client() as client:
        response = client.get("/uplinkrequests?approved=true&reviewed=false", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_requests = data["uplink_requests"]
    assert len(uplink_requests) == 0


def test_list_without_auth(pending_uplink_request):

    with app.test_client() as client:
        response = client.get("/uplinkrequests", headers=no_auth_headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_requests = data["uplink_requests"]
    assert len(uplink_requests) == 0


def test_list_with_sat_id(pending_uplink_request):
    with app.test_client() as client:
        response = client.get(f"/uplinkrequests?sat_id={sat_id}", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    assert len(data["uplink_requests"]) == 1


@patch("api.endpoints.uplinkrequest._authorize_satellite_list")
def test_list_with_multiple_sat_ids(mock_method, pending_uplink_request):
    # ensure multiple sat ids are retrieved, and passed through into the auth function
    new_id = "s4444"
    mock_method.return_value = [], [f"{new_id}"]
    with app.test_client() as client:
        client.get(f"/uplinkrequests?sat_id={sat_id}&sat_id={new_id}", headers=headers)
    mock_method.assert_called_once()
    assert mock_method.call_args.args[1] == [f"{sat_id}", f"{new_id}"]


def test_list_with_unknown_sat(pending_uplink_request):
    unknown_sat = "s333"
    with app.test_client() as client:
        response = client.get(f"/uplinkrequests?sat_id={unknown_sat}", headers=headers)
    assert response.status_code == 404
    data: dict = response.json
    assert data["code"] == ReasonCode.SATELLITE_NOT_FOUND.name
    assert data["unknown_sat_ids"] == [unknown_sat]


def test_get_uplink_request_by_id(pending_uplink_request, upcoming_contact):
    with app.test_client() as client:
        response = client.get(f"/uplinkrequests/{pending_uplink_request}", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    pending = data["uplink_request"]

    assert pending["id"] == str(pending_uplink_request)
    assert pending["approved"] is None
    assert not pending["reviewed_at"]
    assert not pending["reviewed_by"]
    assert pending["context"]
    assert pending["sat_id"] == sat_id
    assert pending["contact_id"] == str(upcoming_contact.id)
    assert pending["instruction_id"] == upcoming_contact.instructions[0].id


def test_get_unknown_uplink_request(pending_uplink_request):
    with app.test_client() as client:
        response = client.get(f"/uplinkrequests/{uuid4()}", headers=headers)
    assert response.status_code == 404


def test_get_without_auth(pending_uplink_request):

    with app.test_client() as client:
        response = client.get(f"/uplinkrequests/{pending_uplink_request}", headers=no_auth_headers)
    assert response.status_code == 404


def test_approve_uplink_request(pending_uplink_request):
    with app.test_client() as client:
        response = client.post(f"/uplinkrequests/{pending_uplink_request}/approve", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_request = data["uplink_request"]

    assert uplink_request["id"] == str(pending_uplink_request)
    assert uplink_request["approved"] is True
    assert uplink_request["reviewed_at"]
    assert uplink_request["reviewed_by"]
    assert uplink_request["context"]
    assert uplink_request["sat_id"] == sat_id


def test_approve_without_auth(pending_uplink_request):

    with app.test_client() as client:
        response = client.post(f"/uplinkrequests/{pending_uplink_request}/approve", headers=no_auth_headers)
    assert response.status_code == 403


def test_deny_uplink_request(pending_uplink_request):
    with app.test_client() as client:
        response = client.post(f"/uplinkrequests/{pending_uplink_request}/deny", headers=headers)
    assert response.status_code == 200
    data: dict = response.json
    uplink_request = data["uplink_request"]

    assert uplink_request["id"] == str(pending_uplink_request)
    assert uplink_request["approved"] is False
    assert uplink_request["reviewed_at"]
    assert uplink_request["reviewed_by"]
    assert uplink_request["context"]
    assert uplink_request["sat_id"] == sat_id


def test_deny_without_auth(pending_uplink_request):
    with app.test_client() as client:
        response = client.post(f"/uplinkrequests/{pending_uplink_request}/deny", headers=no_auth_headers)
    assert response.status_code == 403
