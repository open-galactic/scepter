from datetime import timedelta

from api import app
from api.tests.fixtures.c2 import headers
from models import Alert, Satellite


def test_alert_get_valid_no_filter(alert: Alert):
    with app.test_client() as client:
        response = client.get(
            "/alerts",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    alerts_data = data["alerts"]
    for alert_data in alerts_data:
        assert len(alert_data.items()) == 6
        assert alert_data["id"] == str(alert.id)
        assert alert_data["metadata_id"] == str(alert.metadata_id)
        assert alert_data["faulted_at"] == alert.faulted_at.isoformat()
        assert alert_data["created_at"] == alert.created_at.isoformat()
        assert alert_data["error_code"] == alert.error_code
        assert alert_data["value"] == str(alert.value)


def test_alert_get_valid_filter_by_satellite(alert: Alert, satellite: Satellite):
    with app.test_client() as client:
        response = client.get(
            f"/alerts?sat_ids={satellite._asset_hash}",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    alerts_data = data["alerts"]
    assert len(alerts_data) == 1
    assert alerts_data[0]["id"] == str(alert.id)
    assert alerts_data[0]["metadata_id"] == str(alert.metadata_id)
    assert alerts_data[0]["faulted_at"] == alert.faulted_at.isoformat()
    assert alerts_data[0]["created_at"] == alert.created_at.isoformat()
    assert alerts_data[0]["error_code"] == alert.error_code
    assert alerts_data[0]["value"] == str(alert.value)


def test_alert_get_invalid_filter_by_satellite(alert: Alert):
    with app.test_client() as client:
        response = client.get(
            "/alerts?sat_ids=not_exist",
            headers=headers,
        )
    assert response.status_code == 404
    assert response.json["code"] == "SATELLITE_NOT_FOUND"


def test_alert_get_valid_filter_by_error_code(alert: Alert):
    with app.test_client() as client:
        response = client.get(
            f"/alerts?error_code={alert.error_code}",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    alerts_data = data["alerts"]
    assert len(alerts_data) == 1
    assert alerts_data[0]["id"] == str(alert.id)
    assert alerts_data[0]["metadata_id"] == str(alert.metadata_id)
    assert alerts_data[0]["faulted_at"] == alert.faulted_at.isoformat()
    assert alerts_data[0]["created_at"] == alert.created_at.isoformat()
    assert alerts_data[0]["error_code"] == alert.error_code
    assert alerts_data[0]["value"] == str(alert.value)


def test_alert_get_valid_faulted_range(alert: Alert):
    faulted_time_start = (alert.faulted_at - timedelta(hours=2)).isoformat()
    faulted_time_end = (alert.faulted_at + timedelta(hours=1)).isoformat()
    with app.test_client() as client:
        response = client.get(
            f"/alerts?faulted_time_start={faulted_time_start}&faulted_time_end={faulted_time_end}",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    alerts_data = data["alerts"]
    assert len(alerts_data) == 1
    assert alerts_data[0]["id"] == str(alert.id)
    assert alerts_data[0]["metadata_id"] == str(alert.metadata_id)
    assert alerts_data[0]["faulted_at"] == alert.faulted_at.isoformat()
    assert alerts_data[0]["created_at"] == alert.created_at.isoformat()
    assert alerts_data[0]["error_code"] == alert.error_code
    assert alerts_data[0]["value"] == str(alert.value)


def test_alert_get_faulted_start_after_end_time(alert: Alert):
    faulted_time_start = (alert.faulted_at + timedelta(hours=1)).isoformat()
    faulted_time_end = (alert.faulted_at - timedelta(hours=2)).isoformat()
    with app.test_client() as client:
        response = client.get(
            f"/alerts?faulted_time_start={faulted_time_start}&faulted_time_end={faulted_time_end}",
            headers=headers,
        )
    assert response.status_code == 400
    data = response.json
    assert data["code"] == "END_TIME_BEFORE_START_TIME"


def test_alert_get_invalid_faulted_time_format(alert: Alert):
    faulted_time_start = "INVALID"
    with app.test_client() as client:
        response = client.get(
            f"/alerts?faulted_time_start={faulted_time_start}",
            headers=headers,
        )
    assert response.status_code == 400
    data = response.json
    assert "validation_error" in data
