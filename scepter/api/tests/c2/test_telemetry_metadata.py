import json
import os
import unittest
from uuid import UUID, uuid4

from api import app
from api.database import session
from api.tests.fixtures.c2 import headers
from models import Asset, Permission, Satellite, Subsystem, TelemetryMetadata

ABS_PATH = os.path.abspath(os.path.dirname(__file__))


class TestTelemetryMetadata(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        user_id = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")
        cls.headers = headers

        cls.sat_norad_id = 25544
        cls.sat_id = "s3333"
        sat = Satellite(
            asset_hash=cls.sat_id,
            sat_name="test_sat_name",
            description="test_description",
            norad_id=cls.sat_norad_id,
        )
        session.add(sat)
        session.flush()
        permission = Permission(
            asset_id=sat._id,
            user_uuid=user_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
        session.add(permission)
        cls.sat_numeric_id = sat._id

        # Add systems and components to satellite
        system = Subsystem(sat, "power", "system", "1")
        session.add(system)
        session.flush()
        cls.power_system_id = system.id
        cls.power_system_name = system.name
        component = Subsystem(sat, "power_component", "component", "1", parent=system)
        session.add(component)
        session.flush()
        cls.power_component_id = component.id
        cls.power_component_name = component.name
        system = Subsystem(sat, "thermal", "system", "2")
        session.add(system)
        session.flush()
        cls.thermal_system_id = system.id
        cls.thermal_system_name = system.name
        session.commit()

    def setUp(self):
        tlm = TelemetryMetadata(
            _sat_id=self.sat_numeric_id,
            tlm_name="VBatt",
            description="test desc",
            _subsystem_id=self.power_component_id,
            units="volts",
            cad_id="BATTERY",
            uncertainty=0.1,
            nominal_min=1.0,
            nominal_max=3.0,
            alert_min=0.0,
            alert_max=4.0,
            uuid=str(uuid4()),
        )
        session.add(tlm)
        session.flush()
        self.vbatt_id = tlm._id
        self.vbatt_name = tlm.tlm_name
        self.vbatt_uuid = tlm.uuid

        tlm = TelemetryMetadata(
            _sat_id=self.sat_numeric_id,
            tlm_name="SolarTemp",
            description="test desc2",
            _subsystem_id=self.thermal_system_id,
            units="deg c",
            cad_id="solar_panel",
            uncertainty=2.0,
            nominal_min=10,
            nominal_max=40,
            alert_min=0,
            alert_max=60,
            uuid=str(uuid4()),
        )
        session.add(tlm)
        session.flush()
        self.solar_temp_id = tlm._id
        self.solar_temp_name = tlm.tlm_name
        session.commit()

        tlm = TelemetryMetadata(
            _sat_id=self.sat_numeric_id,
            tlm_name="EmptyCadID",
            description="test desc2",
            _subsystem_id=self.thermal_system_id,
            units="deg c",
            uncertainty=2.0,
            nominal_min=10,
            nominal_max=40,
            alert_min=0,
            alert_max=60,
            uuid=str(uuid4()),
        )
        session.add(tlm)
        session.flush()
        self.empty_cad_id = tlm._id
        self.empty_cad_name = tlm.tlm_name
        session.commit()

    def tearDown(self):
        session.query(TelemetryMetadata).delete()
        session.commit()

    @classmethod
    def tearDownClass(cls):
        session.query(Subsystem).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_telem_meta_get_matches_db(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.vbatt_name),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(len(data["telem_results"]), 1)
        telem_data = data["telem_results"][0]
        db_data = TelemetryMetadata.get_by_id(session, self.vbatt_id)

        self.assertEqual(telem_data["tlm_name"], db_data.tlm_name)
        self.assertEqual(telem_data["uuid"], str(db_data.uuid))
        self.assertEqual(telem_data["units"], db_data.units)
        self.assertEqual(telem_data["cad_id"], "BATTERY")
        self.assertEqual(telem_data["nominal_min"], db_data.nominal_min)
        self.assertEqual(telem_data["nominal_max"], db_data.nominal_max)
        self.assertEqual(telem_data["alert_min"], db_data.alert_min)
        self.assertEqual(telem_data["alert_max"], db_data.alert_max)
        self.assertEqual(telem_data["description"], db_data.description)
        self.assertEqual(telem_data["uncertainty"], db_data.uncertainty)
        self.assertEqual(telem_data["system"], self.power_system_name)
        self.assertEqual(telem_data["component"], self.power_component_name)

    def test_telem_meta_get_valid_multiple(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata".format(self.sat_id),
            headers=self.headers,
        )
        data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data["telem_results"]), 3)

    def test_telem_meta_get_valid_search_by_system(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?system={}".format(self.sat_id, self.power_system_name),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data)
        self.assertEqual(len(data["telem_results"]), 1)

    def test_telem_meta_get_valid_search_by_component(self):
        # add telemetry item to power system, to ensure it's not returned by search
        tlm = TelemetryMetadata(
            _sat_id=self.sat_numeric_id,
            tlm_name="BATT_AMPS",
            description="test desc",
            _subsystem_id=self.power_system_id,
            units="amps",
            cad_id="BATTERY",
            uncertainty=0.1,
            nominal_min=1.0,
            nominal_max=3.0,
            alert_min=0.0,
            alert_max=4.0,
            uuid=str(uuid4()),
        )
        session.add(tlm)
        session.commit()

        response = self.app.get(
            "/satellite/{}/telemetry_metadata?system={}&component={}".format(
                self.sat_id, self.power_system_name, self.power_component_name
            ),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data)
        self.assertEqual(len(data["telem_results"]), 1)
        self.assertEqual(data["telem_results"][0]["component"], self.power_component_name)

    def test_telem_meta_get_valid_search_by_uuid(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?uuid={self.vbatt_uuid}",
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(len(data["telem_results"]), 1)

    def test_telem_meta_get_invalid_search(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?test=ADC".format(self.sat_id),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 400)

    def test_telem_meta_get_invalid_search_by_invalid_uuid(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?uuid=invalid".format(self.sat_id),
            headers=self.headers,
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["uuid"],
                        "msg": "value is not a valid uuid",
                        "type": "value_error",
                    }
                ]
            }
        }

    def test_telem_meta_get_component_no_system(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?component=battery".format(self.sat_id),
            headers=self.headers,
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["component"],
                        "msg": "Component nominated without system key",
                        "type": "value_error",
                    }
                ]
            }
        }

    def test_telem_meta_get_no_system(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?system=abcd",
            headers=self.headers,
        )
        assert response.status_code == 404
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "SYSTEM_NOT_FOUND",
            "msg": "System does not exist for satellite",
        }

    def test_telem_meta_get_system_no_component(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?system={self.power_system_name}&component=abcd",
            headers=self.headers,
        )
        assert response.status_code == 404
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "COMPONENT_NOT_FOUND",
            "msg": "Component does not exist in system for satellite",
        }

    def test_telem_meta_get_cad_id_battery_case_insensitive(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?cad_id=battery",
            headers=self.headers,
        )

        data = response.json
        self.assertEqual(len(data["telem_results"]), 1)
        telem_data = data["telem_results"][0]

        db_data = TelemetryMetadata.get_by_id(session, self.vbatt_id)

        self.assertEqual(telem_data["tlm_name"], db_data.tlm_name)
        self.assertEqual(telem_data["uuid"], str(db_data.uuid))
        self.assertEqual(telem_data["units"], db_data.units)
        self.assertEqual(telem_data["cad_id"], "BATTERY")
        self.assertEqual(telem_data["nominal_min"], db_data.nominal_min)
        self.assertEqual(telem_data["nominal_max"], db_data.nominal_max)
        self.assertEqual(telem_data["alert_min"], db_data.alert_min)
        self.assertEqual(telem_data["alert_max"], db_data.alert_max)
        self.assertEqual(telem_data["description"], db_data.description)
        self.assertEqual(telem_data["uncertainty"], db_data.uncertainty)
        self.assertEqual(telem_data["system"], self.power_system_name)
        self.assertEqual(telem_data["component"], self.power_component_name)

    def test_telem_meta_get_cad_id_solar(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?cad_id=solar_panel",
            headers=self.headers,
        )

        data = response.json
        self.assertEqual(len(data["telem_results"]), 1)
        telem_data = data["telem_results"][0]

        db_data = TelemetryMetadata.get_by_id(session, self.solar_temp_id)

        self.assertEqual(telem_data["tlm_name"], db_data.tlm_name)
        self.assertEqual(telem_data["uuid"], str(db_data.uuid))
        self.assertEqual(telem_data["units"], db_data.units)
        self.assertEqual(telem_data["cad_id"], "solar_panel")
        self.assertEqual(telem_data["nominal_min"], db_data.nominal_min)
        self.assertEqual(telem_data["nominal_max"], db_data.nominal_max)
        self.assertEqual(telem_data["alert_min"], db_data.alert_min)
        self.assertEqual(telem_data["alert_max"], db_data.alert_max)
        self.assertEqual(telem_data["description"], db_data.description)
        self.assertEqual(telem_data["uncertainty"], db_data.uncertainty)
        self.assertEqual(telem_data["system"], self.thermal_system_name)

    def test_telem_meta_cad_id_null(self):
        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?has_cad_id=True",
            headers=self.headers,
        )

        data = response.json
        self.assertEqual(len(data["telem_results"]), 2)
        telem_data = data["telem_results"][0]

        db_data = TelemetryMetadata.get_by_id(session, self.solar_temp_id)
        self.assertEqual(telem_data["cad_id"], "solar_panel")

        self.assertEqual(telem_data["tlm_name"], db_data.tlm_name)

        response = self.app.get(
            f"/satellite/{self.sat_id}/telemetry_metadata?has_cad_id=False",
            headers=self.headers,
        )

        data = response.json
        self.assertEqual(len(data["telem_results"]), 1)

        telem_data = data["telem_results"][0]

        db_data = TelemetryMetadata.get_by_id(session, self.empty_cad_id)
        self.assertEqual(hasattr(telem_data, "cad_id"), False)
