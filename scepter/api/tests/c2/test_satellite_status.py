import json
import unittest
from datetime import datetime

import pytest

from api import app
from api.database import session
from models import Asset, Permission, Satellite, SatStatus


@pytest.mark.usefixtures("patch_auth")
class TestSatelliteStatus(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()

    def setUp(self):
        self.sat_test_data = {
            "sat_name": "test_sat_name",
            "norad_id": 20580,
            "description": "test_description",
        }
        sat_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            content_type="application/json",
        )
        self.assertEqual(sat_response.status_code, 201)
        self.sat_id = json.loads(sat_response.data)["sat_id"]

        self.sat_status_test_data = {
            "update_time": datetime.utcnow().isoformat(),
            "status": "test_status",
        }
        post_respone = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            content_type="application/json",
        )
        assert post_respone.status_code == 201

    def tearDown(self):
        session.query(SatStatus).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_sat_status_post_valid_id(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
        )
        sat_status_data = json.loads(get_response.data)
        for key in self.sat_status_test_data:
            if key == "update_time":
                self.assertEqual(
                    datetime.strptime(sat_status_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_status_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_status_data[key], self.sat_status_test_data[key])
        self.assertEqual(sat_status_data["sat_id"], self.sat_id)

    def test_sat_status_post_valid_norad_id(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_test_data["norad_id"]),
            data=json.dumps(self.sat_status_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/satellite/{}/status".format(self.sat_test_data["norad_id"]),
        )
        self.assertEqual(response.status_code, 201)
        sat_status_data = json.loads(get_response.data)
        for key in self.sat_status_test_data:
            if key == "update_time":
                self.assertEqual(
                    datetime.strptime(sat_status_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_status_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            else:
                self.assertEqual(sat_status_data[key], self.sat_status_test_data[key])
        self.assertEqual(sat_status_data["sat_id"], self.sat_id)

    def test_sat_status_post_invalid_iso_datetime(self):
        self.sat_status_test_data["update_time"] = "not iso8601"
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["update_time"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

    def test_sat_status_get_single_valid(self):
        get_response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        sat_data = json.loads(get_response.data)
        for key in self.sat_status_test_data:
            if key == "update_time":
                self.assertEqual(
                    datetime.strptime(sat_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.sat_status_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            self.assertEqual(sat_data[key], self.sat_status_test_data[key])
        self.assertEqual(sat_data["sat_id"], self.sat_id)

    def test_sat_status_get_single_no_hidden_vals(self):
        get_response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
        )
        sat_data = json.loads(get_response.data)
        for key in sat_data:
            self.assertFalse(key[0] == "_")

    def test_sat_status_no_status_updates(self):
        sat_data = {"sat_name": "new_test_sat", "description": "test_description"}
        sat_response = self.app.post(
            "/satellite",
            data=json.dumps(sat_data),
            content_type="application/json",
        )
        self.assertEqual(sat_response.status_code, 201)
        sat_id = json.loads(sat_response.data)["sat_id"]
        get_response = self.app.get(
            "/satellite/{}/status".format(sat_id),
        )
        self.assertEqual(get_response.status_code, 200)
        self.assertTrue("No status updates" in json.loads(get_response.data)["msg"])
