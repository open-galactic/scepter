from uuid import uuid4

import pytest

from api import app
from api.tests.fixtures.c2 import headers, sat_id
from models import Script


@pytest.fixture
def script_presets(session, script_template):
    preset_a = script_template.create_preset("preset A", {"argument": "A"})
    preset_b = script_template.create_preset("preset B", {"argument": "B"})
    session.add(preset_a)
    session.add(preset_b)
    session.commit()
    yield preset_a, preset_b
    session.delete(preset_a)
    session.delete(preset_b)
    session.commit()


@pytest.fixture
def second_script_preset(session, satellite):
    script = Script(
        id="second_script",
        satellite=satellite,
        name="second_script",
        entrypoint="/customscripts/custom.py",
        args={"type": "object", "properties": {"argument": {"type": "string"}}},
    )
    session.add(script)
    session.flush()

    preset = script.create_preset("preset A", {"argument": "AAAAA"})
    session.add(preset)
    session.commit()
    yield preset, script
    session.delete(preset)
    session.delete(script)
    session.commit()


def test_list_preset(session, script_template, script_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    presets = data["presets"]
    assert type(presets) == list
    assert len(presets) == 2

    for i in range(2):
        assert presets[i]["id"] == str(script_presets[i].id)
        assert presets[i]["name"] == script_presets[i].name
        assert presets[i]["args"] == script_presets[i].args


def test_list_preset_with_unknown_template(session, script_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/unknown/presets", headers=headers
        )
    assert response.status_code == 404


def test_list_preset_with_unknown_sat(session, script_presets, script_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/unknown/scripts/{script_template.name}/presets",
            headers=headers,
        )
    assert response.status_code == 404


def test_list_preset_is_scoped_by_script(session, script_presets, second_script_preset):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/{second_script_preset[1].id}/presets",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    presets = data["presets"]
    assert type(presets) == list
    assert len(presets) == 1
    assert presets[0]["id"] == str(second_script_preset[0].id)


def test_get_preset(session, script_template, script_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/{script_template.name}"
            f"/presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict

    assert data["id"] == str(script_presets[0].id)
    assert data["name"] == script_presets[0].name
    assert data["args"] == script_presets[0].args


def test_get_preset_for_unknown_template(session, script_presets):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/unknown/presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_get_preset_for_unknown_sat(session, script_presets, script_template):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/unknown/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_get_preset_for_missmatched_template(
    session, script_presets, second_script_preset
):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/scripts/{second_script_preset[1].id}/"
            f"presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_create_new(session, script_template):
    data = {"name": "new preset", "args": {"argument": "new"}}
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json
    assert "msg" in data
    preset = data["preset"]
    assert preset["id"]

    db_preset = script_template.get_preset_by_id(preset["id"])
    assert db_preset.name == preset["name"]
    assert db_preset.args == preset["args"]
    assert db_preset.created_at
    session.delete(db_preset)
    session.commit()


def test_create_new_with_bad_args(session, script_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_create_new_missing_name(session, script_template):
    data = {"args": {"argument_1": "new", "argument_2": 20}}
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_create_for_unknown_sat(session, script_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/unknown/scripts/{script_template.name}/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_create_for_unknown_template(session, script_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/scripts/unkwnown/presets",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update(session, script_template, script_presets):
    data = {"name": "new name", "args": {"argument": "new"}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json
    assert "msg" in data
    preset = data["preset"]
    assert preset["id"] == str(script_presets[0].id)

    # TODO replace this once session injection works
    session.commit()

    assert script_presets[0].name == preset["name"]
    assert script_presets[0].args == preset["args"]
    assert script_presets[0].created_at


def test_update_missing_name(session, script_template, script_presets):
    data = {"args": {"argument_1": "new", "argument_2": 20}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_update_bad_args(session, script_template, script_presets):
    data = {"name": "new name", "args": {"argument_1": 30, "argument_2": "not an int"}}
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 400


def test_update_for_unknown_sat(session, script_template, script_presets):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/unknown/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update_for_unknown_template(session, script_template, script_presets):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/unknown/presets/{script_presets[0].id}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_update_for_unknown_preset(session, script_template):
    data = {
        "name": "new preset",
        "args": {"argument_1": 10, "argument_2": "not an int"},
    }
    with app.test_client() as client:
        response = client.put(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets/{uuid4()}",
            json=data,
            headers=headers,
        )
    assert response.status_code == 404


def test_delete(session, script_template):
    preset = script_template.create_preset("test name", {"argument_1": 10})
    session.add(preset)
    session.commit()

    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/scripts/{script_template.name}/"
            f"presets/{preset.id}",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    assert "msg" in data

    # TODO replace this once session injection works
    session.commit()
    assert len(script_template.presets) == 0


def test_delete_for_unknown_sat(session, script_template, script_presets):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/unknown/scripts/{script_template.name}/"
            f"presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_unknown_template(session, script_template, script_presets):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/scripts/unknown/presets/{script_presets[0].id}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_unknown_preset(session, script_template):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/scripts/{script_template.name}/presets/{uuid4()}",
            headers=headers,
        )
    assert response.status_code == 404


def test_delete_for_missmatched_presets(session, script_template, second_script_preset):
    with app.test_client() as client:
        response = client.delete(
            f"/satellite/{sat_id}/scripts/{script_template.name}/"
            f"presets/{second_script_preset[0].id}",
            headers=headers,
        )
    assert response.status_code == 404
