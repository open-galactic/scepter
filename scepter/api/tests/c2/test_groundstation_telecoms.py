import json
import unittest
from datetime import datetime, timedelta

import pytest

from api import app
from api.database import session
from models import GroundStation, GsStatus, GsTelecom, Permission, ieee_bands


@pytest.mark.usefixtures("patch_auth")
class TestGroundstationTelecoms(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        gs_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        assert gs_response.status_code == 201

        self.gs_id = json.loads(gs_response.data)["gs_id"]

        self.gs_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_lower": 1e9,
            "frequency_centre": 2.1234e9,
            "frequency_upper": 5e9,
            "transmits": True,
            "receives": True,
            "address": "10.1.5.3",
            "port": 1324,
            "antenna_gain": 123,
            "modulations": {"FM": [100]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }
        gs_telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        assert gs_telecom_response.status_code == 201

        self.telecom_name = json.loads(gs_telecom_response.data)["telecom_name"]

    def tearDown(self):
        session.query(GsStatus).delete()
        session.query(GsTelecom).delete()
        session.query(Permission).delete()
        session.query(GroundStation).delete()
        session.commit()

    def test_gs_telecom_post_valid(self):
        self.gs_telecom_test_data["telecom_name"] = "test_cmd_name2"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        telecom_name = json.loads(response.data)["telecom_name"]

        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, telecom_name),
        )

        assert get_response.status_code == 200

        gs_data = get_response.json

        assert gs_data["telecom_name"] == self.gs_telecom_test_data["telecom_name"]
        assert gs_data["telecom_type"] == self.gs_telecom_test_data["telecom_type"]
        assert (
            gs_data["frequency_lower"] == self.gs_telecom_test_data["frequency_lower"]
        )
        assert (
            gs_data["frequency_centre"] == self.gs_telecom_test_data["frequency_centre"]
        )
        assert (
            gs_data["frequency_upper"] == self.gs_telecom_test_data["frequency_upper"]
        )
        assert gs_data["transmits"] == self.gs_telecom_test_data["transmits"]
        assert gs_data["port"] == self.gs_telecom_test_data["port"]
        assert gs_data["address"] == self.gs_telecom_test_data["address"]
        assert gs_data["antenna_gain"] == self.gs_telecom_test_data["antenna_gain"]
        assert gs_data["modulations"] == self.gs_telecom_test_data["modulations"]
        assert gs_data["rotatable"] == self.gs_telecom_test_data["rotatable"]
        assert gs_data["beamwidth"] == self.gs_telecom_test_data["beamwidth"]
        assert gs_data["noise_factor"] == self.gs_telecom_test_data["noise_factor"]
        assert gs_data["gs_id"] == self.gs_id

    def test_gs_telecom_post_existing_telecom_name_fails(self):
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 409
        data = response.json
        assert data == {
            "code": "GROUND_STATION_TELECOM_EXISTS",
            "msg": "Telecom name for groundstation already exists",
        }

    def test_gs_telecom_post_incorrect_frequency_range(self):
        self.gs_telecom_test_data["telecom_name"] = "test_telecom_name2"
        self.gs_telecom_test_data["frequency_lower"] = 2
        self.gs_telecom_test_data["frequency_centre"] = 1
        self.gs_telecom_test_data["frequency_upper"] = 3
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["frequency_lower"],
                        "msg": "Invalid frequency specification. Require lower <= centre <= upper",
                        "type": "value_error",
                    }
                ]
            }
        }
        self.gs_telecom_test_data["frequency_lower"] = 1
        self.gs_telecom_test_data["frequency_centre"] = 3
        self.gs_telecom_test_data["frequency_upper"] = 2
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["frequency_upper"],
                        "msg": "Invalid frequency specification. Require lower <= centre <= upper",
                        "type": "value_error",
                    }
                ]
            }
        }

    def test_gs_telecom_post_to_existing_telecom(self):
        response = self.app.post(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 405)

    def test_gs_telecom_post_bad_modulation_type(self):
        self.gs_telecom_test_data["modulations"] = json.dumps(
            {"BPSK": ["9600", "4800"], "not_a_modulation": ["200"]}
        )
        self.gs_telecom_test_data["telecom_name"] = "test_telecom_2"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["modulations"],
                        "msg": "value is not a valid dict",
                        "type": "type_error.dict",
                    }
                ]
            }
        }

    def test_gs_telecom_get_single_valid(self):
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        for key in self.gs_telecom_test_data:
            self.assertEqual(gs_data[key], self.gs_telecom_test_data[key])
        for band in ["L", "S", "C"]:
            self.assertTrue(band in gs_data.get("ieee_band"))
        self.assertEqual(gs_data["gs_id"], self.gs_id)

    def test_gs_telecom_get_ieee_bands_centre(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 13e9,
            "transmits": True,
            "receives": True,
            "antenna_gain": 123,
            "modulations": {"FM": [100]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }

        # Check Ku Band
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        self.assertEqual(gs_data.get("ieee_band")[0], "Ku")

        # Check G Band
        gs_telecom_test_data["frequency_centre"] = 301e9
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        self.assertEqual(gs_data.get("ieee_band")[0], "G")

        # Check HF Band
        gs_telecom_test_data["frequency_centre"] = 1e2
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        self.assertEqual(gs_data.get("ieee_band")[0], "HF")

    def test_gs_telecom_get_ieee_bands_range(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 13e9,
            "frequency_lower": 12.5e9,
            "frequency_upper": 17.5e9,
            "transmits": True,
            "receives": True,
            "antenna_gain": 123,
            "modulations": {"FM": [100]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }

        # Check Ku Band
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        self.assertEqual(gs_data.get("ieee_band")[0], "Ku")
        self.assertTrue(len(gs_data.get("ieee_band")) == 1)

        # Check All Bands
        gs_telecom_test_data["frequency_lower"] = 1
        gs_telecom_test_data["frequency_centre"] = 1e2
        gs_telecom_test_data["frequency_upper"] = 1e12

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        for band in ieee_bands:
            self.assertTrue(band[0] in gs_data.get("ieee_band"))

    def test_gs_telecom_get_single_no_telecom_name(self):
        get_response = self.app.get(
            "/groundstation/{}/telecom/not_a_telecom".format(self.gs_id),
        )
        self.assertEqual(get_response.status_code, 404)

    def test_gs_telecom_get_single_no_hidden_vals(self):
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        gs_data = json.loads(get_response.data)
        for key in gs_data:
            self.assertFalse(key[0] == "_")

    def test_gs_telecom_get_multiple_valid(self):
        self.gs_telecom_test_data["telecom_type"] = "test_telecom_type2"
        self.gs_telecom_test_data["telecom_name"] = "test_telecom_name2"
        self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        get_response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)
        self.assertIn("page", telecom_list)
        self.assertEqual(telecom_list["page"], 1)
        telecom_list = telecom_list["telecom_results"]
        self.assertTrue(isinstance(telecom_list, list))
        self.assertTrue(len(telecom_list) > 1)
        self.assertTrue(
            telecom_list[0]["telecom_type"] != telecom_list[1]["telecom_type"]
        )
        self.assertTrue(
            telecom_list[0]["telecom_name"] != telecom_list[1]["telecom_name"]
        )
        for band in ["L", "S", "C"]:
            self.assertTrue(band in telecom_list[0].get("ieee_band"))
        self.assertEqual(telecom_list[0]["gs_id"], self.gs_id)

    def test_gs_telecom_get_multiple_paginated(self):
        telecom_name = self.gs_telecom_test_data["telecom_name"]
        for i in range(105):
            self.gs_telecom_test_data["telecom_name"] = telecom_name + str(i)
            response = self.app.post(
                "/groundstation/{}/telecom".format(self.gs_id),
                data=json.dumps(self.gs_telecom_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 1)
        self.assertIn("total_pages", gs_list)
        self.assertEqual(gs_list["total_pages"], 3)
        gs_list = gs_list["telecom_results"]
        self.assertEqual(len(gs_list), 50)

        # test we can get other pages
        get_response = self.app.get(
            "/groundstation/{}/telecom?page=3".format(self.gs_id),
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 3)
        gs_list = gs_list["telecom_results"]
        self.assertEqual(len(gs_list), 6)

        # test you can't get more than 100 results per page
        get_response = self.app.get(
            "/groundstation/{}/telecom?page_size=200".format(self.gs_id),
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("total_pages", gs_list)
        self.assertEqual(gs_list["total_pages"], 2)
        gs_list = gs_list["telecom_results"]
        self.assertEqual(len(gs_list), 100)

    def test_gs_telecom_get_multiple_can_set_page_size(self):
        telecom_name = self.gs_telecom_test_data["telecom_name"]
        for i in range(14):
            self.gs_telecom_test_data["telecom_name"] = telecom_name + str(i)
            response = self.app.post(
                "/groundstation/{}/telecom".format(self.gs_id),
                data=json.dumps(self.gs_telecom_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom?page_size=5".format(self.gs_id),
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 1)
        self.assertIn("total_pages", gs_list)
        self.assertEqual(gs_list["total_pages"], 3)
        gs_list = gs_list["telecom_results"]
        self.assertEqual(len(gs_list), 5)

    def test_gs_telecom_get_multiple_filter_by_type(self):
        self.gs_telecom_test_data["telecom_name"] = "test_telecom_name2"
        self.gs_telecom_test_data["telecom_type"] = "test_telecom_type2"
        self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        gs_name_filter = "?telecom_type=type2"
        get_response = self.app.get(
            "/groundstation/{}/telecom{}".format(self.gs_id, gs_name_filter),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)["telecom_results"]
        self.assertTrue(len(telecom_list) == 1)
        self.assertTrue(
            telecom_list[0]["telecom_type"] == self.gs_telecom_test_data["telecom_type"]
        )

    def test_gs_telecom_get_multiple_no_search_results(self):
        telecom_filter = "?telecom_type=not_a_type"
        get_response = self.app.get(
            "/groundstation/{}/telecom{}".format(self.gs_id, telecom_filter),
        )
        self.assertEqual(get_response.status_code, 200)
        telecom_list = json.loads(get_response.data)["telecom_results"]
        self.assertTrue(len(telecom_list) == 0)

    def test_gs_telecom_get_multiple_no_hidden_vals(self):
        get_response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
        )
        gs_list = json.loads(get_response.data)["telecom_results"]
        gs_data = gs_list[0]
        for key in gs_data:
            self.assertFalse(key[0] == "_")

    def test_gs_telecom_put_replace_existing(self):
        telecom_test_data2 = {
            "telecom_type": "test_type2",
            "frequency_centre": 20000,
            "transmits": True,
            "receives": False,
        }
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(telecom_test_data2),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        gs_data = json.loads(get_response.data)
        for key in telecom_test_data2:
            self.assertEqual(gs_data[key], telecom_test_data2[key])

    def test_gs_telecom_put_create_new(self):
        defined_telecom_name = "666666"

        del self.gs_telecom_test_data["telecom_name"]
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, defined_telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, defined_telecom_name),
        )
        gs_data = json.loads(get_response.data)
        for key in self.gs_telecom_test_data:
            if key != "telecom_name":
                self.assertEqual(gs_data[key], self.gs_telecom_test_data[key])

    def test_gs_telecom_put_bad_gs_id(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_lower": 1e9,
            "frequency_centre": 2.1234e9,
            "frequency_upper": 5e9,
            "transmits": True,
            "receives": True,
            "antenna_gain": 123,
            "modulations": {"FM": [100]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(
                self.gs_id, "".join(["a" for i in range(256)])
            ),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data = response.json
        assert data == {
            "msg": "Telecom name too long, limit to 255 chars",
            "code": "TELECOM_NAME_TOO_LONG",
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, "π"),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "msg": "Telecom name must use only the ascii character set",
            "code": "TELECOM_ENCODING_INVALID",
        }

    def test_gs_telecom_put_bad_modulation_type(self):
        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_lower": 1e9,
            "frequency_centre": 2.1234e9,
            "frequency_upper": 5e9,
            "transmits": True,
            "receives": True,
            "antenna_gain": 123,
            "modulations": {"BPSK": ["9600", "4800"], "not_a_modulation": ["200"]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "code": "TELECOM_MODULATION_INVALID",
            "msg": "Invalid modulation type.",
        }

    def test_gs_telecom_put_incorrect_frequency_range(self):
        telecom_name = "test_telecom_name2"

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_lower": 2,
            "frequency_centre": 1,
            "frequency_upper": 3,
            "transmits": True,
            "receives": True,
            "antenna_gain": 123,
            "modulations": {"FM": [100]},
            "rotatable": False,
            "beamwidth": 200.123,
            "noise_factor": 0,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["frequency_lower"],
                        "msg": "Invalid frequency specification. Require lower <= centre <= upper",
                        "type": "value_error",
                    }
                ]
            }
        }

        gs_telecom_test_data["frequency_lower"] = 1
        gs_telecom_test_data["frequency_centre"] = 3
        gs_telecom_test_data["frequency_upper"] = 2
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, telecom_name),
            data=json.dumps(gs_telecom_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["frequency_upper"],
                        "msg": "Invalid frequency specification. Require lower <= centre <= upper",
                        "type": "value_error",
                    }
                ]
            }
        }

    def test_gs_telecom_delete_valid(self):
        deletion_data = {"verify": True, "cascade": True}
        gs = GroundStation.query_by_hash(session, self.gs_id)
        telecom_id = GsTelecom.query_by_name_and_gs(
            session, gs._id, self.telecom_name
        )._id
        response = self.app.delete(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        tele_data = json.loads(response.data)
        self.assertTrue("would" not in tele_data["msg"])
        self.assertTrue(self.telecom_name in tele_data["msg"])
        telecom_obj = GsTelecom.query_by_id(session, telecom_id)
        self.assertEqual(telecom_obj._deleted, True)
        self.assertTrue(
            telecom_obj._deleted_at > (datetime.utcnow() - timedelta(minutes=5))
        )
        self.assertEqual(telecom_obj._original_name, self.telecom_name)
        self.assertTrue(len(telecom_obj.telecom_name) == 36)

    def test_gs_telecom_delete_valid_with_status(self):
        status_data = {"update_time": datetime.utcnow().isoformat(), "status": "OK"}
        status_response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(status_data),
            content_type="application/json",
        )
        self.assertEqual(status_response.status_code, 201)
        deletion_data = {"verify": True, "cascade": True}
        gs = GroundStation.query_by_hash(session, self.gs_id)
        telecom_id = GsTelecom.query_by_name_and_gs(
            session, gs._id, self.telecom_name
        )._id
        response = self.app.delete(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        tele_data = json.loads(response.data)
        self.assertTrue("would" not in tele_data["msg"])
        self.assertTrue(self.telecom_name in tele_data["msg"])
        telecom_obj = GsTelecom.query_by_id(session, telecom_id)
        self.assertEqual(telecom_obj._deleted, True)
        self.assertTrue(
            telecom_obj._deleted_at > (datetime.utcnow() - timedelta(minutes=5))
        )
        self.assertEqual(telecom_obj._original_name, self.telecom_name)
        self.assertTrue(len(telecom_obj.telecom_name) == 36)
        status_response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
        )
        self.assertEqual(status_response.status_code, 200)
        status_data = json.loads(status_response.data)

    def test_gs_telecom_delete_get_single_missing(self):
        deletion_data = {"verify": True, "cascade": True}
        response = self.app.delete(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_delete_get_multiple_missing(self):
        deletion_data = {"verify": True, "cascade": True}
        response = self.app.delete(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
        )
        self.assertEqual(response.status_code, 200)
        tele_data = json.loads(response.data)["telecom_results"]
        self.assertEqual(len(tele_data), 0)

    def test_gs_telecom_delete_post_same_name(self):
        deletion_data = {"verify": True, "cascade": True}
        response = self.app.delete(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        tele_data = json.loads(response.data)
        self.assertEqual(tele_data["telecom_name"], self.telecom_name)
