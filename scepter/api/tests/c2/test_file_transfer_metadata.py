import json
import os
import random
import unittest
from datetime import datetime, timedelta
from uuid import UUID

from api import app
from api.database import session
from api.tests.fixtures.c2 import headers
from models import Asset, FileTransferMetadata, GroundStation, Permission, Satellite

ABS_PATH = os.path.abspath(os.path.dirname(__file__))


class TestFileTransferMetadata(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        num = f"{random.randint(0, 100000)}"
        cls.norad_id = random.randint(0, 10000)

        cls.app = app.test_client()
        cls.user_id = UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1")
        cls.headers = headers

        cls.sat_ids = []
        for i in range(2):
            cls.sat_test_data = {
                "sat_name": "test_sat_name",
                "norad_id": cls.norad_id + i,
                "nssdc_id": "test_id" + num + str(i),
                "commission_date": datetime.utcnow().isoformat(),
                "launch_date": datetime.utcnow().isoformat(),
                "description": "test_description",
            }

            response = cls.app.post(
                "/satellite",
                data=json.dumps(cls.sat_test_data),
                headers=cls.headers,
                content_type="application/json",
            )
            cls.sat_ids.append(response.json["sat_id"])

        cls.gs_test_data = {
            "gs_name": "test_gs_name",
            "gs_hostname": "test_host",
            "gs_port": 2000,
            "commission_date": "2000-01-01T00:00:00.01",
            "latitude": -33.865143,
            "longitude": 151.209900,
            "altitude": 27,
            "address": "2 paper st",
            "config": "none",
        }
        gs_response = cls.app.post(
            "/groundstation",
            data=json.dumps(cls.gs_test_data),
            headers=cls.headers,
            content_type="application/json",
        )
        cls.gs_id = json.loads(gs_response.data)["gs_id"]
        cls.gs_numeric_id = GroundStation.query_by_hash(session, cls.gs_id)._id

        cls.file_types = ["uplink", "pass_recording", "processed", "log", "raw"]
        for sat_id in cls.sat_ids:
            for i in range(22):
                data = {
                    "asset_hash": sat_id,
                    "_file_key": f"{sat_id}/test_file_{i}.csv",
                    "_timestamp": (datetime.now() - timedelta(hours=i)).isoformat(),
                    "_file_size": random.uniform(0, 50),
                    "_deleted": i >= 20,
                    "file_type": cls.file_types[i % 5],
                }
                file_meta = FileTransferMetadata(**data)
                session.add(file_meta)
        session.commit()

    @classmethod
    def tearDownClass(cls):
        session.query(FileTransferMetadata).delete()
        session.query(Permission).delete()
        session.query(Satellite).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_file_metadata_get_valid(self):
        for sat_id in self.sat_ids:
            response = self.app.get(f"/asset/{sat_id}/file", headers=self.headers)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json["files"]), 20)
            self.assertEqual(response.json["page"], 1)

            self.assertEqual(response.json["files"][0]["file_type"], "uplink")
            self.assertEqual(response.json["files"][0]["asset_hash"], sat_id)
            self.assertEqual(response.json["files"][0]["file_key"], "test_file_0.csv")

    def test_file_metadata_get_valid_paginated(self):
        for sat_id in self.sat_ids:
            response = self.app.get(f"/asset/{sat_id}/file?page_size=10", headers=self.headers)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json["files"]), 10)
            self.assertEqual(response.json["page"], 1)

    def test_file_metadata_get_valid_filter_file_type(self):
        for sat_id in self.sat_ids:
            for file_type in self.file_types:
                response = self.app.get(
                    f"/asset/{sat_id}/file?file_type={file_type}",
                    headers=self.headers,
                )
                self.assertEqual(response.status_code, 200)
                self.assertEqual(len(response.json["files"]), 4)

    def test_file_metadata_get_invalid_sat_id(self):
        response = self.app.get("/asset/test/file", headers=self.headers)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json["msg"], "Asset with that ID not found")

    def test_file_metadata_get_invalid_file_type(self):
        ftype = "test"
        response = self.app.get(f"/asset/{self.sat_ids[0]}/file?file_type={ftype}", headers=self.headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json["msg"],
            "File type is invalid",
        )
