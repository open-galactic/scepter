import json
import unittest

import pytest

from api import app
from models import modulations


@pytest.mark.usefixtures("patch_auth")
class TestModulations(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()

    def test_modulations_get_valid(self):
        response = self.app.get("/modulations")
        self.assertEqual(response.status_code, 200)
        for mod in modulations.valid_mod_types:
            self.assertTrue(mod in json.loads(response.data))
        self.assertEqual(
            len(json.loads(response.data)), len(modulations.valid_mod_types)
        )
