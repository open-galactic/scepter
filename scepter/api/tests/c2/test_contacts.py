from datetime import datetime, timedelta
from uuid import uuid4

import pytest

from api import app
from api.exceptions import ReasonCode
from api.tests.fixtures.c2 import headers, sat_id
from models import Contact, Instruction, Overpass


@pytest.fixture
def cancelled_contact(session, upcoming_contact):
    contact = Contact.get_by_id(session, upcoming_contact.id)
    contact.cancelled_at = datetime.utcnow()
    session.commit()
    yield contact


def test_get_upcoming_contacts(session, upcoming_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?upcoming=true", headers=headers
        )
    assert response.status_code == 200

    data: dict = response.json
    assert type(data) == dict
    contacts = data["contacts"]
    assert type(contacts) == list
    assert len(contacts) == 1

    contact = contacts[0]
    assert contact["id"] == str(upcoming_contact.id)
    assert contact["overpass"]["id"] == str(upcoming_contact.overpass.id)
    assert contact["issued_by"] == upcoming_contact.issued_by
    assert contact["issued_at"] == upcoming_contact.issued_at.isoformat()
    assert contact["created_at"]

    db_command = upcoming_contact.instructions[0]
    command = contact["instructions"][0]
    assert command["id"] == db_command.id == 0
    assert command["command_id"] == db_command.command.id
    assert command["entrypoint"] == db_command.entrypoint
    assert command["args"] == db_command.args
    assert command["type"] == "command"
    assert command["sequence_number"] == 0

    db_script = upcoming_contact.instructions[1]
    script = contact["instructions"][1]
    assert script["id"] == db_script.id == 1
    assert script["script_id"] == db_script.script.id
    assert script["entrypoint"] == db_script.entrypoint
    assert script["args"] == db_script.args
    assert script["type"] == "script"
    assert script["sequence_number"] == 1


def test_get_upcoming_with_unknown_qparam(upcoming_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?not_a_param=true", headers=headers
        )
    assert response.status_code == 400


def test_contact_pagination(upcoming_contact, historic_contact):
    # Check first page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?page_size=1", headers=headers
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 2
    contacts = data["contacts"]
    assert type(contacts) == list
    assert len(contacts) == 1

    # Check second page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?page_size=1&cursor={data['cursor']}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 2
    contacts = data["contacts"]
    assert type(contacts) == list
    assert len(contacts) == 1

    # Check overrun page of results
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?page_size=1&cursor={data['cursor']}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    contacts = data["contacts"]
    assert data["total_pages"] == 2
    assert type(contacts) == list
    # Check no results
    assert len(contacts) == 0
    assert data["cursor"] is None


def test_get_latest_contact(superseded_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts/{superseded_contact[1].id}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["id"] == str(superseded_contact[1].id)
    assert data["overpass"]["id"] == str(superseded_contact[1].overpass.id)


def test_get_superceded_contact(superseded_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts/{superseded_contact[0].id}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["id"] == str(superseded_contact[0].id)


def test_get_unknown_contact():
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts/{uuid4()}", headers=headers
        )
    assert response.status_code == 404


def test_get_contact_history_by_latest(superseded_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts/{superseded_contact[1].id}/history",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data["history"]) == list
    assert len(data["history"]) == 2
    # ensures ordering of versions is latest to oldest
    assert data["history"][0]["created_at"] > data["history"][1]["created_at"]


def test_get_contact_history_by_superseded(superseded_contact):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts/{superseded_contact[0].id}/history",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data["history"]) == list
    assert len(data["history"]) == 2
    # ensures ordering of versions is latest to oldest
    assert data["history"][0]["created_at"] > data["history"][1]["created_at"]


def test_issue_contact(session, overpass, command_template, script_template):
    contact_data = {
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
            {
                "script_id": script_template.id,
                "sequence_number": 1,
                "args": {"argument": "another string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    contact = Contact.get_by_id(session, data["id"])

    assert contact.group_id is not None
    assert contact.overpass.id == contact_data["overpass_id"]
    assert contact.issued_at.isoformat() == contact_data["issued_at"]
    assert len(contact.instructions) == 2
    for instruction in contact.instructions:
        if instruction.sequence_number == 0:
            assert instruction.command._id == command_template._id
            assert instruction.args == {"argument_1": "a string"}
        if instruction.sequence_number == 1:
            assert instruction.script._id == script_template._id
            assert instruction.args == {"argument": "another string"}

    session.query(Instruction).delete()
    session.query(Contact).delete()
    session.commit()


def test_issue_without_overpass_id(
    session, groundstation, command_template, script_template
):
    # TODO remove this once the scrapers and calculator are populating the db
    # with overpasses
    contact_data = {
        "issued_at": datetime.utcnow().isoformat(),
        "gs_id": groundstation._asset_hash,
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
            {
                "script_id": script_template.id,
                "sequence_number": 1,
                "args": {"argument": "another string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    contact = Contact.get_by_id(session, data["id"])

    assert contact.overpass_id

    session.query(Instruction).delete()
    session.query(Contact).delete()
    session.query(Overpass).delete()
    session.commit()


def test_issue_with_aos(session, groundstation, command_template, script_template):
    # TODO remove this once the scrapers and calculator are populating the db
    # with overpasses
    aos = datetime.utcnow() + timedelta(minutes=1)
    contact_data = {
        "issued_at": datetime.utcnow().isoformat(),
        "gs_id": groundstation._asset_hash,
        "aos": aos.isoformat(),
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
            {
                "script_id": script_template.id,
                "sequence_number": 1,
                "args": {"argument": "another string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    contact = Contact.get_by_id(session, data["id"])

    assert contact.overpass_id

    assert contact.overpass.gs_id == groundstation._id
    assert contact.overpass.aos == aos
    assert contact.overpass.los > aos
    assert aos < contact.overpass.tca < contact.overpass.los

    session.query(Instruction).delete()
    session.query(Contact).delete()
    session.query(Overpass).delete()
    session.commit()


def test_issue_with_aos_and_los(
    session, groundstation, command_template, script_template
):
    # TODO remove this once the scrapers and calculator are populating the db
    # with overpasses
    aos = datetime.utcnow() + timedelta(minutes=1)
    los = aos + timedelta(minutes=1)
    contact_data = {
        "issued_at": datetime.utcnow().isoformat(),
        "gs_id": groundstation._asset_hash,
        "aos": aos.isoformat(),
        "los": los.isoformat(),
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
            {
                "script_id": script_template.id,
                "sequence_number": 1,
                "args": {"argument": "another string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    contact = Contact.get_by_id(session, data["id"])

    assert contact.overpass_id

    assert contact.overpass.gs_id == groundstation._id
    assert contact.overpass.aos == aos
    assert contact.overpass.los == los
    assert aos < contact.overpass.tca < los

    session.query(Instruction).delete()
    session.query(Contact).delete()
    session.query(Overpass).delete()
    session.commit()


def test_supersede_contact(session, overpass, command_template, upcoming_contact):
    contact_data = {
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "supersedes": upcoming_contact.id,
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 201
    data = response.json

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    contact = Contact.get_by_id(session, data["id"])

    assert contact.group_id == upcoming_contact.group_id
    assert contact.overpass.id == contact_data["overpass_id"]
    assert contact.issued_at.isoformat() == contact_data["issued_at"]
    assert len(contact.instructions) == 1

    session.delete(contact)
    session.commit()


def test_supersede_non_active_contact(overpass, command_template, superseded_contact):
    contact_data = {
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "supersedes": superseded_contact[0].id,
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
        ],
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 409
    data = response.json
    assert data["active_contact"] == str(superseded_contact[1].id)


def test_supersede_unknown_contact(overpass, satellite):
    id = uuid4()
    contact_data = {
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "supersedes": id,
        "instructions": [
            {
                "command_id": "a command",
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
        ],
    }

    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 404
    assert response.json["code"] == ReasonCode.CONTACT_SUPERSEDED_NOT_FOUND.name
    assert response.json["msg"] == ReasonCode.CONTACT_SUPERSEDED_NOT_FOUND.value


def test_issue_contact_with_unknown_param(overpass, satellite):
    contact_data = {
        "unknown_param": True,
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "instructions": [
            {
                "command_id": "my_command",
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
        ],
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 400


def test_issue_contact_with_invalid_instruction_args(
    overpass, satellite, command_template
):
    contact_data = {
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "instructions": [
            {
                "command_id": command_template.id,
                "sequence_number": 0,
                "args": {"argument_1": 10},
            },
        ],
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 400
    assert response.json["code"] == ReasonCode.CONTACT_INVALID_ARGS.name


def test_issue_contact_with_script_and_command_ids(overpass, satellite):
    contact_data = {
        "unknown_param": True,
        "overpass_id": overpass.id,
        "issued_at": datetime.utcnow().isoformat(),
        "instructions": [
            {
                "command_id": "my_command",
                "script_id": "my_command",
                "sequence_number": 0,
                "args": {"argument_1": "a string"},
            },
        ],
    }
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts",
            json=contact_data,
            headers=headers,
        )
    assert response.status_code == 400


def test_cancel_contact(upcoming_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}/cancel",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    assert data["msg"]
    assert data["cancelled_at"]


def test_cancel_cancelled_contact(cancelled_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{cancelled_contact.id}/cancel",
            headers=headers,
        )
    assert response.status_code == 409
    assert response.json["code"] == ReasonCode.CONTACT_CANCELLED_NOT_ACTIVE.name


def test_list_doesnt_include_cancelled_contact(upcoming_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}/cancel",
            headers=headers,
        )
    assert response.status_code == 200

    with app.test_client() as client:
        response = client.get(f"/satellite/{sat_id}/contacts", headers=headers)
    assert response.status_code == 200
    data = response.json
    assert len(data["contacts"]) == 0


def test_list_includes_cancelled_contacts_with_param(
    upcoming_contact, historic_contact
):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}/cancel",
            headers=headers,
        )
    assert response.status_code == 200

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{sat_id}/contacts?include_cancelled=true", headers=headers
        )
    assert response.status_code == 200
    data = response.json
    assert len(data["contacts"]) == 2


def test_dispatch_contact(upcoming_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}/dispatch",
            headers=headers,
        )
    assert response.status_code == 200
    data = response.json
    assert data["msg"]
    assert data["dispatched_at"]


def test_dispatch_unknown_contact(upcoming_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{uuid4()}/dispatch",
            headers=headers,
        )
    assert response.status_code == 404


def test_dispatch_cancelled_contact(cancelled_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{cancelled_contact.id}/dispatch",
            headers=headers,
        )
    assert response.status_code == 409
    assert response.json["code"] == ReasonCode.CONTACT_DISPATCH_CANCELLED.name


def test_start_contact_execution(session, upcoming_contact):
    started_at = datetime.utcnow().isoformat()
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}"
            f"/instructions/{upcoming_contact.instructions[0].id}/start",
            json={"started_at": started_at},
            headers=headers,
        )
    data = response.json
    assert response.status_code == 200
    assert data["msg"]

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    instruction = upcoming_contact.instructions[0]
    assert instruction.started_at.isoformat() == started_at
    # check only started command has been changed
    assert upcoming_contact.instructions[1].started_at is None


def test_start_cancelled_contact(cancelled_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{cancelled_contact.id}"
            f"/instructions/{cancelled_contact.instructions[0].id}/start",
            json={"started_at": datetime.utcnow().isoformat()},
            headers=headers,
        )
    assert response.status_code == 409
    assert response.json["code"] == ReasonCode.CONTACT_START_CANCELLED.name


def test_end_contact_execution(session, upcoming_contact):
    ended_at = datetime.utcnow().isoformat()
    log_msg = "the command was sent successfully"
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{upcoming_contact.id}"
            f"/instructions/{upcoming_contact.instructions[0].id}/end",
            json={"ended_at": ended_at, "log": log_msg},
            headers=headers,
        )
    data = response.json
    assert response.status_code == 200
    assert data["msg"]

    # dirty hack to read in the modified data after the request TODO fix this
    # once session injection works
    session.commit()

    instruction = upcoming_contact.instructions[0]
    assert instruction.ended_at.isoformat() == ended_at
    assert instruction.log == log_msg
    # check only ended command has been changed
    assert upcoming_contact.instructions[1].ended_at is None
    assert upcoming_contact.instructions[1].log is None


def test_end_cancelled_contact(cancelled_contact):
    with app.test_client() as client:
        response = client.post(
            f"/satellite/{sat_id}/contacts/{cancelled_contact.id}"
            f"/instructions/{cancelled_contact.instructions[0].id}/end",
            json={"ended_at": datetime.utcnow().isoformat()},
            headers=headers,
        )
    assert response.status_code == 409
    assert response.json["code"] == ReasonCode.CONTACT_END_CANCELLED.name
