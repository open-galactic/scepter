from datetime import datetime, timedelta
from http import HTTPStatus
from typing import Any, Callable
from uuid import uuid4

import pytest
from sqlalchemy import inspect

from api import app
from api.exceptions import ReasonCode
from api.tests.fixtures.c2 import headers
from models import Satellite, Telemetry, TelemetryMetadata


@pytest.fixture
def telemetry_factory() -> Callable:
    created_telemetry = []

    def _telemetry_factory(
        telemetry_metadata: TelemetryMetadata, observed_at: datetime, value: Any
    ) -> Telemetry:
        telemetry = Telemetry(
            telemetry_metadata=telemetry_metadata,
            observed_at=observed_at,
            value=value,
        )
        session = inspect(telemetry_metadata).session
        session.add(telemetry)
        session.commit()
        created_telemetry.append(telemetry)
        return telemetry

    yield _telemetry_factory

    for telemetry in created_telemetry:
        session = inspect(telemetry).session
        session.delete(telemetry)
        session.commit()


@pytest.fixture
def component_telemetry_metadata(session, satellite, component) -> TelemetryMetadata:
    telemetry_metadata = TelemetryMetadata(
        _timestamp=datetime.now(),
        _sat_id=satellite._id,
        tlm_name="component_tlm_name",
        uuid=str(uuid4()),
        _subsystem_id=component.id,
        description="component description",
        units="component units",
    )
    session.add(telemetry_metadata)
    session.commit()
    yield telemetry_metadata
    session.delete(telemetry_metadata)
    session.commit()


def test_telem_history_no_filter(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata, telemetry_factory
):
    _ = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(minutes=1), 123
    )
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 246)

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}",
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 1
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 2


def test_telem_history_filter_by_start_time(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata, telemetry_factory
):
    _ = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(minutes=1), 123
    )
    tlm: Telemetry = telemetry_factory(telemetry_metadata, datetime.utcnow(), 246)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?observed_at_start={tlm.observation_time - timedelta(seconds=30)}"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 1
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1
    assert telemetry[0]["observed_at"] == tlm.observation_time.isoformat()
    assert telemetry[0]["value"] == tlm.value


def test_telem_history_filter_by_end_time(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata, telemetry_factory
):
    tlm = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(minutes=1), 123
    )
    _: Telemetry = telemetry_factory(telemetry_metadata, datetime.utcnow(), 246)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?observed_at_end={tlm.observation_time + timedelta(seconds=30)}"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 1
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1
    assert telemetry[0]["observed_at"] == tlm.observation_time.isoformat()
    assert telemetry[0]["value"] == tlm.value


def test_telem_history_filter_by_start_and_end_time(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata, telemetry_factory
):
    _ = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(minutes=1), 123
    )
    tlm_mid = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(seconds=30), 246
    )
    _: Telemetry = telemetry_factory(telemetry_metadata, datetime.utcnow(), 369)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?observed_at_end={tlm_mid.observation_time + timedelta(seconds=10)}"
                f"&observed_at_start={tlm_mid.observation_time - timedelta(seconds=10)}"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 1
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1
    assert telemetry[0]["observed_at"] == tlm_mid.observation_time.isoformat()
    assert telemetry[0]["value"] == tlm_mid.value


def test_telem_history_end_time_before_start_time(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata
):

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?observed_at_end={datetime.utcnow() - timedelta(seconds=30)}"
                f"&observed_at_start={datetime.utcnow() + timedelta(seconds=30)}"
            ),
            headers=headers,
        )
    assert response.status_code == 400
    assert response.json["code"] == "END_TIME_BEFORE_START_TIME"


def test_telem_history_invalid_cursor(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata
):

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                "?page_size=1&cursor=INVALID_CURSOR"
            ),
            headers=headers,
        )
    assert response.status_code == 400
    assert response.json["code"] == "INVALID_CURSOR"


def test_telem_history_pagination(
    satellite: Satellite, telemetry_metadata: TelemetryMetadata, telemetry_factory
):
    for i in range(5):
        _ = telemetry_factory(
            telemetry_metadata, datetime.utcnow() - timedelta(seconds=i), i
        )

    tlm = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                "?page_size=1"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    assert type(data) == dict
    assert data["total_pages"] == 6
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1
    assert telemetry[0]["observed_at"] == tlm.observation_time.isoformat()
    assert telemetry[0]["value"] == tlm.value

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?page_size=1&cursor={data['cursor']}"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    print(data)
    assert type(data) == dict
    assert data["total_pages"] == 6
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/telemetry/{telemetry_metadata.uuid}"
                f"?page_size=1&cursor={data['cursor']}"
            ),
            headers=headers,
        )
    assert response.status_code == 200
    data: dict = response.json
    print(data)
    assert type(data) == dict
    assert data["total_pages"] == 6
    telemetry = data["telemetry"]
    assert type(telemetry) == list
    assert len(telemetry) == 1


def test_latest_only_returns_latest(satellite, telemetry_metadata, telemetry_factory):
    _ = telemetry_factory(
        telemetry_metadata, datetime.utcnow() - timedelta(minutes=1), 123
    )
    latest_tlm = telemetry_factory(telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry", headers=headers
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json
    assert data["total_pages"] == 1
    assert data["cursor"]
    data = response.json["telemetry"]

    assert len(data) == 1
    assert data[0]["value"] == latest_tlm.value
    assert data[0]["observed_at"] == latest_tlm.observation_time.isoformat()
    assert data[0]["tlm_name"] == latest_tlm.telem_metadata.tlm_name
    assert data[0]["tlm_id"] == latest_tlm.telem_metadata.uuid
    assert data[0]["system"] == latest_tlm.telem_metadata.subsystem.name
    assert data[0]["component"] is None


def test_latest_multiple_metadata(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add extra value to ensure this isn't returned
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    system_tlm = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    component_tlm = telemetry_factory(
        component_telemetry_metadata, datetime.utcnow(), 321
    )

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry", headers=headers
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json
    assert data["cursor"]
    assert data["total_pages"] == 1
    data = data["telemetry"]
    assert len(data) == 2

    # ordering should be use ASC metadata _id
    system_response = data[0]
    assert system_response["value"] == system_tlm.value
    assert system_response["observed_at"] == system_tlm.observation_time.isoformat()
    assert system_response["tlm_name"] == system_tlm.telem_metadata.tlm_name
    assert system_response["tlm_id"] == system_tlm.telem_metadata.uuid
    assert system_response["system"] == system_tlm.telem_metadata.subsystem.name
    assert system_response["component"] is None

    component_response = data[1]
    assert component_response["value"] == component_tlm.value
    assert (
        component_response["observed_at"] == component_tlm.observation_time.isoformat()
    )
    assert component_response["tlm_name"] == component_tlm.telem_metadata.tlm_name
    assert component_response["tlm_id"] == component_tlm.telem_metadata.uuid
    assert (
        component_response["system"]
        == component_tlm.telem_metadata.subsystem.parent.name
    )
    assert (
        component_response["component"] == component_tlm.telem_metadata.subsystem.name
    )


def test_latest_filter_by_system(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    # Add second value to ensure this isn't returned
    tlm = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?system={tlm.telem_metadata.subsystem.parent.name}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 1
    assert data[0]["tlm_id"] == tlm.telem_metadata.uuid


def test_latest_filter_by_component(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add second value to ensure this isn't returned
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    tlm = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?component={tlm.telem_metadata.subsystem.name}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 1
    assert data[0]["tlm_id"] == tlm.telem_metadata.uuid


def test_latest_filter_by_tlm_name(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add second value to ensure this isn't returned
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    tlm = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?tlm_name={tlm.telem_metadata.tlm_name}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 1
    assert data[0]["tlm_id"] == tlm.telem_metadata.uuid


def test_latest_filter_by_tlm_name_list(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add second value to ensure this isn't returned
    tlm1 = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    tlm2 = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?tlm_name={tlm1.telem_metadata.tlm_name}"
                f"&tlm_name={tlm2.telem_metadata.tlm_name}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 2
    assert data[0]["tlm_id"] == tlm1.telem_metadata.uuid
    assert data[1]["tlm_id"] == tlm2.telem_metadata.uuid


def test_latest_filter_by_everything_except_id(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add second value to ensure this isn't returned
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    tlm = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?system={tlm.telem_metadata.subsystem.parent.name}"
                f"&component={tlm.telem_metadata.subsystem.name}"
                f"&tlm_name={tlm.telem_metadata.tlm_name}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 1
    assert data[0]["tlm_id"] == tlm.telem_metadata.uuid


def test_latest_filter_by_tlm_id(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    # Add second value to ensure this isn't returned
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    tlm = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?tlm_id={tlm.telem_metadata.uuid}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 1
    assert data[0]["tlm_id"] == tlm.telem_metadata.uuid


def test_latest_not_observed(
    satellite,
    telemetry_metadata,
):
    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry", headers=headers
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json
    assert len(data["telemetry"]) == 0
    assert data["total_pages"] == 0


def test_latest_filter_by_tlm_id_not_observed(satellite, telemetry_metadata):
    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?tlm_id={telemetry_metadata.uuid}"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json["telemetry"]
    assert len(data) == 0


def test_latest_pagination(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    _ = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry?page_size=1",
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json
    assert data["total_pages"] == 2
    assert len(data["telemetry"]) == 1
    cursor = data["cursor"]

    with app.test_client() as client:
        response = client.get(
            (
                f"/satellite/{satellite._asset_hash}/latesttelemetry"
                f"?cursor={cursor}&page_size=1"
            ),
            headers=headers,
        )
    assert response.status_code == HTTPStatus.OK
    data = response.json
    assert data["total_pages"] == 2
    assert len(data["telemetry"]) == 1


def test_latest_invalid_cursor(
    satellite,
    telemetry_metadata,
    component_telemetry_metadata,
    telemetry_factory,
):
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)
    _ = telemetry_factory(component_telemetry_metadata, datetime.utcnow(), 321)

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry?cursor=abadcursor",
            headers=headers,
        )
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json["code"] == ReasonCode.INVALID_CURSOR.name


def test_latest_unknown_param(
    satellite,
    telemetry_metadata,
    telemetry_factory,
):
    _ = telemetry_factory(telemetry_metadata, datetime.utcnow(), 123)

    with app.test_client() as client:
        response = client.get(
            f"/satellite/{satellite._asset_hash}/latesttelemetry?unknown=1",
            headers=headers,
        )
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json == {
        "validation_error": {
            "query_params": [
                {
                    "loc": ["unknown"],
                    "msg": "extra fields not permitted",
                    "type": "value_error.extra",
                }
            ]
        }
    }
