import json
import unittest
from datetime import datetime, timedelta
from uuid import UUID

import pytest

from api import app
from api.database import session
from models import Asset, GroundStation, GsStatus, GsTelecom, Permission


@pytest.mark.usefixtures("patch_auth")
class TestGroundStation(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user_id = UUID(
            "ea6fd5e1-9b78-42b8-acb9-2a054577c7a1"
        )  # TODO: Inject this in somehow

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "gs_hostname": "test_host",
            "gs_port": 2000,
            "commission_date": "2000-01-01T00:00:00.01",
            "latitude": -33.865143,
            "longitude": 151.209900,
            "altitude": 27,
            "address": "2 paper st",
            "config": "none",
        }
        gs_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        assert gs_response.status_code == 201

        self.gs_id = json.loads(gs_response.data)["gs_id"]

    def tearDown(self):
        session.query(GsTelecom).delete()
        session.query(Permission).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_groundstation_post_valid(self):
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        get_response = self.app.get(
            "/groundstation/" + gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in self.gs_test_data:
            if key == "commission_date":
                self.assertEqual(
                    datetime.strptime(gs_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.gs_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            elif key != "config":
                self.assertEqual(gs_data[key], self.gs_test_data[key])

    def test_groundstation_post_invalid_iso_datetime(self):
        self.gs_test_data["commission_date"] = "not iso8601"
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["commission_date"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

    def test_groundstation_post_no_gs_name(self):
        del self.gs_test_data["gs_name"]
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)

    def test_groundstation_post_bad_latitude(self):
        for latitude_value in [90.1, -90.1, 190]:
            self.gs_test_data["latitude"] = latitude_value
            response = self.app.post(
                "/groundstation",
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "GROUND_STATION_INVALID_LATITUDE",
                "msg": "invalid latitude coords: -90 > lat > 90",
            }

    def test_groundstation_post_bad_longitude(self):
        for longitude_value in [180.1, -180.1]:
            self.gs_test_data["longitude"] = longitude_value
            response = self.app.post(
                "/groundstation",
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )
            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "GROUND_STATION_INVALID_LONGITUDE",
                "msg": "invalid longitude coords: -180 > long > 180",
            }

    def test_groundstation_post_same_data_new_gs_id(self):
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertFalse(self.gs_id == json.loads(response.data)["gs_id"])

    def test_groundstation_post_existing_gs_id_fails(self):
        response = self.app.post(
            "/groundstation/" + self.gs_id,
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 405)

    def test_groundstation_get_single_valid(self):
        get_response = self.app.get(
            "/groundstation/" + self.gs_id,
        )
        self.assertEqual(get_response.status_code, 200)
        gs_data = json.loads(get_response.data)
        for key in self.gs_test_data:
            if key == "commission_date":
                self.assertEqual(
                    datetime.strptime(gs_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.gs_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            elif key != "config":
                self.assertEqual(gs_data[key], self.gs_test_data[key])
        self.assertEqual(gs_data["telecoms"], [])

    def test_groundstation_get_single_comission_date_formatted(self):
        get_response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
        )
        self.assertEqual(get_response.status_code, 200)
        self.assertEqual(
            json.loads(get_response.data)["commission_date"],
            datetime.strptime(
                self.gs_test_data["commission_date"], "%Y-%m-%dT%H:%M:%S.%f"
            ).isoformat(),
        )

    def test_groundstation_get_single_no_hidden_vals(self):
        get_response = self.app.get(
            "/groundstation/" + self.gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in gs_data:
            self.assertFalse(key[0] == "_")

    def test_groundstation_get_single_returns_telecom(self):
        gs_telecom_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 2e9,
            "transmits": True,
            "receives": True,
        }
        self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(gs_telecom_data),
            content_type="application/json",
        )
        get_response = self.app.get(
            "/groundstation/" + self.gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in gs_telecom_data.keys():
            self.assertEqual(gs_telecom_data[key], gs_data["telecoms"][0][key])
        self.assertEqual(gs_data["telecoms"][0].get("ieee_band")[0], "S")

    def test_groundstation_get_multiple_valid(self):
        gs2_test_data = self.gs_test_data
        gs2_test_data["gs_name"] = "test_name2"
        self.app.post(
            "/groundstation",
            data=json.dumps(gs2_test_data),
            content_type="application/json",
        )
        get_response = self.app.get(
            "/groundstation",
        )
        self.assertEqual(get_response.status_code, 200)
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 1)
        gs_list = gs_list["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) > 1)
        self.assertTrue(gs_list[0]["gs_id"] != gs_list[1]["gs_id"])
        self.assertTrue(gs_list[0]["gs_name"] != gs_list[1]["gs_name"])

    def test_groundstation_get_multiple_paginated(self):
        for i in range(105):
            response = self.app.post(
                "/groundstation",
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation",
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertIn("total_pages", gs_list)
        self.assertEqual(gs_list["page"], 1)
        self.assertEqual(gs_list["total_pages"], 3)
        gs_list = gs_list["gs_results"]
        self.assertEqual(len(gs_list), 50)

        # Check we can get the second page of results
        get_response = self.app.get(
            "/groundstation?page=2",
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 2)
        gs_list = gs_list["gs_results"]
        self.assertEqual(len(gs_list), 50)

        # Check that it's not possible to retrieve more than 100 results
        get_response = self.app.get(
            "/groundstation?page_size=200",
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 1)
        self.assertEqual(gs_list["total_pages"], 2)
        gs_list = gs_list["gs_results"]
        self.assertEqual(len(gs_list), 100)

    def test_groundstation_get_multiple_set_page_size(self):
        for _ in range(14):
            response = self.app.post(
                "/groundstation",
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )
            self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation?page_size=5",
        )
        gs_list = json.loads(get_response.data)
        self.assertIn("page", gs_list)
        self.assertEqual(gs_list["page"], 1)
        self.assertEqual(gs_list["total_pages"], 3)
        gs_list = gs_list["gs_results"]
        self.assertEqual(len(gs_list), 5)

    def test_groundstation_get_multiple_filter(self):
        gs2_test_data = self.gs_test_data
        gs2_test_data["publicly_visible"] = True
        self.app.post(
            "/groundstation",
            data=json.dumps(gs2_test_data),
            content_type="application/json",
        )
        gs_filter = "?publicly_visible=True"
        get_response = self.app.get(
            "/groundstation" + gs_filter,
        )
        self.assertEqual(get_response.status_code, 200)
        gs_list = json.loads(get_response.data)["gs_results"]
        self.assertTrue(len(gs_list) == 1)
        self.assertTrue(
            gs_list[0]["publicly_visible"] == gs2_test_data["publicly_visible"]
        )

    def test_groundstation_get_multiple_returns_telecom(self):
        gs_telecom_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 2e9,
            "transmits": True,
            "receives": True,
        }
        telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(gs_telecom_data),
            content_type="application/json",
        )
        self.assertEqual(telecom_response.status_code, 201)
        get_response = self.app.get(
            "/groundstation",
        )
        gs_data = json.loads(get_response.data)["gs_results"]
        for key in gs_telecom_data.keys():
            self.assertEqual(gs_telecom_data[key], gs_data[0]["telecoms"][0][key])
        self.assertEqual(gs_data[0]["telecoms"][0].get("ieee_band")[0], "S")

    def test_groundstation_get_multiple_boolean_parsed(self):
        get_response = self.app.get(
            "/groundstation?publicly_visible=True",
        )
        self.assertEqual(get_response.status_code, 200)
        gs_list = json.loads(get_response.data)["gs_results"]
        self.assertTrue(len(gs_list) == 0)

    def test_groundstation_get_multiple_no_hidden_vals(self):
        get_response = self.app.get(
            "/groundstation",
        )
        gs_list = json.loads(get_response.data)["gs_results"]
        gs_data = gs_list[0]
        for key in gs_data:
            self.assertFalse(key[0] == "_")

    def test_groundstation_get_multiple_bad_boolean_format(self):
        bad_bool_qry = "?publicly_visible=not a bool"
        response = self.app.get(
            "/groundstation" + bad_bool_qry,
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "query_params": [
                    {
                        "loc": ["publicly_visible"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_groundstation_put_replace_existing(self):
        gs_test_data2 = {
            "gs_name": "test_gs_name2",
            "gs_hostname": "test_host2",
            "gs_port": 2,
            "commission_date": "2000-02-01T00:00:00.01",
            "latitude": 2,
            "longitude": 2,
            "altitude": 2,
            "address": "2",
            "config": "none",
        }
        response = self.app.put(
            "/groundstation/" + self.gs_id,
            data=json.dumps(gs_test_data2),
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

        get_response = self.app.get(
            "/groundstation/" + self.gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in gs_test_data2:
            if key == "commission_date":
                self.assertEqual(
                    datetime.strptime(gs_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(gs_test_data2[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            elif key != "config":
                self.assertEqual(gs_data[key], gs_test_data2[key])

    def test_groundstation_put_null_blank_fields(self):
        gs_data_key_list = [
            "gs_hostname",
            "gs_port",
            "address",
        ]
        for gs_data_key in gs_data_key_list:
            del self.gs_test_data[gs_data_key]
        response = self.app.put(
            "/groundstation/" + self.gs_id,
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

        get_response = self.app.get(
            "/groundstation/" + self.gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in gs_data_key_list:
            self.assertEqual(gs_data[key], None)

    def test_groundstation_put_create_new(self):
        defined_gs_id = "g666666"
        response = self.app.put(
            "/groundstation/" + defined_gs_id,
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/" + defined_gs_id,
        )
        gs_data = json.loads(get_response.data)
        for key in self.gs_test_data:
            if key == "commission_date":
                self.assertEqual(
                    datetime.strptime(gs_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(self.gs_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                )
            elif key != "config":
                self.assertEqual(gs_data[key], self.gs_test_data[key])

    def test_groundstation_put_invalid_gs_id(self):
        defined_gs_id = "kreg"
        response = self.app.put(
            "/groundstation/" + defined_gs_id,
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "code": "GROUND_STATION_ID_PREFIX_INCORRECT",
            "msg": "gs_id must begin with 'g'",
        }

    def test_groundstation_put_bad_gs_id(self):
        response = self.app.put(
            "/groundstation/" + "".join(["a" for i in range(25)]),
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertTrue("too long," in json.loads(response.data)["msg"])
        response = self.app.put(
            "/groundstation/" + "π",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertTrue("must use only" in json.loads(response.data)["msg"])

    def test_groundstation_put_bad_datetime(self):
        self.gs_test_data["commission_date"] = "not_iso_8601"
        response = self.app.put(
            "/groundstation/" + self.gs_id,
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["commission_date"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

    def test_groundstation_put_bad_boolean_format(self):
        self.gs_test_data["publicly_visible"] = "not a bool"
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["publicly_visible"],
                        "msg": "value could not be parsed to a boolean",
                        "type": "type_error.bool",
                    }
                ]
            }
        }

    def test_groundstation_put_bad_latitude(self):
        for latitude_value in [90.1, -90.1, 190]:
            self.gs_test_data["latitude"] = latitude_value
            response = self.app.put(
                "/groundstation/{}".format(self.gs_id),
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )

            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "GROUND_STATION_INVALID_LATITUDE",
                "msg": "invalid latitude coords: -90 > lat > 90",
            }

    def test_groundstation_put_bad_longitude(self):
        for longitude_val in [180.1, -180.1]:
            self.gs_test_data["longitude"] = longitude_val
            response = self.app.put(
                "/groundstation/{}".format(self.gs_id),
                data=json.dumps(self.gs_test_data),
                content_type="application/json",
            )

            assert response.status_code == 400
            data: dict = response.json
            assert type(data) == dict
            assert data == {
                "code": "GROUND_STATION_INVALID_LONGITUDE",
                "msg": "invalid longitude coords: -180 > long > 180",
            }

    def test_groundstation_delete_valid(self):
        deletion_data = {"verify": True, "cascade": True}
        gs_id = GroundStation.query_by_hash(session, self.gs_id)._id
        response = self.app.delete(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(deletion_data),
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"Gs with ID '{self.gs_id}' deleted",
        }

        gs_obj = GroundStation.query_by_id(session, gs_id)
        self.assertEqual(gs_obj._deleted, True)
        self.assertTrue(gs_obj._deleted_at > (datetime.utcnow() - timedelta(minutes=5)))
        self.assertEqual(gs_obj._original_hash, self.gs_id)
        self.assertTrue(len(gs_obj._asset_hash) == 36)

    def test_groundstation_delete_get_single_missing(self):
        response = self.app.delete(
            "/groundstation/{}".format(self.gs_id),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_groundstation_delete_get_multiple_missing(self):
        response = self.app.delete(
            "/groundstation/{}".format(self.gs_id),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        get_response = self.app.get(
            "/groundstation",
        )
        self.assertEqual(get_response.status_code, 200)
        get_data = json.loads(get_response.data)["gs_results"]
        self.assertEqual(len(get_data), 0)

    def test_groundstation_delete_valid_with_status(self):
        status_data = {
            "update_time": datetime.utcnow().isoformat(),
            "status": "OK",
        }
        status_response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(status_data),
            content_type="application/json",
        )
        self.assertEqual(status_response.status_code, 201)

        gs_id = GroundStation.query_by_hash(session, self.gs_id)._id

        response = self.app.delete(
            "/groundstation/{}".format(self.gs_id),
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"Gs with ID '{self.gs_id}' deleted",
        }

        gs_obj = GroundStation.query_by_id(session, gs_id)
        self.assertEqual(gs_obj._deleted, True)
        self.assertTrue(gs_obj._deleted_at > (datetime.utcnow() - timedelta(minutes=5)))
        self.assertEqual(gs_obj._original_hash, self.gs_id)
        self.assertTrue(len(gs_obj._asset_hash) == 36)

        session.query(GsStatus).delete()

    def test_groundstation_delete_valid_with_telecom(self):
        telecom_data = {
            "telecom_name": "test_telecom_name_2",
            "frequency_centre": 200,
            "telecom_type": "test_type",
            "receives": True,
            "transmits": True,
        }
        telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(telecom_data),
            content_type="application/json",
        )
        self.assertEqual(telecom_response.status_code, 201)

        gs_id = GroundStation.query_by_hash(session, self.gs_id)._id
        response = self.app.delete(
            "/groundstation/{}".format(self.gs_id),
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "msg": f"Gs with ID '{self.gs_id}' deleted",
        }

        gs_obj = GroundStation.query_by_id(session, gs_id)
        self.assertEqual(gs_obj._deleted, True)
        self.assertTrue(gs_obj._deleted_at > (datetime.utcnow() - timedelta(minutes=5)))
        self.assertEqual(gs_obj._original_hash, self.gs_id)
        self.assertTrue(len(gs_obj._asset_hash) == 36)
