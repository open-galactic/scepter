import json
import unittest
from datetime import datetime

import pytest

from api import app
from api.database import session
from models import Asset, GroundStation, GsStatus, GsTelecom, Permission, Satellite


@pytest.mark.usefixtures("patch_auth")
class TestGroundstationStatus(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        gs_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.gs_id = json.loads(gs_response.data)["gs_id"]

        self.sat_test_data = {
            "sat_name": "test_sat_name",
            "description": "test_description",
        }
        sat_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            content_type="application/json",
        )
        self.sat_id = json.loads(sat_response.data)["sat_id"]

        self.gs_status_test_data = {
            "update_time": datetime.utcnow().isoformat(),
            "device": "test_device",
            "properties": {"property_key": "property_val"},
            "status": "OK",
            "sat_tracking": self.sat_id,
        }
        post_status_response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )

        assert post_status_response.status_code == 201

    def tearDown(self):
        session.query(GsStatus).delete()
        session.query(GsTelecom).delete()
        session.query(Permission).delete()
        session.query(GroundStation).delete()
        session.query(Satellite).delete()
        session.query(Asset).delete()
        session.commit()

    def test_gs_status_post_valid(self):
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        get_response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
        )
        gs_status_data = json.loads(get_response.data)
        for key in self.gs_status_test_data:
            if key == "update_time":
                self.assertEqual(
                    datetime.strptime(gs_status_data[key], "%Y-%m-%dT%H:%M:%S.%f"),
                    datetime.strptime(
                        self.gs_status_test_data[key], "%Y-%m-%dT%H:%M:%S.%f"
                    ),
                )
            elif key == "telecoms":
                self.assertEqual(
                    gs_status_data[key], json.loads(self.gs_status_test_data[key])
                )
            else:
                self.assertEqual(gs_status_data[key], self.gs_status_test_data[key])
        self.assertEqual(gs_status_data["gs_id"], self.gs_id)

    def test_gs_status_post_invalid_iso_datetime(self):
        self.gs_status_test_data["update_time"] = "not iso8601"
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["update_time"],
                        "msg": "invalid datetime format",
                        "type": "value_error.datetime",
                    }
                ]
            }
        }

    def test_gs_status_post_bad_properties_json(self):
        self.gs_status_test_data["properties"] = "not a json"
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )

        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["properties"],
                        "msg": "value is not a valid dict",
                        "type": "type_error.dict",
                    }
                ]
            }
        }

    def test_gs_status_post_bad_sat_id(self):
        self.gs_status_test_data["sat_tracking"] = "not_a_sat"
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )
        assert response.status_code == 404
        data = response.json
        assert data == {
            "code": "SATELLITE_NOT_FOUND",
            "msg": "Satellite not found",
        }

    def test_gs_status_get_single_valid(self):
        get_response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
        )

        assert get_response.status_code == 200

        gs_data = get_response.json
        assert gs_data["update_time"] == self.gs_status_test_data["update_time"]
        assert gs_data["device"] == self.gs_status_test_data["device"]
        assert gs_data["properties"] == self.gs_status_test_data["properties"]
        assert gs_data["status"] == self.gs_status_test_data["status"]
        assert gs_data["sat_tracking"] == self.gs_status_test_data["sat_tracking"]
        assert gs_data["gs_id"] == self.gs_id

    def test_gs_status_get_single_no_updates(self):
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        get_response = self.app.get(
            "/groundstation/{}/status".format(gs_id),
        )
        self.assertEqual(get_response.status_code, 200)
        data = get_response.json
        assert data == {
            "msg": "No status updates for that groundstation found",
        }

    def test_gs_status_post_bad_status(self):
        self.gs_status_test_data["status"] = "not_a_status"
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            content_type="application/json",
        )
        assert response.status_code == 400
        data = response.json
        assert data == {
            "validation_error": {
                "body_params": [
                    {
                        "loc": ["status"],
                        "msg": "Invalid status value.",
                        "type": "value_error",
                    }
                ]
            }
        }
