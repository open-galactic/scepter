import json
from http import HTTPStatus

from flask import Flask, make_response
from flask_cors import CORS  # type: ignore
from flask_restful import Api  # type: ignore

from api.config import config
from api.exceptions import (
    ConflictError,
    Error,
    ForbiddenError,
    NotFoundError,
    ValidationError,
)

app = Flask(__name__)
app.config.from_mapping(SECRET_KEY=config.secret_key)


# TODO extend this to httpexceptions to catch 500 and 404s, which return as html
@app.errorhandler(Error)
def scepter_error_handler(e):
    response = make_response()
    if isinstance(e, NotFoundError):
        status_code = HTTPStatus.NOT_FOUND
    elif isinstance(e, ValidationError):
        status_code = HTTPStatus.BAD_REQUEST
    elif isinstance(e, ConflictError):
        status_code = HTTPStatus.CONFLICT
    elif isinstance(e, ForbiddenError):
        status_code = HTTPStatus.FORBIDDEN
    else:
        status_code = HTTPStatus.INTERNAL_SERVER_ERROR

    response.data = json.dumps({"code": e.code.name, "msg": e.msg})
    response.content_type = "application/json"
    response.status_code = status_code
    return response


def create_api_resources(rest_api):
    from . import resources

    return rest_api


def configure_cors(app):
    cors = CORS(
        app,
        expose_headers=["Authorization", "Content-Type"],
        allow_headers=[
            "Access-Control-Allow-Headers",
            "Authorization",
            "Content-Type",
            "X-Amz-Security-Token",
        ],
    )

    return cors


api = create_api_resources(Api(app))
cors = configure_cors(app)
