from datetime import datetime
from typing import Optional, Union

from pydantic import BaseModel, Field, validator

from api.exceptions import ReasonCode, ValidationError
from api.schemas import groundstationtelecom


class Representation(BaseModel):
    gs_id: str = Field(alias="_asset_hash")
    gs_name: str
    gs_hostname: Optional[str]
    gs_port: Optional[int]
    commission_date: datetime
    latitude: Union[float, int]
    longitude: Union[float, int]
    altitude: Union[float, int]
    address: Optional[str]
    config: Optional[str]
    publicly_visible: bool
    telecoms: Optional[list[groundstationtelecom.Representation]]

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    publicly_visible: Optional[bool]

    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    page: int
    total_pages: int
    gs_results: list[Representation]


class CreateRequest(BaseModel):
    gs_name: str
    gs_hostname: Optional[str]
    gs_port: Optional[int]
    commission_date: datetime
    latitude: Union[float, int]
    longitude: Union[float, int]
    altitude: Union[float, int]
    address: Optional[str]
    config: Optional[str]
    publicly_visible: Optional[bool] = False

    @validator("latitude")
    def validate_latitude(cls, v):
        if not -90 <= v <= 90:
            raise ValidationError(ReasonCode.GROUND_STATION_INVALID_LATITUDE)
        return v

    @validator("longitude")
    def validate_longitude(cls, v):
        if not -180 <= v <= 180:
            raise ValidationError(ReasonCode.GROUND_STATION_INVALID_LONGITUDE)
        return v


class CreateResponse(BaseModel):
    msg: str
    gs_id: str


class PutRequest(BaseModel):
    gs_name: str
    gs_hostname: Optional[str]
    gs_port: Optional[int]
    commission_date: datetime
    latitude: Union[float, int]
    longitude: Union[float, int]
    altitude: Union[float, int]
    address: Optional[str]
    config: Optional[str]
    publicly_visible: Optional[bool] = False

    @validator("latitude")
    def validate_latitude(cls, v):
        if not -90 <= v <= 90:
            raise ValidationError(ReasonCode.GROUND_STATION_INVALID_LATITUDE)
        return v

    @validator("longitude")
    def validate_longitude(cls, v):
        if not -180 <= v <= 180:
            raise ValidationError(ReasonCode.GROUND_STATION_INVALID_LONGITUDE)
        return v


class MsgResponse(BaseModel):
    msg: str
