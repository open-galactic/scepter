from datetime import datetime
from typing import Any, Dict

from pydantic import BaseModel


class Base(BaseModel):
    args: Dict[str, Any]


class CreateRequest(BaseModel):
    update_time: datetime
    status: str

    class Config:
        extra = "forbid"
