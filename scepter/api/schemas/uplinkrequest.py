from datetime import datetime
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, Field, validator


class Context(BaseModel):
    command_name: str
    script_line_number: Optional[int]


class Representation(BaseModel):
    id: UUID
    created_at: datetime
    approved: Optional[bool]
    reviewed_at: Optional[datetime]
    reviewed_by: Optional[str]
    sat_id: str
    contact_id: UUID
    instruction_id: int = Field(alias="instruction_id_repr")
    context: Context

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    approved: Optional[bool]
    reviewed: Optional[bool]
    sat_id: Optional[List[str]]

    class Config:
        extra = "forbid"

    @validator("sat_id")
    def limit_sat_ids_length(cls, v):
        if len(v) > 50:
            raise ValueError("number of sat_ids must be less than 10")
        return v


class ListResponse(BaseModel):
    uplink_requests: List[Representation]


class GetResponse(BaseModel):
    uplink_request: Representation


class ApproveResponse(BaseModel):
    msg: str
    code: str
    uplink_request: Representation


class DenyResponse(BaseModel):
    msg: str
    code: str
    uplink_request: Representation


class CreateRequest(BaseModel):
    contact_id: UUID
    command_id: int
    context: Context

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    code: str
    msg: str
    uplink_request: Representation
