from datetime import datetime
from typing import Any, Dict, List, Optional
from uuid import UUID

from pydantic import BaseModel, validator

from api.exceptions import ReasonCode, ValidationError


class Base(BaseModel):
    args: Dict[str, Any]


class Representation(BaseModel):
    id: UUID
    metadata_id: UUID
    faulted_at: datetime
    created_at: datetime
    error_code: str
    value: str

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    sat_ids: Optional[List[str]]
    faulted_time_start: Optional[datetime]
    faulted_time_end: Optional[datetime]
    error_code: Optional[str]

    @validator("faulted_time_end")
    def end_time_after_start_time(cls, v, values):
        if (start := values.get("faulted_time_start")) is not None:
            if v < start:
                raise ValidationError(ReasonCode.END_TIME_BEFORE_START_TIME)
        return v

    @validator("sat_ids")
    def sat_id_limit(cls, v):
        if len(v) > 20:
            raise ValidationError(ReasonCode.SATELLITE_TOO_MANY_IDS)
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    alerts: List[Representation]
