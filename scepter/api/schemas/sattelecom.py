from typing import Optional

from pydantic import BaseModel, Field, validator


class Representation(BaseModel):
    sat_id: str = Field(alias="sat_id_repr")
    frequency: int
    telecom_name: str
    telecom_type: str
    modulations: Optional[dict[str, list[int]]]
    transmits: bool
    receives: bool
    encrypted: bool

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    telecom_name: Optional[str]
    telecom_type: Optional[str]
    modulations: Optional[dict]
    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    page: int
    total_pages: int
    telecom_results: list[Representation]


class CreateRequest(BaseModel):
    frequency: int
    telecom_name: str
    telecom_type: str
    modulations: Optional[dict[str, list[int]]]
    transmits: bool
    receives: bool
    encrypted: Optional[bool] = False

    @validator("telecom_name")
    def telecom_name_valid(cls, v):

        if len(v) > 255:
            raise ValueError("Telecom name too long, limit to 255 chars")
        try:
            v.encode("ascii")
        except UnicodeEncodeError:
            raise ValueError("Telecom name must use only the ascii character set")
        return v

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    msg: str
    sat_id: str
    telecom_name: str


class PutRequest(BaseModel):
    frequency: int
    telecom_type: str
    modulations: Optional[dict[str, list[int]]]
    transmits: bool
    receives: bool
    encrypted: Optional[bool] = False

    class Config:
        extra = "forbid"


class PutResponse(BaseModel):
    msg: str
