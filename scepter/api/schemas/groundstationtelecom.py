from typing import Optional

from pydantic import BaseModel, Field, root_validator, validator

from models.gstelecom import find_ieee_radio_band


class Representation(BaseModel):
    gs_id: str = Field(alias="gs_id_repr")
    frequency_lower: Optional[int]
    frequency_centre: int
    frequency_upper: Optional[int]
    telecom_name: str
    telecom_type: str
    antenna_gain: Optional[float]
    modulations: Optional[dict[str, list[int]]]
    rotatable: Optional[bool]
    beamwidth: Optional[float]
    noise_factor: Optional[float]
    transmits: bool
    receives: bool
    ieee_band: Optional[list[str]]
    address: Optional[str]
    port: Optional[int]

    # root validator to create ieee_band
    @root_validator
    def compute_ieee_band(cls, values):

        frequencies = []

        if values.get("frequency_lower"):
            frequencies.append(values["frequency_lower"])
        if values.get("frequency_upper"):
            frequencies.append(values["frequency_upper"])
        if not frequencies:
            frequencies.append(values["frequency_centre"])

        values["ieee_band"] = find_ieee_radio_band(frequencies)

        return values

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    telecom_name: Optional[str]
    telecom_type: Optional[str]

    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    page: int
    total_pages: int
    telecom_results: list[Representation]


class CreateRequest(BaseModel):
    frequency_centre: int
    frequency_lower: Optional[int]
    frequency_upper: Optional[int]
    telecom_name: str
    telecom_type: str
    antenna_gain: Optional[float]
    modulations: Optional[dict[str, list[int]]]
    rotatable: Optional[bool]
    beamwidth: Optional[float]
    noise_factor: Optional[float]
    transmits: bool
    receives: bool
    address: Optional[str]
    port: Optional[int]

    @validator("frequency_lower")
    def frequency_lower_valid(cls, field_value, values):
        if field_value > values["frequency_centre"]:
            raise ValueError(
                "Invalid frequency specification. Require lower <= centre <= upper"
            )
        return field_value

    @validator("frequency_upper")
    def frequency_upper_valid(cls, field_value, values):
        if field_value < values["frequency_centre"]:
            raise ValueError(
                "Invalid frequency specification. Require lower <= centre <= upper"
            )
        return field_value

    @validator("telecom_name")
    def telecom_name_valid(cls, v):

        if len(v) > 255:
            raise ValueError("Telecom name too long, limit to 255 chars")
        try:
            v.encode("ascii")
        except UnicodeEncodeError:
            raise ValueError("Telecom name must use only the ascii character set")
        return v

    class Config:
        extra = "forbid"


class PostResponse(BaseModel):
    msg: str
    gs_id: str
    telecom_name: str


class PutRequest(BaseModel):
    frequency_centre: int
    frequency_lower: Optional[int]
    frequency_upper: Optional[int]
    telecom_type: str
    antenna_gain: Optional[float]
    modulations: Optional[dict[str, list[int]]]
    rotatable: Optional[bool]
    beamwidth: Optional[float]
    noise_factor: Optional[float]
    transmits: bool
    receives: bool
    address: Optional[str]
    port: Optional[int]

    @validator("frequency_lower")
    def frequency_lower_valid(cls, field_value, values):
        if field_value > values["frequency_centre"]:
            raise ValueError(
                "Invalid frequency specification. Require lower <= centre <= upper"
            )
        return field_value

    @validator("frequency_upper")
    def frequency_upper_valid(cls, field_value, values):
        if field_value < values["frequency_centre"]:
            raise ValueError(
                "Invalid frequency specification. Require lower <= centre <= upper"
            )
        return field_value

    class Config:
        extra = "forbid"


class MsgResponse(BaseModel):
    msg: str
