from typing import Any, Dict

from pydantic import BaseModel


class Representation(BaseModel):
    id: str
    name: str
    args: Dict[str, Any]
    entrypoint: str

    class Config:
        orm_mode = True


class UpdateRequest(BaseModel):
    name: str
    args: Dict[str, Any]
    entrypoint: str

    class Config:
        extra = "forbid"


class UpdateResponse(BaseModel):
    msg: str
    code: str
    script: Representation
