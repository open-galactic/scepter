from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, validator


class Representation(BaseModel):

    gs_id: str = Field(alias="gs_id_repr")
    update_time: datetime
    device: Optional[str]
    properties: Optional[dict]
    status: str
    sat_tracking: Optional[str] = Field(alias="sat_id_repr")

    class Config:
        orm_mode = True


class PostRequest(BaseModel):

    update_time: datetime
    device: Optional[str]
    properties: Optional[dict]
    status: str
    errors: Optional[str]
    sat_tracking: Optional[str]

    @validator("status")
    def status_valid(cls, v):
        if v not in ["OK", "DEGRADED", "ERROR", "OFFLINE"]:
            raise ValueError("Invalid status value.")
        return v

    class Config:
        extra = "forbid"


class PostResponse(BaseModel):

    msg: str
    gs_id: str
