from datetime import datetime
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

from pydantic import BaseModel, Field, validator

from api.exceptions import ReasonCode, ValidationError


class BaseResponse(BaseModel):
    code: str
    msg: str


class Overpass(BaseModel):
    id: UUID
    sat_id: str = Field(alias="sat_id_repr")
    gs_id: str = Field(alias="gs_id_repr")
    AOS: datetime = Field(alias="aos")
    TCA: datetime = Field(alias="tca")
    LOS: datetime = Field(alias="los")
    maximum_el: float
    tle_updated_at: datetime

    class Config:
        orm_mode = True


class Script(BaseModel):
    id: int
    script_id: str = Field(alias="script_id_repr")
    entrypoint: str
    type: str
    sequence_number: int
    started_at: Optional[datetime]
    ended_at: Optional[datetime]
    log: Optional[str]
    args: dict

    class Config:
        orm_mode = True


class Command(BaseModel):
    id: int
    command_id: str = Field(alias="command_id_repr")
    entrypoint: str
    type: str
    sequence_number: int
    started_at: Optional[datetime]
    ended_at: Optional[datetime]
    log: Optional[str]
    args: dict

    class Config:
        orm_mode = True


class IssueScript(BaseModel):
    script_id: str
    args: Dict[str, Any]
    sequence_number: int


class IssueCommand(BaseModel):
    command_id: str
    args: Dict[str, Any]
    sequence_number: int


class CreateRequest(BaseModel):
    instructions: List[Union[IssueScript, IssueCommand]]
    overpass_id: Optional[UUID]
    aos: Optional[datetime]
    los: Optional[datetime]
    gs_id: Optional[str]
    issued_at: datetime
    supersedes: Optional[UUID]

    class Config:
        extra = "forbid"

    @validator("aos")
    def aos_in_past(cls, v, values):
        if v is not None and values.get("overpass_id") is not None:
            raise ValidationError(ReasonCode.OVERPASS_CANNOT_CREATE_AND_REFERENCE)
        if v is not None and v < datetime.utcnow():
            raise ValidationError(ReasonCode.OVERPASS_AOS_IN_PAST)
        return v

    @validator("los")
    def los_after_aos(cls, v, values):
        if v is not None and values.get("overpass_id") is not None:
            raise ValidationError(ReasonCode.OVERPASS_CANNOT_CREATE_AND_REFERENCE)
        aos = values.get("aos")
        if not aos:
            raise ValidationError(ReasonCode.OVERPASS_LOS_WITHOUT_AOS)
        if aos > v:
            raise ValidationError(ReasonCode.OVERPASS_LOS_AFTER_AOS)
        return v

    @validator("gs_id", always=True)
    def gs_id_sent_if_not_overpass_id(cls, v, values):
        if v is not None and values.get("overpass_id") is not None:
            raise ValidationError(ReasonCode.OVERPASS_CANNOT_CREATE_AND_REFERENCE)
        if v is None and values.get("overpass_id") is None:
            raise ValidationError(ReasonCode.OVERPASS_MISSING_GS_OR_OVERPASS_ID)
        return v


class CreateResponse(BaseResponse):
    id: UUID


class Representation(BaseModel):
    id: UUID
    created_at: datetime
    overpass: Overpass
    issued_by: str
    issued_at: datetime
    cancelled_at: Optional[datetime]
    dispatched_at: Optional[datetime]
    instructions: List[Union[Command, Script]]

    class Config:
        orm_mode = True


class Cursor(BaseModel):
    id: UUID

    class Config:
        extra = "forbid"


class ListRequest(BaseModel):
    upcoming: Optional[bool]
    include_cancelled: Optional[bool]
    cursor: Optional[str]
    page_size: int = 100

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    contacts: List[Representation]
    cursor: Optional[str]
    total_pages: int


class HistoryResponse(BaseModel):
    history: List[Representation]


class CancelResponse(BaseResponse):
    contact_id: UUID
    cancelled_at: datetime


class DispatchResponse(BaseResponse):
    contact_id: UUID
    dispatched_at: datetime


class ExecuteStartRequest(BaseModel):
    started_at: Optional[datetime]

    class Config:
        extra = "forbid"


class ExecuteStartResponse(BaseResponse):
    instruction_id: int
    started_at: datetime


class ExecuteEndRequest(BaseModel):
    ended_at: Optional[datetime]
    log: Optional[str]

    class Config:
        extra = "forbid"


class ExecuteEndResponse(BaseResponse):
    instruction_id: int
    ended_at: datetime
