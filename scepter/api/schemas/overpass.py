from datetime import datetime
from typing import Any, Dict, List, Optional
from uuid import UUID

from pydantic import BaseModel, Field, validator

MAX_REQUESTED_GS_COMPARISONS = 25
MAX_REQUESTED_SAT_COMPARISONS = 5


class Representation(BaseModel):
    overpass_id: UUID = Field(alias="id")
    sat_id: str = Field(alias="sat_id_repr")
    gs_id: str = Field(alias="gs_id_repr")
    rise_time: datetime = Field(alias="aos")
    maxima_time: datetime = Field(alias="tca")
    set_time: datetime = Field(alias="los")
    maxima_el: float = Field(alias="maximum_el")
    leapfrog_path: Dict[str, Any] = Field(alias="trajectory")
    tle_update_time: datetime = Field(alias="tle_updated_at")

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    sat_id: list[str]
    start_time: Optional[datetime] = datetime.utcnow()
    end_time: Optional[datetime]
    gs_id: Optional[list[str]]

    @validator("gs_id")
    def gs_id_valid(cls, v):
        if len(v) > MAX_REQUESTED_GS_COMPARISONS:
            raise ValueError(f"Number of requested ground stations may not exceed {MAX_REQUESTED_GS_COMPARISONS}")
        return v

    @validator("sat_id")
    def sat_id_valid(cls, v):
        if len(v) > MAX_REQUESTED_SAT_COMPARISONS:
            raise ValueError(f"Number of requested satellites may not exceed {MAX_REQUESTED_SAT_COMPARISONS}")
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    __root__: List[Representation]
