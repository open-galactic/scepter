from typing import Optional

from pydantic import BaseModel, Field, validator


class Representation(BaseModel):

    file_type: str
    asset_hash: str
    file_key: str = Field(alias="_file_key")
    file_size: float = Field(alias="_file_size")

    @validator("file_key")
    def strip_file_key(cls, v):
        return v[v.find("/") + 1 :]  # noqa: E203

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    file_type: Optional[str]
    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    page: int
    total_pages: int
    files: list[Representation]
