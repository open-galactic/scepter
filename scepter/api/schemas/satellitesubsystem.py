from typing import Literal, Optional

from pydantic import BaseModel, Field


class Representation(BaseModel):
    name: str
    address: str
    system_name: Optional[str]

    class Config:
        orm_mode = True


class Component(BaseModel):
    name: str
    type: Literal["component"]
    address: Optional[str]

    class Config:
        orm_mode = True


class System(BaseModel):
    name: str
    type: Literal["system"]
    address: Optional[str]
    components: list[Component] = Field(alias="children")

    class Config:
        orm_mode = True


class ListResponse(BaseModel):
    systems: list[System]


class CreateRequest(BaseModel):
    name: str
    address: str
    system_name: Optional[str]

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    msg: str
    code: str
    sat_id: str
    subsystem: Representation
