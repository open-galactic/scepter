import uuid
from typing import Optional, Union

from pydantic import BaseModel, Field, root_validator

from api.exceptions import Error


class Representation(BaseModel):

    id: Optional[str] = Field(alias="user_id")
    read_access: bool
    command_access: bool
    manage_asset: bool
    manage_users: bool
    owner: bool
    asset_id: str = Field(alias="asset_id_repr")
    user_id: Optional[str]
    user_uuid: Optional[uuid.UUID]

    @root_validator
    def set_user_id(cls, values):

        if values.get("user_id"):
            values["id"] = str(values["user_id"])

        if values.get("user_uuid"):
            values["id"] = str(values["user_uuid"])

        if not values.get("id"):
            raise Error(
                f"User ID permission not set for asset_id: {values['asset_id']}"
            )

        del values["user_id"]
        del values["user_uuid"]

        return values

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    user_id: Optional[Union[uuid.UUID, int]]

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    __root__: list[Representation]

    class Config:
        orm_mode = True


class CreateRequest(BaseModel):

    read_access: Optional[bool] = False
    command_access: Optional[bool] = False
    manage_asset: Optional[bool] = False
    manage_users: Optional[bool] = False
    owner: Optional[bool] = False

    class Config:
        extra = "forbid"


class MsgResponse(BaseModel):
    msg: str
