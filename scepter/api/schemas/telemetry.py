from datetime import datetime
from typing import Any, Dict, Optional, Union
from uuid import UUID

from pydantic import BaseModel, Field, validator

from api.exceptions import ReasonCode, ValidationError


class Base(BaseModel):
    args: Dict[str, Any]


class ListRequest(BaseModel):
    observation_time_start: Optional[datetime]
    observation_time_end: Optional[datetime]
    uuid: Optional[list[UUID]]
    system: Optional[str]
    tlm_page: Optional[int] = 1
    data_page_size: Optional[int] = 100
    page_size: Optional[int] = 50
    data_cursor: Optional[str]

    @validator("data_page_size")
    def data_page_size_valid(cls, v):
        if not (0 < v < 250):
            return 250
        return v

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    @validator("observation_time_end")
    def end_time_after_start_time(cls, v, values):
        if (start := values.get("observation_time_start")) is not None:
            if v < start:
                raise ValidationError(ReasonCode.END_TIME_BEFORE_START_TIME)
        return v

    class Config:
        extra = "forbid"


class ListLatestRequest(BaseModel):
    system: Optional[str]
    component: Optional[str]
    tlm_name: Optional[list[str]]
    tlm_id: Optional[UUID]
    page_size: Optional[int] = 1000
    cursor: Optional[str]

    class Config:
        extra = "forbid"

    @validator("page_size")
    def limit_max_page_size(v):
        if v > 5000:
            return 5000
        return v


class ListLatestRepresentation(BaseModel):
    observed_at: datetime = Field(alias="observation_time")
    tlm_id: UUID = Field(alias="uuid")
    tlm_name: str
    component: Optional[str] = Field(alias="component_name")
    system: Optional[str] = Field(alias="system_name")
    value: Any

    class Config:
        from_orm = True

    @validator("system", always=True)
    def swap_component_to_system(v, values):
        if not v and values["component"]:
            v = values["component"]
            values["component"] = None
        return v


class ListLatestResponse(BaseModel):
    cursor: Optional[str]
    total_pages: int
    telemetry: list[ListLatestRepresentation]


class LatestCursor(BaseModel):
    id: UUID

    class Config:
        extra = "forbid"


class HistoryRepresentation(BaseModel):
    observed_at: datetime = Field(alias="observation_time")
    value: Union[float, str, int, bool]

    class Config:
        orm_mode = True


class HistoryCursor(BaseModel):
    observation_time: datetime
    value: str

    class Config:
        extra = "forbid"


class HistoryRequest(BaseModel):
    observed_at_start: Optional[datetime]
    observed_at_end: Optional[datetime]
    cursor: Optional[str]
    page_size: int = 1000

    @validator("observed_at_end")
    def end_time_after_start_time(cls, v, values):
        if (start := values.get("observed_at_start")) is not None:
            if v < start:
                raise ValidationError(ReasonCode.END_TIME_BEFORE_START_TIME)
        return v

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 5000):
            return 5000
        return v

    class Config:
        extra = "forbid"


class HitoryResponse(BaseModel):
    total_pages: int
    cursor: Optional[str]
    telemetry: list[HistoryRepresentation]
