from typing import Any, Dict, Optional

from pydantic import BaseModel


class Representation(BaseModel):
    id: str
    name: str
    args: Dict[str, Any]
    entrypoint: str
    code: str
    requires_approval: bool
    encoder_data: Optional[Dict[str, Any]]

    class Config:
        orm_mode = True


class UpdateRequest(BaseModel):
    name: str
    args: Dict[str, Any]
    entrypoint: str
    code: str
    requires_approval: bool
    encoder_data: Optional[Dict[str, Any]]

    class Config:
        extra = "forbid"


class UpdateResponse(BaseModel):
    msg: str
    code: str
    command: Representation
