import logging
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, validator

from api.exceptions import ReasonCode, ValidationError

logger = logging.getLogger(__name__)


def validate_tle_json(tle: dict):

    for key in [
        "norad_cat_id",
        "tle_line_0",
        "tle_line_1",
        "tle_line_2",
        "last_updated",
        "object_type",
    ]:
        value = tle.get(key)
        if not value:
            logger.debug("field missing from tle json", extra={"field": key})
            raise ValidationError(ReasonCode.SATELLITE_TLE_MISSING_FIELD)
        if len(str(value)) > 120:
            raise ValidationError(ReasonCode.SATELLITE_TLE_FIELD_TOO_LONG)


class Representation(BaseModel):
    sat_id: str = Field(alias="_asset_hash")
    sat_name: str
    norad_id: Optional[int]
    nssdc_id: Optional[str]
    encoder: Optional[str]
    decoder: Optional[str]
    tle: Optional[dict]
    launch_date: Optional[datetime]
    commission_date: Optional[datetime]
    description: str
    config: Optional[dict]
    publicly_visible: bool

    class Config:
        orm_mode = True


class ListRequest(BaseModel):
    publicly_visible: Optional[bool]

    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 100):
            return 100
        return v

    class Config:
        extra = "forbid"


class ListResponse(BaseModel):
    page: int
    total_pages: int
    sat_results: list[Representation]


class CreateRequest(BaseModel):
    sat_name: str
    norad_id: Optional[int]
    nssdc_id: Optional[str]
    encoder: Optional[str]
    decoder: Optional[str]
    tle: Optional[dict]
    launch_date: Optional[datetime]
    commission_date: Optional[datetime]
    description: str
    config: Optional[dict]
    publicly_visible: Optional[bool] = False

    @validator("sat_name")
    def validate_sat_name(cls, v):
        if len(v) > 256:
            raise ValidationError(ReasonCode.SATELLITE_NAME_TOO_LONG)
        return v

    @validator("encoder")
    def validate_encoder(cls, v):
        if len(v) >= 64:
            raise ValidationError(ReasonCode.SATELLITE_ENCODER_NAME_TOO_LONG)
        return v

    @validator("decoder")
    def validate_decoder(cls, v):
        if len(v) >= 64:
            raise ValidationError(ReasonCode.SATELLITE_DECODER_NAME_TOO_LONG)
        return v

    @validator("tle")
    def validate_tle(cls, v):
        validate_tle_json(v)
        return v

    @validator("description")
    def validate_description(cls, v):
        if len(v) >= 1024:
            raise ValidationError(ReasonCode.SATELLITE_DESCRIPTION_TOO_LONG)
        return v

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    msg: str
    sat_id: str
