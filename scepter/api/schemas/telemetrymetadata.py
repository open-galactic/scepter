from typing import Any, Dict, Optional
from uuid import UUID

from pydantic import BaseModel, validator


class Base(BaseModel):
    args: Dict[str, Any]


class ListRequest(BaseModel):
    tlm_name: Optional[str]
    system: Optional[str]
    component: Optional[str]
    uuid: Optional[str]
    cad_id: Optional[str]
    has_cad_id: Optional[bool]
    page: Optional[int] = 1
    page_size: Optional[int] = 50

    @validator("uuid")
    def uuid_valid(cls, v):
        try:
            UUID(v).version
        except ValueError:
            raise ValueError("value is not a valid uuid")
        return v

    @validator("component")
    def component_exists_with_system(cls, field_value, values):
        if values.get("system") is None:
            raise ValueError("Component nominated without system key")
        return field_value

    @validator("page_size")
    def page_size_valid(cls, v):
        if not (0 < v < 2000):
            return 2000
        return v

    class Config:
        extra = "forbid"
