from typing import Any, Dict, List
from uuid import UUID

from pydantic import BaseModel


class Base(BaseModel):
    name: str
    args: Dict[str, Any]


class Representation(Base):
    id: UUID

    class Config:
        orm_mode = True


class UpdateRequest(Base):
    pass

    class Config:
        extra = "forbid"


class UpdateResponse(BaseModel):
    code: str
    msg: str
    preset: Representation


class CreateRequest(Base):
    pass

    class Config:
        extra = "forbid"


class CreateResponse(BaseModel):
    code: str
    msg: str
    preset: Representation


class ListResponse(BaseModel):
    presets: List[Representation]


class DeleteResponse(BaseModel):
    code: str
    msg: str
