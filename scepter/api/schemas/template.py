from typing import Any, Dict, List, Literal, Optional

from pydantic import BaseModel, validator


class Representation(BaseModel):
    id: str
    args: Dict[str, Any]
    entrypoint: str
    type: Literal["command", "script"]

    class Config:
        orm_mode = True


class Cursor(BaseModel):
    id: str

    class Config:
        extra = "forbid"


class ListRequest(BaseModel):
    cursor: Optional[str]
    page_size: int = 100
    type: Optional[Literal["command", "script"]]

    class Config:
        extra = "forbid"

    @validator("page_size")
    def limit_max_page_size(v):
        if v > 5000:
            return 5000
        return v


class ListResponse(BaseModel):
    templates: List[Representation]
    cursor: Optional[str]
    total_pages: int
