import uuid
from http import HTTPStatus
from typing import Union

from flask_pydantic import validate

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import ConflictError, NotFoundError, ReasonCode
from api.schemas import permissions as permissionschema
from models import Asset, Permission


@app.route("/permissions/<asset_hash>", methods=["GET"])
@auth_endpoint("administrate")
@validate()
def list_permissions(asset_hash: str, query: permissionschema.ListRequest):

    if not (asset := Asset.query_by_hash(reader_session, asset_hash)):
        raise NotFoundError(ReasonCode.ASSET_NOT_FOUND)

    permission_list = Permission.select_permissions_for_asset(query.user_id, asset)

    return permissionschema.ListResponse(__root__=permission_list), HTTPStatus.OK


@app.route("/permissions/user/<user_id>/asset/<asset_hash>", methods=["GET"])
@auth_endpoint("administrate")
@validate()
def get_permissions(
    user_id: Union[uuid.UUID, int],
    asset_hash: str,
):

    if not (asset := Asset.query_by_hash(reader_session, asset_hash)):
        raise NotFoundError(ReasonCode.ASSET_NOT_FOUND)

    if not (per := Permission.query_by_user_and_asset_id(session, user_id, asset._id)):
        raise NotFoundError(ReasonCode.PERMISSION_NOT_FOUND)

    return permissionschema.Representation.from_orm(per), HTTPStatus.OK


@app.route("/permissions/user/<user_id>/asset/<asset_hash>", methods=["POST"])
@auth_endpoint("administrate")
@validate()
def create_permissions(
    user_id: Union[uuid.UUID, int],
    asset_hash: str,
    body: permissionschema.CreateRequest,
):

    if not (asset := Asset.query_by_hash(session, asset_hash)):
        raise NotFoundError(ReasonCode.ASSET_NOT_FOUND)

    if Permission.query_by_user_and_asset_id(session, user_id, asset._id):
        raise ConflictError(ReasonCode.PERMISSION_ALREADY_EXIST)

    if type(user_id) == int:
        permission = Permission(
            asset_id=asset._id,
            user_id=user_id,
            read_access=body.read_access,
            command_access=body.command_access,
            manage_asset=body.manage_asset,
            manage_users=body.manage_users,
            owner=body.owner,
        )
    elif type(user_id) == uuid.UUID:
        permission = Permission(
            asset_id=asset._id,
            user_uuid=user_id,
            read_access=body.read_access,
            command_access=body.command_access,
            manage_asset=body.manage_asset,
            manage_users=body.manage_users,
            owner=body.owner,
        )

    session.add(permission)

    return (
        permissionschema.MsgResponse(
            msg=f"permission for {user_id} to asset {asset_hash} added"
        ),
        HTTPStatus.CREATED,
    )


@app.route("/permissions/user/<user_id>/asset/<asset_hash>", methods=["PUT"])
@auth_endpoint("administrate")
@validate()
def put_permissions(
    user_id: Union[uuid.UUID, int],
    asset_hash: str,
    body: permissionschema.CreateRequest,
):

    if not (asset := Asset.query_by_hash(session, asset_hash)):
        raise NotFoundError(ReasonCode.ASSET_NOT_FOUND)

    if per := Permission.query_by_user_and_asset_id(session, user_id, asset._id):
        per.asset_id = asset._id

        if type(user_id) == int:
            per.user_id = user_id
        if type(user_id) == uuid.UUID:
            per.user_uuid = user_id

        per.read_access = body.read_access
        per.command_access = body.command_access
        per.manage_asset = body.manage_asset
        per.manage_users = body.manage_users
        per.owner = body.owner

        return (
            permissionschema.MsgResponse(
                msg=f"permission for {user_id} to asset {asset_hash} updated"
            ),
            HTTPStatus.OK,
        )

    if type(user_id) == int:
        permission = Permission(
            asset_id=asset._id,
            user_id=user_id,
            read_access=body.read_access,
            command_access=body.command_access,
            manage_asset=body.manage_asset,
            manage_users=body.manage_users,
            owner=body.owner,
        )
    elif type(user_id) == uuid.UUID:
        permission = Permission(
            asset_id=asset._id,
            user_uuid=user_id,
            read_access=body.read_access,
            command_access=body.command_access,
            manage_asset=body.manage_asset,
            manage_users=body.manage_users,
            owner=body.owner,
        )
    session.add(permission)

    return (
        permissionschema.MsgResponse(
            msg=f"permission for {user_id} to asset {asset_hash} added"
        ),
        HTTPStatus.CREATED,
    )
