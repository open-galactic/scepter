import logging
from http import HTTPStatus
from typing import List, Tuple, Union
from uuid import UUID

from flask_pydantic import validate  # type: ignore
from sqlalchemy.orm import Session

from api import app
from api.database import session
from api.dependencies.auth_handler import (
    auth_endpoint,
    check_users_access_to_asset,
    get_users_assets,
)
from api.exceptions import ReasonCode
from api.schemas import alert as alertschema
from models import Alert, Satellite

logger = logging.getLogger(__name__)


def _authorize_satellite_list(
    session: Session,
    sat_ids: List[str],
    user_id: Union[UUID, int],
) -> Tuple[List[int], List[str]]:
    """
    accepts a list of sat hashes, and converts them into a list of satellites.
    Then it looks for whether the user has command access for any of them, and
    returns a list of satellite ids, and unknown satellite hashes
    """
    # convert the list of sat hashes into numeric ids
    satellites = [Satellite.query_by_hash(session, x) for x in sat_ids]

    # check the permissions for each satellite, returning false for any unknowns
    permissions = [
        check_users_access_to_asset(user_id, sat._id, "read_access") if sat else False
        for sat in satellites
    ]

    # produce a list of sat hashes that don't match a satellite, or the user
    # doesn't have access to
    unknown_ids = [
        sat_id for sat_id, approved in zip(sat_ids, permissions) if approved is False
    ]
    return satellites, unknown_ids


def _get_user_readable_satellites(user_id: Union[UUID, int]):
    """
    retrieve a list of all satellite ids the user has command access for
    """
    assets = get_users_assets(user_id, "read_access")
    return [x for x in assets if x._asset_type == "satellite"]


@app.route("/alerts", methods=["GET"])
@auth_endpoint()
@validate()
def list_alerts(query: alertschema.ListRequest, **kwargs):
    """
    retrieve a list of alerts, with filters for blocking vs historic rows
    """
    if query.sat_ids:
        satellites, unknown_ids = _authorize_satellite_list(
            session, query.sat_ids, kwargs["authed_user_id"]
        )
        if unknown_ids:
            return {
                "code": ReasonCode.SATELLITE_NOT_FOUND.name,
                "msg": ReasonCode.SATELLITE_NOT_FOUND.value,
                "unknown_sat_ids": unknown_ids,
            }, HTTPStatus.NOT_FOUND
    else:
        satellites = _get_user_readable_satellites(kwargs["authed_user_id"])
    alerts = Alert.list_for_sats(
        session,
        satellites,
        query.faulted_time_start,
        query.faulted_time_end,
        query.error_code,
    )
    response = alertschema.ListResponse(alerts=alerts)
    return response, HTTPStatus.OK
