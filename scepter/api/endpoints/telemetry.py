import logging
import os
import re
from binascii import Error as BinAsciiError
from datetime import datetime
from http import HTTPStatus
from math import ceil
from zlib import error as ZlibError

from cbor2.types import CBORDecodeError, CBORDecodeValueError  # type: ignore
from dateutil.parser import isoparse  # type: ignore
from flask import request
from flask_pydantic import validate  # type: ignore
from pydantic import ValidationError as PydValidationError

from api import app
from api.database import reader_session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import telemetry as telemetryschema
from api.utils.utils import PageCursor
from models import Satellite, Subsystem, Telemetry, TelemetryMetadata

ABS_PATH = os.path.abspath(os.path.dirname(__file__))

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/telemetry/<uuid>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_historical_telemetry(
    sat_id: str, uuid: str, query: telemetryschema.HistoryRequest
):
    """
    Resource for retrieving historical telemetry for a specified telemetry metadata uuid
    """
    if not (
        telemetry_metadata := TelemetryMetadata.query_by_uuid(reader_session, uuid)
    ):
        raise NotFoundError(ReasonCode.TELEMETRY_METADATA_NOT_FOUND)

    cursor_telemetry = None
    if query.cursor:
        try:
            cursor = telemetryschema.HistoryCursor.parse_obj(
                PageCursor.decode_cursor(query.cursor)
            )
        except (PydValidationError, CBORDecodeError, BinAsciiError, ZlibError) as e:
            # Cursor doesn't decode, or doesn't deserialize into sat_id and name
            logger.info("failed to decode cursor", extra={"tracelog": e})
            raise ValidationError(ReasonCode.INVALID_CURSOR)
        cursor_telemetry = Telemetry.get_by_observation_time(
            telemetry_metadata, cursor.observation_time
        )
        cursor_telemetry = [
            telem for telem in cursor_telemetry if str(telem.value) == cursor.value
        ][0]
        if not cursor_telemetry:
            logger.info(
                "cursor contains unknown telemetry references",
                extra={
                    "metadata_uuid": uuid,
                    "observation_time": cursor.observation_time,
                    "value": cursor.value,
                },
            )
            raise ValidationError(ReasonCode.INVALID_CURSOR)

    telemetry_data, row_count = Telemetry.get_historical_telemetry(
        tlm_meta=telemetry_metadata,
        observation_time_start=query.observed_at_start,
        observation_time_end=query.observed_at_end,
        cursor=cursor_telemetry,
        page_size=query.page_size,
    )

    if telemetry_data:
        # create cursor using last value in the list of templates
        cursor_str = PageCursor.create_cursor(
            {
                "observation_time": telemetry_data[-1].observation_time.isoformat(),
                "value": telemetry_data[-1].value,
            }
        )
    else:
        # no results, return null for the cursor
        logger.debug("no results to create cursor with")
        cursor_str = None

    response = telemetryschema.HitoryResponse(
        telemetry=telemetry_data,
        cursor=cursor_str,
        total_pages=ceil(row_count / query.page_size),
    )
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/legacytelemetry", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_telemetry(sat_id: str, query: telemetryschema.ListRequest):
    """
    Retrieve the set of telemetry for each satellite
    """
    sat: Satellite
    if not (sat := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    system = None
    if query.system and not (system := Subsystem.get_system_by_name(sat, query.system)):
        raise NotFoundError(ReasonCode.SYSTEM_NOT_FOUND)

    cursor: dict = None
    if query.data_cursor:
        try:
            cursor = PageCursor.decode_cursor(query.data_cursor)
            for tlm_uuid, dt in cursor.items():
                if not (isinstance(tlm_uuid, str) and isinstance(dt, datetime)):
                    raise CBORDecodeValueError
        except (CBORDecodeError, BinAsciiError, ZlibError) as e:
            logger.debug("failed to decode cursor", extra={"tracelog": e})
            raise ValidationError(ReasonCode.INVALID_CURSOR)

    metadatas: list[TelemetryMetadata] = []
    if query.uuid:
        for tlm_uuid in query.uuid:
            if cursor and str(tlm_uuid) not in cursor:
                logger.debug(
                    "unable to find metadata with uuid in cursor",
                    extra={"cursor": cursor, "uuid": tlm_uuid},
                )
                raise ValidationError(ReasonCode.INVALID_CURSOR)
            if not (
                telem_meta := TelemetryMetadata.query_by_uuid(
                    reader_session, str(tlm_uuid)
                )
            ):
                logger.debug(
                    "metadata not found for requested uuid",
                    extra={"uuid": tlm_uuid},
                )
                return {
                    "code": ReasonCode.TELEMETRY_METADATA_NOT_FOUND.name,
                    "msg": ReasonCode.TELEMETRY_METADATA_NOT_FOUND.value,
                    "uuid": tlm_uuid,
                }, HTTPStatus.NOT_FOUND
            metadatas.append(telem_meta)

    telem_dict = Telemetry.search_for_telem(
        sat,
        metadatas,
        system,
        query.observation_time_start,
        query.observation_time_end,
        query.tlm_page,
        cursor,
        query.data_page_size,
    )

    if telem_dict.get("tlm_fields"):
        telem_dict = _create_cursor(telem_dict)
    else:
        logger.debug("no data returneHistoryCursor database for search parameters")

    response_dict = {"sat_id": sat_id, **telem_dict}
    return response_dict, HTTPStatus.OK


def _create_cursor(telem_dict):
    cursor_data = {}
    for tlm_item, tlm_data in telem_dict["tlm_fields"].items():
        # rely here on python dicts being ordered! Need the last time
        last_time = [*tlm_data["data"].keys()][-1]
        cursor_data[tlm_item] = isoparse(last_time)
    cursor = PageCursor.create_cursor(cursor_data)
    telem_dict["pages"]["next_data_cursor"] = cursor

    clean_url = re.sub("[&]?data_cursor=[^&]*", "", request.url)
    if "?" in clean_url:
        if clean_url.endswith("?"):
            clean_url += f"data_cursor={cursor}"
        else:
            clean_url += f"&data_cursor={cursor}"
    else:
        clean_url += f"?data_cursor={cursor}"
    telem_dict["pages"]["next_data_link"] = clean_url
    return telem_dict


@app.route("/satellite/<sat_id>/latesttelemetry", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_latest_telemetry(sat_id: str, query: telemetryschema.ListLatestRequest):
    """
    Retrieve the set of telemetry for each satellite
    """
    sat: Satellite
    if not (sat := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    if query.tlm_id is not None:
        # special handling for the case the user knows the item they want to query
        if not (
            telemetry_metadata := TelemetryMetadata.query_by_uuid(
                reader_session, str(query.tlm_id)
            )
        ):
            raise NotFoundError(ReasonCode.TELEMETRY_METADATA_NOT_FOUND)
        if telemetry_metadata.sat._asset_hash != sat._asset_hash:
            raise NotFoundError(ReasonCode.TELEMETRY_METADATA_NOT_FOUND)

        telemetry = []
        if latest_value := Telemetry.query_latest_value_for_metadata(
            telemetry_metadata
        ):
            # this structure must match what's expected in pydantic
            telemetry = [
                {
                    "observation_time": latest_value.observation_time,
                    "value": latest_value.value,
                    "tlm_name": telemetry_metadata.tlm_name,
                    "uuid": telemetry_metadata.uuid,
                    "component_name": telemetry_metadata.subsystem.name,
                    "system_name": telemetry_metadata.subsystem.parent.name,
                }
            ]
        response = telemetryschema.ListLatestResponse(
            cursor=None, telemetry=telemetry, total_pages=1
        )
        return response, HTTPStatus.OK

    cursor_tlm = None
    if query.cursor:
        try:
            cursor = telemetryschema.LatestCursor.parse_obj(
                PageCursor.decode_cursor(query.cursor)
            )
        except (PydValidationError, CBORDecodeError, BinAsciiError, ZlibError) as e:
            logger.info("failed to decode cursor", extra={"tracelog": e})
            raise ValidationError(ReasonCode.INVALID_CURSOR)
        if not (
            cursor_tlm := TelemetryMetadata.query_by_uuid(
                reader_session, str(cursor.id)
            )
        ):
            logger.info(
                "cursor contains unknown metadata uuid",
                extra={"metadata uuid": cursor.id},
            )
            raise ValidationError(ReasonCode.INVALID_CURSOR)

    telemetry, row_count = Telemetry.list_latest_values(
        sat, query.system, query.component, query.tlm_name, cursor_tlm, query.page_size
    )
    if telemetry:
        # creat cursor using the last value in the list of templates
        cursor_str = PageCursor.create_cursor({"id": str(telemetry[-1].uuid)})
    else:
        cursor_str = None

    response = telemetryschema.ListLatestResponse(
        cursor=cursor_str,
        total_pages=ceil(row_count / query.page_size),
        # creat cursor using the last value in the list of templates
        telemetry=telemetry,
    )
    return response, HTTPStatus.OK
