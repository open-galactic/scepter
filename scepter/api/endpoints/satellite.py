import logging
from datetime import datetime
from http import HTTPStatus
from typing import Optional, Union
from uuid import UUID

from flask_pydantic import validate

from api import app
from api.config import config
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import ConflictError, NotFoundError, ReasonCode, ValidationError
from api.schemas import satellite as satelliteschema
from models import Asset, Permission, Satellite
from models.utils import get_hash_id

logger = logging.getLogger(__name__)


@app.route("/satellite", methods=["GET"])
@auth_endpoint("search")
@validate()
def list_satellite(
    authed_user_id: Union[UUID, int], query: satelliteschema.ListRequest
):

    sat_list, total_pages = Satellite.select_user_satellites(
        reader_session,
        authed_user_id,
        query.publicly_visible,
        query.page,
        query.page_size,
    )

    return (
        satelliteschema.ListResponse(
            page=query.page,
            total_pages=total_pages,
            sat_results=sat_list,
        ),
        HTTPStatus.OK,
    )


@app.route("/satellite", methods=["POST"])
@auth_endpoint("create")
@validate()
def create_satellite(
    authed_user_id: Union[UUID, int], body: satelliteschema.CreateRequest
):

    if body.norad_id and Satellite.query_by_norad_id(session, body.norad_id):
        raise ConflictError(ReasonCode.SATELLITE_NORAD_ID_EXISTS)
    if body.nssdc_id and Satellite.query_by_nssdc_id(session, body.nssdc_id):
        raise ConflictError(ReasonCode.SATELLITE_NSSDC_ID_EXISTS)

    timestamp = datetime.utcnow()
    asset_hash = get_hash_id(
        session,
        Asset.query_by_hash,
        6,
        "s",
        body.sat_name,
        timestamp.isoformat(),
        config.id_hash_pepper,
    )
    new_sat = Satellite(
        _timestamp=timestamp,
        norad_id=body.norad_id,
        nssdc_id=body.nssdc_id,
        sat_name=body.sat_name,
        launch_date=body.launch_date,
        commission_date=body.commission_date,
        decoder=body.decoder,
        encoder=body.encoder,
        description=body.description,
        tle=body.tle,
        _config=body.config,
        publicly_visible=body.publicly_visible,
        asset_hash=asset_hash,
    )
    session.add(new_sat)
    session.flush()
    if type(authed_user_id) == UUID:
        permission = Permission(
            asset_id=new_sat._id,
            user_uuid=authed_user_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
    elif type(authed_user_id) == int:
        permission = Permission(
            asset_id=new_sat._id,
            user_id=authed_user_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
    session.add(permission)
    return (
        satelliteschema.CreateResponse(
            msg="New Satellite added",
            sat_id=new_sat._asset_hash,
        ),
        HTTPStatus.CREATED,
    )


@app.route("/satellite/<sat_id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_satellite(sat_id: str):

    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    return satelliteschema.Representation.from_orm(satellite), HTTPStatus.OK


@app.route("/satellite/<sat_id>", methods=["PUT"])
@auth_endpoint()
@validate()
def put_satellite(
    sat_id: str,
    authed_user_id: Union[UUID, int],
    body: satelliteschema.CreateRequest,
):

    if len(sat_id) > 24:
        raise ValidationError(ReasonCode.SATELLITE_ID_TOO_LONG)
    try:
        sat_id.encode("ascii")
    except UnicodeEncodeError:
        raise ValidationError(ReasonCode.SATELLITE_ID_ENCODING_INVALID)

    if sat_id[0] != "s":
        raise ValidationError(ReasonCode.SATELLITE_ID_PREFIX_INCORRECT)

    if body.norad_id and Satellite.query_by_norad_id(session, body.norad_id):
        raise ConflictError(ReasonCode.SATELLITE_NORAD_ID_EXISTS)
    if body.nssdc_id and Satellite.query_by_nssdc_id(session, body.nssdc_id):
        raise ConflictError(ReasonCode.SATELLITE_NSSDC_ID_EXISTS)

    satellite = Satellite.query_by_hash(session, sat_id)
    timestamp = datetime.utcnow()

    if satellite:
        logger.debug("found existing satellite, updating")
        return _modify_sat(
            existing_sat=satellite,
            sat_name=body.sat_name,
            timestamp=timestamp,
            norad_id=body.norad_id,
            nssdc_id=body.nssdc_id,
            encoder=body.encoder,
            decoder=body.decoder,
            tle=body.tle,
            launch_date=body.launch_date,
            commission_date=body.commission_date,
            description=body.description,
            config=body.config,
            publicly_visible=body.publicly_visible,
        )

    else:
        logger.debug("creating new satellite")
        return _add_new_sat(
            user_id=authed_user_id,
            sat_name=body.sat_name,
            timestamp=timestamp,
            norad_id=body.norad_id,
            nssdc_id=body.nssdc_id,
            encoder=body.encoder,
            decoder=body.decoder,
            tle=body.tle,
            launch_date=body.launch_date,
            commission_date=body.commission_date,
            description=body.description,
            config=body.config,
            publicly_visible=body.publicly_visible,
            asset_hash=sat_id,
        )


@auth_endpoint("write")
def _modify_sat(
    existing_sat: Satellite,
    sat_name: str,
    timestamp: datetime,
    norad_id: Optional[int],
    nssdc_id: Optional[str],
    encoder: Optional[str],
    decoder: Optional[str],
    tle: Optional[dict],
    launch_date: Optional[datetime],
    commission_date: Optional[datetime],
    description: str,
    config: Optional[dict],
    publicly_visible: Optional[bool],
):

    existing_sat._timestamp = timestamp
    existing_sat.norad_id = norad_id
    existing_sat.nssdc_id = nssdc_id
    existing_sat.sat_name = sat_name
    existing_sat.launch_date = launch_date
    existing_sat.commission_date = commission_date
    existing_sat.decoder = decoder
    existing_sat.encoder = encoder
    existing_sat.description = description
    existing_sat.tle = tle
    existing_sat._config = config
    existing_sat.publicly_visible = publicly_visible

    return (
        satelliteschema.CreateResponse(
            msg=f"satellite {existing_sat._asset_hash} updated",
            sat_id=existing_sat._asset_hash,
        ),
        HTTPStatus.OK,
    )


@auth_endpoint("create")
def _add_new_sat(
    user_id: Union[UUID, int],
    sat_name: str,
    timestamp: datetime,
    norad_id: Optional[int],
    nssdc_id: Optional[str],
    encoder: Optional[str],
    decoder: Optional[str],
    tle: Optional[dict],
    launch_date: Optional[datetime],
    commission_date: Optional[datetime],
    description: str,
    config: Optional[dict],
    publicly_visible: Optional[bool],
    asset_hash: str,
):

    new_sat = Satellite(
        _timestamp=timestamp,
        norad_id=norad_id,
        nssdc_id=nssdc_id,
        sat_name=sat_name,
        launch_date=launch_date,
        commission_date=commission_date,
        decoder=decoder,
        encoder=encoder,
        description=description,
        tle=tle,
        _config=config,
        publicly_visible=publicly_visible,
        asset_hash=asset_hash,
    )
    session.add(new_sat)
    session.flush()
    if type(user_id) == UUID:
        permission = Permission(
            asset_id=new_sat._id,
            user_uuid=user_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
    elif type(user_id) == int:
        permission = Permission(
            asset_id=new_sat._id,
            user_id=user_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
    session.add(permission)

    return (
        satelliteschema.CreateResponse(
            msg="New Satellite added",
            sat_id=new_sat._asset_hash,
        ),
        HTTPStatus.CREATED,
    )
