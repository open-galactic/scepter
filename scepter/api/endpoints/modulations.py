import json
from http import HTTPStatus

from flask_pydantic import validate  # type: ignore

from api import app
from api.dependencies.auth_handler import auth_endpoint
from models import modulations


@app.route("/modulations", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_modulations():
    """
    retrieves list of modulation types
    """

    return (
        json.dumps(modulations.valid_mod_types),
        HTTPStatus.OK,
    )
