import logging
from binascii import Error as BinAsciiError
from math import ceil
from zlib import error as ZlibError

from cbor2.types import CBORDecodeError  # type: ignore
from flask_pydantic import validate  # type: ignore
from pydantic import ValidationError

from api import app
from api.database import reader_session
from api.dependencies.auth_handler import auth_endpoint
from api.schemas import template as templateschema
from api.schemas.error import ErrorResponse
from api.utils.messages import ErrorMessages, SatelliteMessages
from api.utils.utils import PageCursor
from models import Command, Satellite, Script

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/templates", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_templates(sat_id: str, query: templateschema.ListRequest):
    """
    retrieve a list of templates that can be used to construct commands to be executed
    on a satellite.
    """
    satellite = Satellite.query_by_hash(reader_session, sat_id)
    if not satellite:
        return {"msg": SatelliteMessages.SATELLITE_NOT_FOUND.format(sat_id)}, 404

    cursor_template = None
    if query.cursor:
        try:
            cursor = templateschema.Cursor.parse_obj(
                PageCursor.decode_cursor(query.cursor)
            )
        except (ValidationError, CBORDecodeError, BinAsciiError, ZlibError) as e:
            # Cursor doesn't decode, or doesn't deserialize into sat_id and name
            logger.debug("failed to decode cursor", extra={"tracelog": e})
            return ErrorResponse(msg=ErrorMessages.INVALID_CURSOR), 400
        if not (cursor_template := Script.get_by_id(cursor.id, satellite)):
            if not cursor_template and not (
                cursor_template := Command.get_by_id(cursor.id, satellite)
            ):
                # template for this satellite doesn't exist with the given name
                logger.debug(
                    "cursor contains unknown template name",
                    extra={"template_name": cursor.name},
                )
                ErrorResponse(msg=ErrorMessages.INVALID_CURSOR), 404

    templates, row_count = satellite.list_templates(
        template_type=query.type,
        page_size=query.page_size,
        cursor=cursor_template,
    )
    if templates:
        # create cursor using last value in the list of templates
        cursor_str = PageCursor.create_cursor({"id": templates[-1].id})
    else:
        # no results, return null for the cursor
        logger.debug("no results to create cursor with")
        cursor_str = None

    response = templateschema.ListResponse(
        templates=[
            templateschema.Representation.from_orm(template) for template in templates
        ],
        cursor=cursor_str,
        total_pages=ceil(row_count / query.page_size),
    )
    return response, 200
