import logging
from http import HTTPStatus

from flask_pydantic import validate  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import ConflictError, NotFoundError, ReasonCode
from api.schemas import satellitesubsystem as subsystemschema
from api.utils.messages import SatSystemMessages
from models import Satellite, Subsystem

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/systems", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_satellite_subsystem(sat_id: str):
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    response = subsystemschema.ListResponse(systems=satellite.get_systems())
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/systems", methods=["POST"])
@auth_endpoint("execute")
@validate()
def post_satellite_subsystem(sat_id: str, body: subsystemschema.CreateRequest):
    satellite: Satellite = Satellite.query_by_hash(session, sat_id)

    if body.system_name:
        if system := Subsystem.get_system_by_name(satellite, body.system_name):
            if body.name and Subsystem.get_component_by_name(system, body.name):
                raise ConflictError(ReasonCode.COMPONENT_ALREADY_EXISTS)
        else:
            raise NotFoundError(ReasonCode.SYSTEM_NOT_FOUND)
        logger.debug("adding component")
        subsystem = Subsystem(
            satellite,
            body.name,
            "component",
            body.address,
            parent=system,
        )
    else:
        if Subsystem.get_system_by_name(satellite, body.name):
            raise ConflictError(ReasonCode.SYSTEM_ALREADY_EXISTS)
        logger.debug("adding system")
        subsystem = Subsystem(satellite, body.name, "system", body.address, parent=None)

    session.add(subsystem)

    response = subsystemschema.CreateResponse(
        subsystem=subsystem,
        code=SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.name,
        msg=SatSystemMessages.SUBSYSTEM_CREATE_SUCCESS.value,
        sat_id=satellite._asset_hash,
    )
    return response, HTTPStatus.CREATED
