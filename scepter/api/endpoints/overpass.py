from datetime import datetime, timedelta
from http import HTTPStatus
from typing import List, Union
from uuid import UUID

from flask_pydantic import validate  # type: ignore

from api import app
from api.database import reader_session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import Error, NotFoundError, ReasonCode, ValidationError
from api.schemas import overpass as overpassschema
from models import GroundStation, Overpass, Permission, Satellite

MAX_REQUESTED_GS_COMPARISONS = 25
MAX_REQUESTED_SAT_COMPARISONS = 5


@app.route("/overpass/<overpass_id>", methods=["GET"])
@auth_endpoint("search")
@validate()
def get_overpass(
    overpass_id: UUID,
    authed_user_id: Union[UUID, int],
):
    if not (overpass := Overpass.get_by_id(reader_session, overpass_id)):
        raise NotFoundError(ReasonCode.OVERPASS_NOT_FOUND)

    gs_per: Permission = Permission.query_by_user_and_asset_id(reader_session, authed_user_id, overpass.gs_id)
    if not getattr(gs_per, "read_access", None):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)
    sat_per = Permission.query_by_user_and_asset_id(reader_session, authed_user_id, overpass.sat_id)
    if not getattr(sat_per, "read_access", None):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    return overpassschema.Representation.from_orm(overpass), HTTPStatus.OK


@app.route("/overpass", methods=["GET"])
@auth_endpoint("search")
@validate()
def list_overpasses(
    authed_user_id: Union[UUID, int],
    query: overpassschema.ListRequest,
):
    if not query.end_time:
        query.end_time = query.start_time + timedelta(days=7)
    if (query.end_time - query.start_time) > timedelta(days=14):
        raise ValidationError(ReasonCode.OVERPASS_WINDOW_EXCEEDS_A_FORTNIGHT)
    if (query.end_time - datetime.utcnow()) > timedelta(days=4 * 7):
        raise ValidationError(ReasonCode.OVERPASS_WINDOW_TOO_FAR_AHEAD)

    gs_list = _verify_list_of_ground_stations(query.gs_id, authed_user_id)
    sat_list = _verify_list_of_satellites(query.sat_id, authed_user_id)
    overpasses = Overpass.list_passes_in_window(gs_list, sat_list, query.start_time, query.end_time)

    return (
        overpassschema.ListResponse(__root__=sorted(overpasses, key=lambda x: x.tca)),
        HTTPStatus.OK,
    )


def _verify_list_of_ground_stations(gs_ids: list[str], user_id: Union[UUID, int]) -> List[GroundStation]:
    # If no list of gs' were submitted, assume they want all ground stations
    if not gs_ids:
        if len(gs_list := GroundStation.query_user_gs(reader_session, user_id)) >= MAX_REQUESTED_GS_COMPARISONS:
            raise Error(ReasonCode.OVERPASS_TOO_MANY_COMPARISONS)
        return gs_list

    # If a user submitted a list of gs hashes, iterate through them and check
    # permissions etc
    gs_list = []
    for gs_id in gs_ids:
        # Check for a read permission, and return the same error as if the
        # gs didn't exist
        if not (gs := GroundStation.query_by_hash(reader_session, gs_id)) or not Permission.check_access(
            reader_session, user_id, gs._id, "read_access"
        ):
            raise NotFoundError(
                ReasonCode.GROUND_STATION_NOT_FOUND,
                msg=f"Ground Station with ID {gs_id} not found",
            )
        gs_list.append(gs)
    return gs_list


def _verify_list_of_satellites(sat_ids: List[str], user_id: Union[UUID, int]) -> List[Satellite]:
    # User must submit one or more satellites, iterate through them and collect
    # numeric IDs
    sat_list = []
    for sat_id in sat_ids:
        if not (sat := Satellite.query_by_hash(reader_session, sat_id)) or not Permission.check_access(
            reader_session, user_id, sat._id, "read_access"
        ):
            raise NotFoundError(
                ReasonCode.SATELLITE_NOT_FOUND,
                msg=f"Satellite with ID {sat_id} not found",
            )
        sat_list.append(sat)
    return sat_list
