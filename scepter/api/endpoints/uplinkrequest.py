from http import HTTPStatus
from typing import List, Tuple, Union
from uuid import UUID

from flask_pydantic import validate  # type: ignore
from sqlalchemy.orm import Session

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import (
    auth_endpoint,
    check_users_access_to_asset,
    get_users_assets,
)
from api.exceptions import ForbiddenError, NotFoundError, ReasonCode
from api.schemas import uplinkrequest as uplinkrequestschema
from api.utils.messages import UplinkRequestMessages
from models import Contact, Instruction, Satellite, UplinkRequest


def _authorize_satellite_list(
    session: Session,
    sat_ids: List[str],
    user_id: Union[UUID, int],
) -> Tuple[List[int], List[str]]:
    """
    accepts a list of sat hashes, and converts them into a list of satellites.
    Then it looks for whether the user has command access for any of them, and
    returns a list of satellite ids, and unknown satellite hashes
    """
    # convert the list of sat hashes into numeric ids
    satellites = [Satellite.query_by_hash(session, x) for x in sat_ids]

    # check the permissions for each satellite, returning false for any unknowns
    permissions = [
        check_users_access_to_asset(user_id, sat._id, "command_access")
        if sat
        else False
        for sat in satellites
    ]

    # produce a list of sat hashes that don't match a satellite, or the user
    # doesn't have access to
    unknown_ids = [
        sat_id for sat_id, approved in zip(sat_ids, permissions) if approved is False
    ]
    return satellites, unknown_ids


def _get_user_commandable_satellites(session: Session, user_id: Union[UUID, int]):
    """
    retrieve a list of all satellite ids the user has command access for
    """
    assets = get_users_assets(user_id, "command_access")
    return [x for x in assets if x._asset_type == "satellite"]


@app.route("/uplinkrequests", methods=["GET"])
@auth_endpoint()
@validate()
def list_uplink_requests(query: uplinkrequestschema.ListRequest, **kwargs):
    """
    retrieve a list of uplink requests, with filters for blocking vs historic rows
    """
    if query.sat_id:
        satellites, unknown_ids = _authorize_satellite_list(
            session, query.sat_id, kwargs["authed_user_id"]
        )
        if unknown_ids:
            return {
                "code": ReasonCode.SATELLITE_NOT_FOUND.name,
                "msg": ReasonCode.SATELLITE_NOT_FOUND.value,
                "unknown_sat_ids": unknown_ids,
            }, HTTPStatus.NOT_FOUND
    else:
        satellites = _get_user_commandable_satellites(session, kwargs["authed_user_id"])
    uplink_requests = UplinkRequest.list_for_sats(
        session, satellites, query.approved, query.reviewed
    )
    response = uplinkrequestschema.ListResponse(uplink_requests=uplink_requests)
    return response, HTTPStatus.OK


@app.route("/uplinkrequests/<id>", methods=["GET"])
@auth_endpoint()
@validate()
def get_uplink_request(id: UUID, **kwargs):
    if not (
        uplink_request := UplinkRequest.get_by_id(reader_session, id)
    ) or not check_users_access_to_asset(
        kwargs["authed_user_id"], uplink_request.sat_pkey, "command_access"
    ):
        raise NotFoundError(ReasonCode.UPLINK_REQUEST_NOT_FOUND)
    response = uplinkrequestschema.GetResponse(uplink_request=uplink_request)
    return response, HTTPStatus.OK


@app.route("/uplinkrequests", methods=["POST"])
@auth_endpoint()
@validate()
def create_uplink_request(body: uplinkrequestschema.CreateRequest, **kwargs):
    """
    create a new uplink request awaiting review
    """
    # TODO remove this method. Currently only exists to allow the pigi devs to test with
    if not (contact := Contact.get_by_id(session, id=str(body.contact_id))):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)
    if not (instruction := Instruction.get_by_contact_and_id(contact, body.command_id)):
        raise NotFoundError(ReasonCode.INSTRUCTION_NOT_FOUND)
    if not check_users_access_to_asset(
        kwargs["authed_user_id"], contact.sat_id, "command_access"
    ):
        raise ForbiddenError(ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_CREATE)

    uplink_request = UplinkRequest(instruction, body.context.dict())
    session.add(uplink_request)
    session.flush()

    response = uplinkrequestschema.CreateResponse(
        code=UplinkRequestMessages.UPLINK_REQUEST_CREATE_SUCCESS.name,
        msg=UplinkRequestMessages.UPLINK_REQUEST_CREATE_SUCCESS.value,
        uplink_request=uplink_request,
    )
    return response, HTTPStatus.CREATED


@app.route("/uplinkrequests/<id>/approve", methods=["POST"])
@auth_endpoint()
@validate()
def approve_uplink_request(id: UUID, **kwargs):
    """
    approve an uplink request
    """
    if not (uplink_request := UplinkRequest.get_by_id(session, id)):
        raise NotFoundError(ReasonCode.UPLINK_REQUEST_NOT_FOUND)
    if not check_users_access_to_asset(
        kwargs["authed_user_id"], uplink_request.sat_pkey, "command_access"
    ):
        raise ForbiddenError(ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_APPROVE)
    uplink_request.approve(user="user")
    response = uplinkrequestschema.ApproveResponse(
        code=UplinkRequestMessages.UPLINK_REQUEST_APPROVE_SUCCESS.name,
        msg=UplinkRequestMessages.UPLINK_REQUEST_APPROVE_SUCCESS.value,
        uplink_request=uplink_request,
    )
    return response, HTTPStatus.OK


@app.route("/uplinkrequests/<id>/deny", methods=["POST"])
@auth_endpoint()
@validate()
def deny_uplink_request(id: UUID, **kwargs):
    """
    deny an uplink request
    """
    if not (uplink_request := UplinkRequest.get_by_id(session, id)):
        raise NotFoundError(ReasonCode.UPLINK_REQUEST_NOT_FOUND)
    if not check_users_access_to_asset(
        kwargs["authed_user_id"], uplink_request.sat_pkey, "command_access"
    ):
        raise ForbiddenError(ReasonCode.UPLINK_REQUEST_UNAUTHORIZED_DENY)
    uplink_request.deny(user="user")
    response = uplinkrequestschema.DenyResponse(
        code=UplinkRequestMessages.UPLINK_REQUEST_DENY_SUCCESS.name,
        msg=UplinkRequestMessages.UPLINK_REQUEST_DENY_SUCCESS.value,
        uplink_request=uplink_request,
    )
    return response, HTTPStatus.OK
