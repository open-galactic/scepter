import logging
from datetime import datetime
from http import HTTPStatus
from uuid import uuid4

from flask_pydantic import validate

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import ConflictError, NotFoundError, ReasonCode, ValidationError
from api.schemas import groundstationtelecom as groundstationschema
from models import GroundStation, GsTelecom, modulations

logger = logging.getLogger(__name__)


@app.route("/groundstation/<gs_id>/telecom", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_groundstation_telecoms(gs_id: str, query: groundstationschema.ListRequest):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(reader_session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    telecom_list, total_pages = GsTelecom.search_for_telecom(
        groundstation=groundstation,
        telecom_name=query.telecom_name,
        telecom_type=query.telecom_type,
        page=query.page,
        page_size=query.page_size,
    )

    return (
        groundstationschema.ListResponse(
            page=query.page,
            total_pages=total_pages,
            telecom_results=telecom_list,
        ),
        HTTPStatus.OK,
    )


@app.route("/groundstation/<gs_id>/telecom", methods=["POST"])
@auth_endpoint("execute")
@validate()
def create_groundstation_telecom(gs_id: str, body: groundstationschema.CreateRequest):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    if GsTelecom.query_by_name_and_gs(session, groundstation._id, body.telecom_name):
        raise ConflictError(ReasonCode.GROUND_STATION_TELECOM_EXISTS)

    if body.modulations:
        for modulation in body.modulations:
            if modulation not in modulations.valid_mod_types:
                raise ValidationError(ReasonCode.TELECOM_MODULATION_INVALID)

    telecom = GsTelecom(
        gs_id=groundstation._id,
        frequency_centre=body.frequency_centre,
        frequency_lower=body.frequency_lower,
        frequency_upper=body.frequency_upper,
        telecom_name=body.telecom_name,
        telecom_type=body.telecom_type,
        antenna_gain=body.antenna_gain,
        modulations=body.modulations,
        rotatable=body.rotatable,
        beamwidth=body.beamwidth,
        noise_factor=body.noise_factor,
        transmits=body.transmits,
        receives=body.receives,
        address=body.address,
        port=body.port,
    )
    session.add(telecom)

    return (
        groundstationschema.PostResponse(
            msg="Telecom Added",
            gs_id=gs_id,
            telecom_name=telecom.telecom_name,
        ),
        HTTPStatus.CREATED,
    )


@app.route("/groundstation/<gs_id>/telecom/<telecom_name>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_groundstation_telecom(gs_id: str, telecom_name: str):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(reader_session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    if not (
        telecom := GsTelecom.query_by_name_and_gs(
            reader_session, groundstation._id, telecom_name
        )
    ):
        raise NotFoundError(ReasonCode.GROUND_STATION_TELECOM_NOT_FOUND)

    return groundstationschema.Representation.from_orm(telecom), HTTPStatus.OK


@app.route("/groundstation/<gs_id>/telecom/<telecom_name>", methods=["PUT"])
@auth_endpoint("execute")
@validate()
def put_groundstation_telecom(
    gs_id: str, telecom_name: str, body: groundstationschema.PutRequest
):

    if len(telecom_name) > 255:
        raise ValidationError(ReasonCode.TELECOM_NAME_TOO_LONG)
    try:
        telecom_name.encode("ascii")
    except UnicodeEncodeError:
        raise ValidationError(ReasonCode.TELECOM_ENCODING_INVALID)

    if body.modulations:
        for modulation in body.modulations:
            if modulation not in modulations.valid_mod_types:
                raise ValidationError(ReasonCode.TELECOM_MODULATION_INVALID)

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    if old_telecom := GsTelecom.query_by_name_and_gs(
        session, groundstation._id, telecom_name
    ):

        old_telecom.gs_id = groundstation._id
        old_telecom.frequency_centre = body.frequency_centre
        old_telecom.frequency_lower = body.frequency_lower
        old_telecom.frequency_upper = body.frequency_upper
        old_telecom.telecom_name = telecom_name
        old_telecom.telecom_type = body.telecom_type
        old_telecom.antenna_gain = body.antenna_gain
        old_telecom.modulations = body.modulations
        old_telecom.rotatable = body.rotatable
        old_telecom.beamwidth = body.beamwidth
        old_telecom.noise_factor = body.noise_factor
        old_telecom.transmits = body.transmits
        old_telecom.receives = body.receives
        old_telecom.address = body.address
        old_telecom.port = body.port

    else:
        new_telecom = GsTelecom(
            gs_id=groundstation._id,
            frequency_centre=body.frequency_centre,
            frequency_lower=body.frequency_lower,
            frequency_upper=body.frequency_upper,
            telecom_name=telecom_name,
            telecom_type=body.telecom_type,
            antenna_gain=body.antenna_gain,
            modulations=body.modulations,
            rotatable=body.rotatable,
            beamwidth=body.beamwidth,
            noise_factor=body.noise_factor,
            transmits=body.transmits,
            receives=body.receives,
            address=body.address,
            port=body.port,
        )

        session.add(new_telecom)

    return (
        groundstationschema.MsgResponse(
            msg=f"Ground station {gs_id} telecom {telecom_name} updated",
        ),
        HTTPStatus.CREATED,
    )


@app.route("/groundstation/<gs_id>/telecom/<telecom_name>", methods=["DELETE"])
@auth_endpoint("delete")
@validate()
def delete_groundstation_telecom(gs_id: str, telecom_name: str):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    if not (
        telecom := GsTelecom.query_by_name_and_gs(
            session, groundstation._id, telecom_name
        )
    ):
        raise NotFoundError(ReasonCode.GROUND_STATION_TELECOM_NOT_FOUND)

    telecom._deleted = True
    telecom._deleted_at = datetime.utcnow()
    telecom._original_name = telecom.telecom_name
    telecom.telecom_name = str(uuid4())

    return (
        groundstationschema.MsgResponse(
            msg=f"telecom object with name '{telecom_name}'  deleted",
        ),
        HTTPStatus.OK,
    )
