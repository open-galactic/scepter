import logging
from http import HTTPStatus
from uuid import UUID

from flask_pydantic import validate  # type: ignore
from jsonschema import Draft202012Validator  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import preset as presetschema
from api.utils.messages import PresetMessages
from models import Command, Satellite

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/commands/<command_id>/presets/<id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_cmd_preset(sat_id: str, command_id: str, id: UUID):
    """
    retrieves an individual preset scoped by command and id
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(command_id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)
    if not (preset := command.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    return presetschema.Representation.from_orm(preset), HTTPStatus.OK


@app.route("/satellite/<sat_id>/commands/<command_id>/presets", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_for_command(sat_id: str, command_id: str):
    """
    retrieve the list of saved args for a command
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(command_id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)

    response = presetschema.ListResponse(
        presets=[presetschema.Representation.from_orm(x) for x in command.presets],
    )
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/commands/<command_id>/presets/<id>", methods=["PUT"])
@auth_endpoint("write")
@validate()
def update_cmd_preset(
    sat_id: str, command_id: str, id: UUID, body: presetschema.UpdateRequest
):
    """
    Update an existing preset
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(command_id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)
    if not (preset := command.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    validator = Draft202012Validator(command.args)
    if error_msg := [
        str(error) for error in sorted(validator.iter_errors(body.args), key=str)
    ]:
        raise ValidationError(ReasonCode.PRESET_INVALID_ARGS, msg=error_msg)
    preset.name = body.name
    preset.args = body.args

    response = presetschema.UpdateResponse(
        code=PresetMessages.PRESET_UPDATE_SUCCESS.name,
        msg=PresetMessages.PRESET_UPDATE_SUCCESS.value,
        preset=presetschema.Representation.from_orm(preset),
    )
    return response, HTTPStatus.CREATED


@app.route("/satellite/<sat_id>/commands/<command_id>/presets", methods=["POST"])
@auth_endpoint("write")
@validate()
def create_cmd_preset(sat_id: str, command_id: str, body: presetschema.CreateRequest):
    """
    create a new command preset
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(command_id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)

    validator = Draft202012Validator(command.args)
    if error_msg := [
        str(error) for error in sorted(validator.iter_errors(body.args), key=str)
    ]:
        raise ValidationError(ReasonCode.PRESET_INVALID_ARGS, msg=error_msg)

    preset = command.create_preset(body.name, body.args)
    session.add(preset)
    response = presetschema.CreateResponse(
        code=PresetMessages.PRESET_CREATE_SUCCESS.name,
        msg=PresetMessages.PRESET_CREATE_SUCCESS.value,
        preset=presetschema.Representation.from_orm(preset),
    )
    return response, HTTPStatus.CREATED


@app.route("/satellite/<sat_id>/commands/<command_id>/presets/<id>", methods=["DELETE"])
@auth_endpoint("write")
@validate()
def delete_cmd_preset(sat_id: str, command_id: str, id: UUID):
    """
    delete an existing saved_arg, or create a new one
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(command_id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)

    if not (preset := command.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    session.delete(preset)
    response = presetschema.DeleteResponse(
        code=PresetMessages.PRESET_DELETE_SUCCESS.name,
        msg=PresetMessages.PRESET_DELETE_SUCCESS.value,
    )
    return response, HTTPStatus.OK
