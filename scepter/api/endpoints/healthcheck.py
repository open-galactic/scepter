import logging
from http import HTTPStatus

from api import app
from api.database import session

logger = logging.getLogger(__name__)


@app.route("/healthcheck", methods=["GET"])
def healthcheck():
    session.execute("SELECT 1=1")
    return ("", HTTPStatus.NO_CONTENT)
