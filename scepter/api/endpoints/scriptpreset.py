import logging
from http import HTTPStatus
from uuid import UUID

from flask_pydantic import validate  # type: ignore
from jsonschema import Draft202012Validator  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import preset as presetschema
from api.utils.messages import PresetMessages
from models import Satellite, Script

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/scripts/<script_id>/presets/<id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_script_preset(sat_id: str, script_id: str, id: UUID):
    """
    retrieves an individual preset scoped by script and id
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(script_id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)
    if not (preset := script.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    response = presetschema.Representation.from_orm(preset)
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/scripts/<script_id>/presets", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_for_script(sat_id: str, script_id: str):
    """
    retrieve the list of saved args for a script
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(script_id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)

    response = presetschema.ListResponse(
        presets=[presetschema.Representation.from_orm(x) for x in script.presets],
    )
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/scripts/<script_id>/presets/<id>", methods=["PUT"])
@auth_endpoint("write")
@validate()
def update_script_preset(
    sat_id: str, script_id: str, id: UUID, body: presetschema.UpdateRequest
):
    """
    Update an existing preset
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(script_id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)
    if not (preset := script.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    validator = Draft202012Validator(script.args)
    if error_msg := [
        str(error) for error in sorted(validator.iter_errors(body.args), key=str)
    ]:
        raise ValidationError(ReasonCode.PRESET_INVALID_ARGS, msg=error_msg)

    preset.name = body.name
    preset.args = body.args

    response = presetschema.UpdateResponse(
        preset=presetschema.Representation.from_orm(preset),
        code=PresetMessages.PRESET_UPDATE_SUCCESS.name,
        msg=PresetMessages.PRESET_UPDATE_SUCCESS.value,
    )
    return response, HTTPStatus.CREATED


@app.route("/satellite/<sat_id>/scripts/<script_id>/presets", methods=["POST"])
@auth_endpoint("write")
@validate()
def create_script_preset(sat_id: str, script_id: str, body: presetschema.CreateRequest):
    """
    create a new script preset
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(script_id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)

    validator = Draft202012Validator(script.args)
    if error_msg := [
        str(error) for error in sorted(validator.iter_errors(body.args), key=str)
    ]:
        raise ValidationError(ReasonCode.PRESET_INVALID_ARGS, msg=error_msg)

    preset = script.create_preset(body.name, body.args)
    session.add(preset)
    response = presetschema.CreateResponse(
        code=PresetMessages.PRESET_CREATE_SUCCESS.name,
        msg=PresetMessages.PRESET_CREATE_SUCCESS.value,
        preset=presetschema.Representation.from_orm(preset),
    )
    return response, HTTPStatus.CREATED


@app.route("/satellite/<sat_id>/scripts/<script_id>/presets/<id>", methods=["DELETE"])
@auth_endpoint("write")
@validate()
def delete_script_preset(sat_id: str, script_id: str, id: UUID):
    """
    delete an existing saved_arg, or create a new one
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(script_id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)
    if not (preset := script.get_preset_by_id(id)):
        raise NotFoundError(ReasonCode.PRESET_NOT_FOUND)

    session.delete(preset)
    response = presetschema.DeleteResponse(
        code=PresetMessages.PRESET_DELETE_SUCCESS.name,
        msg=PresetMessages.PRESET_DELETE_SUCCESS.value,
    )
    return response, HTTPStatus.OK
