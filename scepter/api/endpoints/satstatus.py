import logging
from datetime import datetime
from http import HTTPStatus

from flask_pydantic import validate  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.schemas import satellitestatus as satstatusschema
from api.utils.messages import SatelliteMessages
from models import Satellite, SatStatus

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/status", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_sat_status(
    sat_id: str,
):
    """
    GET status updates from satellites
    """
    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        return {
            "msg": SatelliteMessages.SATELLITE_NOT_FOUND.format(sat_id)
        }, HTTPStatus.NOT_FOUND
    sat_last_status: SatStatus
    if not (
        sat_last_status := SatStatus.query_last_sat_status(reader_session, satellite)
    ):
        return {"msg": "No status updates for that satellite found"}, HTTPStatus.OK
    return sat_last_status, HTTPStatus.OK


@app.route("/satellite/<sat_id>/status", methods=["POST"])
@auth_endpoint("execute")
@validate()
def post_sat_status(sat_id: str, body: satstatusschema.CreateRequest):
    """
    POST status updates from satellites
    """
    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        return {
            "msg": SatelliteMessages.SATELLITE_NOT_FOUND.format(sat_id)
        }, HTTPStatus.NOT_FOUND

    init_values = {"sat_id": satellite._id, "_timestamp": datetime.utcnow()}
    status_update = SatStatus(**body.dict(), **init_values)
    session.add(status_update)

    return {
        "msg": "Status update recieved",
        "sat_id": satellite._asset_hash,
    }, HTTPStatus.CREATED
