import logging
from http import HTTPStatus

from flask_pydantic import validate  # type: ignore
from jsonschema import Draft202012Validator, SchemaError  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import command as commandschema
from api.utils.messages import CommandMessages
from models import Command, Satellite

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/commands/<id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_command(sat_id: str, id: str):
    """
    retrieves an individual command template scoped by satellite and name
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (command := Command.get_by_id(id, satellite)):
        raise NotFoundError(ReasonCode.COMMAND_NOT_FOUND)

    return commandschema.Representation.from_orm(command), HTTPStatus.OK


@app.route("/satellite/<sat_id>/commands/<id>", methods=["PUT"])
@auth_endpoint("write")
@validate()
def update_command(sat_id: str, id: str, body: commandschema.UpdateRequest):
    """
    Update an existing command, or create a new one with a unique name
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    try:
        Draft202012Validator.check_schema(body.args)
    except SchemaError:
        raise ValidationError(ReasonCode.INVALID_ARG_JSCHEMA)

    if not (command := Command.get_by_id(id, satellite)):
        # create new script
        logger.debug("no script found, creating new row")
        command = Command(
            id=id,
            satellite=satellite,
            name=body.name,
            args=body.args,
            entrypoint=body.entrypoint,
            code=body.code,
            requires_approval=body.requires_approval,
            encoder_data=body.encoder_data,
        )
        session.add(command)
    else:
        # update existing script
        logger.debug("script found, updating existing row")
        command.name = body.name
        command.args = body.args
        command.entrypoint = body.entrypoint
        command.code = body.code
        command.requires_approval = body.requires_approval
        command.encoder_data = body.encoder_data

    session.flush()
    return (
        commandschema.UpdateResponse(
            code=CommandMessages.UPDATE_SUCCESS.name,
            msg=CommandMessages.UPDATE_SUCCESS.value,
            command=commandschema.Representation.from_orm(command),
        ),
        HTTPStatus.CREATED,
    )
