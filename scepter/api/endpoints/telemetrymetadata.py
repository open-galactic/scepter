import logging
from http import HTTPStatus

from flask_pydantic import validate

from api import app
from api.database import reader_session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode
from api.schemas import telemetrymetadata as telemmetadataschema
from models import Satellite, Subsystem, TelemetryMetadata

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/telemetry_metadata", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_telemetry_metadata(sat_id: str, query: telemmetadataschema.ListRequest):
    """
    Resource for adding to or retrieving from the set of telemetry metadata for a satellite
    """

    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    system: Subsystem = None
    if query.system:
        if not (system := Subsystem.get_system_by_name(satellite, query.system)):
            logger.debug(
                "could not find system",
                extra={"system_name": query.system},
            )
            raise NotFoundError(ReasonCode.SYSTEM_NOT_FOUND)

    component_id: int = None
    if query.component:
        if not (component := Subsystem.get_component_by_name(system, query.component)):
            logger.debug(
                "could not find component",
                extra={
                    "component_name": {query.component},
                    "system_name": {query.system},
                },
            )
            raise NotFoundError(ReasonCode.COMPONENT_NOT_FOUND)
        component_id = component.id

    telem_list, total_pages = TelemetryMetadata.search_for_telem_meta(
        session=reader_session,
        sat_id=satellite._id,
        tlm_name=query.tlm_name,
        uuid=query.uuid,
        system=system,
        component_id=component_id,
        cad_id=query.cad_id,
        has_cad_id=query.has_cad_id,
        page_size=query.page_size,
        page=query.page,
    )
    return {
        "page": query.page,
        "total_pages": total_pages,
        "telem_results": telem_list,
    }, HTTPStatus.OK
