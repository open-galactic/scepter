import logging
from http import HTTPStatus

from flask_pydantic import validate

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import ConflictError, NotFoundError, ReasonCode, ValidationError
from api.schemas import sattelecom as sattelecomschema
from models import Satellite, SatTelecom, modulations

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/telecom", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_telecoms(sat_id: str, query: sattelecomschema.ListRequest):

    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    telecom_list, total_pages = SatTelecom.list_telecoms(
        satellite=satellite,
        telecom_name=query.telecom_name,
        telecom_type=query.telecom_type,
        modulations=query.modulations,
        page=query.page,
        page_size=query.page_size,
    )

    return (
        sattelecomschema.ListResponse(
            page=query.page,
            total_pages=total_pages,
            telecom_results=telecom_list,
        ),
        HTTPStatus.OK,
    )


@app.route("/satellite/<sat_id>/telecom", methods=["POST"])
@auth_endpoint("execute")
@validate()
def create_satellite_telecom(sat_id: str, body: sattelecomschema.CreateRequest):

    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    if SatTelecom.query_by_name_and_sat(session, satellite._id, body.telecom_name):
        raise ConflictError(ReasonCode.SATELLITE_TELECOM_ALREADY_EXISTS)

    if body.modulations:
        for modulation in body.modulations:
            if modulation not in modulations.valid_mod_types:
                raise ValidationError(ReasonCode.TELECOM_MODULATION_INVALID)
    telecom = SatTelecom(
        sat_id=satellite._id,
        frequency=body.frequency,
        telecom_name=body.telecom_name,
        telecom_type=body.telecom_type,
        modulations=body.modulations,
        transmits=body.transmits,
        receives=body.receives,
        encrypted=body.encrypted,
    )
    session.add(telecom)

    return (
        sattelecomschema.CreateResponse(
            msg="Telecom Added",
            sat_id=satellite._asset_hash,
            telecom_name=telecom.telecom_name,
        ),
        HTTPStatus.CREATED,
    )


@app.route("/satellite/<sat_id>/telecom/<telecom_name>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_satellite_telecom(sat_id: str, telecom_name: str):

    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    telecom: SatTelecom
    if not (
        telecom := SatTelecom.query_by_name_and_sat(
            reader_session, satellite._id, telecom_name
        )
    ):
        raise NotFoundError(ReasonCode.SATELLITE_TELECOM_NOT_FOUND)

    return sattelecomschema.Representation.from_orm(telecom), HTTPStatus.OK


@app.route("/satellite/<sat_id>/telecom/<telecom_name>", methods=["PUT"])
@auth_endpoint("execute")
@validate()
def put_satellite_telecom(
    sat_id: str, telecom_name: str, body: sattelecomschema.PutRequest
):

    if len(telecom_name) > 255:
        raise ValidationError(ReasonCode.TELECOM_NAME_TOO_LONG)

    try:
        telecom_name.encode("ascii")
    except UnicodeEncodeError:
        raise ValidationError(ReasonCode.TELECOM_ENCODING_INVALID)

    satellite: Satellite
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    if body.modulations:
        for modulation in body.modulations:
            if modulation not in modulations.valid_mod_types:
                raise ValidationError(ReasonCode.TELECOM_MODULATION_INVALID)

    existing_telecom: SatTelecom
    if existing_telecom := SatTelecom.query_by_name_and_sat(
        session, satellite._id, telecom_name
    ):

        logger.debug("found existing telecom, updating")

        existing_telecom.frequency = body.frequency
        existing_telecom.telecom_name = telecom_name
        existing_telecom.telecom_type = body.telecom_type
        existing_telecom.modulations = body.modulations
        existing_telecom.transmits = body.transmits
        existing_telecom.receives = body.receives
        existing_telecom.encrypted = body.encrypted
    else:
        new_telecom = SatTelecom(
            sat_id=satellite._id,
            frequency=body.frequency,
            telecom_name=telecom_name,
            telecom_type=body.telecom_type,
            modulations=body.modulations,
            transmits=body.transmits,
            receives=body.receives,
            encrypted=body.encrypted,
        )
        logger.debug("adding new telecom")
        session.add(new_telecom)

    return (
        sattelecomschema.PutResponse(
            msg=f"Satellite {satellite._asset_hash} telecom {telecom_name} updated"
        ),
        HTTPStatus.CREATED,
    )
