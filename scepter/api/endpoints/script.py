import logging
from http import HTTPStatus

from flask_pydantic import validate  # type: ignore
from jsonschema import Draft202012Validator, SchemaError  # type: ignore

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import script as scriptschema
from api.utils.messages import ScriptMessages
from models import Satellite, Script

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/scripts/<id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_script(sat_id: str, id: str):
    """
    retrieves an individual script template scoped by satellite and name
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (script := Script.get_by_id(id, satellite)):
        raise NotFoundError(ReasonCode.SCRIPT_NOT_FOUND)

    return scriptschema.Representation.from_orm(script), HTTPStatus.OK


@app.route("/satellite/<sat_id>/scripts/<id>", methods=["PUT"])
@auth_endpoint("write")
@validate()
def update_script(sat_id: str, id: str, body: scriptschema.UpdateRequest):
    """
    Update an existing script, or create a new one with a unique name
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    try:
        Draft202012Validator.check_schema(body.args)
    except SchemaError:
        raise ValidationError(ReasonCode.INVALID_ARG_JSCHEMA)

    if not (script := Script.get_by_id(id, satellite)):
        # create new script
        logger.debug("no script found, creating new row")
        script = Script(
            id=id,
            satellite=satellite,
            name=body.name,
            args=body.args,
            entrypoint=body.entrypoint,
        )
        session.add(script)
    else:
        # update existing script
        logger.debug("script found, updating existing row")
        script.name = body.name
        script.args = body.args
        script.entrypoint = body.entrypoint

    return (
        scriptschema.UpdateResponse(
            code=ScriptMessages.UPDATE_SUCCESS.name,
            msg=ScriptMessages.UPDATE_SUCCESS.value,
            script=scriptschema.Representation.from_orm(script),
        ),
        HTTPStatus.CREATED,
    )
