from http import HTTPStatus

from flask_pydantic import validate

from api import app
from api.database import reader_session, session
from api.dependencies import file_handler
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import filetransfer as filetransferschema
from models import Asset, FileTransferMetadata
from models import file_types as valid_file_types


@app.route("/asset/<asset_hash>/file", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_filemetadata(asset_hash: str, query: filetransferschema.ListRequest):
    """
    Endpoint to retreive file transfer metadata
    """

    if query.file_type and query.file_type not in valid_file_types:
        raise ValidationError(ReasonCode.INVALID_FILE_TYPE)

    if not (
        asset := Asset.query_by_hash(session=reader_session, asset_hash=asset_hash)
    ):
        raise NotFoundError(ReasonCode.ASSET_NOT_FOUND)

    file_list, total_pages = FileTransferMetadata.select_files_for_asset(
        asset,
        file_type=query.file_type,
        page_size=query.page_size,
        page=query.page,
    )

    return (
        filetransferschema.CreateResponse(
            page=query.page,
            total_pages=total_pages,
            files=file_list,
        ),
        HTTPStatus.OK,
    )


@app.route("/asset/<asset_hash>/file/<file_name>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_file(asset_hash: str, file_name: str):
    """
    Endpoint to retreive file
    """

    file_key = "{}/{}".format(asset_hash, file_name)

    response = file_handler.get(file_key)
    if "error" in response:
        return {"msg": response["error"]}, HTTPStatus.BAD_REQUEST
    return response["stream"], HTTPStatus.OK


@app.route("/asset/<asset_hash>/file/<file_name>", methods=["DELETE"])
@auth_endpoint("read")
@validate()
def delete_file(asset_hash: str, file_name: str):
    """
    Endpoint to set delete flag on file
    """

    file_key = "{}/{}".format(asset_hash, file_name)

    metadata: FileTransferMetadata = FileTransferMetadata.query_by_file_key(
        session, file_key
    )
    if not metadata or metadata._deleted:
        raise NotFoundError(ReasonCode.FILE_NOT_FOUND)

    response = file_handler.delete(file_key)

    if "error" in response:
        return {"msg": response["error"]}, HTTPStatus.BAD_REQUEST
    metadata._deleted = True
    return response, HTTPStatus.OK
