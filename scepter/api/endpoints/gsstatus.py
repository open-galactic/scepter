import logging
from datetime import datetime
from http import HTTPStatus

from flask_pydantic import validate

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode
from api.schemas import groundstationstatus as groundstationschema
from api.utils.messages import GroundStationMessages
from models import GroundStation, GsStatus, Satellite

logger = logging.getLogger(__name__)


@app.route("/groundstation/<gs_id>/status", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_groundstation_status(gs_id: str):
    """
    Get most recent groundstation status
    """
    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(reader_session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    if not (gs_last_status := GsStatus.select_last_status(groundstation)):
        return {"msg": GroundStationMessages.GS_NO_STATUS_MSG}, HTTPStatus.OK

    return groundstationschema.Representation.from_orm(gs_last_status), HTTPStatus.OK


@app.route("/groundstation/<gs_id>/status", methods=["POST"])
@auth_endpoint("execute")
@validate()
def post_groundstation_status(gs_id: str, body: groundstationschema.PostRequest):
    """
    Post new groundstation status
    """
    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    tracked_sat_id: int = None
    if body.sat_tracking:
        if not (satellite := Satellite.query_by_hash(session, body.sat_tracking)):
            raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
        tracked_sat_id = satellite._id

    status_update = GsStatus(
        gs_id=groundstation._id,
        _timestamp=datetime.utcnow(),
        update_time=body.update_time,
        device=body.device,
        properties=body.properties,
        status=body.status,
        errors=body.errors,
        sat_tracking=tracked_sat_id,
        sat_tracking_bool=True if tracked_sat_id else False,
    )

    session.add(status_update)
    session.flush()

    return (
        groundstationschema.PostResponse(
            msg=GroundStationMessages.GS_STATUS_CREATED_MSG, gs_id=gs_id
        ),
        HTTPStatus.CREATED,
    )
