import logging
from datetime import datetime
from http import HTTPStatus
from typing import Optional, Union
from uuid import UUID, uuid4

from flask_pydantic import validate

from api import app
from api.config import config
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint
from api.exceptions import NotFoundError, ReasonCode, ValidationError
from api.schemas import groundstation as groundstationschema
from models import Asset, GroundStation, Permission
from models.utils import get_hash_id

logger = logging.getLogger(__name__)


@app.route("/groundstation", methods=["GET"])
@auth_endpoint("search")
@validate()
def list_groundstations(
    authed_user_id: Union[UUID, int], query: groundstationschema.ListRequest
):

    gs_list, total_pages = GroundStation.select_user_groundstations(
        reader_session,
        authed_user_id,
        query.publicly_visible,
        query.page,
        query.page_size,
    )

    return (
        groundstationschema.ListResponse(
            page=query.page,
            total_pages=total_pages,
            gs_results=gs_list,
        ),
        HTTPStatus.OK,
    )


@app.route("/groundstation", methods=["POST"])
@auth_endpoint("create")
@validate()
def create_groundstation(
    authed_user_id: Union[UUID, int], body: groundstationschema.CreateRequest
):
    timestamp = datetime.utcnow()

    asset_hash = get_hash_id(
        session,
        Asset.query_by_hash,
        6,
        "g",
        body.gs_name,
        timestamp.isoformat(),
        config.id_hash_pepper,
    )
    new_gs = GroundStation(
        _timestamp=timestamp,
        gs_name=body.gs_name,
        gs_hostname=body.gs_hostname,
        gs_port=body.gs_port,
        commission_date=body.commission_date,
        latitude=body.latitude,
        longitude=body.longitude,
        altitude=body.altitude,
        address=body.address,
        _config=body.config,
        publicly_visible=body.publicly_visible,
        asset_hash=asset_hash,
    )
    session.add(new_gs)
    session.flush()
    permission = Permission.create_permission(
        asset_id=new_gs._id,
        user_id=authed_user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    return (
        groundstationschema.CreateResponse(
            msg="New groundstation added",
            gs_id=new_gs._asset_hash,
        ),
        HTTPStatus.CREATED,
    )


@app.route("/groundstation/<gs_id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_groundstation(gs_id: str):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(reader_session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    return groundstationschema.Representation.from_orm(groundstation), HTTPStatus.OK


@app.route("/groundstation/<gs_id>", methods=["PUT"])
@auth_endpoint()
@validate()
def put_groundstation(
    authed_user_id: Union[UUID, int], gs_id: str, body: groundstationschema.PutRequest
):

    if len(gs_id) > 24:
        raise ValidationError(ReasonCode.GROUND_STATION_ID_TOO_LONG)
    try:
        gs_id.encode("ascii")
    except UnicodeEncodeError:
        raise ValidationError(ReasonCode.GROUND_STATION_ID_ENCODING_INVALID)

    if gs_id[0] != "g":
        raise ValidationError(ReasonCode.GROUND_STATION_ID_PREFIX_INCORRECT)

    groundstation = GroundStation.query_by_hash(session, gs_id)
    timestamp = datetime.utcnow()

    if groundstation:
        logger.debug("found existing gs, updating")

        return _modify_gs(
            groundstation,
            timestamp=timestamp,
            gs_name=body.gs_name,
            gs_hostname=body.gs_hostname,
            gs_port=body.gs_port,
            commission_date=body.commission_date,
            latitude=body.latitude,
            longitude=body.longitude,
            altitude=body.altitude,
            address=body.address,
            config=body.config,
            publicly_visible=body.publicly_visible,
            gs_id=gs_id,
        )

    else:
        logger.debug("creating new gs")
        return _add_new_gs(
            user_id=authed_user_id,
            timestamp=timestamp,
            gs_name=body.gs_name,
            gs_hostname=body.gs_hostname,
            gs_port=body.gs_port,
            commission_date=body.commission_date,
            latitude=body.latitude,
            longitude=body.longitude,
            altitude=body.altitude,
            address=body.address,
            config=body.config,
            publicly_visible=body.publicly_visible,
            asset_hash=gs_id,
        )


@app.route("/groundstation/<gs_id>", methods=["DELETE"])
@auth_endpoint("delete")
@validate()
def delete_groundstation(gs_id: str):

    groundstation: GroundStation
    if not (groundstation := GroundStation.query_by_hash(session, gs_id)):
        raise NotFoundError(ReasonCode.GROUND_STATION_NOT_FOUND)

    groundstation._original_hash = groundstation._asset_hash
    groundstation._deleted = True
    groundstation._deleted_at = datetime.utcnow()
    groundstation._asset_hash = str(uuid4())

    return (
        groundstationschema.MsgResponse(
            msg=f"Gs with ID '{gs_id}' deleted",
        ),
        HTTPStatus.OK,
    )


@auth_endpoint("write")
def _modify_gs(
    existing_gs: GroundStation,
    timestamp: datetime,
    gs_name: str,
    gs_hostname: Optional[str],
    gs_port: Optional[int],
    commission_date: datetime,
    latitude: Union[float, int],
    longitude: Union[float, int],
    altitude: Union[float, int],
    address: Optional[str],
    config: Optional[str],
    publicly_visible: Optional[bool],
    gs_id: str,
):

    existing_gs._timestamp = timestamp
    existing_gs.gs_name = gs_name
    existing_gs.gs_hostname = gs_hostname
    existing_gs.gs_port = gs_port
    existing_gs.commission_date = commission_date
    existing_gs.latitude = latitude
    existing_gs.longitude = longitude
    existing_gs.altitude = altitude
    existing_gs.address = address
    existing_gs._config = config
    existing_gs.publicly_visible = publicly_visible

    return (
        groundstationschema.CreateResponse(
            msg=f"groundstation {gs_id} updated",
            gs_id=gs_id,
        ),
        HTTPStatus.OK,
    )


@auth_endpoint("create")
def _add_new_gs(
    user_id: Union[UUID, int],
    timestamp: datetime,
    gs_name: str,
    gs_hostname: Optional[str],
    gs_port: Optional[int],
    commission_date: datetime,
    latitude: Union[float, int],
    longitude: Union[float, int],
    altitude: Union[float, int],
    address: Optional[str],
    config: Optional[str],
    publicly_visible: Optional[bool],
    asset_hash: str,
):
    new_gs = GroundStation(
        _timestamp=timestamp,
        gs_name=gs_name,
        gs_hostname=gs_hostname,
        gs_port=gs_port,
        commission_date=commission_date,
        latitude=latitude,
        longitude=longitude,
        altitude=altitude,
        address=address,
        _config=config,
        publicly_visible=publicly_visible,
        asset_hash=asset_hash,
    )
    session.add(new_gs)
    session.flush()
    permission = Permission.create_permission(
        asset_id=new_gs._id,
        user_id=user_id,
        read_access=True,
        command_access=True,
        manage_asset=True,
        manage_users=True,
        owner=True,
    )
    session.add(permission)
    return (
        groundstationschema.CreateResponse(
            msg="New groundstation added",
            gs_id=asset_hash,
        ),
        HTTPStatus.CREATED,
    )
