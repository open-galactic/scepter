import logging
from binascii import Error as BinAsciiError
from datetime import datetime, timedelta
from http import HTTPStatus
from math import ceil
from random import randint
from typing import Optional, Union
from uuid import UUID
from zlib import error as ZlibError

from cbor2.types import CBORDecodeError  # type: ignore
from flask_pydantic import validate  # type: ignore
from jsonschema import Draft202012Validator  # type: ignore
from pydantic import ValidationError as PydValidationError

from api import app
from api.database import reader_session, session
from api.dependencies.auth_handler import auth_endpoint, check_users_access_to_asset
from api.exceptions import ConflictError, NotFoundError, ReasonCode, ValidationError
from api.schemas import contact as contactschema
from api.utils.messages import ContactMessages
from api.utils.utils import PageCursor
from models import (
    Command,
    Contact,
    GroundStation,
    Instruction,
    Overpass,
    Satellite,
    Script,
)

logger = logging.getLogger(__name__)


@app.route("/satellite/<sat_id>/contacts/<id>", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_contact_details(sat_id: str, id: UUID):
    """
    retrieves information about an issued contact
    """
    if not Satellite.query_by_hash(reader_session, sat_id):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (contact := Contact.get_by_id(session=reader_session, id=id)):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)

    return contactschema.Representation.from_orm(contact), HTTPStatus.OK


@app.route("/satellite/<sat_id>/contacts/<id>/history", methods=["GET"])
@auth_endpoint("read")
@validate()
def get_contact_history(sat_id: str, id: UUID):
    """
    retrieves the history of superceded contacts in a group
    """
    if not Satellite.query_by_hash(reader_session, sat_id):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (contact := Contact.get_by_id(session=reader_session, id=id)):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)

    contact_group = contact.get_history()
    response = contactschema.HistoryResponse(
        history=[contactschema.Representation.from_orm(contact) for contact in contact_group]
    )
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/contacts", methods=["GET"])
@auth_endpoint("read")
@validate()
def list_contacts(sat_id: str, query: contactschema.ListRequest):
    """
    retrieve a list of issued contacts to be executed by the satellite
    upcoming is used to return only contacts that are due to occur in
    future
    """
    if not (satellite := Satellite.query_by_hash(reader_session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    cursor_contact = None
    if query.cursor:
        try:
            cursor = contactschema.Cursor.parse_obj(PageCursor.decode_cursor(query.cursor))
        except (PydValidationError, CBORDecodeError, BinAsciiError, ZlibError) as e:
            # Cursor doesn't decode, or doesn't deserialize into sat_id and name
            logger.info("failed to decode cursor", extra={"tracelog": e})
            raise ValidationError(ReasonCode.INVALID_CURSOR)
        if not (cursor_contact := Contact.get_by_id(reader_session, cursor.id)):
            # template for this satellite doesn't exist with the given name
            logger.info(
                "cursor contains unknown template name",
                extra={"template_name": cursor.name},
            )
            raise ValidationError(ReasonCode.INVALID_CURSOR)

    contacts, row_count = Contact.list_active_for_sat(
        satellite=satellite,
        upcoming=query.upcoming,
        include_cancelled=query.include_cancelled,
        page_size=query.page_size,
        cursor=cursor_contact,
    )
    if contacts:
        # create cursor using last value in the list of templates
        cursor_str = PageCursor.create_cursor({"id": str(contacts[-1].id)})
    else:
        # no results, return null for the cursor
        logger.debug("no results to create cursor with")
        cursor_str = None

    response = contactschema.ListResponse(
        contacts=[contactschema.Representation.from_orm(contact) for contact in contacts],
        cursor=cursor_str,
        total_pages=ceil(row_count / query.page_size),
    )
    return response, HTTPStatus.OK


def _create_test_overpass(session, user_id: Union[UUID, int], satellite: Satellite, body) -> Optional[Overpass]:
    gs = GroundStation.query_by_hash(session, body.gs_id)
    if not gs or not check_users_access_to_asset(user_id, gs._id, "command_access"):
        raise ValidationError(ReasonCode.OVERPASS_MISSING_GS_OR_OVERPASS_ID)

    default_aos = datetime.utcnow() + timedelta(seconds=30)
    aos = body.aos if body.aos else default_aos
    los = body.los if body.los else aos + timedelta(seconds=60)
    tca = aos + (los - aos) / 2

    overpass = Overpass(
        source="api-autogen",
        groundstation=gs,
        satellite=satellite,
        aos=aos,
        tca=tca,
        los=los,
        maximum_el=randint(10, 90),
        trajectory={},
        tle_updated_at=default_aos,
    )
    session.add(overpass)
    session.flush()
    return overpass


@app.route("/satellite/<sat_id>/contacts", methods=["POST"])
@auth_endpoint("execute")
@validate()
def issue_contact(sat_id: str, body: contactschema.CreateRequest, **kwargs):
    """
    issue a contact for execution on the satellite
    """
    if not (satellite := Satellite.query_by_hash(session, sat_id)):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)

    if body.overpass_id:
        if not (overpass := Overpass.get_by_id(session, body.overpass_id)):
            raise NotFoundError(ReasonCode.OVERPASS_NOT_FOUND)
    else:
        # used for creating overpasses during testing.
        overpass = _create_test_overpass(session, kwargs["authed_user_id"], satellite, body)

    # Handle the case we're superseding an existing contact
    group_id = None
    if body.supersedes:
        if not (contact := Contact.get_by_id(session, str(body.supersedes))):
            logger.debug("could not find contact with ID specified by supersedes")
            raise NotFoundError(ReasonCode.CONTACT_SUPERSEDED_NOT_FOUND)
        # ensure the id sent by the client is the active contact to prevent
        # concurrent writes by two clients
        contact_history = contact.get_history()
        if body.supersedes != contact_history[0].id:
            return {
                "msg": ReasonCode.CONTACT_SUPERSEDED_NOT_ACTIVE.value,
                "code": ReasonCode.CONTACT_SUPERSEDED_NOT_ACTIVE.name,
                "active_contact": contact_history[0].id,
            }, HTTPStatus.CONFLICT
        if contact_history[0].cancelled_at:
            raise ConflictError(ReasonCode.CONTACT_SUPERSEDED_WAS_CANCELLED)
        group_id = contact_history[0].group_id

        # TODO remove placeholder values for the user ids
        contact = Contact(
            group_id=group_id,
            overpass=overpass,
            satellite=satellite,
            issued_by="user1",
            issued_at=body.issued_at,
        )
    else:
        contact = Contact(
            overpass=overpass,
            satellite=satellite,
            issued_by="user1",
            issued_at=body.issued_at,
        )
    session.add(contact)
    session.flush()

    for id, instruction in enumerate(body.instructions):
        # iterate through each instruction, ensure the args are appropriate
        # based on the template, and create new instruction entries

        if isinstance(instruction, contactschema.IssueScript):
            command = None
            if not (script := Script.get_by_id(instruction.script_id, satellite)):
                raise ValidationError(
                    ReasonCode.SCRIPT_NOT_FOUND,
                    msg=f"Script with id {instruction.script_id} not found",
                )
        elif isinstance(instruction, contactschema.IssueCommand):
            script = None
            if not (command := Command.get_by_id(instruction.command_id, satellite)):
                raise ValidationError(
                    ReasonCode.COMMAND_NOT_FOUND,
                    msg=f"Command with id {instruction.command_id} not found",
                )

        template = script or command
        validator = Draft202012Validator(template.args)
        if error_msg := [str(error) for error in sorted(validator.iter_errors(instruction.args), key=str)]:
            raise ValidationError(ReasonCode.CONTACT_INVALID_ARGS, msg=error_msg)

        entry = Instruction(
            id=id,
            command=command,
            script=script,
            contact=contact,
            args=instruction.args,
            sequence_number=instruction.sequence_number,
        )
        session.add(entry)
    session.flush()

    response = contactschema.CreateResponse(
        code=ContactMessages.ISSUED_CONTACT.name,
        msg=ContactMessages.ISSUED_CONTACT.value,
        id=contact.id,
    )
    return response, HTTPStatus.CREATED


@app.route("/satellite/<sat_id>/contacts/<id>/cancel", methods=["POST"])
@auth_endpoint("execute")
@validate()
def cancel_contact(sat_id: str, id: UUID):
    """
    cancel a contact (soft delete)
    """
    if not (contact := Contact.get_by_id(session, id)):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)
    if contact.cancelled_at:
        raise ConflictError(ReasonCode.CONTACT_ALREADY_CANCELLED)

    # only allow deleting active contacts to prevent
    contact_history = contact.get_history()
    if id != contact_history[0].id:
        return {
            "msg": ReasonCode.CONTACT_SUPERSEDED_NOT_ACTIVE.value,
            "code": ReasonCode.CONTACT_SUPERSEDED_NOT_ACTIVE.name,
            "active_contact": contact_history[0].id,
        }, HTTPStatus.CONFLICT

    contact.cancelled_at = datetime.utcnow()

    response = contactschema.CancelResponse(
        code=ContactMessages.CANCEL_SUCCESS.name,
        msg=ContactMessages.CANCEL_SUCCESS.value,
        contact_id=id,
        cancelled_at=contact.cancelled_at,
    )
    return response, HTTPStatus.OK


@app.route("/satellite/<sat_id>/contacts/<id>/dispatch", methods=["POST"])
@auth_endpoint("execute")
@validate()
def dispatch_contact(sat_id: str, id: UUID):
    if not Satellite.query_by_hash(session, sat_id):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (contact := Contact.get_by_id(session=session, id=id)):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)
    if contact.cancelled_at:
        raise ConflictError(ReasonCode.CONTACT_DISPATCH_CANCELLED)

    contact.dispatched_at = datetime.utcnow()
    response = contactschema.DispatchResponse(
        code=ContactMessages.DISPATCH_SUCCESS.name,
        msg=ContactMessages.DISPATCH_SUCCESS.value,
        contact_id=id,
        dispatched_at=contact.dispatched_at,
    )
    return response, HTTPStatus.OK


@app.route(
    "/satellite/<sat_id>/contacts/<contact_id>/instructions/<id>/start",
    methods=["POST"],
)
@auth_endpoint("execute")
@validate()
def start_contact_execution(
    sat_id: str,
    contact_id: UUID,
    id: int,
    body: contactschema.ExecuteStartRequest,
):
    if not Satellite.query_by_hash(session, sat_id):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (contact := Contact.get_by_id(session=session, id=str(contact_id))):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)
    if not (instruction := Instruction.get_by_contact_and_id(contact=contact, id=id)):
        raise NotFoundError(ReasonCode.CONTACT_INSTRUCTION_NOT_FOUND)
    if contact.cancelled_at:
        raise ConflictError(ReasonCode.CONTACT_START_CANCELLED)

    # update the start time
    instruction.started_at = body.started_at

    response = contactschema.ExecuteStartResponse(
        code=ContactMessages.EXECUTION_START_SUCCESS.name,
        msg=ContactMessages.EXECUTION_START_SUCCESS.value,
        instruction_id=id,
        started_at=instruction.started_at,
    )
    return response, HTTPStatus.OK


@app.route(
    "/satellite/<sat_id>/contacts/<contact_id>/instructions/<id>/end",
    methods=["POST"],
)
@auth_endpoint("execute")
@validate()
def end_contact_execution(sat_id: str, contact_id: UUID, id: int, body: contactschema.ExecuteEndRequest):
    if not Satellite.query_by_hash(session, sat_id):
        raise NotFoundError(ReasonCode.SATELLITE_NOT_FOUND)
    if not (contact := Contact.get_by_id(session=session, id=str(contact_id))):
        raise NotFoundError(ReasonCode.CONTACT_NOT_FOUND)
    if not (instruction := Instruction.get_by_contact_and_id(contact=contact, id=id)):
        raise NotFoundError(ReasonCode.CONTACT_INSTRUCTION_NOT_FOUND)
    if contact.cancelled_at:
        raise ConflictError(ReasonCode.CONTACT_END_CANCELLED)

    # update the end time
    instruction.ended_at = body.ended_at
    instruction.log = body.log

    response = contactschema.ExecuteEndResponse(
        code=ContactMessages.EXECUTION_END_SUCCESS.name,
        msg=ContactMessages.EXECUTION_END_SUCCESS.value,
        instruction_id=id,
        ended_at=instruction.ended_at,
    )
    return response, HTTPStatus.OK
