import base64
import logging
import zlib
from datetime import timezone
from decimal import Decimal, InvalidOperation
from typing import Callable, Optional, Tuple, Union

import cbor2
from flask_restful import inputs
from flask_restful.reqparse import RequestParser

logger = logging.getLogger(__name__)


class PageCursor:
    @classmethod
    def create_cursor(cls, cursor_data: dict) -> str:
        cursor = cbor2.dumps(cursor_data, timezone=timezone.utc, datetime_as_timestamp=True)
        cursor = base64.urlsafe_b64encode(zlib.compress(cursor, 9)).decode("utf-8")
        return cursor

    @classmethod
    def decode_cursor(cls, cursor_str: Optional[str]) -> Optional[dict]:
        if not cursor_str:
            logger.debug("no cursor submitted")
            return None
        key_json = zlib.decompress(base64.urlsafe_b64decode(cursor_str))
        results = cbor2.loads(key_json)
        return results


def b_to_kb(b):
    """convert bytes to kilobytes in base 2"""
    return b * 0.0009765625
