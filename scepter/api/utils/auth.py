from abc import abstractmethod
from http import HTTPStatus
from typing import List, Tuple, Union
from uuid import UUID

from typing_extensions import Protocol

from models import Asset


class Authable(Protocol):
    @abstractmethod
    def allow_access(
        self,
        access: str = "",
    ) -> Tuple[Union[UUID, int, None], bool, Union[str, None], Union[HTTPStatus, None]]:
        # verify user's access to an endpoint by examining the asset in the url path
        pass

    @abstractmethod
    def check_users_access_to_asset(
        self,
        user_id: Union[UUID, int],
        asset_id: int,
        access: str,
    ) -> bool:
        # check whether a permission exists between a user and asset with the given
        # access level
        pass

    @abstractmethod
    def get_users_assets(
        self,
        user_id: Union[UUID, int],
        access: str,
    ) -> List[Asset]:
        # return a list of assets the user has the nominated access level for
        pass
