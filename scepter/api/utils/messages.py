from enum import Enum


class ErrorMessages:
    INVALID_CURSOR = "Cursor is invalid"


class SatelliteMessages:
    INVALID_SAT_TELECOM_MSG = "Telecom {} does not exist for satellite {}"
    INVALID_SAT_NAME_MSG = "Satellite name length is limited to 256 characters"
    SATELLITE_NOT_FOUND = "Satellite with ID {} not found"


class GroundStationMessages:
    INVALID_GS_ID_MSG = "Ground station {} does not exist"
    INVALID_GS_TELECOM_MSG = "Telecom {} does not exist for ground station {}"
    GS_STATUS_CREATED_MSG = "Status update received"
    GS_NO_STATUS_MSG = "No status updates for that groundstation found"


class TelemetryMessages:
    METADATA_NOT_FOUND = "Telemetry item {} does not exist for satellite {}"
    COMPONENT_WITHOUT_SYSTEM = "Component nominated without system key"
    INVALID_UUID = "Invalid UUID"


class UplinkRequestMessages(Enum):
    UPLINK_REQUEST_CREATE_SUCCESS = "Created uplink request"
    UPLINK_REQUEST_APPROVE_SUCCESS = "Approved uplink request"
    UPLINK_REQUEST_DENY_SUCCESS = "Denied uplink request"


class ContactMessages(Enum):
    ISSUED_CONTACT = "Contact issued"
    UPDATED_CONTACT = "Updated contact"
    CANCEL_SUCCESS = "Contact canceled"
    DISPATCH_SUCCESS = "Dispatch time recorded for contact"
    EXECUTION_START_SUCCESS = "Execution start time recorded for instruction"
    EXECUTION_END_SUCCESS = "Execution end time recorded for instruction"


class PresetMessages(Enum):
    PRESET_CREATE_SUCCESS = "Created new preset"
    PRESET_UPDATE_SUCCESS = "Updated preset"
    PRESET_DELETE_SUCCESS = "Deleted preset"


class ScriptMessages(Enum):
    UPDATE_SUCCESS = "Updated script"


class CommandMessages(Enum):
    UPDATE_SUCCESS = "Updated command"


class ResponseMessages:
    INVALID_PASSIVE_RESP_FILE_TYPE_MSG = "Invalid file type '{}' for Passive Response"
    INVALID_CMD_RESP_FILE_TYPE_MSG = "Invalid file type '{}' for Command Response"
    INVALID_GS_TELECOM_MSG = "Ground station telecom must be specified for response"


class ApiKeyMessages:
    VALID_KEY_CREATE_MSG = "API key successfully created"
    INVALID_KEY_CREATE_MSG = "Failed to create API key"
    VALID_API_KEY_MSG = "Valid API Key"
    INVALID_ACCESS_KEY_MSG = "API key is not valid"
    INVALID_DATES_MSG = "Invalid data; Must include either a start and/or end datetime"
    INVALID_EXPIRE_DATE_MSG = "Invalid datetime; datetime must not be in the past"
    INVALID_START_DATE_MSG = "Invalid datetime; start datetime must preceed end datetime"
    INVALID_ASSET_HASH_MSG = "Invalid asset hash for key {}"
    INVALID_FILE_KEY_MSG = "Invalid file key {} for key {}"
    INVALID_FILE_TYPE_MSG = "Invalid file type {}"
    INVALID_ACCESS_KEY_MSG = "Invalid access"

    INVALID_KEY_NOT_EXIST_MSG = INVALID_ACCESS_KEY_MSG + "; key {} does not exist"
    INVALID_EXPIRED_KEY_MSG = INVALID_ACCESS_KEY_MSG + "; key {} has expired"
    INVALID_FUTURE_START_DATE_MSG = INVALID_ACCESS_KEY_MSG + "; key {} will become active from {}"
    INVALID_ACCESS_TYPE_MSG = INVALID_ACCESS_KEY_MSG + "; invalid action for key with access type {}"


class FileHandlerMessages:
    SUCCESS_MSG = "Success"
    UPLOAD_FAILED_MSG = "Failed to upload"
    DELETE_FAILED_MSG = "Failed to delete"
    DOWNLOAD_FAILED_MSG = "Failed to download"

    DUPLICATE_FILE_MSG = "; duplicate file name"
    INVALID_CREDENTIALS_MSG = "; invalid credentials"
    INVALID_NORAD_MSG = "; invalid Norad ID"
    INVALID_FILE_NAME_MSG = "; invalid file name"
    EMPTY_FILE_MSG = "; empty file"
    FILE_NOT_EXIST_MSG = "; file does not exist"
    TOO_LONG_SAT_ID_MSG = "; sat ID too long, limit to 24 characters"
    INVALID_SAT_ID_MSG = "; satellite does not exist"
    INVALID_USER_PERMISSION_MSG = "; invalid user permission"
    INVALID_TEAM_MSG = "; invalid membership. User must be registered to team account"
    INVALID_KEY_SAT_ID_MSG = "; key {} is not valid for satellite {}"
    INVALID_FILE_TYPE_MSG = "File type {} is invalid"

    UPLOAD_DUPLICATE_NAME_MSG = UPLOAD_FAILED_MSG + DUPLICATE_FILE_MSG
    UPLOAD_INVALID_CREDENTIALS_MSG = UPLOAD_FAILED_MSG + INVALID_CREDENTIALS_MSG
    UPLOAD_INVALID_FILE_NAME_MSG = UPLOAD_FAILED_MSG + INVALID_FILE_NAME_MSG
    UPLOAD_EMPTY_FILE_MSG = UPLOAD_FAILED_MSG + EMPTY_FILE_MSG
    UPLOAD_TOO_LONG_SAT_ID_MSG = UPLOAD_FAILED_MSG + TOO_LONG_SAT_ID_MSG
    UPLOAD_INVALID_SAT_ID_MSG = UPLOAD_FAILED_MSG + INVALID_SAT_ID_MSG
    UPLOAD_INVALID_TEAM_MSG = UPLOAD_FAILED_MSG + INVALID_TEAM_MSG
    UPLOAD_INVALID_KEY_SAT_ID_MSG = UPLOAD_FAILED_MSG + INVALID_KEY_SAT_ID_MSG

    DELETE_INVALID_CREDENTIALS_MSG = DELETE_FAILED_MSG + INVALID_CREDENTIALS_MSG
    DELETE_INVALID_NORAD_MSG = DELETE_FAILED_MSG + INVALID_NORAD_MSG
    DELETE_INVALID_FILE_NAME_MSG = DELETE_FAILED_MSG + INVALID_FILE_NAME_MSG
    DELETE_FILE_NOT_EXIST_MSG = DELETE_FAILED_MSG + FILE_NOT_EXIST_MSG
    DELETE_INVALID_SAT_ID_MSG = DELETE_FAILED_MSG + INVALID_SAT_ID_MSG
    DELETE_INVALID_USER_PERMISSION_MSG = DELETE_FAILED_MSG + INVALID_USER_PERMISSION_MSG
    DELETE_INVALID_FILE_NAME_MSG = DELETE_FAILED_MSG + "; key {} is not valid for file {}"

    DOWNLOAD_INVALID_NORAD_MSG = DOWNLOAD_FAILED_MSG + INVALID_NORAD_MSG
    DOWNLOAD_INVALID_FILE_NAME_MSG = DOWNLOAD_FAILED_MSG + INVALID_FILE_NAME_MSG
    DOWNLOAD_FILE_NOT_EXIST_MSG = DOWNLOAD_FAILED_MSG + FILE_NOT_EXIST_MSG
    DOWNLOAD_INVALID_SAT_ID_MSG = DOWNLOAD_FAILED_MSG + INVALID_SAT_ID_MSG
    DOWNLOAD_INVALID_USER_PERMISSION_MSG = DOWNLOAD_FAILED_MSG + INVALID_USER_PERMISSION_MSG


class SatSystemMessages(Enum):
    SUBSYSTEM_CREATE_SUCCESS = "Subsystem created successfully"
