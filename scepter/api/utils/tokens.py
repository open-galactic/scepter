import jwt
from api.config import config
from cryptography.fernet import Fernet
from dateutil.parser import isoparse


def serialize_token(**data):
    """Return encoded web token and securely store secret parameters"""

    for k, val in data.items():
        # Store secret fields in SSM parameter store
        if k.startswith("_") and val is not None:
            enc_val = encrypt_param(val)

            # terminate if a parameter fails to upload
            if not enc_val:
                return False

            data[k] = {"value": enc_val, "format": type(val).__name__}
        else:
            data[k] = {"value": val, "format": type(val).__name__}

    return jwt.encode(data, config.payload_token_secret_key, algorithm="HS256")


def encrypt_param(value):
    value = str(value).encode()

    fernet = Fernet(config.payload_token_secret_key)

    return fernet.encrypt(value).decode()


def decrypt_param(value):
    value = str(value).encode()

    fernet = Fernet(config.payload_token_secret_key)

    return fernet.decrypt(value).decode()


def unserialize_token(token):
    """Decode and clean token data"""
    try:
        data = jwt.decode(token, config.payload_token_secret_key, algorithms=["HS256"])
        cleaned_data = {}
        for k, val in data.items():

            if k.startswith("_"):
                if val["format"] == "NoneType":
                    cleaned_data[k] = None
                else:
                    dec_value = decrypt_param(val["value"])
                    if not dec_value:
                        cleaned_data[k] = dec_value
                    elif val["format"] == "bool":
                        cleaned_data[k] = bool(dec_value)
                    elif val["format"] == "datetime":
                        cleaned_data[k] = isoparse(dec_value)
                    elif val["format"] == "int":
                        cleaned_data[k] = int(dec_value)
                    else:
                        cleaned_data[k] = dec_value

            else:
                cleaned_data[k] = val["value"]
        return cleaned_data
    except Exception:
        return None
