from api.database import session
from models import GroundStation, Permission, Satellite


def get_permission_of_asset(user_id, asset):
    per = Permission.query_by_user_and_asset_id(session, user_id, asset._id)
    if not per:
        if asset.publicly_visible:
            return "public"
        return None
    return per


def get_asset(**kwargs):
    asset = None
    if kwargs.get("sat_id") or kwargs.get("norad_id"):
        if kwargs.get("sat_id"):
            sat_id = kwargs["sat_id"]
        else:
            sat_id = str(kwargs["norad_id"])
        asset = Satellite.query_by_hash(session, sat_id)
    elif kwargs.get("gs_id"):
        asset = GroundStation.query_by_hash(session, kwargs["gs_id"])
    elif kwargs.get("asset_hash"):
        if kwargs["asset_hash"][0] == "s":
            asset = Satellite.query_by_hash(session, kwargs["asset_hash"])
        elif kwargs["asset_hash"][0] == "g":
            asset = GroundStation.query_by_hash(session, kwargs["asset_hash"])

    if not asset or asset._deleted:
        return None

    return asset
