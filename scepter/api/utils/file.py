from abc import abstractmethod
from typing import Dict, Union

from flask import Response
from typing_extensions import Protocol


class FileHandler(Protocol):
    @abstractmethod
    def get(self, file_key) -> Dict[str, Union[Response, str]]:
        pass

    @abstractmethod
    def create_from_stream(self, stream, file_key, **kwargs) -> Dict[str, Union[int, str]]:
        pass

    @abstractmethod
    def delete(self, file_key) -> Dict[str, str]:
        pass

    @abstractmethod
    def generate_presigned_path(self, file_key) -> Union[str, None]:
        pass

    @abstractmethod
    def create_from_file(self, file_key: str, file, tags) -> Dict[str, str]:
        pass
