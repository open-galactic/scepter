import logging
from pathlib import Path

import psycopg2
from alembic import command
from alembic.config import Config

from migrations.config import db

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def handler(event, context):
    logger.info("Received event: %s" % event)

    conn = psycopg2.connect(host=db.host, user=db.superuser, password=db.superuser_password, port=db.port)
    conn.autocommit = True
    cursor = conn.cursor()

    logger.debug("Testing if database exists")
    cursor.execute(f"SELECT 1 FROM pg_catalog.pg_database WHERE datname = '{db.name}'")
    exists = cursor.fetchone()
    if not exists:
        logger.info(f"Creating database {db.name}")
        cursor.execute(f"CREATE DATABASE {db.name}")

    logger.debug("Testing if user exists")
    cursor.execute(f"SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = '{db.apiuser}'")
    exists = cursor.fetchone()
    if not exists:
        logger.info(f"Creating {db.apiuser}")
        cursor.execute(f"CREATE USER {db.apiuser} WITH PASSWORD '{db.apiuser_password}'")

    conn.close()

    logger.info("Running migrations")
    this_dir = Path(__file__).parent
    ini_file = this_dir.parent / "migrations" / "alembic.ini"
    alembic_cfg = Config(ini_file.resolve())
    command.upgrade(alembic_cfg, "head")

    logger.info("Completed migration")
    return {"success": True}


if __name__ == "__main__":
    handler(None, None)
