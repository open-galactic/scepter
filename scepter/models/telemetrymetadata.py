from __future__ import annotations

from datetime import datetime
from math import ceil
from typing import Optional

from sqlalchemy import (
    BigInteger,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
    and_,
    func,
    inspect,
    select,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Session, relationship

from . import Subsystem
from .base import Base
from .utils import ModelCommonMixIn


class TelemetryMetadata(Base, ModelCommonMixIn):
    __tablename__ = "telem_metadata"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    _timestamp = Column(DateTime, default=datetime.utcnow(), nullable=False)
    _sat_id = Column(Integer, ForeignKey("ops.satellite._id"), nullable=False)
    tlm_name = Column(String, nullable=False)
    uuid = Column(UUID, nullable=False, index=True, unique=True)
    cad_id = Column(String, nullable=True)
    _subsystem_id = Column(BigInteger, ForeignKey("ops.subsystem.id"), nullable=False)
    description = Column(String(1024), nullable=False)
    units = Column(String, nullable=False)
    uncertainty = Column(Float, nullable=True)
    nominal_min = Column(Float, nullable=True)
    nominal_max = Column(Float, nullable=True)
    alert_min = Column(Float, nullable=True)
    alert_max = Column(Float, nullable=True)

    subsystem = relationship("Subsystem", lazy=True)

    UniqueConstraint(
        _subsystem_id,
        tlm_name,
        name="ix_ops_telem_metadata__subsystem_id_tlm_name",
    )

    __table_args__ = {"schema": "ops"}

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    @classmethod
    def get_by_id(cls, session: Session, id: int) -> Optional[TelemetryMetadata]:
        """
        Returns a telemetry metadata object by id
        """
        query = select(cls).where(cls._id == id)
        result = session.execute(query)
        return result.scalar_one_or_none()

    def get_info(self):
        """
        Returns a dictionary of the telemetry metadata information
        """
        cleaned_dict = self._get_info()
        for datetime_key in ["timestamp"]:
            if cleaned_dict.get(datetime_key):
                cleaned_dict[datetime_key] = cleaned_dict[datetime_key].isoformat()
        cleaned_dict["system"] = getattr(self.subsystem.get_parent_system(), "name")
        cleaned_dict["component"] = (
            self.subsystem.name if self.subsystem.type == "component" else None
        )
        return cleaned_dict

    @classmethod
    def search_for_telem_meta(
        cls,
        session: Session,
        sat_id: int,
        tlm_name: Optional[str],
        uuid: Optional[str],
        system: Optional[Subsystem],
        component_id: Optional[int],
        cad_id: Optional[str],
        has_cad_id: Optional[bool],
        page_size: int,
        page: int,
    ):
        """
        Search for telemetry metadata.
        """

        query = session.query(cls).where(cls._sat_id == sat_id)
        query = cls._append_filters(
            query,
            tlm_name,
            uuid,
            system,
            component_id,
            cad_id,
            has_cad_id,
        )

        query = query.order_by(cls.tlm_name)
        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)
        results = query.all()
        results = [telem.get_info() for telem in results]

        return results, total_pages

    @classmethod
    def _append_filters(
        cls,
        query,
        tlm_name: Optional[str],
        uuid: Optional[str],
        system: Optional[Subsystem],
        component_id: Optional[int],
        cad_id: Optional[str],
        has_cad_id: Optional[bool],
    ):
        filters = []

        # search by all subsystem_ids, if one isn't passed
        if system and not component_id:
            ids = [component.id for component in system.children]
            ids.append(system.id)
            query = query.where(cls._subsystem_id.in_(ids))

        elif system and component_id:
            query = query.where(cls._subsystem_id == component_id)

        if tlm_name:
            filters.append(func.lower(TelemetryMetadata.tlm_name) == tlm_name.lower())

        if uuid:
            filters.append(TelemetryMetadata.uuid == uuid)

        if cad_id:
            filters.append(func.lower(TelemetryMetadata.cad_id) == cad_id.lower())

        if has_cad_id is not None:
            if has_cad_id:
                # True
                filters.append(TelemetryMetadata.cad_id.is_not(None))
            else:
                filters.append(TelemetryMetadata.cad_id.is_(None))

        if filters:
            query = query.where(and_(*filters))

        return query

    @classmethod
    def query_by_uuid(
        cls,
        session: Session,
        id: str,
    ) -> Optional[TelemetryMetadata]:
        """
        Returns the telemetry metadata object for the given satellite and uuid
        """
        query = select(cls).where(cls.uuid == id)
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def query_by_tlm_name_and_subsystem(
        cls,
        telem_name: str,
        subsystem: Subsystem,
    ) -> Optional[TelemetryMetadata]:
        """
        Returns the telemetry metadata object for the given telemetry name and subsystem
        """
        session = inspect(subsystem).session
        query = select(cls).where(
            and_(
                cls.tlm_name == telem_name,
                cls._subsystem_id == subsystem.id,
            )
        )
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def get_name_id_map_from_subsystem_id(cls, subsystem: Subsystem):
        """
        Returns a dictionary of telemetry names and ids for a given subsystem id
        """
        session = inspect(subsystem).session
        query = select(cls.tlm_name, cls._id).where(cls._subsystem_id == subsystem.id)
        result = session.execute(query)
        return {row[0]: row[1] for row in result}
