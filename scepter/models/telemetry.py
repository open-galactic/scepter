from __future__ import annotations

from datetime import datetime
from math import ceil
from typing import TYPE_CHECKING, Any, Optional, Tuple

from sqlalchemy import (
    BigInteger,
    Column,
    DateTime,
    ForeignKey,
    Index,
    Integer,
    Text,
    and_,
    bindparam,
    func,
    inspect,
    or_,
    select,
    text,
    union_all,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Session, relationship

from models.base import Base
from models.subsystem import Subsystem
from models.telemetrymetadata import TelemetryMetadata

if TYPE_CHECKING:
    from models.satellite import Satellite


class Telemetry(Base):
    __tablename__ = "telemetry"
    _id = Column(BigInteger, primary_key=True, autoincrement=True)
    observation_time = Column(DateTime, nullable=False, default=datetime.utcnow)
    _tlm_id = Column(Integer, ForeignKey("ops.telem_metadata._id"), nullable=False)
    value = Column(JSONB(astext_type=Text(), none_as_null=True), nullable=False)

    telem_metadata = relationship("TelemetryMetadata", backref="telemetry", lazy=True)

    Index("telemetry__tlm_id_obs_time_idx", _tlm_id, observation_time.desc())

    __table_args__ = {"schema": "ops"}

    TLM_PAGE_SIZE = 10

    def __init__(self, telemetry_metadata: TelemetryMetadata, observed_at: datetime, value: Any):
        self._tlm_id = telemetry_metadata._id
        self.observation_time = observed_at
        self.value = value

    @classmethod
    def list_latest_values(
        cls,
        satellite: Satellite,
        system_name: Optional[str],
        component_name: Optional[str],
        tlm_name: Optional[list[str]],
        cursor: Optional[TelemetryMetadata],
        page_size: Optional[int],
    ) -> Tuple[list, int]:
        session = inspect(satellite).session

        cursor_id = cursor._id if cursor else None

        count_stmt = text(
            """
            SELECT COUNT(*)
            FROM ops.telem_metadata AS meta
            CROSS JOIN LATERAL (
                    SELECT *
                    FROM ops.telemetry
                    WHERE meta._id = _tlm_id
                    LIMIT 1
            ) AS t1
            LEFT OUTER JOIN ops.subsystem AS component ON meta._subsystem_id = component.id
            LEFT OUTER JOIN ops.subsystem AS system ON component.parent_id = system.id
            WHERE meta._sat_id = :sat_id
            AND (meta.tlm_name = ANY (:tlm_name) OR :tlm_name IS NULL)
            AND (system.name = :system_name OR :system_name IS NULL)
            AND (component.name = :component_name OR :component_name IS NULL)
        """
        )
        stmt = count_stmt.bindparams(
            bindparam("sat_id", satellite._id),
            bindparam("tlm_name", tlm_name),
            bindparam("system_name", system_name),
            bindparam("component_name", component_name),
        )
        result = session.execute(stmt)
        row_count = result.scalar()

        stmt = text(
            """
            SELECT
                meta.uuid,
                meta.tlm_name,
                system.name AS system_name,
                component.name AS component_name,
                t1.observation_time,
                t1.value
            FROM ops.telem_metadata AS meta
            CROSS JOIN LATERAL (
                    SELECT *
                    FROM ops.telemetry
                    WHERE meta._id = _tlm_id
                    ORDER BY _tlm_id, observation_time DESC
                    LIMIT 1
            ) AS t1
            LEFT OUTER JOIN ops.subsystem AS component ON meta._subsystem_id = component.id
            LEFT OUTER JOIN ops.subsystem AS system ON component.parent_id = system.id
            WHERE meta._sat_id = :sat_id
            AND (meta.tlm_name = ANY (:tlm_name) OR :tlm_name IS NULL)
            AND (system.name = :system_name OR :system_name IS NULL)
            AND (component.name = :component_name OR :component_name IS NULL)
            AND (meta._id > :cursor_id OR :cursor_id IS NULL)
            ORDER BY meta._id
            LIMIT :page_size
        """
        )
        stmt = stmt.bindparams(
            bindparam("sat_id", satellite._id),
            bindparam("tlm_name", tlm_name),
            bindparam("system_name", system_name),
            bindparam("component_name", component_name),
            bindparam("page_size", page_size),
            bindparam("cursor_id", cursor_id),
        )

        result = session.execute(stmt)
        telemetry = result.all()
        return telemetry, row_count

    @classmethod
    def query_latest_value_for_metadata(cls, telemetry_metadata: TelemetryMetadata) -> Optional[Telemetry]:
        """
        Returns the latest observed telemetry value for this metadata
        """
        session = inspect(telemetry_metadata).session

        query = select(Telemetry).where(Telemetry._tlm_id == telemetry_metadata._id)
        query = query.order_by(Telemetry.observation_time.desc()).limit(1)
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def _search_for_tlm_metadata_ids(
        cls,
        session: Session,
        satellite: Satellite,
        tlm_meta: list[TelemetryMetadata],
        system: Optional[Subsystem],
        tlm_page: int,
    ) -> Tuple[dict[int, str], int]:
        """
        Called by search_for_telem to produce a list of telemetry ids and names
        based on a number of search filters. We're interested in;
          - system
          - tlm_ids (converted from tlm_names before reaching this func)

        Returns a dict of id:name pairs, paginated into groups of TLM_PAGE_SIZE
        The page key is used to OFFSET the query by this amount
        """

        # Only return instances that actually have telemetry data
        tlm_exists = select(Telemetry._id).where(Telemetry._tlm_id == TelemetryMetadata._id).limit(1).exists()

        stmt = select(TelemetryMetadata._id, TelemetryMetadata.uuid).where(
            TelemetryMetadata._sat_id == satellite._id, tlm_exists
        )

        # handling for when specific telemetry items are nominated
        if tlm_meta:
            tlm_ids = [x._id for x in tlm_meta]
            stmt = stmt.where(TelemetryMetadata._id.in_(tlm_ids))

        # handling for when a system is nominated
        if system:
            ids = [component.id for component in system.children]
            ids.append(system.id)
            stmt = stmt.where(TelemetryMetadata._subsystem_id.in_(ids))

        # count the number of rows returned by the query's WHERE statements,
        # excluding the cursor. Note the count of a subquery is as performant
        # as a standard query;
        # https://dba.stackexchange.com/questions/168022/performance-of-count-in-subquery
        count_q = select(func.count()).select_from(stmt.order_by(None).subquery())
        total_tlm_items = session.scalar(count_q)
        total_tlm_pages = ceil(total_tlm_items / cls.TLM_PAGE_SIZE)

        # apply limit/offset pagination to results
        stmt = stmt.order_by(TelemetryMetadata.tlm_name)
        stmt = stmt.limit(cls.TLM_PAGE_SIZE)
        stmt = stmt.offset(cls.TLM_PAGE_SIZE * (tlm_page - 1))
        result = session.execute(stmt)

        id_map: dict[int, str] = {row._id: row.uuid for row in result}
        return id_map, total_tlm_pages

    @classmethod
    def _query_by_metadata_ids(
        cls,
        session: Session,
        tlm_ids: dict,
        observation_time_start: datetime,
        observation_time_end: datetime,
        cursor: dict[str, datetime],
        page_size: int,
    ) -> Tuple[list, dict[int, int]]:
        """
        Query for telemetry data based on an input group of telemetry metadata
        fields (tlm_ids), and a number of additional user inputs. These are;
          - observation_time
          - data_page_size

        This function paginates results based on the data_page_size, and
        an optional cursor that describes the starting timestamp for each tlm
        field. Ordering is always done by DESC observation_time.

        Results are returned as scalar objects matching:
            (tlm_id, observation_time, value)

        Also returned are the remaining results for each telem field, in the
        form of a dict that looks like tlm_id: total_data_pages
        """

        stmt = select(Telemetry._tlm_id, Telemetry.observation_time, Telemetry.value)
        if observation_time_start:
            stmt = stmt.where(Telemetry.observation_time >= observation_time_start)
        if observation_time_end:
            stmt = stmt.where(Telemetry.observation_time <= observation_time_end)

        # this iterates though each of the _tlm_ids returned by querying that
        # table above, and constructs queries for each based on the appropriate
        # cursor, and page size. These are then combined via a UNION_ALL
        data_page_sizes = {}
        data_queries = []
        for tlm_id, tlm_name in tlm_ids.items():
            data_stmt = stmt.where(Telemetry._tlm_id == tlm_id)

            # count the number of rows returned by the query's WHERE
            # statements, excluding the cursor. Note the count of a subquery is
            # as performant as a standard query;
            data_stmt_count = select(func.count()).select_from(data_stmt.order_by(None).subquery())
            data_page_sizes[tlm_id] = ceil(session.scalar(data_stmt_count) / page_size)

            data_stmt = stmt.where(Telemetry._tlm_id == tlm_id)
            if cursor and cursor.get(tlm_name):
                data_stmt = data_stmt.where(Telemetry.observation_time < cursor[tlm_name].replace(tzinfo=None))
            data_stmt = data_stmt.order_by(Telemetry.observation_time.desc())
            data_stmt = data_stmt.limit(page_size)
            data_queries.append(data_stmt)

        # need .all() here as the data is iterated through twice
        data_values = session.execute(union_all(*data_queries)).all()
        return data_values, data_page_sizes

    @classmethod
    def _query_by_data_page_borders(cls, session: Session, data_values: list) -> list[Tuple[int, datetime, Any]]:
        """
        retrieve all values for the last timestamp in each tlm field. This
        fixes the page boundary issue described in search_for_telem. This
        assumes the results are sorted by desc time
        """

        data_dict: dict = {}
        last_id, last_dt, last_val = data_values[0]
        for data in data_values:
            if data[0] != last_id:
                data_dict[last_id] = [last_dt, last_val]
            last_id, last_dt, last_val = data
        data_dict[data_values[-1][0]] = [data_values[-1][1], data_values[-1][2]]

        data_stmt = select(Telemetry._tlm_id, Telemetry.observation_time, Telemetry.value)
        data_queries = []
        for tlm_id, data in data_dict.items():
            data_queries.append(
                data_stmt.where(
                    and_(
                        Telemetry._tlm_id == tlm_id,
                        Telemetry.observation_time == data[0],
                    )
                )
            )
        data_stmt = union_all(*data_queries)

        last_ts_results = session.execute(data_stmt)
        for tlm_id, obs_time, value in last_ts_results:
            if value != data_dict[tlm_id][1]:
                data_values.append((tlm_id, obs_time, value))
        return data_values

    @classmethod
    def _format_tlm_search_results(
        cls,
        data_values: list[Tuple[int, datetime, Any]],
        data_page_sizes: dict[int, int],
        tlm_ids: dict[int, str],
    ) -> Tuple[dict[str, dict[str, Any]], int]:
        """
        Turns a flat list of scalar telemetry values into the return format
        expected by the API. Handles duplicate values, and formatting multiple
        values for a given timestamp.

        Data is output in the format:
        {
            tlm_uuid: {
                total_data_pages: x,
                data: {
                    timestamp_1: value,
                    timestamp_2: value
                    timestamp_3: [value1, value2]
                }
            },
            ...
        }
        """

        tlm_dict: dict[str, dict[str, Any]] = {}
        max_total_data_pages = 1
        for tlm in data_values:
            obs_time = tlm[1].isoformat()
            tlm_uuid = tlm_ids[tlm[0]]
            if not tlm_dict.get(tlm_uuid):
                # Add new telemetry field dict
                tlm_dict[tlm_uuid] = {}
                tlm_dict[tlm_uuid]["total_data_pages"] = data_page_sizes[tlm[0]]
                tlm_dict[tlm_uuid]["data"] = {}
                if data_page_sizes[tlm[0]] > max_total_data_pages:
                    max_total_data_pages = data_page_sizes[tlm[0]]

            if tlm_dict[tlm_uuid]["data"].get(obs_time) is not None:
                # have a duplicate value for a given timestamp
                if not isinstance(tlm_dict[tlm_uuid]["data"][obs_time], list):
                    # create a list for the duplicate results
                    tlm_dict[tlm_uuid]["data"][obs_time] = [tlm_dict[tlm_uuid]["data"].pop(obs_time)]
                if tlm[2] not in tlm_dict[tlm_uuid]["data"][obs_time]:
                    # add the new result ONLY if it's not already in the list
                    # this prevents duplicate values appearing in the same time
                    tlm_dict[tlm_uuid]["data"][obs_time].append(tlm[2])
            else:
                tlm_dict[tlm_uuid]["data"][obs_time] = tlm[2]
        return tlm_dict, max_total_data_pages

    @classmethod
    def search_for_telem(
        cls,
        satellite: Satellite,
        tlm_meta: list[TelemetryMetadata],
        system: Subsystem,
        observation_time_start: datetime,
        observation_time_end: datetime,
        tlm_page: int,
        cursor: dict,
        data_page_size: int,
    ) -> dict:
        """
        DESCRIPTION
        This function is the primary mechanism we have for retrieving telemetry
        data and serving it to clients.

        Its intent is to retrieve 2D slices of our telemetry table; through the
        temporal and telemetry field axes. It outputs a dictionary of telemetry
        fields, each containing a dictionary of telemetry data values keyed and
        ordered by descending timestamps.

        The telemetry fields are paginated using limit/offset. This was deemed
        OK for now, as they should reasonably static. The data is paginated
        using a cursor, as this table will frequently have data added to it,
        and we expect to require very large OFFSET values.

        CURSOR BEHAVIOUR
        The data cursor uses the observation_time column for indexing. This is
        an issue, as the column is only unique in combination with the _id
        column. As such we produce a boundary problem on each of our pages;

        2021-01-01T00:00:01, 1
        2021-01-01T00:00:02, 2
        2021-01-01T00:00:03, 3  <- last result in page x
        2021-01-01T00:00:03, 4  <- gets skipped when we cursor by > last date

        Usually you introduce a second column to make each result unique, so we
        could instead cursor by:
        (observation_time, _id) but then we need to retrieve the _id col in our
            query, and encrypt the cursor we send to the client
        (observation_time, value) however now we need to compare jsonb values,
            which will produce mixed datatype comparisons on arbitrarily large
            values. In addition we need to add the value to the cursor, making
            it potentially very large

        Instead what we do is make a separate query for all values for a tlm_id
        at the last timestamp in the data, and append any values to the current
        page. This is a little inefficient, as we need a second query, and can
        lead to pages longer than was requested but IMO is the neatest solution

        2021-01-01T00:00:01, 1
        2021-01-01T00:00:02, 2
        2021-01-01T00:00:03, 3  <- last result in page x
        2021-01-01T00:00:03, 4  <- added to page x via a query for all data
                                   with the timestamp "2021-01-01T00:00:03"
        """
        session = inspect(satellite).session

        # Return the telemetry metadata that match the search terms
        tlm_ids, total_tlm_pages = cls._search_for_tlm_metadata_ids(session, satellite, tlm_meta, system, tlm_page)
        if not tlm_ids:
            # No telemetry metadata found, so return empty results
            total_tlm_pages = 1 if total_tlm_pages < 1 else total_tlm_pages
            return {
                "pages": {
                    "tlm_page": tlm_page,
                    "total_tlm_pages": total_tlm_pages,
                    "max_total_data_pages": 0,
                },
                "tlm_fields": {},
            }

        # Start query for telemetry data, based on the metadata returned
        data_values, data_page_sizes = cls._query_by_metadata_ids(
            session,
            tlm_ids,
            observation_time_start,
            observation_time_end,
            cursor,
            data_page_size,
        )
        if not data_values:
            # No telemetry data found, so return empty results
            return {
                "pages": {
                    "tlm_page": tlm_page,
                    "total_tlm_pages": total_tlm_pages,
                    "max_total_data_pages": 0,
                },
                "tlm_fields": {},
            }

        # resolve values that exist on the border of each data page
        data_values = cls._query_by_data_page_borders(session, data_values)

        # all data should now exist in the results list. format it into the
        # data structure returned to users
        tlm_dict, max_total_data_pages = cls._format_tlm_search_results(data_values, data_page_sizes, tlm_ids)

        return {
            "pages": {
                "tlm_page": tlm_page,
                "total_tlm_pages": total_tlm_pages,
                "max_total_data_pages": max_total_data_pages,
            },
            "tlm_fields": tlm_dict,
        }

    @classmethod
    def get_by_observation_time(
        cls,
        tlm_meta: TelemetryMetadata,
        observation_time: datetime,
    ) -> list[Telemetry]:

        session: Session = inspect(tlm_meta).session

        query = select(Telemetry).where(
            and_(
                Telemetry._tlm_id == tlm_meta._id,
                Telemetry.observation_time == observation_time,
            )
        )
        result = session.execute(query).scalars().all()
        return result

    @classmethod
    def get_historical_telemetry(
        cls,
        tlm_meta: TelemetryMetadata,
        observation_time_start: Optional[datetime],
        observation_time_end: Optional[datetime],
        cursor: Optional[Telemetry],
        page_size: int,
    ) -> Tuple[list[Telemetry], int]:
        """
        Return historical telemetry data for a given telemetry metadata
        and time range.
        """
        session = inspect(tlm_meta).session

        query = select(Telemetry)
        query = query.where(Telemetry._tlm_id == tlm_meta._id)
        if observation_time_start:
            query = query.where(Telemetry.observation_time >= observation_time_start)
        if observation_time_end:
            query = query.where(Telemetry.observation_time <= observation_time_end)

        count_query = select(func.count()).select_from(query.order_by(None).subquery())
        row_count = session.scalar(count_query)

        if cursor is not None:
            query = query.where(
                or_(
                    Telemetry.observation_time < cursor.observation_time,
                    (
                        and_(
                            Telemetry.observation_time == cursor.observation_time,
                            Telemetry._id < cursor._id,
                        )
                    ),
                )
            )

        query = query.order_by(Telemetry.observation_time.desc(), Telemetry._id.desc())
        query = query.limit(page_size)

        telemetry = session.execute(query).scalars().all()
        return telemetry, row_count
