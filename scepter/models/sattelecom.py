from __future__ import annotations

from math import ceil
from typing import TYPE_CHECKING, Optional

from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    func,
    inspect,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Session, relationship
from sqlalchemy.schema import UniqueConstraint

from .base import Base
from .utils import ModelCommonMixIn

if TYPE_CHECKING:
    from models.satellite import Satellite


class SatTelecom(Base, ModelCommonMixIn):
    __tablename__ = "sat_telecom"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    sat_id = Column(Integer, ForeignKey("ops.satellite._id"), nullable=False)
    frequency = Column(BigInteger, nullable=False)
    telecom_name = Column(String)
    telecom_type = Column(String)
    modulations = Column(JSONB)
    transmits = Column(Boolean)
    receives = Column(Boolean)
    encrypted = Column(Boolean, nullable=False)
    __table_args__ = (UniqueConstraint("sat_id", "telecom_name"), {"schema": "ops"})

    satellite: Satellite = relationship("Satellite", lazy=True, viewonly=True)

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    @property
    def sat_id_repr(self):
        return self.satellite._asset_hash

    @classmethod
    def query_by_name_and_sat(cls, session: Session, sat_id, telecom_name):
        query = session.query(cls).filter(cls.sat_id == sat_id)
        if not query.all():
            return None
        sat_telecom_list = query.filter(
            func.lower(cls.telecom_name) == func.lower(telecom_name)
        ).first()
        return sat_telecom_list

    def get_info(self, sat_hash):
        cleaned_dict = self._get_info()
        cleaned_dict["sat_id"] = sat_hash
        return cleaned_dict

    @classmethod
    def list_telecoms(
        cls,
        satellite: Satellite,
        telecom_name: Optional[str],
        telecom_type: Optional[str],
        modulations: Optional[str],
        page: Optional[int],
        page_size: Optional[int],
    ):

        session = inspect(satellite).session

        query = session.query(cls).filter(cls.sat_id == satellite._id)

        if telecom_name:
            query = query.where((SatTelecom.telecom_name.ilike(f"%{telecom_name}%")))

        if telecom_type:
            query = query.where((SatTelecom.telecom_type.ilike(f"%{telecom_type}%")))

        if modulations:
            query = query.where((SatTelecom.modulations == modulations))

        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)
        results: list[SatTelecom] = query.all()

        return results, total_pages

    @classmethod
    def query_by_id(cls, session: Session, telecom_id):
        return session.query(cls).filter(cls._id == telecom_id).first()
