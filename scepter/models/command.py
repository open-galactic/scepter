from __future__ import annotations

import uuid
from typing import TYPE_CHECKING, Optional

from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    ForeignKey,
    String,
    UniqueConstraint,
    inspect,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Identity

from models.base import Base
from models.preset import Preset

if TYPE_CHECKING:
    from models.satellite import Satellite


class Command(Base):
    __tablename__ = "command"
    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    id = Column(String, nullable=False)
    sat_id = Column(BigInteger, ForeignKey("ops.satellite._id"), nullable=False)
    name = Column(String, nullable=False)
    args = Column(JSONB, nullable=False)
    entrypoint = Column(String, nullable=True)
    code = Column(String, nullable=True)
    requires_approval = Column(Boolean, nullable=True)
    encoder_data = Column(JSONB, nullable=True)

    presets = relationship("Preset")

    UniqueConstraint(id, sat_id, name="ix_ops_command_id_sat_id")

    __table_args__ = {"schema": "ops"}

    @property
    def type(self) -> str:
        return "command"

    def __init__(
        self,
        id: str,
        satellite: Satellite,
        name: str,
        args: dict,
        code: str,
        requires_approval: bool,
        entrypoint: str,
        encoder_data: Optional[str],
    ):
        self.id = id
        self.sat_id = satellite._id
        self.name = name
        self.args = args
        self.code = code
        self.requires_approval = requires_approval
        self.entrypoint = entrypoint
        self.encoder_data = encoder_data

    @classmethod
    def get_by_id(cls, id: str, satellite: Satellite) -> Optional[Command]:
        session = inspect(satellite).session
        query = select(cls).where(cls.id == id, cls.sat_id == satellite._id)
        result = session.execute(query)
        return result.scalar_one_or_none()

    def get_preset_by_id(self, id: uuid.UUID) -> Optional[Preset]:
        query = select(Preset).where(Preset.command_id == self._id, Preset.id == id)
        result = inspect(self).session.execute(query)
        return result.scalar_one_or_none()

    def create_preset(self, name: str, args: dict) -> None:
        preset = Preset(
            id=uuid.uuid4(),
            name=name,
            args=args,
            command_id=self._id,
            script_id=None,
        )
        return preset
