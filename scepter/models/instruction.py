from __future__ import annotations

from typing import Optional

from sqlalchemy import (
    BigInteger,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Text,
    inspect,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Identity

from models.base import Base
from models.command import Command
from models.contact import Contact
from models.script import Script


class Instruction(Base):
    __tablename__ = "instruction"
    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    id = Column(Integer, nullable=False)
    contact_id = Column(
        BigInteger,
        ForeignKey("ops.contact._id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    command_id = Column(
        BigInteger,
        ForeignKey("ops.command._id"),
        nullable=True,
        index=True,
    )
    script_id = Column(
        BigInteger,
        ForeignKey("ops.script._id"),
        nullable=True,
        index=True,
    )
    entrypoint = Column(Text, nullable=False)
    args = Column(JSONB, nullable=False)
    sequence_number = Column(Integer, nullable=True)
    started_at = Column(DateTime, nullable=True)
    ended_at = Column(DateTime, nullable=True)
    log = Column(Text, nullable=True)

    contact = relationship("Contact", back_populates="instructions")
    script = relationship("Script")
    command = relationship("Command")
    uplink_requests = relationship("UplinkRequest", back_populates="instruction")

    __table_args__ = {"schema": "ops"}

    def __init__(
        self,
        id: int,
        args: dict,
        sequence_number: int,
        contact: Contact,
        command: Optional[Command] = None,
        script: Optional[Script] = None,
    ):
        if (not command and not script) or (command and script):
            raise Exception("Must nominate either a command or script")
        if len({inspect(x).session for x in (contact, command, script) if x}) > 1:
            raise Exception("Orm objects do not share a common session")
        self.id = id
        self.args = args
        self.sequence_number = sequence_number
        self.contact_id = contact._id
        self.command_id = command._id if command else None
        self.script_id = script._id if script else None
        self.entrypoint = script.entrypoint if script else command.entrypoint

    @property
    def type(self):
        if self.command_id:
            return "command"
        if self.script_id:
            return "script"
        return None

    @property
    def script_id_repr(self):
        # need a funky property name, as script_id is used by the fkey
        return self.script.id if self.script_id else None

    @property
    def command_id_repr(self):
        # need a funky property name, as command_id is used by the fkey
        return self.command.id if self.command_id else None

    @classmethod
    def get_by_contact_and_id(cls, contact: Contact, id: int) -> Optional[Instruction]:
        session = inspect(contact).session
        query = select(cls).where(cls.contact_id == contact._id, cls.id == id)
        result = session.execute(query)
        instruction = result.scalar_one_or_none()
        return instruction
