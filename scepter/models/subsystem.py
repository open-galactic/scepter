from __future__ import annotations

from typing import TYPE_CHECKING, Optional

from sqlalchemy import (
    BigInteger,
    Column,
    ForeignKey,
    String,
    UniqueConstraint,
    func,
    inspect,
    select,
)
from sqlalchemy.orm import aliased, backref, relationship
from sqlalchemy.schema import Identity

from models.base import Base

if TYPE_CHECKING:
    from models.satellite import Satellite


class Subsystem(Base):
    __tablename__ = "subsystem"
    id = Column(BigInteger, Identity(always=True), primary_key=True)
    name = Column(String, nullable=False)
    type = Column(String, nullable=False)
    address = Column(String, index=True)
    sat_id = Column(BigInteger, ForeignKey("ops.satellite._id"), nullable=False)
    parent_id = Column(
        BigInteger, ForeignKey("ops.subsystem.id"), nullable=True, index=True
    )

    children = relationship("Subsystem", backref=backref("parent", remote_side=[id]))

    UniqueConstraint(name, parent_id, name="ix_ops_subsystem_name_parent_id")
    UniqueConstraint(
        sat_id,
        name,
        address,
        func.coalesce(parent_id, -1),
        name="ix_ops_subsystem_name_parent_id",
    )

    __table_args__ = {"schema": "ops"}

    def __init__(
        self,
        satellite: Satellite,
        name: str,
        type: str,
        address: str,
        parent: Subsystem = None,
    ) -> None:
        if parent and inspect(satellite).session is not inspect(parent).session:
            raise Exception(
                "satellite and parent objects do not share a common session"
            )
        self.name = name
        self.type = type
        self.address = address
        self.sat_id = satellite._id
        self.parent_id = parent.id if parent else None

    def get_parent_system(self) -> Subsystem:
        """
        Typically expect structure for subsystem types to follow;
        - system
          - component
        Generally systems should only exist at the top level, and components
        should be children to those parents, 1 layer deep (no components as
        parents)
        """
        if self.type == "system":
            return self
        else:
            return self.parent

    @classmethod
    def get_system_by_name(cls, satellite: Satellite, name: str) -> Optional[Subsystem]:
        """
        Returns the system with the given name, or None if not found
        """
        session = inspect(satellite).session
        query = select(cls).where(
            cls.type == "system", cls.name == name, cls.sat_id == satellite._id
        )
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def get_component_by_name(cls, system: Subsystem, name: str) -> Optional[Subsystem]:
        """
        Returns the component with the given name, or None if it doesn't exist
        """
        session = inspect(system).session
        query = select(cls).where(cls.name == name, cls.parent_id == system.id)
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def get_component_by_addresses(
        cls, sat: Satellite, component_address: str, system_address: str
    ) -> list[Subsystem]:
        """
        Returns a list of tuples of (component_id, component_name) for the given
        component address and system address.
        """
        session = inspect(sat).session
        system = aliased(cls)
        query = (
            select(cls)
            .join(system, cls.parent_id == system.id)
            .where(
                cls.address == component_address,
                system.address == system_address,
                system.sat_id == sat._id,
            )
        )
        result = session.execute(query)
        return result.scalars().all()
