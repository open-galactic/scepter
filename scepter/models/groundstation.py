from __future__ import annotations

from datetime import datetime
from math import ceil
from typing import Optional, Union
from uuid import UUID, uuid4

from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    and_,
    func,
    or_,
)
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Session, relationship

from . import Asset, Permission


class GroundStation(Asset):
    __tablename__ = "groundstation"
    _timestamp = Column(DateTime)
    _id = Column(
        Integer,
        ForeignKey("ops.asset._id"),
        primary_key=True,
        unique=True,
        nullable=False,
    )
    gs_name = Column(String(256))
    gs_hostname = Column(String(120))
    gs_port = Column(Integer)
    commission_date = Column(DateTime)
    latitude = Column(Float)
    longitude = Column(Float)
    altitude = Column(Float)
    address = Column(String(256))
    statuses = relationship("GsStatus", backref="gs", lazy=True)
    telecoms = relationship("GsTelecom", backref="gs", lazy=False)
    _config = Column(postgresql.JSON)

    __mapper_args__ = {"polymorphic_identity": "groundstation"}
    __table_args__ = {"schema": "ops"}

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)
        if kwargs.get("asset_hash"):
            self._asset_hash = kwargs["asset_hash"]
        else:
            raise Exception("GS initiated with no hash ID")

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = value

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config_json):
        self._config = config_json

    @classmethod
    def query_by_hash(cls, session: Session, gs_hash) -> Optional[GroundStation]:
        return (
            session.query(cls)
            .filter(func.lower(cls._asset_hash) == func.lower(gs_hash))
            .first()
        )

    @classmethod
    def query_by_id(cls, session: Session, gs_id):
        return session.query(cls).filter(cls._id == gs_id).first()

    @classmethod
    def select_user_groundstations(
        cls,
        session: Session,
        user_id: Union[UUID, int],
        publicly_visible: Optional[bool],
        page: Optional[int],
        page_size: Optional[int],
    ) -> tuple[list[GroundStation], int]:
        query = session.query(cls).outerjoin(Permission)
        if type(user_id) == UUID:
            query = query.filter(
                or_(
                    and_(
                        Permission.user_uuid == user_id,
                        Permission.read_access == True,  # noqa: E712
                    ),
                    cls.publicly_visible == True,  # noqa: E712
                )
            )
        elif type(user_id) == int:
            query = query.filter(
                or_(
                    and_(
                        Permission.user_id == user_id,
                        Permission.read_access == True,  # noqa: E712
                    ),
                    cls.publicly_visible == True,  # noqa: E712
                )
            )
        query = query.filter(
            or_(cls._deleted == False, cls._deleted == None)  # noqag: E712
        )

        if publicly_visible is not None:
            query = query.where((GroundStation.publicly_visible.is_(publicly_visible)))

        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)
        results: list[GroundStation] = query.all()

        return results, total_pages

    def delete(self, session: Session):
        self._original_hash = self._asset_hash
        self._deleted = True
        self._deleted_at = datetime.utcnow()
        self._asset_hash = str(uuid4())

    @classmethod
    def query_user_gs(cls, session: Session, user_id: Union[UUID, int]):
        query = session.query(cls).outerjoin(Permission)
        if type(user_id) == UUID:
            query = query.filter(
                or_(
                    and_(
                        Permission.user_uuid == user_id,
                        Permission.read_access == True,  # noqa: E712
                    ),
                    cls.publicly_visible == True,  # noqa: E712
                )
            )
        elif type(user_id) == int:
            query = query.filter(
                or_(
                    and_(
                        Permission.user_id == user_id,
                        Permission.read_access == True,  # noqa: E712
                    ),
                    cls.publicly_visible == True,  # noqa: E712
                )
            )
        query = query.filter(
            or_(cls._deleted == False, cls._deleted == None)  # noqag: E712
        )
        return query.all()
