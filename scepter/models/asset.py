from __future__ import annotations

from typing import Union

from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.orm import Session, relationship

from .base import Base
from .utils import ModelCommonMixIn


class Asset(Base, ModelCommonMixIn):
    __tablename__ = "asset"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    _asset_hash = Column(String(36), unique=True, nullable=True)
    _asset_type = Column(String)
    publicly_visible = Column(Boolean, default=False)
    users = relationship("Permission", back_populates="asset")
    _deleted = Column(Boolean)
    _deleted_at = Column(DateTime)
    _original_hash = Column(String(36))

    __mapper_args__ = {"polymorphic_identity": "asset", "polymorphic_on": _asset_type}
    __table_args__ = {"schema": "ops"}

    @classmethod
    def query_by_hash(cls, session: Session, asset_hash: str) -> Union[Asset, None]:
        return session.query(cls).filter(cls._asset_hash == asset_hash).first()
