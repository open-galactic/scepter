from __future__ import annotations

import logging
import uuid
from datetime import datetime
from typing import TYPE_CHECKING, List, Optional

from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
    and_,
    inspect,
    or_,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.orm import Session, relationship

from .base import Base
from .utils import ModelCommonMixIn

if TYPE_CHECKING:
    from models import GroundStation, Satellite

logger = logging.getLogger(__name__)


class Overpass(Base, ModelCommonMixIn):
    __tablename__ = "overpass"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    id = Column(UUID(as_uuid=True), nullable=False, unique=True)
    source = Column(String, nullable=False)
    source_reference = Column(String, nullable=True)
    created_at = Column(DateTime, nullable=False)
    gs_id = Column(Integer, ForeignKey("ops.groundstation._id"), nullable=False)
    sat_id = Column(Integer, ForeignKey("ops.satellite._id"), nullable=False)
    aos = Column(DateTime, nullable=False)
    tca = Column(DateTime, nullable=False)
    los = Column(DateTime, nullable=False)
    maximum_el = Column(Float, nullable=False)
    trajectory = Column(JSONB, nullable=False)
    tle_updated_at = Column(DateTime, nullable=False)

    groundstation = relationship("GroundStation", lazy=True)
    satellite = relationship("Satellite", lazy=True)

    UniqueConstraint(
        source, source_reference, name="ix_ops_overpass_source_source_reference"
    )

    __table_args__ = {"schema": "ops"}

    @property
    def sat_id_repr(self):
        return self.satellite._asset_hash

    @property
    def gs_id_repr(self):
        return self.groundstation._asset_hash

    def __init__(
        self,
        groundstation: GroundStation,
        satellite: Satellite,
        source: str,
        aos: datetime,
        tca: datetime,
        los: datetime,
        maximum_el: float,
        trajectory: dict,
        tle_updated_at: datetime,
        source_reference: Optional[str] = None,
    ):
        if inspect(satellite).session is not inspect(groundstation).session:
            logger.error("satellite and ground station do not share a common session")
            raise Exception(
                "satellite and ground station do not share a common session"
            )

        self.id = uuid.uuid4()
        self.created_at = datetime.utcnow()
        self.gs_id = groundstation._id
        self.sat_id = satellite._id
        self.source = source
        self.source_reference = source_reference
        self.aos = aos
        self.tca = tca
        self.los = los
        self.maximum_el = maximum_el
        self.trajectory = trajectory
        self.tle_updated_at = tle_updated_at

    @classmethod
    def get_by_id(cls, session: Session, id: uuid.UUID) -> Optional["Overpass"]:
        return session.query(cls).filter(cls.id == id).first()

    @classmethod
    def list_passes_in_window(
        cls,
        groundstations: List[GroundStation],
        satellites: List[Satellite],
        start_time: datetime,
        end_time: datetime,
    ) -> List[Overpass]:

        if not satellites or not groundstations:
            return []
        if len({inspect(x).session for x in satellites + groundstations}) > 1:
            logger.error("satellite and gs objects do not share a common session")
            raise Exception("satellite and gs objects do not share a common session")
        session = inspect(groundstations[0]).session

        query = select(cls).where(
            cls.gs_id.in_([x._id for x in groundstations]),
            cls.sat_id.in_([x._id for x in satellites]),
            or_(
                and_(start_time <= cls.aos, end_time >= cls.aos),
                and_(start_time <= cls.los, end_time >= cls.los),
                and_(start_time >= cls.aos, start_time <= cls.los),
            ),
        )

        query = query.order_by(cls.aos.asc())
        results = session.scalars(query).all()
        return results
