from __future__ import annotations

import json
from math import ceil
from typing import Optional, Tuple, Union
from uuid import UUID

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    and_,
    func,
    inspect,
    literal,
    or_,
    select,
    union_all,
)
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Session, relationship

from models.asset import Asset
from models.command import Command
from models.permission import Permission
from models.script import Script
from models.subsystem import Subsystem
from models.utils import ModelCommonMixIn


class SatMixIn(object):
    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = value


# TODO add other useful fields for satellites
class Satellite(SatMixIn, Asset, ModelCommonMixIn):
    __tablename__ = "satellite"
    _timestamp = Column(DateTime)
    _id = Column(
        Integer,
        ForeignKey("ops.asset._id"),
        primary_key=True,
        nullable=False,
        unique=True,
    )
    norad_id = Column(Integer, unique=True)
    nssdc_id = Column(String(24), unique=True)
    sat_name = Column(String(256))
    launch_date = Column(DateTime)
    commission_date = Column(DateTime)
    decoder = Column(String(64))
    encoder = Column(String(64))
    description = Column(String(1024))
    tle = Column(postgresql.JSONB, nullable=True)
    _config = Column(postgresql.JSON)

    statuses = relationship("SatStatus", backref="sat", lazy=True)
    telem_metadata = relationship("TelemetryMetadata", backref="sat", lazy=True)

    __mapper_args__ = {"polymorphic_identity": "satellite"}
    __table_args__ = {"schema": "ops"}

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)
        if kwargs.get("asset_hash"):
            self._asset_hash = kwargs["asset_hash"]
        else:
            raise Exception("Sat initiated without hash ID")

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config_json):
        self._config = config_json

    @classmethod
    def query_by_id(cls, session: Session, sat_id):
        return session.query(cls).filter(cls._id == sat_id).first()

    @classmethod
    def query_by_hash(cls, session: Session, sat_id):
        if sat_id[0] == "s":
            return cls.query_by_s_id(session, sat_id)
        else:
            try:
                int(sat_id)
                return cls.query_by_norad_id(session, sat_id)
            except ValueError:
                return None

    @classmethod
    def query_by_s_id(cls, session: Session, s_id):
        return session.query(cls).filter(func.lower(cls._asset_hash) == func.lower(s_id)).first()

    @classmethod
    def query_by_norad_id(cls, session: Session, norad_id):
        return session.query(cls).filter(cls.norad_id == norad_id).first()

    @classmethod
    def query_by_nssdc_id(cls, session: Session, nssdc_id):
        return session.query(cls).filter(func.lower(cls.nssdc_id) == func.lower(nssdc_id)).first()

    @classmethod
    def get_info(cls, session: Session, sat_id):
        sat_dict = dict(cls.query_by_hash(session, sat_id).__dict__)
        cleaned_dict = {key: value for key, value in sat_dict.items() if key[0] != "_"}
        for datetime_key in ["commission_date", "launch_date"]:
            if cleaned_dict.get(datetime_key):
                cleaned_dict[datetime_key] = cleaned_dict[datetime_key].isoformat()
        if cleaned_dict["tle"]:
            cleaned_dict["tle"] = json.loads(cleaned_dict["tle"])
        else:
            cleaned_dict["tle"] = None
        cleaned_dict["sat_id"] = sat_dict["_asset_hash"]
        return cleaned_dict

    def get_systems(self) -> list[Subsystem]:
        session = inspect(self).session
        query = select(Subsystem).where(Subsystem.sat_id == self._id, Subsystem.parent_id.is_(None))
        results = session.execute(query)
        return results.scalars().all()

    @classmethod
    def select_user_satellites(
        cls,
        session: Session,
        user_id: Union[UUID, int],
        publicly_visible: Optional[bool],
        page: Optional[int],
        page_size: Optional[int],
    ) -> tuple[list[Satellite], int]:
        if type(user_id) == UUID:
            query = (
                session.query(cls)
                .outerjoin(Permission)
                .filter(
                    or_(
                        cls.publicly_visible == True,  # noqa: E712
                        and_(
                            Permission.user_uuid == user_id,
                            Permission.read_access == True,  # noqa: E712
                        ),
                    )
                )
                .filter(or_(cls._deleted == False, cls._deleted == None))  # noqa: E712 E711
            )
        elif type(user_id) == int:
            query = (
                session.query(cls)
                .outerjoin(Permission)
                .filter(
                    or_(
                        cls.publicly_visible == True,  # noqa: E712
                        and_(
                            Permission.user_id == user_id,
                            Permission.read_access == True,  # noqa: E712
                        ),
                    )
                )
                .filter(or_(cls._deleted == False, cls._deleted == None))  # noqa: E712 E711  # noqa: E712 E711
            )
        if publicly_visible is not None:
            query = query.where((Satellite.publicly_visible.is_(publicly_visible)))

        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)
        results: list[Satellite] = query.all()

        return results, total_pages

    def list_templates(
        self,
        template_type: Optional[str],
        page_size: int,
        cursor: Optional[Union[Command, Script]] = None,
    ) -> Tuple[list, int]:
        session = inspect(self).session

        command_query = select(
            Command.id,
            Command.name,
            Command.args,
            Command.entrypoint,
            literal("command").label("type"),
        ).where(Command.sat_id == self._id)
        script_query = select(
            Script.id,
            Script.name,
            Script.args,
            Script.entrypoint,
            literal("script").label("type"),
        ).where(Script.sat_id == self._id)

        if not template_type:
            query = union_all(command_query, script_query)
        if template_type == "command":
            query = command_query
        if template_type == "script":
            query = script_query

        count_query = select(func.count()).select_from(query.order_by(None).subquery())
        row_count = session.scalar(count_query)

        query = query.subquery().select()
        if cursor is not None:
            query = query.where(query.selected_columns.id > cursor.id)
        query = query.order_by(query.selected_columns.id.asc()).limit(page_size)
        results = session.execute(query).all()
        return results, row_count
