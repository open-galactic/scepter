from __future__ import annotations

from typing import TYPE_CHECKING, Union

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, inspect
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import Session, relationship

from .base import Base
from .utils import ModelCommonMixIn

if TYPE_CHECKING:
    from models.groundstation import GroundStation
    from models.satellite import Satellite


class GsStatus(Base, ModelCommonMixIn):
    __tablename__ = "gs_status"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    _timestamp = Column(DateTime)
    gs_id = Column(Integer, ForeignKey("ops.groundstation._id"), nullable=False)
    update_time = Column(DateTime)
    device = Column(String)
    properties = Column(JSON)
    status = Column(String)
    errors = Column(String)
    sat_tracking = Column(Integer, ForeignKey("ops.satellite._id"), nullable=True)
    sat_tracking_bool = Column(Boolean, nullable=True)
    data_link = Column(Boolean, default=False)

    groundstation: GroundStation = relationship(
        "GroundStation", lazy=True, viewonly=True
    )
    satellite: Satellite = relationship("Satellite", lazy=True, viewonly=True)

    __table_args__ = {"schema": "ops"}

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    @property
    def gs_id_repr(self):
        return self.groundstation._asset_hash

    @property
    def sat_id_repr(self):
        return self.satellite._asset_hash

    @classmethod
    def get_number_of_gs_status_updates(cls, session: Session, gs_id):
        query = session.query(cls).filter(cls.gs_id == gs_id)
        return query.count()

    @classmethod
    def select_last_status(cls, groundstation: GroundStation) -> Union[GsStatus, None]:

        session = inspect(groundstation).session

        gs_last_status = (
            session.query(cls)
            .filter(cls.gs_id == groundstation._id)
            .order_by(cls._timestamp.desc())
            .first()
        )
        if not gs_last_status:
            return None

        return gs_last_status
