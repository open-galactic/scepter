from __future__ import annotations

import uuid
from datetime import datetime
from typing import TYPE_CHECKING, List, Optional

from sqlalchemy import (  # type: ignore
    BigInteger,
    Column,
    DateTime,
    ForeignKey,
    Sequence,
    String,
    func,
    inspect,
    select,
    text,
)
from sqlalchemy.dialects.postgresql import UUID  # type: ignore
from sqlalchemy.orm import Session, aliased, relationship  # type: ignore
from sqlalchemy.schema import Identity  # type: ignore

from models.base import Base
from models.overpass import Overpass

if TYPE_CHECKING:
    from models import Satellite


class Contact(Base):
    __tablename__ = "contact"
    group_id_seq = Sequence("ops.contact_group_id_seq", metadata=Base.metadata)

    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    id = Column(UUID(as_uuid=True), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=text("now()"))
    cancelled_at = Column(DateTime, nullable=True)
    group_id = Column(BigInteger, index=True, server_default=group_id_seq.next_value(), nullable=False)
    overpass_id = Column(BigInteger, ForeignKey("ops.overpass._id"), nullable=True)
    sat_id = Column(BigInteger, ForeignKey("ops.satellite._id"), nullable=False)
    issued_by = Column(String, nullable=False)
    issued_at = Column(DateTime, nullable=False)
    dispatched_at = Column(DateTime, nullable=True)

    instructions = relationship("Instruction", back_populates="contact", cascade="all, delete")
    satellite = relationship("Satellite", uselist=False)
    overpass = relationship("Overpass", uselist=False)

    __table_args__ = {"schema": "ops"}

    def __init__(
        self,
        overpass: Overpass,
        satellite: Satellite,
        issued_by: str,
        issued_at: datetime,
        group_id: Optional[int] = None,
    ):
        if inspect(overpass).session is not inspect(satellite).session:
            raise Exception("satellite and overpass objects do not share a common session")
        # TODO replace uuid4() with server default once migrations are used in tests
        self.id = uuid.uuid4()
        self.group_id = group_id
        self.overpass_id = overpass._id
        self.sat_id = satellite._id
        self.issued_by = issued_by
        self.issued_at = issued_at

    @classmethod
    def get_by_id(cls, session: Session, id: uuid.UUID) -> Optional[Contact]:
        query = select(Contact).where(cls.id == id)
        result = session.execute(query)
        contact = result.scalar_one_or_none()
        return contact

    def get_history(self) -> List[Contact]:
        session = inspect(self).session
        query = select(Contact).where(Contact.group_id == self.group_id)
        query = query.order_by(Contact.created_at.desc())
        result = session.execute(query)
        contact = result.scalars().all()
        return contact

    @classmethod
    def list_active_for_sat(
        cls,
        satellite: Satellite,
        upcoming: Optional[bool],
        include_cancelled: Optional[bool],
        page_size: int,
        cursor: Optional[Contact],
    ) -> List[Contact]:
        """
        This method returns the latest version of each contact as a list. It uses
        a correlation query which in SQL looks like;

            SELECT *
            FROM ops.contact
            WHERE ops.contact.timestamp = (
                SELECT MAX(timestamp)
                FROM ops.contact AS contact_alias
                WHERE contact_alias.group_id=ops.contact.group_id
            )
            AND ...
        """
        session = inspect(satellite).session

        contact_alias = aliased(Contact)
        latest_instance_query = (
            select(func.max(contact_alias.created_at)).where(contact_alias.group_id == cls.group_id).scalar_subquery()
        )

        query = select(cls).join(Overpass).where(cls.created_at == latest_instance_query, cls.sat_id == satellite._id)
        if not include_cancelled:
            query = query.where(Contact.cancelled_at.is_(None))
        if upcoming is True:
            query = query.where(Overpass.aos >= datetime.utcnow())
        elif upcoming is False:
            query = query.where(Overpass.aos <= datetime.utcnow())

        # count the number of rows returned by the query's WHERE statements,
        # excluding the cursor. Note the count of a subquery is as performant
        # as a standard query;
        # https://dba.stackexchange.com/questions/168022/performance-of-count-in-subquery
        count_query = select(func.count()).select_from(query.order_by(None).subquery())
        row_count = session.scalar(count_query)

        if cursor is not None:
            query = query.where(Contact.id < cursor.id)

        query = query.order_by(Contact.id.desc()).limit(page_size)
        result = session.execute(query)
        contacts = result.scalars().all()
        return contacts, row_count
