from __future__ import annotations

import logging
import uuid
from typing import List, Optional, Union

from sqlalchemy import (
    BigInteger,
    Column,
    DateTime,
    ForeignKey,
    String,
    Text,
    inspect,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.orm import Session

from models.base import Base
from models.satellite import Satellite

logger = logging.getLogger(__name__)


class Alert(Base):
    __tablename__ = "alert"

    _id = Column(BigInteger, primary_key=True)
    id = Column(UUID(as_uuid=True), nullable=False)
    metadata_id = Column(UUID(as_uuid=True), nullable=False)
    faulted_at = Column(DateTime, nullable=False)
    created_at = Column(DateTime, nullable=False)
    value = Column(JSONB(astext_type=Text(), none_as_null=True), nullable=False)
    error_code = Column(String(50), nullable=False)

    sat_id = Column(BigInteger, ForeignKey("ops.satellite._id"), nullable=False)

    __table_args__ = {"schema": "ops"}

    def __init__(
        self,
        metadata_id: UUID,
        satellite: Satellite,
        faulted_at: DateTime,
        created_at: DateTime,
        value: Union[int, float, str],
        error_code: str,
    ):
        self.id = uuid.uuid4()
        self.metadata_id = metadata_id
        self.sat_id = satellite._id
        self.faulted_at = faulted_at
        self.created_at = created_at
        self.value = value
        self.error_code = error_code

    @classmethod
    def get_by_id(cls, session: Session, id: UUID) -> Optional[Alert]:
        return session.query(cls).filter(cls.id == id).first()

    @classmethod
    def list_for_sats(
        cls,
        session: Session,
        satellites: List[Satellite],
        faulted_time_start: Optional[DateTime],
        faulted_time_end: Optional[DateTime],
        error_code: Optional[str],
    ) -> List[Alert]:
        """
        returns a list of alerts for the provided filter parameters
        """

        # check the session of each satellite instance, and the session var itself
        # are all the same, to avoid mixing together multiple sessions
        if (
            satellites
            and len({inspect(x).session for x in satellites} | {session()}) > 1
        ):
            logger.error("satellite objects do not share a common session")
            raise Exception("satellite objects do not share a common session")

        query = select(cls).join(Satellite)
        query = query.where(Satellite._id.in_([x._id for x in satellites]))
        if faulted_time_start:
            query = query.where(cls.faulted_at >= faulted_time_start)
        if faulted_time_end:
            query = query.where(cls.faulted_at <= faulted_time_end)
        if error_code:
            query = query.where(cls.error_code == error_code)
        query = query.order_by(cls.created_at.desc())
        result = session.execute(query)
        alerts = result.scalars().all()
        return alerts
