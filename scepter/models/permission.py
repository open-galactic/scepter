from __future__ import annotations

import uuid
from typing import List, Optional, Union

from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    ForeignKey,
    Integer,
    UniqueConstraint,
    and_,
    inspect,
    select,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Session, relationship
from sqlalchemy.schema import Identity

from . import Asset
from .base import Base
from .utils import ModelCommonMixIn


class Permission(Base, ModelCommonMixIn):
    __tablename__ = "permission"
    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    asset_id = Column(Integer, ForeignKey("ops.asset._id"), nullable=False)
    user_id = Column(Integer, nullable=True)
    user_uuid = Column(UUID(as_uuid=True), nullable=True)
    read_access = Column(Boolean, default=False)
    command_access = Column(Boolean, default=False)
    manage_asset = Column(Boolean, default=False)
    manage_users = Column(Boolean, default=False)
    owner = Column(Boolean, default=False)

    asset = relationship("Asset", lazy=True)

    UniqueConstraint(asset_id, user_uuid)
    UniqueConstraint(asset_id, user_id)

    __table_args__ = {"schema": "ops"}

    @property
    def asset_id_repr(self):
        return self.asset._asset_hash

    @classmethod
    def create_permission(
        cls,
        user_id: Union[uuid.UUID, int],
        asset_id: int,
        read_access: bool,
        command_access: bool,
        manage_asset: bool,
        manage_users: bool,
        owner: bool,
    ):
        if type(user_id) == int:
            permission = Permission(
                user_id=user_id,
                asset_id=asset_id,
                read_access=read_access,
                command_access=command_access,
                manage_asset=manage_asset,
                manage_users=manage_users,
                owner=owner,
            )
        elif type(user_id) == uuid.UUID:
            permission = Permission(
                user_uuid=user_id,
                asset_id=asset_id,
                read_access=read_access,
                command_access=command_access,
                manage_asset=manage_asset,
                manage_users=manage_users,
                owner=owner,
            )
        else:
            permission = None
        return permission

    @classmethod
    def query_by_user_and_asset_id(
        cls, session: Session, user_id: Union[uuid.UUID, int], asset_id: int
    ) -> Union[Permission, None]:
        if type(user_id) == int:
            permission_query = session.query(cls).filter(
                and_(cls.asset_id == asset_id, cls.user_id == user_id)
            )
        elif type(user_id) == uuid.UUID:
            permission_query = session.query(cls).filter(
                and_(cls.asset_id == asset_id, cls.user_uuid == user_id)
            )
        else:
            return None
        return permission_query.first()

    @classmethod
    def validate_access_string(cls, access_string: Optional[str]) -> bool:
        if access_string in [
            "read_access",
            "command_access",
            "manage_assets",
            "manage_users",
        ]:
            return True
        return False

    @classmethod
    def get_assets_by_user_access(
        cls, session: Session, user_id: Union[uuid.UUID, int], access: Optional[str]
    ) -> List[Asset]:
        if not cls.validate_access_string(access):
            raise Exception(f"Invalid access: {access}")

        if type(user_id) == int:
            query = select(Asset).join(cls).where(cls.user_id == user_id)
        elif type(user_id) == uuid.UUID:
            query = select(Asset).join(cls).where(cls.user_uuid == user_id)
        else:
            return []
        if access:
            query = query.where(getattr(cls, access, None) == True)  # noqa: E712
        result = session.execute(query)
        assets = result.scalars().all()
        return assets

    @classmethod
    def select_permissions_for_asset(
        cls,
        user_id: Optional[Union[uuid.UUID, int]],
        asset: Asset,
    ) -> List[Permission]:

        session = inspect(asset).session

        query = select(cls).join(Asset).where(Asset._asset_hash == asset._asset_hash)

        if type(user_id) == int:
            query = query.where(Permission.user_id == user_id)

        elif type(user_id) == uuid.UUID:
            query = query.where(Permission.user_uuid == user_id)

        query.order_by(Asset._asset_hash).limit(50)

        result = session.scalars(query).all()

        return result

    @classmethod
    def check_access(
        cls,
        session: Session,
        user_id: Union[uuid.UUID, int],
        asset_id: int,
        access: str,
    ) -> bool:
        if not cls.validate_access_string(access):
            raise Exception(f"Invalid access: {access}")
        return getattr(
            Permission.query_by_user_and_asset_id(session, user_id, asset_id),
            access,
            None,
        )
