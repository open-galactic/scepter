from __future__ import annotations

from datetime import datetime
from math import ceil
from typing import TYPE_CHECKING

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    and_,
    inspect,
)
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.orm import Session

from .base import Base
from .utils import ModelCommonMixIn

file_types = {
    "uplink": 0,
    "pass_recording": 1,
    "raw": 2,
    "processed": 3,
    "log": 4,
    "unknown": 5,
    "beacon": 6,
}

if TYPE_CHECKING:
    from models.asset import Asset


class FileTransferMetadata(Base, ModelCommonMixIn):
    __tablename__ = "file_transfer_metadata"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    asset_hash = Column(String(12), ForeignKey("ops.asset._asset_hash"), nullable=False)
    _file_key = Column(String(120), unique=True, nullable=False)
    _timestamp = Column(DateTime, default=datetime.utcnow, nullable=False)
    _file_size = Column(Float, nullable=True)
    _system = Column(String(50), nullable=True)
    _deleted = Column(Boolean, nullable=True, default=False)
    file_type = Column(
        ENUM(*list(file_types.keys()), name="file_type_enum", metadata=Base.metadata),
        nullable=False,
    )

    __table_args__ = {"schema": "ops"}

    @classmethod
    def query_by_file_key(cls, session: Session, file_key):
        return session.query(cls).filter(cls._file_key == file_key).first()

    @classmethod
    def query_by_asset_hash(cls, session: Session, asset_hash):
        return (
            session.query(cls)
            .filter(cls.asset_hash == asset_hash)
            .filter(cls._deleted != True)  # noqa: E712
            .all()
        )

    @classmethod
    def query_by_id(cls, session: Session, metadata_id):
        return session.query(cls).filter(cls._id == metadata_id).first()

    @classmethod
    def select_files_for_asset(
        cls,
        asset: Asset,
        file_type: str,
        page_size: int,
        page: int,
    ):

        session = inspect(asset).session

        query = (
            session.query(cls)
            .where(cls.asset_hash == asset._asset_hash)
            .where(cls._deleted != True)  # noqa: E712
        )

        if file_type:
            query = query.where(and_(FileTransferMetadata.file_type == file_type))

        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)

        results: list[FileTransferMetadata] = query.all()

        return results, total_pages
