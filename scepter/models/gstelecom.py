from __future__ import annotations

from math import ceil
from typing import TYPE_CHECKING, Optional, Union

from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    func,
    inspect,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Session, relationship
from sqlalchemy.schema import UniqueConstraint

if TYPE_CHECKING:
    from models.groundstation import GroundStation

from .base import Base
from .utils import ModelCommonMixIn

ieee_bands = [
    ("HF", 0.03e9),
    ("VHF", 0.3e9),
    ("UHF", 1e9),
    ("L", 2e9),
    ("S", 4e9),
    ("C", 8e9),
    ("X", 12e9),
    ("Ku", 18e9),
    ("K", 27e9),
    ("Ka", 40e9),
    ("V", 75e9),
    ("W", 110e9),
    ("G", 300e9),
]


def find_ieee_radio_band(frequency) -> Union[list[str], None]:

    if len(frequency) == 1:  # centre frequency used
        frequency = [frequency[0], frequency[0]]

    band_limits = [0, 0]
    for i in range(2):
        for j in range(len(ieee_bands) - 1):
            if ieee_bands[j][1] <= frequency[i]:
                band_limits[i] = j + 1
            elif ieee_bands[j][1] > frequency[i]:
                break
    return [
        band[0]
        for band in ieee_bands[band_limits[0] : band_limits[1] + 1]  # noqa: E203
    ]


class GsTelecom(Base, ModelCommonMixIn):
    __tablename__ = "gs_telecom"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    gs_id = Column(Integer, ForeignKey("ops.groundstation._id"), nullable=False)
    frequency_lower = Column(BigInteger, nullable=True)
    frequency_centre = Column(BigInteger, nullable=False)
    frequency_upper = Column(BigInteger, nullable=True)
    telecom_name = Column(String)
    telecom_type = Column(String)
    antenna_gain = Column(Float)
    modulations = Column(JSONB)
    rotatable = Column(Boolean)
    beamwidth = Column(Float)
    noise_factor = Column(Float)
    transmits = Column(Boolean)
    receives = Column(Boolean)
    address = Column(String, nullable=True)
    port = Column(Integer, nullable=True)
    _deleted = Column(Boolean)
    _deleted_at = Column(DateTime)
    _original_name = Column(String)

    groundstation: GroundStation = relationship(
        "GroundStation", lazy=True, viewonly=True
    )

    __table_args__ = (UniqueConstraint("gs_id", "telecom_name"), {"schema": "ops"})

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    @property
    def gs_id_repr(self):
        return self.groundstation._asset_hash

    @classmethod
    def query_by_name_and_gs(
        cls, session: Session, gs_id, telecom_name
    ) -> Union[GsTelecom, None]:
        query = session.query(cls).filter(cls.gs_id == gs_id)
        if not query.all():
            return None
        gs_telecom_list = query.filter(
            func.lower(cls.telecom_name) == func.lower(telecom_name)
        ).all()
        if not gs_telecom_list:
            return None
        return gs_telecom_list[0]

    @classmethod
    def query_by_id(cls, session: Session, telecom_id):
        query = session.query(cls).filter(cls._id == telecom_id)
        return query.first()

    @classmethod
    def get_number_of_gs_telecoms(cls, session: Session, gs_id):
        query = session.query(cls).filter(cls.gs_id == gs_id)
        return query.count()

    def get_info(self, gs_hash):
        telecom_dict = dict(self.__dict__)
        cleaned_dict = {
            key: value for key, value in telecom_dict.items() if key[0] != "_"
        }
        freq_range = []
        for freq_limit in ("frequency_lower", "frequency_upper"):
            if cleaned_dict.get(freq_limit):
                freq_range.append(cleaned_dict[freq_limit])
            else:
                freq_range.append(cleaned_dict["frequency_centre"])
        ieee_band = find_ieee_radio_band(freq_range)
        cleaned_dict["ieee_band"] = ieee_band
        cleaned_dict["gs_id"] = gs_hash
        return cleaned_dict

    @classmethod
    def search_for_telecom(
        cls,
        groundstation: GroundStation,
        telecom_name: Optional[str],
        telecom_type: Optional[str],
        page: Optional[int],
        page_size: Optional[int],
    ):

        session = inspect(groundstation).session

        query = session.query(cls).filter(cls.gs_id == groundstation._id)

        if telecom_name:
            query = query.where(GsTelecom.telecom_name.ilike(f"%{telecom_name}%"))

        if telecom_type:
            query = query.where(GsTelecom.telecom_type.ilike(f"%{telecom_type}%"))

        count = query.count()
        query = query.limit(page_size)

        total_pages = ceil(count / page_size)
        total_pages = 1 if total_pages < 1 else total_pages

        query = query.offset((page - 1) * page_size)

        results: list[GsTelecom] = query.all()
        results = [telecom for telecom in results if telecom._deleted is not True]

        return results, total_pages
