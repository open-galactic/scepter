from __future__ import annotations

from sqlalchemy import BigInteger, Column, DateTime, ForeignKey, String, text
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.schema import Identity

from models.base import Base


class Preset(Base):
    __tablename__ = "preset"
    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    id = Column(UUID(as_uuid=True), nullable=False, unique=True)
    name = Column(String, nullable=False)
    args = Column(JSONB, nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=text("NOW()"))
    command_id = Column(BigInteger, ForeignKey("ops.command._id"), nullable=True)
    script_id = Column(BigInteger, ForeignKey("ops.script._id"), nullable=True)

    __table_args__ = {"schema": "ops"}
