from decimal import Decimal
from enum import Enum
from hashlib import sha1
from math import ceil
from typing import Callable

from sqlalchemy import and_, func, text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.exc import IntegrityError, InvalidRequestError
from sqlalchemy.orm import Session


def get_hash_id(session: Session, check_id_func: Callable, digits: int = 6, prefix: str = "", *args):
    """
    Method for hashing a series of concatenated listed values
    Checks whether the hash exists in a table via a supplied function

    inputs:
     - *args            (str)       the values to be hashed
     - check_id_func    (function)  a method for checking whether the hash
                                    exists in a table
     - digits (op)      (int)       the number of chars to truncate the hash to
     - prefix (op)      (str)       a prefix character to place at the
                                    beggining of the hash

    returns:
     - hash_str         (str)       the sha1 hash of the concatenated string,
                                    truncated to a specified length in base 16
    """
    if digits > 20:
        return False
    str_args = "".join([*args])
    count = 0
    while True:
        hash_str = prefix + sha1((str_args + str(count)).encode("UTF-8")).hexdigest()[:digits]
        if not check_id_func(session, hash_str):
            return hash_str
        else:
            count += 1
            if count > 100:
                break
    return False


class ModelCommonMixIn(object):
    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    def read_input_dict(self, **kwargs):
        for prop, value in kwargs.items():
            setattr(self, prop, value)

    @classmethod
    def batch_insert(cls, session: Session, objects):
        session.bulk_insert_mappings(cls, objects)
        return objects

    @classmethod
    def search(cls, query, search_params, order_by=None):
        query = cls._search(query, search_params)
        if order_by:
            query = query.order_by(order_by)
        count = query.count()
        query = query.limit(search_params["page_size"])
        query = query.offset((search_params["page"] - 1) * search_params["page_size"]).all()
        total_pages = ceil(count / search_params["page_size"])
        total_pages = 1 if total_pages < 1 else total_pages
        return query, total_pages

    def update(self, session: Session, **kwargs):
        try:
            self.read_input_dict(**kwargs)
            session.flush()
            return True
        except (IntegrityError, InvalidRequestError):
            session.rollback()
            return False

    @classmethod
    def _update_entry(cls, session: Session, id: int, **data):
        try:
            query = cls.query.filter(cls._id == id)
            if not query.all():
                return False
            query.update(data, synchronize_session=False)
            session.flush()
            return True

        except (IntegrityError, InvalidRequestError):
            session.rollback()
            return False

    def get_info(self):
        """
        pub/priv methods here are required as in inheriting classes we
        overwrite the get_info method in order to enrich it with additional
        data
        """
        return self._get_info()

    def _get_info(self):
        entry_dict = dict(self.__dict__)
        clean_dict = {key: value for key, value in entry_dict.items() if key[0] != "_"}
        return clean_dict

    @classmethod
    def _search(cls, query, search_params):
        filters = []
        params = {}
        for key, value in search_params["data"].items():
            if key and value is not None:
                if (column := getattr(cls, key)) is None:
                    raise AttributeError(f"No column found with name {key}")

                # need to catch UUIDs, which here in python are provided as strings,
                # but in the db don't have the lower(method)
                if isinstance(column.type, UUID):
                    filters.append(column == value)
                elif isinstance(value, str):
                    filters.append(func.lower(column).like("%" + func.lower(value) + "%"))
                elif isinstance(value, bool):
                    filters.append(column == value)
                elif isinstance(value, (int, float, Decimal)):
                    tol = len(str(abs(value % 1))) - 2
                    if tol < 1:
                        tol = 0
                    filters.append(text((f"TRUNC({key}::numeric, :{key}_tol) = :{key}_value")))
                    params.update({f"{key}_value": value, f"{key}_tol": tol})
                elif isinstance(value, tuple):
                    filters.append(and_(column >= value[0], column <= value[1]))
                elif isinstance(value, Enum):
                    filters.append(column == value.name)
        if filters:
            query = query.filter(and_(*filters))
            if params:
                query = query.params(**params)
        return query

    def clean_update(self, session: Session, **data):
        """
        pub/priv methods here are required as in inheriting classes we
        overwrite the clean_update method in order to remove assotiated objects
        """
        return self._clean_update(session, **data)

    def _clean_update(self, session: Session, **data):
        try:
            clean_dict = {k: None for k, v in self.__dict__.items() if k[0] != "_"}
            clean_dict.update(data)
            result = self.update(session, **clean_dict)
            if not result:
                return False
            return True
        except (IntegrityError, InvalidRequestError):
            return False
