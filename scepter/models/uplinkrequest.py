from __future__ import annotations

import logging
import uuid
from datetime import datetime
from typing import TYPE_CHECKING, List, Optional

from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Index,
    String,
    inspect,
    select,
    text,
)
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import Session, relationship

from models.base import Base
from models.contact import Contact
from models.instruction import Instruction

if TYPE_CHECKING:
    from models.satellite import Satellite

logger = logging.getLogger(__name__)


class UplinkRequest(Base):
    __tablename__ = "uplink_request"
    _id = Column(BigInteger, Identity(always=True), primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=text("NOW()"))
    id = Column(UUID(as_uuid=True), nullable=False, index=True, unique=True)
    instruction_id = Column(BigInteger, ForeignKey("ops.instruction._id"), nullable=False)
    approved = Column(Boolean, nullable=True)
    reviewed_at = Column(DateTime, nullable=True)
    reviewed_by = Column(String, nullable=True)
    context = Column(JSONB, nullable=False)

    instruction = relationship("Instruction", back_populates="uplink_requests")
    contact = association_proxy("instruction", "contact")

    Index("ix_uplink_requests_instruction_id_created_at", instruction_id, created_at)

    __table_args__ = {"schema": "ops"}

    @property
    def sat_pkey(self):
        return self.contact.sat_id

    @property
    def sat_id(self):
        return self.contact.satellite._asset_hash

    @property
    def contact_id(self):
        return self.contact.id

    @property
    def instruction_id_repr(self):
        # need a weird name here, as instruction_id is used by the fkey column
        return self.instruction.id

    def __init__(self, instruction: Instruction, context: dict) -> None:
        self.instruction_id = instruction._id
        self.context = context
        self.id = uuid.uuid4()

    def approve(self, user: str) -> None:
        self.approved = True
        self.reviewed_by = user
        self.reviewed_at = datetime.utcnow()

    def deny(self, user: str) -> None:
        self.approved = False
        self.reviewed_by = user
        self.reviewed_at = datetime.utcnow()

    @classmethod
    def get_by_id(cls, session: Session, id: uuid.UUID) -> Optional[UplinkRequest]:
        query = select(cls).where(cls.id == id)
        result = session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    def list_for_sats(
        cls,
        session: Session,
        satellites: List[Satellite],
        approved: Optional[bool],
        reviewed: Optional[bool],
    ) -> List[UplinkRequest]:
        """
        returns a list of uplink requests for the provided filter parameters
        """

        # check the session of each satellite instance, and the session var itself
        # are all the same, to avoid mixing together multiple sessions
        if satellites and len({inspect(x).session for x in satellites} | {session()}) > 1:
            logger.error("satellite objects do not share a common session")
            raise Exception("satellite objects do not share a common session")

        query = select(cls).join(Instruction).join(Contact)
        query = query.where(Contact.sat_id.in_([x._id for x in satellites]))
        if approved is not None:
            query = query.where(cls.approved == approved)
        if reviewed is not None:
            if reviewed is True:
                query = query.where(cls.reviewed_at.is_not(None))
            else:
                query = query.where(cls.reviewed_at.is_(None))
        query = query.order_by(cls.created_at.desc())
        result = session.execute(query)
        uplink_requests = result.scalars().all()
        return uplink_requests
