from __future__ import annotations

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import Session

from .base import Base
from .utils import ModelCommonMixIn


class SatStatus(Base, ModelCommonMixIn):
    __tablename__ = "sat_status"
    _id = Column(Integer, primary_key=True, autoincrement=True)
    _timestamp = Column(DateTime)
    sat_id = Column(Integer, ForeignKey("ops.satellite._id"), nullable=False)
    update_time = Column(DateTime)
    status = Column(String)

    __table_args__ = {"schema": "ops"}

    def __init__(self, **kwargs):
        self.read_input_dict(**kwargs)

    @classmethod
    def query_last_sat_status(cls, session: Session, sat):
        sat_last_status = (
            session.query(cls)
            .filter(cls.sat_id == sat._id)
            .order_by(cls.update_time.desc())
            .first()
        )
        if not sat_last_status:
            return None
        sat_last_status_dict = dict(sat_last_status.__dict__)
        cleaned_dict = {
            key: value for key, value in sat_last_status_dict.items() if key[0] != "_"
        }
        for datetime_key in ["update_time"]:
            if cleaned_dict.get(datetime_key):
                cleaned_dict[datetime_key] = cleaned_dict[datetime_key].isoformat()
        cleaned_dict["sat_id"] = sat._asset_hash
        return cleaned_dict
