import typing
from logging.config import fileConfig
from os import fsdecode

from alembic import context
from alembic.runtime.environment import EnvironmentContext
from sqlalchemy import create_engine, pool

from migrations.config import db
from models import Base

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = typing.cast(EnvironmentContext, context).config

# Setup Python logging, but don't disable existing loggers
# May be running under pytest which relies on them
if config.config_file_name:
    fileConfig(fsdecode(config.config_file_name), disable_existing_loggers=False)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = config.attributes.get("connection", None)
    if connectable is None:
        url = config.get_main_option("sqlalchemy.url") or db.sqlalchemy_url
        connectable = create_engine(url=url, poolclass=pool.NullPool)

    db_name = config.attributes.get("db_name", db.name)

    apiuser = config.attributes.get("apiuser", db.apiuser)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            version_table_schema="ops",
            include_schemas=True,
        )

        connection.execute("CREATE SCHEMA IF NOT EXISTS ops")

        with context.begin_transaction():
            context.run_migrations(
                apiuser=apiuser,
                db_name=db_name,
            )


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
