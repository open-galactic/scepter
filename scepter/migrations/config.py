import logging
import os
from pathlib import Path

import dotenv

dotenv.load_dotenv(Path(__file__).parent.parent.parent / ".env")

logger = logging.getLogger(__name__)


class DatabaseSettings:
    @property
    def host(self) -> str:
        return os.getenv("DB_HOST") or "localhost"

    @property
    def port(self):
        return os.getenv("DB_PORT") or 5432

    @property
    def name(self) -> str:
        return os.getenv("DB_NAME") or "scepter_db"

    @property
    def superuser(self) -> str:
        return os.getenv("DB_SUPERUSER") or "postgres"

    @property
    def superuser_password(self) -> str:
        return os.getenv("DB_SUPERUSER_PASSWORD") or "postgres"

    @property
    def apiuser(self) -> str:
        return os.getenv("DB_APIUSER") or "scepter_apiuser"

    @property
    def apiuser_password(self) -> str:
        return os.getenv("DB_APIUSER_PASSWORD") or "password"

    @property
    def sqlalchemy_url(self) -> str:
        return (
            f"postgresql+psycopg2://{self.superuser}:{self.superuser_password}@{self.host}:{str(self.port)}/{self.name}"
        )


db = DatabaseSettings()
