import json
import unittest
from datetime import datetime, timedelta
from uuid import UUID, uuid4

from flask import g

from api import app
from api.database import session
from models import (
    Asset,
    GroundStation,
    GsTelecom,
    Overpass,
    Permission,
    Satellite,
    SatTelecom,
    Subsystem,
    Telemetry,
    TelemetryMetadata,
)
from models.utils import get_hash_id


class TestTelemPermissions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = uuid4()
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = uuid4()
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        gs_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.gs_id = json.loads(gs_response.data)["gs_id"]
        self.gs_numeric_id = GroundStation.query_by_hash(session, self.gs_id)._id

        self.gs_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 2e9,
            "transmits": True,
            "receives": True,
            "modulations": {"FM": ["100"]},
            "rotatable": False,
        }
        gs_telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )

        assert gs_telecom_response.status_code == 201

        self.gs_telecom_name = json.loads(gs_telecom_response.data)["telecom_name"]

        self.sat_id = "s12347"
        sat = Satellite(
            asset_hash=self.sat_id,
            sat_name="test_sat_name",
            description="test_description",
            norad_id=25544,
        )
        session.add(sat)
        session.flush()
        permission = Permission(
            asset_id=sat._id,
            user_uuid=self.user1_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
        session.add(permission)
        self.asset_id = sat._id

        # Add systems and components to satellite
        self.system_name = "power"
        self.component_name = "APC_YP"
        self.system = Subsystem(sat, self.system_name, "system", "1", parent=None)
        session.add(self.system)
        session.flush()
        self.component = Subsystem(sat, self.component_name, "component", "1", parent=self.system)
        session.add(self.component)

        session.flush()
        self.component_id = self.component.id
        session.commit()

        self.sat_telecom_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "tese_type",
            "frequency": 480000000,
            "modulations": {"FM": [9600]},
            "transmits": True,
            "receives": True,
        }
        sat_response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.sat_telecom_name = json.loads(sat_response.data)["telecom_name"]

        self.telem_name = "VBatt"
        self.telem_meta_data = {
            "system": "power",
            "units": "volts",
            "uncertainty": 0.1,
            "nominal_min": 1.0,
            "nominal_max": 3.0,
            "alert_min": 0.0,
            "alert_max": 4.0,
            "description": "test desc",
            "tlm_name": self.telem_name,
        }

        self.telem_data = {
            "observed_at": datetime.utcnow().isoformat(),
            "value": 5.0,
        }

        self.add_telemetry("VBatt2")
        session.commit()

    def add_overpass(self, timestamp, sat_numeric_id, gs_numeric_id):
        overpass_data = {
            "_timestamp": datetime.utcnow(),
            "_gs_id": gs_numeric_id,
            "_sat_id": sat_numeric_id,
            "rise_time": timestamp - timedelta(minutes=10),
            "maxima_time": timestamp,
            "set_time": timestamp + timedelta(minutes=10),
            "leapfrog_path": '{"key": "value"}',
            "tle_update_time": datetime.utcnow(),
            "maxima_el": 50,
            "overpass_id": get_hash_id(session, Overpass.query_by_hash, 12, "o", timestamp.isoformat()),
        }
        with app.test_request_context():
            g.main_db = session
            new_pass = Overpass(**overpass_data)
            session.add(new_pass)
            session.commit()

    def add_telemetry(self, tlm_name):
        telem = TelemetryMetadata(
            _sat_id=self.asset_id,
            tlm_name=tlm_name,
            description="temp description",
            _subsystem_id=self.component_id,
            units="temporary",
            uuid=str(uuid4()),
        )
        session.add(telem)
        session.flush()
        self.telem_data["telemetry_metadata"] = telem
        telem = Telemetry(**self.telem_data)
        session.add(telem)
        session.commit()

    def tearDown(self):
        session.query(Telemetry).delete()
        session.query(TelemetryMetadata).delete()
        session.query(Permission).delete()
        session.query(Overpass).delete()
        session.query(SatTelecom).delete()
        session.query(Subsystem).delete()
        session.query(Satellite).delete()
        session.query(GsTelecom).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_telem_meta_read_authorized(self):
        self.add_telemetry("VBatt")

        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )

        self.assertEqual(response.status_code, 200)

    def test_telem_meta_read_no_per(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_read_only_read_asset_auth(self):
        self.add_telemetry("VBatt")

        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )

        self.assertEqual(response.status_code, 200)

    def test_telem_meta_read_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format("not_sat", self.telem_name),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_telem_meta_read_no_read_no_auth(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, "not_telem_name"),
        )
        self.assertEqual(response.status_code, 401)

    def test_telem_meta_search_authorized(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata".format(self.sat_id),
            headers=self.headers1,
        )
        sat_list = json.loads(response.data)["telem_results"]
        self.assertEqual(response.status_code, 200)
        self.assertTrue(isinstance(sat_list, list))
        self.assertEqual(len(sat_list), 1)

    def test_telem_meta_search_not_authed(self):
        self.add_telemetry("testing telem")

        response = self.app.get(
            "/satellite/{}/telemetry_metadata".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_search_no_per(self):
        response = self.app.get("/satellite/{}/telemetry_metadata".format(self.sat_id))
        self.assertEqual(response.status_code, 401)

    def test_telem_search_authorized(self):
        self.add_telemetry("VBatt")

        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        sat_dict = json.loads(response.data)
        self.assertTrue(isinstance(sat_dict, dict))
        self.assertTrue(len(sat_dict["tlm_fields"].keys()) == 2)

    def test_telem_search_not_authed(self):
        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id), headers=self.headers2)
        self.assertEqual(response.status_code, 403)

    def test_telem_search_no_per(self):
        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id))
        self.assertEqual(response.status_code, 401)

    def test_telem_search_header_auth(self):
        self.add_telemetry("VBatt")

        response = self.app.get(
            "/satellite/{}/legacytelemetry".format(self.sat_id),
            headers={"x-osso-user": self.user1_id, "x-osso-token": "dev"},
        )
        self.assertEqual(response.status_code, 200)
        sat_dict = json.loads(response.data)
        self.assertTrue(isinstance(sat_dict, dict))
        self.assertTrue(len(sat_dict["tlm_fields"].keys()) == 2)

    def test_telem_search_header_auth_missing_subs(self):
        response = self.app.get(
            "/satellite/{}/legacytelemetry".format(self.sat_id),
            headers={
                "x-osso-user": UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1"),
                "x-osso-token": "",
            },
        )
        self.assertEqual(response.status_code, 401)


class TestTelemPermissionsIntUserId(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = 1111
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = 2222
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        gs_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.gs_id = json.loads(gs_response.data)["gs_id"]
        self.gs_numeric_id = GroundStation.query_by_hash(session, self.gs_id)._id

        self.gs_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 2e9,
            "transmits": True,
            "receives": True,
            "modulations": {"FM": ["100"]},
            "rotatable": False,
        }
        gs_telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )

        assert gs_telecom_response.status_code == 201

        self.gs_telecom_name = json.loads(gs_telecom_response.data)["telecom_name"]

        self.sat_id = "s12347"
        sat = Satellite(
            asset_hash=self.sat_id,
            sat_name="test_sat_name",
            description="test_description",
            norad_id=25544,
        )
        session.add(sat)
        session.flush()
        permission = Permission(
            asset_id=sat._id,
            user_id=self.user1_id,
            read_access=True,
            command_access=True,
            manage_asset=True,
            manage_users=True,
            owner=True,
        )
        session.add(permission)
        self.asset_id = sat._id

        # Add systems and components to satellite
        self.system_name = "power"
        self.component_name = "APC_YP"
        self.system = Subsystem(sat, self.system_name, "system", "1", parent=None)
        session.add(self.system)
        session.flush()
        self.component = Subsystem(sat, self.component_name, "component", "1", parent=self.system)
        session.add(self.component)

        session.flush()
        self.component_id = self.component.id
        session.commit()

        self.sat_telecom_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "tese_type",
            "frequency": 480000000,
            "modulations": {"FM": [9600]},
            "transmits": True,
            "receives": True,
        }
        sat_response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.sat_telecom_name = json.loads(sat_response.data)["telecom_name"]

        self.telem_name = "VBatt"
        self.telem_meta_data = {
            "system": "power",
            "units": "volts",
            "uncertainty": 0.1,
            "nominal_min": 1.0,
            "nominal_max": 3.0,
            "alert_min": 0.0,
            "alert_max": 4.0,
            "description": "test desc",
            "tlm_name": self.telem_name,
        }

        self.telem_data = {
            "observed_at": datetime.utcnow().isoformat(),
            "value": 5.0,
        }

        self.add_telemetry("VBatt2")
        session.commit()

    def add_overpass(self, timestamp, sat_numeric_id, gs_numeric_id):
        overpass_data = {
            "_timestamp": datetime.utcnow(),
            "_gs_id": gs_numeric_id,
            "_sat_id": sat_numeric_id,
            "rise_time": timestamp - timedelta(minutes=10),
            "maxima_time": timestamp,
            "set_time": timestamp + timedelta(minutes=10),
            "leapfrog_path": '{"key": "value"}',
            "tle_update_time": datetime.utcnow(),
            "maxima_el": 50,
            "overpass_id": get_hash_id(session, Overpass.query_by_hash, 12, "o", timestamp.isoformat()),
        }
        with app.test_request_context():
            g.main_db = session
            new_pass = Overpass(**overpass_data)
            session.add(new_pass)
            session.commit()

    def add_telemetry(self, tlm_name):
        telem = TelemetryMetadata(
            _sat_id=self.asset_id,
            tlm_name=tlm_name,
            description="temp description",
            _subsystem_id=self.component_id,
            units="temporary",
            uuid=str(uuid4()),
        )
        session.add(telem)
        session.flush()
        self.telem_data["telemetry_metadata"] = telem
        telem = Telemetry(**self.telem_data)
        session.add(telem)
        session.commit()

    def tearDown(self):
        session.query(Telemetry).delete()
        session.query(TelemetryMetadata).delete()
        session.query(Permission).delete()
        session.query(Overpass).delete()
        session.query(SatTelecom).delete()
        session.query(Subsystem).delete()
        session.query(Satellite).delete()
        session.query(GsTelecom).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_telem_meta_read_authorized(self):
        self.add_telemetry("VBatt")

        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )

        self.assertEqual(response.status_code, 200)

    def test_telem_meta_read_no_per(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_read_only_read_asset_auth(self):
        self.add_telemetry("VBatt")

        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, self.telem_name),
            headers=self.headers1,
        )

        self.assertEqual(response.status_code, 200)

    def test_telem_meta_read_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format("not_sat", self.telem_name),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_telem_meta_read_no_read_no_auth(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata?tlm_name={}".format(self.sat_id, "not_telem_name"),
        )
        self.assertEqual(response.status_code, 401)

    def test_telem_meta_search_authorized(self):
        response = self.app.get(
            "/satellite/{}/telemetry_metadata".format(self.sat_id),
            headers=self.headers1,
        )
        sat_list = json.loads(response.data)["telem_results"]
        self.assertEqual(response.status_code, 200)
        self.assertTrue(isinstance(sat_list, list))
        self.assertEqual(len(sat_list), 1)

    def test_telem_meta_search_not_authed(self):
        telem = Telemetry(**self.telem_data)
        session.add(telem)
        session.commit()

        response = self.app.get(
            "/satellite/{}/telemetry_metadata".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_telem_meta_search_no_per(self):
        response = self.app.get("/satellite/{}/telemetry_metadata".format(self.sat_id))
        self.assertEqual(response.status_code, 401)

    def test_telem_search_authorized(self):
        self.add_telemetry("VBatt")

        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        sat_dict = json.loads(response.data)
        self.assertTrue(isinstance(sat_dict, dict))
        self.assertTrue(len(sat_dict["tlm_fields"].keys()) == 2)

    def test_telem_search_not_authed(self):
        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id), headers=self.headers2)
        self.assertEqual(response.status_code, 403)

    def test_telem_search_no_per(self):
        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id))
        self.assertEqual(response.status_code, 401)

    def test_telem_search_header_auth(self):
        self.add_telemetry("VBatt")

        response = self.app.get("/satellite/{}/legacytelemetry".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        sat_dict = json.loads(response.data)
        self.assertTrue(isinstance(sat_dict, dict))
        self.assertTrue(len(sat_dict["tlm_fields"].keys()) == 2)

    def test_telem_search_header_auth_missing_subs(self):
        response = self.app.get(
            "/satellite/{}/legacytelemetry".format(self.sat_id),
            headers={
                "x-osso-user": UUID("ea6fd5e1-9b78-42b8-acb9-2a054577c7a1"),
                "x-osso-token": "",
            },
        )
        self.assertEqual(response.status_code, 401)
