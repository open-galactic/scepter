import json
import unittest
from datetime import datetime
from uuid import uuid4

from api import app
from api.database import session
from models import Asset, GroundStation, GsStatus, GsTelecom, Permission


class TestGsPermissions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = uuid4()
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = uuid4()
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.gs_id = json.loads(response.data)["gs_id"]
        self.asset_id = Asset.query_by_hash(session, self.gs_id)._id

        self.gs_status_test_data = {
            "status": "OK",
            "update_time": datetime.utcnow().isoformat(),
        }
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        self.gs_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }
        gs_telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.telecom_name = json.loads(gs_telecom_response.data)["telecom_name"]

    def tearDown(self):
        session.query(Permission).delete()
        session.query(GsTelecom).delete()
        session.query(GsStatus).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_gs_create_valid(self):
        """
        tests the valid create authorization method
        """
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_gs_doesnt_exist(self):
        """
        tests the write authorization method
        """
        defined_gs_id = "g111111"
        response = self.app.put(
            "/groundstation/{}".format(defined_gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_no_per(self):
        """
        tests the unauthorized put attempt by a different user
        """
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_write_authorized(self):
        """
        tests the authorized put attempt by the owner
        """
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(self.gs_id, gs_id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_manage_asset_auth_only(self):
        """
        tests the authorized put attempt by a user with only manage asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.command_access = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

    def test_gs_write_no_manage_asset_auth(self):
        """
        tests the authorized put attempt by a user with everything but manage_asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.manage_asset = False
        session.commit()

        # should check "write" permission is on internal func
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_write_public_asset(self):
        """
        tests the unauthorized put attempt by a user on a public asset with no permission
        """
        self.gs_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        gs_id = json.loads(create_response.data)["gs_id"]
        response = self.app.put(
            "/groundstation/{}".format(gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_authorized(self):
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_read_no_per(self):
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_read_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_search_authorized(self):
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)

    def test_gs_search_public_asset(self):
        self.gs_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        response = self.app.get("/groundstation", headers=self.headers2)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)

    def test_gs_search_no_per(self):
        response = self.app.get(
            "/groundstation",
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 0)

    def test_gs_search_only_auth_gs_returned(self):
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)
        self.assertTrue(gs_list[0]["gs_id"] == self.gs_id)

    def test_gs_search_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 0)

    def test_gs_search_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)
        self.assertTrue(gs_list[0]["gs_id"] == self.gs_id)

    def test_gs_status_execute_authorized(self):
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_status_execute_no_per(self):
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_no_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_only_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_status_execute_no_gs(self):
        response = self.app.post(
            "/groundstation/not_a_gs_id/status",
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_status_read_authorized(self):
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)
        self.assertTrue(gs_data["status"] == self.gs_status_test_data["status"])

    def test_gs_status_read_no_per(self):
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_public_asset(self):
        gs = GroundStation.query_by_hash(session, self.gs_id)
        gs.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_status_read_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_execute_authorized(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_execute_no_per(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_execute_no_telecom_asset_auth(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_execute_only_telecom_asset_auth(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_execute_no_gs(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/not_a_gs_id/telecom",
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_single_authorized(self):
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)
        self.assertEqual(gs_data["telecom_name"], self.gs_telecom_test_data["telecom_name"])

    def test_gs_telecom_read_single_no_per(self):
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_single_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_single_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_telecom_read_single_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id/telecom/not_a_telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_single_no_read_no_gs_cmd(self):
        response = self.app.get(
            "/groundstation/{}/telecom/not_a_cmd".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_many_authorized(self):
        response = self.app.get("/groundstation/{}/telecom".format(self.gs_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)["telecom_results"][0]
        self.assertEqual(
            gs_data["telecom_name"],
            self.gs_telecom_test_data["telecom_name"],
        )

    def test_gs_telecom_read_many_no_per(self):
        response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_many_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_many_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.telecom_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/groundstation/{}/telecom".format(self.gs_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)

    def test_gs_telecom_read_many_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id/telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_put_new(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/new_cmd".format(self.gs_id),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_authorized(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_execute_no_per(self):
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_put_execute_no_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_put_execute_only_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_execute_no_gs(self):
        response = self.app.put(
            "/groundstation/not_a_gs_id/telecom/{}",
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_execute_public_asset(self):
        gs = GroundStation.query_by_hash(session, self.gs_id)
        gs.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)


class TestGsPermissionsIntUserId(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = 1111
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = 2222
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.gs_test_data = {
            "gs_name": "test_gs_name",
            "commission_date": datetime.utcnow().isoformat(),
            "latitude": 10,
            "longitude": 20,
            "altitude": 30,
        }
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.gs_id = json.loads(response.data)["gs_id"]
        self.asset_id = Asset.query_by_hash(session, self.gs_id)._id

        self.gs_status_test_data = {
            "status": "OK",
            "update_time": datetime.utcnow().isoformat(),
        }
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        self.gs_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }
        gs_telecom_response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.telecom_name = json.loads(gs_telecom_response.data)["telecom_name"]

    def tearDown(self):
        session.query(Permission).delete()
        session.query(GsTelecom).delete()
        session.query(GsStatus).delete()
        session.query(GroundStation).delete()
        session.query(Asset).delete()
        session.commit()

    def test_gs_create_valid(self):
        """
        tests the valid create authorization method
        """
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_gs_doesnt_exist(self):
        """
        tests the write authorization method
        """
        defined_gs_id = "g111111"
        response = self.app.put(
            "/groundstation/{}".format(defined_gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_no_per(self):
        """
        tests the unauthorized put attempt by a different user
        """
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_write_authorized(self):
        """
        tests the authorized put attempt by the owner
        """
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )

        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

        gs_id = json.loads(response.data)["gs_id"]
        asset = Asset.query_by_hash(session, gs_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(self.gs_id, gs_id)
        self.assertEqual(asset._asset_hash, gs_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_gs_write_manage_asset_auth_only(self):
        """
        tests the authorized put attempt by a user with only manage asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.command_access = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        assert response.status_code == 200
        data: dict = response.json
        assert type(data) == dict
        assert data == {
            "gs_id": f"{self.gs_id}",
            "msg": f"groundstation {self.gs_id} updated",
        }

    def test_gs_write_no_manage_asset_auth(self):
        """
        tests the authorized put attempt by a user with everything but manage_asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.manage_asset = False
        session.commit()

        # should check "write" permission is on internal func
        response = self.app.put(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_write_public_asset(self):
        """
        tests the unauthorized put attempt by a user on a public asset with no permission
        """
        self.gs_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        gs_id = json.loads(create_response.data)["gs_id"]
        response = self.app.put(
            "/groundstation/{}".format(gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_authorized(self):
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_read_no_per(self):
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}".format(self.gs_id),
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_read_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_search_authorized(self):
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)

    def test_gs_search_public_asset(self):
        self.gs_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        response = self.app.get("/groundstation", headers=self.headers2)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)

    def test_gs_search_no_per(self):
        response = self.app.get(
            "/groundstation",
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 0)

    def test_gs_search_only_auth_gs_returned(self):
        response = self.app.post(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)
        self.assertTrue(gs_list[0]["gs_id"] == self.gs_id)

    def test_gs_search_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation",
            data=json.dumps(self.gs_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 0)

    def test_gs_search_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/groundstation", headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_list = json.loads(response.data)["gs_results"]
        self.assertTrue(isinstance(gs_list, list))
        self.assertTrue(len(gs_list) == 1)
        self.assertTrue(gs_list[0]["gs_id"] == self.gs_id)

    def test_gs_status_execute_authorized(self):
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_status_execute_no_per(self):
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_no_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_only_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_status_execute_no_gs(self):
        response = self.app.post(
            "/groundstation/not_a_gs_id/status",
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_status_read_authorized(self):
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)
        self.assertTrue(gs_data["status"] == self.gs_status_test_data["status"])

    def test_gs_status_read_no_per(self):
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_execute_public_asset(self):
        gs = GroundStation.query_by_hash(session, self.gs_id)
        gs.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/groundstation/{}/status".format(self.gs_id),
            data=json.dumps(self.gs_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_status_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/status".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_status_read_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_execute_authorized(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_execute_no_per(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_execute_no_telecom_asset_auth(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_execute_only_telecom_asset_auth(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_execute_no_gs(self):
        self.gs_telecom_test_data["telecom_name"] = self.gs_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/groundstation/not_a_gs_id/telecom",
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_single_authorized(self):
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)
        self.assertEqual(gs_data["telecom_name"], self.gs_telecom_test_data["telecom_name"])

    def test_gs_telecom_read_single_no_per(self):
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_single_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_single_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_gs_telecom_read_single_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id/telecom/not_a_telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_single_no_read_no_gs_cmd(self):
        response = self.app.get(
            "/groundstation/{}/telecom/not_a_cmd".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_read_many_authorized(self):
        response = self.app.get("/groundstation/{}/telecom".format(self.gs_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        gs_data = json.loads(response.data)["telecom_results"][0]
        self.assertEqual(
            gs_data["telecom_name"],
            self.gs_telecom_test_data["telecom_name"],
        )

    def test_gs_telecom_read_many_no_per(self):
        response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_many_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_read_many_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.telecom_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/groundstation/{}/telecom".format(self.gs_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)

    def test_gs_telecom_read_many_no_read_no_gs(self):
        response = self.app.get(
            "/groundstation/not_a_gs_id/telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_put_new(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/new_cmd".format(self.gs_id),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_authorized(self):

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_execute_no_per(self):
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_put_execute_no_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_gs_telecom_put_execute_only_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()

        gs_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency_centre": 20000000,
            "transmits": True,
            "receives": True,
        }

        response = self.app.put(
            "/groundstation/{}/telecom/{}".format(self.gs_id, self.telecom_name),
            data=json.dumps(gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_gs_telecom_put_execute_no_gs(self):
        response = self.app.put(
            "/groundstation/not_a_gs_id/telecom/{}",
            data=json.dumps(self.gs_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_gs_telecom_execute_public_asset(self):
        gs = GroundStation.query_by_hash(session, self.gs_id)
        gs.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/groundstation/{}/telecom".format(self.gs_id),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)
