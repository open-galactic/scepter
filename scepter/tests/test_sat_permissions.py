import json
import unittest
from datetime import datetime
from uuid import uuid4

import pytest

from api import app
from api.database import session
from models import (
    Asset,
    GroundStation,
    GsTelecom,
    Overpass,
    Permission,
    Satellite,
    SatStatus,
    SatTelecom,
    Subsystem,
)


class TestSatPermissions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = uuid4()
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = uuid4()
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.sat_test_data = {"sat_name": "test_sat", "description": "test_description"}
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.sat_id = json.loads(response.data)["sat_id"]
        self.asset_id = Asset.query_by_hash(session, self.sat_id)._id

        self.sat_status_test_data = {
            "status": "OK",
            "update_time": datetime.utcnow().isoformat(),
        }
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        self.sat_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.telecom_name = json.loads(response.data)["telecom_name"]

    @classmethod
    def tearDown(cls):
        session.query(Permission).delete()
        session.query(Overpass).delete()
        session.query(SatTelecom).delete()
        session.query(SatStatus).delete()
        session.query(Subsystem).delete()
        session.query(Satellite).delete()
        session.query(GsTelecom).delete()
        session.query(GroundStation).delete()
        session.commit()

    def test_sat_create_valid(self):
        """
        tests the valid create authorization method
        """
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_gs_doesnt_exist(self):
        """
        tests the write authorization method
        """
        defined_sat_id = "s111111"
        response = self.app.put(
            "/satellite/{}".format(defined_sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_no_per(self):
        """
        tests the unauthorized put attempt by a different user
        """
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_write_authorized(self):
        """
        tests the authorized put attempt by the owner
        """
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        self.assertEqual(self.sat_id, sat_id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_uuid, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_manage_asset_auth_only(self):
        """
        tests the authorized put attempt by a user with only manage asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.command_access = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_write_no_manage_asset_auth(self):
        """
        tests the authorized put attempt by a user with everything but manage_asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.manage_asset = False
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_authorized(self):
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_search_public_asset(self):
        self.sat_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        response = self.app.get("/satellite", headers=self.headers2)
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)

    def test_sat_read_no_per(self):
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_read_no_read_no_gs(self):
        response = self.app.get(
            "/satellite/not_a_sat_id",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_write_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_search_authorized(self):
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)

    def test_sat_search_no_per(self):
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 0)

    def test_sat_search_only_auth_sat_returned(self):
        response = self.app.post(
            "/satellite",
            data=json.dumps({"sat_name": "test_sat_name2", "description": "test_description"}),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)
        self.assertTrue(sat_list[0]["sat_id"] == self.sat_id)

    def test_sat_search_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 0)

    def test_sat_search_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)
        self.assertTrue(sat_list[0]["sat_id"] == self.sat_id)

    def test_sat_status_execute_authorized(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_status_execute_no_per(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_execute_no_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_execute_only_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_status_execute_no_sat(self):
        response = self.app.post(
            "/satellite/not_a_sat_id/status",
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_status_execute_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_authorized(self):
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)
        self.assertTrue(sat_data["status"] == self.sat_status_test_data["status"])

    def test_sat_status_read_no_per(self):
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_status_read_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_execute_authorized(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_execute_no_per(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_no_telecom_asset_auth(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_only_telecom_asset_auth(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_execute_no_sat(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/not_a_sat_id/telecom",
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_single_authorized(self):
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)
        self.assertEqual(sat_data["telecom_name"], self.sat_telecom_test_data["telecom_name"])

    def test_sat_telecom_read_single_no_per(self):
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_single_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_single_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_telecom_read_single_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id/telecom/{}".format(self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_single_no_read_no_sat_cmd(self):
        response = self.app.get(
            "/satellite/{}/telecom/not_a_cmd".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_many_authorized(self):
        response = self.app.get("/satellite/{}/telecom".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)["telecom_results"][0]
        self.assertEqual(
            sat_data["telecom_name"],
            self.sat_telecom_test_data["telecom_name"],
        )

    def test_sat_telecom_read_many_no_per(self):
        response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_many_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_many_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.telecom_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/satellite/{}/telecom".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)

    def test_sat_telecom_read_many_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id/telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_put_new(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        response = self.app.put(
            "/satellite/{}/telecom/new_cmd".format(self.sat_id),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_authorized(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_execute_no_per(self):
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_put_execute_no_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_put_execute_only_telecom_asset_auth(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_execute_no_sat(self):
        response = self.app.put(
            "/satellite/not_a_sat_id/telecom/{}",
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)


class TestSatPermissionsIntUserId(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.user1_id = 1111
        cls.headers1 = {"x-osso-user": cls.user1_id, "x-osso-token": "dev"}
        cls.user2_id = 2222
        cls.headers2 = {"x-osso-user": cls.user2_id, "x-osso-token": "dev"}

    def setUp(self):
        self.sat_test_data = {"sat_name": "test_sat", "description": "test_description"}
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.sat_id = json.loads(response.data)["sat_id"]
        self.asset_id = Asset.query_by_hash(session, self.sat_id)._id

        self.sat_status_test_data = {
            "status": "OK",
            "update_time": datetime.utcnow().isoformat(),
        }
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        self.sat_telecom_test_data = {
            "telecom_name": "test_telecom_name",
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.telecom_name = json.loads(response.data)["telecom_name"]

    @classmethod
    def tearDown(cls):
        session.query(Permission).delete()
        session.query(Overpass).delete()
        session.query(SatTelecom).delete()
        session.query(SatStatus).delete()
        session.query(Subsystem).delete()
        session.query(Satellite).delete()
        session.query(GsTelecom).delete()
        session.query(GroundStation).delete()
        session.commit()

    def test_sat_create_valid(self):
        """
        tests the valid create authorization method
        """
        response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_gs_doesnt_exist(self):
        """
        tests the write authorization method
        """
        defined_sat_id = "s22222"
        response = self.app.put(
            "/satellite/{}".format(defined_sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, asset._id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_no_per(self):
        """
        tests the unauthorized put attempt by a different user
        """
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_write_authorized(self):
        """
        tests the authorized put attempt by the owner
        """
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_id = json.loads(response.data)["sat_id"]
        asset = Asset.query_by_hash(session, sat_id)
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        self.assertEqual(self.sat_id, sat_id)
        self.assertEqual(asset._asset_hash, sat_id)
        self.assertEqual(per.user_id, self.user1_id)
        self.assertEqual(per.read_access, True)
        self.assertEqual(per.command_access, True)
        self.assertEqual(per.manage_asset, True)
        self.assertEqual(per.manage_users, True)
        self.assertEqual(per.owner, True)

    def test_sat_write_manage_asset_auth_only(self):
        """
        tests the authorized put attempt by a user with only manage asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.command_access = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_write_no_manage_asset_auth(self):
        """
        tests the authorized put attempt by a user with everything but manage_asset access
        """
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.manage_asset = False
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_authorized(self):
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_search_public_asset(self):
        self.sat_test_data["publicly_visible"] = "True"
        create_response = self.app.post(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(create_response.status_code, 201)
        response = self.app.get("/satellite", headers=self.headers2)
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)

    def test_sat_read_no_per(self):
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_read_no_read_no_gs(self):
        response = self.app.get(
            "/satellite/not_a_sat_id",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_write_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.put(
            "/satellite/{}".format(self.sat_id),
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_search_authorized(self):
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)

    def test_sat_search_no_per(self):
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 0)

    def test_sat_search_only_auth_sat_returned(self):
        response = self.app.post(
            "/satellite",
            data=json.dumps({"sat_name": "test_sat_name2", "description": "test_description"}),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)
        self.assertTrue(sat_list[0]["sat_id"] == self.sat_id)

    def test_sat_search_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 0)

    def test_sat_search_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite",
            data=json.dumps(self.sat_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        sat_list = json.loads(response.data)["sat_results"]
        self.assertTrue(isinstance(sat_list, list))
        self.assertTrue(len(sat_list) == 1)
        self.assertTrue(sat_list[0]["sat_id"] == self.sat_id)

    def test_sat_status_execute_authorized(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_status_execute_no_per(self):
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_execute_no_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_execute_only_command_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_status_execute_no_sat(self):
        response = self.app.post(
            "/satellite/not_a_sat_id/status",
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_status_execute_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/satellite/{}/status".format(self.sat_id),
            data=json.dumps(self.sat_status_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_authorized(self):
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)
        self.assertTrue(sat_data["status"] == self.sat_status_test_data["status"])

    def test_sat_status_read_no_per(self):
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_status_read_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/status".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_status_read_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id",
            headers=self.headers1,
        )

    def test_sat_telecom_execute_authorized(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_execute_no_per(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_public_asset(self):
        sat = Satellite.query_by_hash(session, self.sat_id)
        sat.publicly_visible = True
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_no_telecom_asset_auth(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_execute_only_telecom_asset_auth(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.post(
            "/satellite/{}/telecom".format(self.sat_id),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_execute_no_sat(self):
        self.sat_telecom_test_data["telecom_name"] = self.sat_telecom_test_data["telecom_name"] + "1"
        response = self.app.post(
            "/satellite/not_a_sat_id/telecom",
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_single_authorized(self):
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)
        self.assertEqual(sat_data["telecom_name"], self.sat_telecom_test_data["telecom_name"])

    def test_sat_telecom_read_single_no_per(self):
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_single_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_single_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 200)

    def test_sat_telecom_read_single_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id/telecom/{}".format(self.telecom_name),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_single_no_read_no_sat_cmd(self):
        response = self.app.get(
            "/satellite/{}/telecom/not_a_cmd".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_read_many_authorized(self):
        response = self.app.get("/satellite/{}/telecom".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)
        sat_data = json.loads(response.data)["telecom_results"][0]
        self.assertEqual(
            sat_data["telecom_name"],
            self.sat_telecom_test_data["telecom_name"],
        )

    def test_sat_telecom_read_many_no_per(self):
        response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
            headers=self.headers2,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_many_no_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        session.commit()
        response = self.app.get(
            "/satellite/{}/telecom".format(self.sat_id),
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_read_many_only_read_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.telecom_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.get("/satellite/{}/telecom".format(self.sat_id), headers=self.headers1)
        self.assertEqual(response.status_code, 200)

    def test_sat_telecom_read_many_no_read_no_sat(self):
        response = self.app.get(
            "/satellite/not_a_sat_id/telecom",
            headers=self.headers1,
        )
        self.assertEqual(response.status_code, 404)

    def test_sat_telecom_put_new(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        response = self.app.put(
            "/satellite/{}/telecom/new_cmd".format(self.sat_id),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_authorized(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_execute_no_per(self):
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers2,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_put_execute_no_telecom_asset_auth(self):
        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.command_access = False
        session.commit()
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_sat_telecom_put_execute_only_telecom_asset_auth(self):

        sat_telecom_test_data = {
            "telecom_type": "test_telecom_type",
            "frequency": 20000000,
            "modulations": {"FM": [9600]},
            "transmits": "True",
            "receives": "True",
        }

        per = Permission.query_by_user_and_asset_id(session, self.user1_id, self.asset_id)
        per.read_access = False
        per.manage_asset = False
        per.manage_users = False
        per.owner = False
        session.commit()
        response = self.app.put(
            "/satellite/{}/telecom/{}".format(self.sat_id, self.telecom_name),
            data=json.dumps(sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_sat_telecom_put_execute_no_sat(self):
        response = self.app.put(
            "/satellite/not_a_sat_id/telecom/{}",
            data=json.dumps(self.sat_telecom_test_data),
            headers=self.headers1,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)
