import os
import shutil
from pathlib import Path
from typing import Dict, Union

import smart_open
from api.utils.file import FileHandler
from flask import Response


class LocalFileErrorMessages:
    SUCCESS_MSG = "Success"
    CREATE_FAILED_MSG = "Failed to add to local file system"
    DELETE_FAILED_MSG = "Failed to delete from local file system"
    EMPTY_FILE_MSG = "File is empty"
    DUPLICATE_FILE_MSG = "File name already exists"
    FILE_NOT_EXIST_MSG = "File does not exist"


class LocalFileHandler(FileHandler):

    scepter_directory = Path(__file__).parent.absolute()
    scepter_filesystem_directory = scepter_directory / "scepter_fs_local"

    @classmethod
    def get(self, file_key) -> Dict[str, Union[Response, str]]:
        # create streamed response object
        def generate(stream):
            while True:
                chunk = stream.read()
                if not chunk:
                    return
                yield chunk

        # get the file
        if os.path.exists(os.fsdecode(self.scepter_filesystem_directory / file_key)):
            with smart_open.open(
                os.fsdecode(self.scepter_filesystem_directory / file_key),
                "rb",
            ) as f:
                return {"stream": Response(generate(f))}
        else:
            return {"error": LocalFileErrorMessages.FILE_NOT_EXIST_MSG}

    @classmethod
    def create_from_stream(
        self, stream, file_key, **kwargs
    ) -> Dict[str, Union[int, str]]:
        # create the file

        read_length = 0
        if not os.path.exists(
            os.fsdecode(self.scepter_filesystem_directory / file_key)
        ):
            with smart_open.open(
                os.fsdecode(self.scepter_filesystem_directory / file_key), "wb"
            ) as f:
                while True:
                    chunk = stream.read()

                    read_length += len(chunk)

                    if len(chunk) == 0:
                        break
                    f.write(chunk)

                # delete file if empty
                if read_length <= 0:
                    os.remove(os.fsdecode(self.scepter_filesystem_directory / file_key))
                    return {"error": LocalFileErrorMessages.EMPTY_FILE_MSG}
                else:
                    return {"read_length": read_length}
        else:
            return {"error": LocalFileErrorMessages.DUPLICATE_FILE_MSG}

    @classmethod
    def delete(self, file_key) -> Dict[str, str]:

        if os.path.exists(os.fsdecode(self.scepter_filesystem_directory / file_key)):
            os.remove(os.fsdecode(self.scepter_filesystem_directory / file_key))
            return {"msg": LocalFileErrorMessages.SUCCESS_MSG}
        else:
            return {"error": LocalFileErrorMessages.FILE_NOT_EXIST_MSG}

    @classmethod
    def generate_presigned_path(self, file_key) -> Union[str, None]:
        if os.path.exists(os.fsdecode(self.scepter_filesystem_directory / file_key)):
            return os.fsdecode(self.scepter_filesystem_directory / file_key)
        else:
            return None

    @classmethod
    def create_from_file(self, file_key: str, file, tags={}) -> Dict[str, str]:
        # tags will be ignored on this implementation
        if not os.path.exists(
            os.fsdecode(self.scepter_filesystem_directory / file_key)
        ):
            shutil.copy(file, os.fsdecode(self.scepter_filesystem_directory / file_key))
            return {"msg": LocalFileErrorMessages.SUCCESS_MSG}
        else:
            return {"error": LocalFileErrorMessages.DUPLICATE_FILE_MSG}
