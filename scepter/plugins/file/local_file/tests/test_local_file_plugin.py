import os
import random
import tempfile
from io import BytesIO

import pytest
from plugins.file.local_file import LocalFileHandler

scepter_test_filesystem_directory = LocalFileHandler.scepter_filesystem_directory


def _generate(stream):
    while True:
        chunk = stream.read()
        if not chunk:
            return
        yield chunk


def _generate_random_csv():
    text = b""
    size = 0
    while size < 5120:
        text = text + b"%f,%.6f,%.6f,%i\n" % (
            random.random() * 50,
            random.random() * 50,
            random.random() * 50,
            random.randrange(1000),
        )
        size += len(text)
    return text


@pytest.fixture
def local_file_plugin():
    file_plugin: LocalFileHandler = LocalFileHandler()
    file_plugin.scepter_filesystem_directory = scepter_test_filesystem_directory
    yield file_plugin


@pytest.fixture
def random_file_path():
    text = _generate_random_csv()
    file: BytesIO = BytesIO(text)
    file_path = "test_file.txt"

    os.mkdir(scepter_test_filesystem_directory)

    with open(os.fsdecode(scepter_test_filesystem_directory / file_path), "wb") as f:
        f.write(file.getbuffer())
    yield file_path

    os.remove(os.fsdecode(scepter_test_filesystem_directory / file_path))
    os.rmdir(scepter_test_filesystem_directory)


@pytest.fixture
def random_delete_file_path():
    text = _generate_random_csv()
    file: BytesIO = BytesIO(text)
    file_path = "test_file_delete.txt"

    os.mkdir(scepter_test_filesystem_directory)

    with open(os.fsdecode(scepter_test_filesystem_directory / file_path), "wb") as f:
        f.write(file.getbuffer())
    yield file_path

    os.rmdir(scepter_test_filesystem_directory)


@pytest.fixture
def random_file():
    text = _generate_random_csv()
    file: BytesIO = BytesIO(text)

    os.mkdir(scepter_test_filesystem_directory)

    with tempfile.NamedTemporaryFile() as tmp:
        tmp.write(file.getbuffer())
        yield tmp.name


@pytest.fixture
def random_bytes():
    text = _generate_random_csv()
    file: BytesIO = BytesIO(text)

    os.mkdir(scepter_test_filesystem_directory)

    yield file

    os.rmdir(scepter_test_filesystem_directory)


def test_get(
    local_file_plugin: LocalFileHandler,
    random_file_path: str,
):
    get_return_value = local_file_plugin.get(random_file_path)
    assert get_return_value["stream"] is not None


def test_get_bad_file(
    local_file_plugin: LocalFileHandler,
):
    get_return_value = local_file_plugin.get("some_file.txt")
    assert get_return_value == {"error": "File does not exist"}


def test_delete(
    local_file_plugin: LocalFileHandler,
    random_delete_file_path,
):
    delete_return_value = local_file_plugin.delete(random_delete_file_path)
    assert delete_return_value == {"msg": "Success"}


def test_delete_no_file(
    local_file_plugin: LocalFileHandler,
):
    delete_return_value = local_file_plugin.delete("not_a_file.txt")
    assert delete_return_value == {"error": "File does not exist"}


def test_generate_presigned_path(
    local_file_plugin: LocalFileHandler,
    random_file_path: str,
):
    presigned_return_value = local_file_plugin.generate_presigned_path(random_file_path)
    assert presigned_return_value == os.fsdecode(
        local_file_plugin.scepter_filesystem_directory / random_file_path
    )


def test_generate_presigned_path_none(
    local_file_plugin: LocalFileHandler,
):
    presigned_return_value = local_file_plugin.generate_presigned_path(
        "none_existant_file.txt"
    )
    assert presigned_return_value is None


def test_create_file(
    local_file_plugin: LocalFileHandler,
    random_file,
):
    file_path = "test_file.txt"

    create_return_value = local_file_plugin.create_from_file(
        file_path,
        random_file,
    )
    assert create_return_value == {"msg": "Success"}

    os.remove(os.fsdecode(scepter_test_filesystem_directory / file_path))
    os.rmdir(scepter_test_filesystem_directory)


def test_create_file_duplicate(
    local_file_plugin: LocalFileHandler,
    random_file_path,
):
    file_path = "test_file.txt"

    create_return_value = local_file_plugin.create_from_file(
        file_path,
        random_file_path,
    )
    assert create_return_value == {"error": "File name already exists"}


def test_create_from_stream(
    local_file_plugin: LocalFileHandler,
    random_bytes,
):

    file_path = "test_file.txt"

    create_stream_return_value = local_file_plugin.create_from_stream(
        random_bytes,
        file_path,
    )
    assert create_stream_return_value == {
        "read_length": random_bytes.getbuffer().nbytes
    }

    os.remove(os.fsdecode(scepter_test_filesystem_directory / file_path))


def test_create_from_stream_duplicate(
    local_file_plugin: LocalFileHandler,
    random_file_path,
):

    text = _generate_random_csv()
    file: BytesIO = BytesIO(text)

    create_stream_return_value = local_file_plugin.create_from_stream(
        file,
        random_file_path,
    )
    assert create_stream_return_value == {"error": "File name already exists"}
