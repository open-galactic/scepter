from http import HTTPStatus
from typing import Optional, Tuple, Union
from uuid import UUID

from flask import request

from api.config import config
from api.database import session
from api.utils.asset import get_asset, get_permission_of_asset
from api.utils.auth import Authable
from models import Asset, Permission


class BasicAuth(Authable):
    @classmethod
    def check_users_access_to_asset(
        cls, user_id: Union[UUID, int], asset_id: int, access: str
    ) -> bool:
        permitted = Permission.check_access(session, user_id, asset_id, access)
        return permitted

    @classmethod
    def get_users_assets(
        cls, user_id: Union[UUID, int], access: Optional[str]
    ) -> list[Asset]:
        assets = Permission.get_assets_by_user_access(session, user_id, access)
        return assets

    @classmethod
    def allow_access(
        cls,
        access: str = "",
        request_type: str = "access",
        req_args: dict = {},
        **kwargs,
    ) -> Tuple[Union[UUID, int, None], bool, Union[str, None], Union[HTTPStatus, None]]:

        user_id = request.headers.get("x-osso-user")

        if not user_id:
            return None, False, "No user found", HTTPStatus.UNAUTHORIZED

        token_header = request.headers.get("x-osso-token")

        try:
            authed_user_id = UUID(user_id, version=4)
        except ValueError:
            try:
                authed_user_id = int(user_id)
            except Exception:
                return (
                    None,
                    False,
                    "Invalid user ID",
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                )

        if not authed_user_id:
            return None, False, "No user found", HTTPStatus.UNAUTHORIZED

        if token_header != config.secret_token:
            return None, False, "Unauthorized", HTTPStatus.UNAUTHORIZED

        if access in ["create", "search", "all"]:
            return authed_user_id, True, None, None

        if access in ["read", "write", "execute", "administrate", "delete"]:
            # As Flask-Pydantic does not attach path params to request
            # we need to inspect the path params, to check if the asset exists
            # before we check if the user has permissions

            param = [
                path_param
                for path_param in ["sat_id", "gs_id", "asset_hash"]
                if (path_param in request.url_rule.rule)
            ]
            asset_val = request.view_args[param[0]]
            asset_dict = {param[0]: asset_val}

            asset: Asset
            if (asset := get_asset(**asset_dict)) is None:
                return None, False, "Asset with that ID not found", HTTPStatus.NOT_FOUND

            per = get_permission_of_asset(authed_user_id, asset)
            if per is None or (
                per == "public"
                and access in ("write", "execute", "administrate", "delete")
            ):
                return None, False, "Unauthorized to modify asset", HTTPStatus.FORBIDDEN
            elif access == "read" and (per == "public" or per.read_access):
                return (
                    authed_user_id,
                    True,
                    None,
                    None,
                )
            elif access == "write" and per.manage_asset:
                return (
                    authed_user_id,
                    True,
                    None,
                    None,
                )
            elif access == "execute" and per.command_access:
                return (
                    authed_user_id,
                    True,
                    None,
                    None,
                )
            elif access == "administrate" and per.manage_users:
                return (
                    authed_user_id,
                    True,
                    None,
                    None,
                )
            elif access == "delete" and per.owner:
                return (
                    authed_user_id,
                    True,
                    None,
                    None,
                )
            else:
                return None, False, "Unauthorized to modify asset", HTTPStatus.FORBIDDEN
        else:
            return (
                None,
                False,
                "Error in authentication decorator metavalue",
                HTTPStatus.INTERNAL_SERVER_ERROR,
            )
