FROM public.ecr.aws/docker/library/python:3.9.10-slim

ENV PATH=/usr/local/bin:$PATH

WORKDIR /scepter
COPY scepter/api /scepter/api
COPY scepter/models /scepter/models
COPY scepter/plugins /scepter/plugins

RUN python -m pip install --upgrade pip wheel setuptools
RUN python -m pip install -r ./api/requirements.txt 
RUN python -m pip install ./plugins/auth ./plugins/file

EXPOSE 5000 5432
CMD  python -m gunicorn -c api/gunicorn.conf.py api:app
