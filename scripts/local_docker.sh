#!/bin/bash
build_flag=''
while getopts 't' flag; do
    case "${flag}" in
        t) run_tests='true' ;;
    esac
done

source .env

DATE=`date +%Y.%m.%d.%H.%M`

name="scepter-local"
app="${name}:$DATE"

docker stop ${name}
docker rm ${name}
docker build -f ./Dockerfile -t ${app} .

if [ "$(uname)" == "Darwin" ]; then
    docker run -dp 5000:5000 --env-file .env -e DB_HOST=host.docker.internal --name ${name} ${app}
else
    docker run -dp 5000:5000 --net=host --env-file .env --name ${name} ${app}
fi